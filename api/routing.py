from channels.routing import ProtocolTypeRouter, URLRouter
from django.conf.urls import url
from channels.auth import AuthMiddlewareStack
from channels.security.websocket import AllowedHostsOriginValidator
from api.consumers import BoardConsumer

application = ProtocolTypeRouter({
  'websocket': AuthMiddlewareStack(
    URLRouter(
      [
        url("", BoardConsumer)
      ]
    )
  )
})

