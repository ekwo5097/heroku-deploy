from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from django import urls
from django.urls import include, re_path
from emerge import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', TemplateView.as_view(template_name='index.html')),
    path('login/', TemplateView.as_view(template_name='index.html')),
    path('register/', TemplateView.as_view(template_name='index.html')),
    path('registerCoach/', TemplateView.as_view(template_name='index.html')),
    path('registerCoachAdmin/', TemplateView.as_view(template_name='index.html')),
    path('whatwedo/', TemplateView.as_view(template_name='index.html')),
    path('ourcoaches/', TemplateView.as_view(template_name='index.html')),
    path('registerInterest/', TemplateView.as_view(template_name='index.html')),

    # Leader paths/routes
    path('userhome/', TemplateView.as_view(template_name='index.html')),
    path('coaches/', TemplateView.as_view(template_name='index.html')),
    path('feedback/', TemplateView.as_view(template_name='index.html')),
    path('goalsettings/', TemplateView.as_view(template_name='index.html')),
    path('settings/', TemplateView.as_view(template_name='index.html')),
    path('questionnaire/', TemplateView.as_view(template_name='index.html')),
    path('messages/', TemplateView.as_view(template_name='index.html')),
    path('payment/', TemplateView.as_view(template_name='index.html')),
    path('settings?success=true/', TemplateView.as_view(template_name='index.html')),
    path('settings?canceled=true/', TemplateView.as_view(template_name='index.html')),
    # path('scheduletest', TemplateView.as_view(template_name='index.html')),
    path('countertest', TemplateView.as_view(template_name='index.html')),
    path('messagetest', TemplateView.as_view(template_name='index.html')),
    path('message1', TemplateView.as_view(template_name='index.html')),
    path('message2', TemplateView.as_view(template_name='index.html')),
    path('zoomsdktest', TemplateView.as_view(template_name='index.html')),
    path('coachpagedesign', TemplateView.as_view(template_name='index.html')),
    
    #Admin paths/routes
    path('adminpage/', TemplateView.as_view(template_name='index.html')),
    path('adminlinkpage/', TemplateView.as_view(template_name='index.html')),
    path('adminunlinkpage/', TemplateView.as_view(template_name='index.html')),
    path('eoicoach/', TemplateView.as_view(template_name='index.html')),
    path('adminfeedback/', TemplateView.as_view(template_name='index.html')),
    

    #Coach paths/routes
    path('coachfeedback/', TemplateView.as_view(template_name='index.html')),
    path('questionnairecoach/', TemplateView.as_view(template_name='index.html')),
    path('coacheepagemod/', TemplateView.as_view(template_name='index.html')),

    path('zoomMeeting/', TemplateView.as_view(template_name='index.html')),
    path('coachees/', TemplateView.as_view(template_name='index.html')),



    # Testing for progress bar
    re_path(r'^circularprogressbar/', TemplateView.as_view(template_name='index.html')),
    re_path(r'^actiontest/', TemplateView.as_view(template_name='index.html')),

    re_path(r'^rest-auth/', include('rest_auth.urls')),
    re_path(r'^rest-auth/registration/', include('rest_auth.registration.urls')),

    re_path(r'^api/register',views.register),
    re_path(r'^api/login',views.login),

    # url(r'^api/questionnaire',views.questionnaire),
    re_path(r'^api/settings',views.settings),
    re_path(r'^api/changeSettings',views.changeSettings),
    re_path(r'^api/questionnairecoach',views.questionnairecoach),
    re_path(r'^api/questionnaire',views.questionnaire),

    re_path(r'^api/addSchedule',views.addSchedule),
    re_path(r'^api/getLeaderSchedules',views.getLeaderSchedules),
    re_path(r'^api/getNextSchedule',views.getNextSchedule),
    re_path(r'^api/confirmNextSchedule',views.confirmNextSchedule),
    re_path(r'^api/getLeaderNextSchedules',views.getLeaderNextSchedules),
    re_path(r'^api/addMessage',views.addMessage),
    re_path(r'^api/getMessageHistory',views.getMessageHistory),
    # url(r'^api/scheduletime',views.scheduletime),

    re_path(r'^api/addHomework',views.addHomework),
    re_path(r'^api/getHomework',views.getHomework),
    re_path(r'^api/getLeaderHomework',views.getLeaderHomework),
    re_path(r'^api/completeHomework',views.completeHomework),

    re_path(r'^api/addZoomLink',views.addZoomLink),
    re_path(r'^api/getZoomLink',views.getZoomLink),
    re_path(r'^api/getLeaderZoomLinks',views.getLeaderZoomLinks),
    re_path(r'^api/coachGetZoomDetails',views.coachGetZoomDetails),

    re_path(r'^api/getAllLeadersGoals',views.getAllLeadersGoals),
    re_path(r'^api/getLeaderGoals',views.getLeaderGoals),
    re_path(r'^api/updateActionScore', views.updateActionScore),
    # re_path(r'^api/addSubGoal', views.addSubGoal),
    # re_path(r'^api/removeSubGoal', views.removeSubGoal),
    # re_path(r'^api/addAction', views.addAction),
    # re_path(r'^api/removeAction', views.removeAction),
    re_path(r'^api/getAllLeaderActions', views.getAllLeaderActions),

    # re_path(r'^api/leaderAddSubGoal', views.leaderAddSubGoal),
    # re_path(r'^api/leaderRemoveSubGoal', views.leaderRemoveSubGoal),
    re_path(r'^api/leaderAddAction', views.leaderAddAction),
    re_path(r'^api/leaderUpdateAction', views.leaderUpdateAction),
    re_path(r'^api/leaderRemoveAction', views.leaderRemoveAction),

    re_path(r'^api/pay',views.create_payment),
    re_path(r'^api/assignLeaderToCoach',views.assignLeaderToCoach),
    re_path(r'^api/unlinkLeaderToCoach',views.unlinkLeaderToCoach),

    re_path(r'^api/getAllLeadersAndCoaches',views.getAllLeadersAndCoaches),
    re_path(r'^api/getNewLeadersAndCoaches',views.getNewLeadersAndCoaches),
    re_path(r'^api/getAssignedLeadersAndCoaches',views.getAssignedLeadersAndCoaches),
    re_path(r'^api/removeExistingUser',views.removeExistingUser),
    re_path(r'^api/getQuestionnaireCompleted',views.getQuestionnaireCompleted),
    re_path(r'^api/getLeaderIsAssigned',views.getLeaderIsAssigned),

    re_path(r'^api/submitFeedback',views.submitFeedback),
    re_path(r'^api/getLeadersFeedback',views.getLeadersFeedback),
    re_path(r'^api/getAllFeedback',views.getAllFeedback),
    re_path(r'^api/confirmFeedback',views.confirmFeedback),
    re_path(r'^api/deleteFeedback',views.deleteFeedback),
    re_path(r'^webhook',views.my_webhook_view),
    re_path(r'^api/change_subscription',views.change_subscription),

    re_path('check/',views.check),

    # Paths for scheduling/calendar backend
    re_path(r'^api/getAvailableBookedTimes', views.getAvailableBookedTimes),
    re_path(r'^api/leaderBookTime', views.leaderBookTime),
    re_path(r'^api/leaderRemoveBooking', views.leaderRemoveBooking),
    re_path(r'^api/coachChooseAvailableTime/', views.coachChooseAvailableTime),
    re_path(r'^api/coachRemoveAvailableTime/', views.coachRemoveAvailableTime),
    re_path(r'^api/getUsersForUpdate/', views.getUsersForUpdate),
    re_path(r'^api/leaderGetZoomDetails/', views.leaderGetZoomDetails),
    # re_path(r'^api/scheduletestRESET/', views.scheduletestRESET),

    re_path(r'^api/getEOICoach',views.getEOICoach),
    re_path(r'^api/setEOICoach',views.setEOICoach),
    re_path(r'^api/deleteEOICoach',views.deleteEOICoach),

    re_path(r'^api/getEOICoach',views.getEOICoach),
    re_path(r'api/getLeaderid',views.getLeaderid),
    re_path(r'api/getLeaderCoachid',views.getLeaderCoachid),
    re_path(r'api/getLeaderCoacheeid',views.getLeaderCoacheeid),
   
    re_path(r'^api/getLeaderToCoach',views.getLeaderToCoach),

    # Leader's MyCoach page
    re_path(r'^api/getCoachQuestionnaireInfo',views.getCoachQuestionnaireInfo),
    re_path(r'^api/getMessages',views.getMessages),
    re_path(r'^api/saveMessages',views.saveMessages),







    re_path(r'^api/countertestGET/', views.countertestGET),
    re_path(r'^api/countertestPOST/', views.countertestPOST),
    re_path(r'^api/logout/', views.logout),

    #Zoom Backend Paths
    re_path(r'^api/createMeeting/', views.createMeeting),
    re_path(r'^api/generateSignature/', views.generateSignature),
    re_path(r'api/getMessageNotifications/',views.getMessageNotifications),
    re_path(r'api/getCoachNotifications/',views.getCoachNotifications),

    # re_path('scheduletestLEADERPOSTRemoveBooking/', views.scheduletestLEADERPOSTRemoveBooking),
    # re_path('scheduletestCOACHPOSTRemoveAvailableTime/', views.scheduletestCOACHPOSTRemoveAvailableTime),

]
