from os import remove
from django.test import TestCase
from django.test import LiveServerTestCase
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import Select


class hostTest(LiveServerTestCase):

    def testRegister(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        driver.get('http://127.0.0.1:8000/register')
        username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password1 = driver.find_element_by_id('basic_password1')
        basic_password2 = driver.find_element_by_id('basic_password2')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        username.send_keys('chamer')
        basic_email.send_keys('chamer@gmail.com')
        basic_password1.send_keys('cHamer1234')
        basic_password2.send_keys('cHamer1234')

        submit.send_keys(Keys.RETURN)
        time.sleep(3)



    
    def testLandingPage(self):
        #note: add own local path to chromedriver
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome('../api/chromedriver')
        driver.get('http://127.0.0.1:8000/')
        assert "React App" in driver.title
        
class MessageTestLeader(LiveServerTestCase):
    def testMessage(self):

        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome('../api/chromedriver')
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('sample1234')
        basic_email.send_keys('sample1234@gmail.com')
        basic_password.send_keys('sample1234')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)
        coach_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        message=driver.find_element_by_class_name('message-icon-coachpage').click()
        time.sleep(1)
        messagetext=driver.find_element_by_xpath('//*[@id="root"]/div[3]/main/div[2]/div/div/nav/form/input')
        messagetext.send_keys('hello coach')
        messagetext.send_keys(Keys.ENTER)
        time.sleep(2)

class MessageTestCoach(LiveServerTestCase):
  
    def testMessageCoach(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome('../api/chromedriver')
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('samar')
        basic_email.send_keys('sam5@gmail.com')
        basic_password.send_keys('samar45')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)
        coachee_page = driver.find_element_by_xpath('//*[@id="sidebarContainer"]/nav/div/div/a[2]/a/i').click()
        time.sleep(1)
        leader=driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(2)
        message=driver.find_element_by_xpath('//*[@id="root"]/div[4]/div/div/nav/form/input')
        message.send_keys('hello sample1234')
        message.send_keys(Keys.ENTER)
        time.sleep(2)
        



class loginTest(LiveServerTestCase):

    def testLogin(self):
        
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome('../api/chromedriver')
        # driver = webdriver.Chrome('emerge/chromedriver.exe')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('sample1234')
        basic_email.send_keys('sample1234@gmail.com')
        basic_password.send_keys('sample1234')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)
        logout=driver.find_element_by_class_name('bx.bxs-comment-edit.nav-icon-homepage')
        driver.implicitly_wait(10)
        ActionChains(driver).move_to_element(logout).click(logout).perform()


class coachTest(LiveServerTestCase):

    def testCoach(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome('../api/chromedriver')
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')#

        basic_username.send_keys('sample1234')
        basic_email.send_keys('sample1234@gmail.com')
        basic_password.send_keys('sample1234')
        submit.send_keys(Keys.RETURN)

        # access coach page
        time.sleep(1)
        coach_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

    def testQuest(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome('../api/chromedriver')
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('samar')
        basic_email.send_keys('sam5@gmail.com')
        basic_password.send_keys('samar45')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)
        ques_page= driver.find_element_by_class_name('fas.fa-question-circle.nav-icon-homepage').click()

        name=driver.find_element_by_name('name')
        age=driver.find_element_by_name('age')
        gender=driver.find_element_by_name('gender')
        about=driver.find_element_by_name('about')
        industry=driver.find_element_by_name('industry')
        practice=driver.find_element_by_name('years_of_practice')
        qualification=driver.find_element_by_name('highest_edu_qualification')
        coach_goal_1=driver.find_element_by_xpath('/html/body/div/div/form/div[10]/select')
        coach_goal_2=driver.find_element_by_xpath('/html/body/div/div/form/div[11]/select')
        coach_goal_3=driver.find_element_by_xpath('/html/body/div/div/form/div[12]/select')
        availability=driver.find_element_by_class_name('AvailableTimeContainer')
        # give browser time to load elements 
        time.sleep(1)

        name.send_keys('Samar Jacobs')
        age.send_keys('21')

        gender.click()
        time.sleep(1)
        driver.find_element_by_xpath('//*[@id="root"]/div/form/div[5]/select/option[2]').click()
        time.sleep(1)
        about.send_keys('Coach in time management')
        time.sleep(1)
        industry.send_keys('Digital')
        time.sleep(1)
        practice.send_keys('7')
        time.sleep(1)
        qualification.send_keys('coaching in time management')
        time.sleep(5)
        coach_goal_1.click()
        time.sleep(1)
        driver.find_element_by_xpath('//*[@id="root"]/div/form/div[10]/select/option[2]').click()
        time.sleep(1)
        coach_goal_2.click()
        time.sleep(1)
        driver.find_element_by_xpath('//*[@id="root"]/div/form/div[11]/select/option[6]').click()
        time.sleep(1)
        coach_goal_3.click()
        time.sleep(1)
        driver.find_element_by_xpath('//*[@id="root"]/div/form/div[12]/select/option[4]').click()
        time.sleep(1)




        Monday=driver.find_element_by_xpath('//*[@id="root"]/div/form/div[13]/div/label[1]/span').click()
        time.sleep(1)
        Wednesday=driver.find_element_by_xpath('//*[@id="root"]/div/form/div[13]/div/label[3]/span').click()
        time.sleep(3)
        driver.find_element_by_xpath('/html/body/div/div/form/input').click()
        time.sleep(4)


    def testCreateAppointment(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome('/chromedriver.exe')
        #driver = webdriver.Chrome('../api/chromedriver')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('sample1234')
        basic_email.send_keys('sample1234@gmail.com')
        basic_password.send_keys('sample1234')
        submit.send_keys(Keys.RETURN)

        # access coach page
        time.sleep(1)
        coach_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        # find appointment section
        appointment_container = driver.find_elements_by_class_name('scheduleHeader')
        date = driver.find_elements_by_class_name('date')
        month = driver.find_elements_by_class_name('month')
        year = driver.find_elements_by_class_name('year')
        tm = driver.find_elements_by_class_name('time')

        date_text = '1'
        month_text = 'January'
        year_text = '2022'
        tm_text = '9:00am-10:00am'

        # give browser time to load elements 
        time.sleep(1)

        # enter options into appointment drop-down form using xpath (absolute paths obtained from Chrome browser extension 'ChroPath')
        date_drp = Select(driver.find_element_by_xpath("//body/div[@id='root']/div[3]/main[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/form[1]/select[1]"))
        date_drp.select_by_visible_text(date_text)

        month_drp = Select(driver.find_element_by_xpath("//body/div[@id='root']/div[3]/main[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/form[1]/select[2]"))
        month_drp.select_by_visible_text(month_text)

        year_drp = Select(driver.find_element_by_xpath("//body/div[@id='root']/div[3]/main[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/form[1]/select[3]"))
        year_drp.select_by_visible_text(year_text)

        tm_drp = Select(driver.find_element_by_xpath("//body/div[@id='root']/div[3]/main[1]/div[1]/div[1]/div[2]/div[3]/div[1]/div[2]/form[1]/select[4]"))
        tm_drp.select_by_visible_text(tm_text)

        appointment_submit = driver.find_element_by_class_name('btn')
        appointment_submit.send_keys(Keys.RETURN)

        # testing to make sure the appointment is saved to the leader's database, and the assigned coach's database
        #def testCoachAppointmentDataSave(self):
            #to do
        
    

class coacheePageTests(LiveServerTestCase):
    #checks that leader names are generated based on coach user 
    def testLeaderNames(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()
        leader_container = driver.find_element_by_class_name('leader-container')
        leader_template = driver.find_element_by_class_name('leader-template')
        leader_list = driver.find_element_by_class_name('leader-list')
        leader_name_form = driver.find_element_by_class_name('leader.leaderNameContainer')

        time.sleep(3)
        
        first_name = driver.find_element_by_xpath("//label[@for='radio1']").text
        assert first_name == 'sample-leader'

        # second_name = driver.find_element_by_xpath("//label[@for='radio2']").text
        # assert second_name == 'irene'

        # third_name = driver.find_element_by_xpath("//label[@for='radio3']").text
        # assert third_name == 'matt'

        # fourth_name = driver.find_element_by_xpath("//label[@for='radio4']").text
        # assert fourth_name == 'johnson'

        # fifth_name = driver.find_element_by_xpath("//label[@for='radio5']").text
        # assert fifth_name == 'sally'

    '''def testOneUnconfirmedBoooking(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()
        leader_container = driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)
        
        driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(1)
        booking_data = driver.find_element_by_xpath('//*[@id="leader-info"]/div[1]/form/ul/div/div[1]/label/span').text
        assert booking_data == 'Date: 6/9/2024 Time:10:00am-11:00am
        '''


    def testMultipleUnconfirmedBookings(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()
        leader_container = driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="radio3"]').click()
        time.sleep(3)
        
        booking_data_1 = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[1]/div[3]/form/ul/div[1]/div[1]/label/span').text
        time.sleep(1)
        booking_data_2 = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[1]/div[3]/form/ul/div[2]/div[1]/label/span').text
        time.sleep(1)
        assert booking_data_1 == 'Date: 8/9/2021 Time:4:00pm-5:00pm'
        time.sleep(1)
        assert booking_data_2 == 'Date: 2/9/2021 Time:5:00pm-6:00pm'

    def testLeaderPersonalInfo(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()
        leader_container = driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(3)
        driver.find_element_by_xpath('/html/body/div/div[4]/div/div/div[1]/i').click()
        time.sleep(3)

        # coachee goals text
        coachee_goal_h_text = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/h2').text
        time.sleep(1)
        assert coachee_goal_h_text == 'Coachee Goals'

    def testEnterZoomLink(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()
        leader_container = driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(3)

        zoom_link_form = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[3]/form[1]/input[2]')
        zoom_link_form_submit = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[3]/form[1]/button')
        zoom_link_form.send_keys('zoom-5001-100-1')
        zoom_link_form_submit.send_keys(Keys.RETURN)
        time.sleep(3)

    def testZoomLinkCoachAndLeaderDisplay(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')

       
        # login as coach to check zoom link updated
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
    
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(3)
        #current_zoom = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[3]/h4[1]').text
        current_zoom_coachee_pg = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[3]/h4[1]').text
        time.sleep(2)
        assert current_zoom_coachee_pg == "sample-leader's current zoom link: zoom-5001-100-1"
        time.sleep(2)


        #login as first user
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
    
        basic_username.send_keys('sample-leader')
        basic_email.send_keys('sample-leader@gmail.com')
        basic_password.send_keys('sample-leader')
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        leader_zoom_link = driver.find_element_by_xpath('/html/body/div/div[1]/div/div[3]/main/div[1]/div/div[1]/div[1]/div/div[2]/div/h4').text
        time.sleep(1)
        assert leader_zoom_link == "zoom-5001-100-1"
  
    def testZoomLinkUpdate(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        # update zoom link 
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()
        leader_container = driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(3)

        zoom_link_form = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[3]/form[1]/input[2]')
        zoom_link_form_submit = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[3]/form[1]/button')
        zoom_link_form.send_keys('zoom-7001-100-1')
        zoom_link_form_submit.send_keys(Keys.RETURN)
        time.sleep(3)

        #login again
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
    
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)
        time.sleep(2)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage')
        coachee_page.click()
        time.sleep(3)

        driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(3)
        #current_zoom = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[3]/h4[1]').text
        current_zoom_coachee_pg = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[3]/h4[1]').text
        time.sleep(2)
        assert current_zoom_coachee_pg == "sample-leader's current zoom link: zoom-7001-100-1"
        time.sleep(2)

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
    
        basic_username.send_keys('sample-leader')
        basic_email.send_keys('sample-leader@gmail.com')
        basic_password.send_keys('sample-leader')
        submit.send_keys(Keys.RETURN)
        time.sleep(1)

        leader_zoom_link = driver.find_element_by_xpath('/html/body/div/div[1]/div/div[3]/main/div[1]/div/div[1]/div[1]/div/div[2]/div/h4').text
        time.sleep(1)
        assert leader_zoom_link == "zoom-7001-100-1"
        
    ''''
    def testHomeworkInput(self):
            driver = webdriver.Chrome('/emerge/chromedriver.exe')

            driver.get('http://127.0.0.1:8000/login')
            basic_username = driver.find_element_by_id('basic_username')
            basic_email = driver.find_element_by_id('basic_email')
            basic_password = driver.find_element_by_id('basic_password')
            submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
            
            basic_username.send_keys('sample-coach')
            basic_email.send_keys('sample-coach@gmail.com')
            basic_password.send_keys('sample-coach')
            submit.send_keys(Keys.RETURN)

            time.sleep(1)

            coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

            time.sleep(3)

            driver.find_element_by_xpath("//input[@value='radio1']").click()
            homework_title = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[9]/div[1]/div[1]/form[1]/input[2]')
            homework_description = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[9]/div[1]/div[1]/form[1]/input[3]')
            homework_submit = driver.find_element_by_class_name('hwSubmitStyle')
            homework_title.send_keys('trim lawn')
            homework_description.send_keys('hedges need trimming')
            homework_submit.send_keys(Keys.RETURN)

    def testHomeworkInputCoachLeaderDisplay(self):
        
        driver = webdriver.Chrome('../api/chromedriver')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        time.sleep(3)

        driver.find_element_by_xpath("//input[@value='radio1']").click()
        homework_title = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[9]/div[1]/div[1]/form[1]/input[2]')
        homework_description = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[9]/div[1]/div[1]/form[1]/input[3]')
        homework_submit = driver.find_element_by_class_name('hwSubmitStyle')
        homework_title.send_keys('see accountant')
        homework_description.send_keys('file tax return')
        homework_submit.send_keys(Keys.RETURN)

        # re-login as coach
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        time.sleep(3)

        driver.find_element_by_xpath("//input[@value='radio1']").click()
        homework_title_text = driver.find_element_by_id('see accountant').text
        homework_description_text = driver.find_element_by_id('file tax return').text

        # re-login as leader
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('patrick')
        basic_email.send_keys('patrick@gmail.com')
        basic_password.send_keys('test')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        homework_title_text = driver.find_element_by_id('see accountant')
        homework_description_text = driver.find_element_by_id('file tax return')

    def testHomeworkStrikeThrough(self):
        driver = webdriver.Chrome('../api/chromedriver')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('patrick')
        basic_email.send_keys('patrick@gmail.com')
        basic_password.send_keys('test')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        homework_title_text = driver.find_element_by_id('depression')
        homework_description_text = driver.find_element_by_id('meditation 100 hours')
        complete_btn = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/div[1]/div[3]/main[1]/div[1]/div[1]/div[1]/div[2]/div[1]/span[1]/div[3]/form[1]/button[1]').send_keys(Keys.RETURN)
        
        #re-login as leader
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('patrick')
        basic_email.send_keys('patrick@gmail.com')
        basic_password.send_keys('test')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        homework_title_text = driver.find_elements_by_class_name('strike-through')
        
        #re-login as coach
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        time.sleep(3)

        driver.find_element_by_xpath("//input[@value='radio1']").click()
        homework_title_text = driver.find_elements_by_class_name('strike-through')
    '''

    def testLeaderGoals(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)

        time.sleep(1)

        coachee_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()
        leader_container = driver.find_element_by_class_name('leader-info-container')

        time.sleep(3)

        driver.find_element_by_xpath('//*[@id="radio1"]').click()
        time.sleep(3)


       
        
        main_goal_text = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/div/div[1]/b').text
        assert main_goal_text == 'Building effective work habits'
        time.sleep(3)

        # subgoal 1 tests
        subgoal1 = driver.find_element_by_xpath('//*[@id="subGoalForm"]/div[2]/input').text
        time.sleep(2)
        print(subgoal1)

        assert subgoal1 == ""
        time.sleep(1)

        action1 = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/div/div[3]/div/form/div[2]/div[1]/input').text
        assert action1 == ""
        time.sleep(1)

        score1 = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/div/div[3]/div/form/div[2]/div[2]').text
        assert score1 == 'Score: 8'
       
        # #subgoal 2 tests
        # subgoal2 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[5]/div[1]/div[5]').text
        # assert subgoal2 == 'Subgoal: calendar'

        # action2 = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/div[1]/div[3]/div/form/div[2]/div[1]/input').text
        # assert action2 == 'write down all business deadlines on calendar'

        # score2 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/div[1]/div[1]/div[5]/div[1]/div[7]').text
        # assert score2 == 'Score: 9'
    
    

class LeaderSubmitFeedback(LiveServerTestCase):
    #checks that leader names are generated based on coach user 
    def testAdminAcceptFeedback(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome(ChromeDriverManager().install())

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('patrick')
        basic_email.send_keys('patrick@gmail.com')
        basic_password.send_keys('patrick')
        submit.send_keys(Keys.RETURN)

        # load page
        time.sleep(2)

        # access feedback page
        leader_feedback_page = driver.find_element_by_class_name('fas.fa-clipboard.nav-icon-homepage').click()

        time.sleep(2)

        # select score
        # note: need to manually scroll page down in order to detect radio buttons for feedback page
        q1 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[3]/div[1]/label[9]')
        time.sleep(1)
        q1.click()
        q2 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[6]/div[1]/label[9]')
        time.sleep(1)
        q2.click()
        q3 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[9]/div[1]/label[9]')
        time.sleep(1)
        q3.click()
        q4 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[12]/div[1]/label[9]')
        time.sleep(1)
        q4.click()
        q5 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[15]/div[1]/label[9]')
        time.sleep(1)
        q5.click()
        q6 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[18]/div[1]/label[9]')
        time.sleep(1)
        q6.click()
        q7 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[21]/div[1]/label[9]')
        time.sleep(1)
        q7.click()
        q8 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[24]/div[1]/label[9]')
        time.sleep(1)
        q8.click()
        q9 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[27]/div[1]/label[9]')
        time.sleep(1)
        q9.click()
        q10 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[30]/div[1]/label[9]')
        time.sleep(1)
        q10.click()
        
        leader_feedback_submit_btn = driver.find_element_by_class_name('Feedback-Submit-Btn')
        leader_feedback_submit_btn.send_keys(Keys.RETURN)

        time.sleep(3)
        
        # sign in as admin user to confirm feedback
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('admin')
        basic_email.send_keys('admin@gmail.com')
        basic_password.send_keys('admin')
        submit.send_keys(Keys.RETURN)
        
        time.sleep(1)

        #admin confirm form here
        confirm_leader_form_btn = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/div[2]/div[2]/form[1]/button[1]')
        confirm_leader_form_btn.send_keys(Keys.RETURN)
        time.sleep(3)


    def testAdminRejectFeedback(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome(ChromeDriverManager().install())

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('patrick')
        basic_email.send_keys('patrick@gmail.com')
        basic_password.send_keys('patrick')
        submit.send_keys(Keys.RETURN)

        # load page
        time.sleep(2)

        # access feedback page
        leader_feedback_page = driver.find_element_by_class_name('fas.fa-clipboard.nav-icon-homepage').click()

        time.sleep(2)

        # select score
        # note: need to manually scroll page down in order to detect radio buttons for feedback page
        q1 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[3]/div[1]/label[1]')
        time.sleep(1)
        q1.click()
        q2 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[6]/div[1]/label[1]')
        time.sleep(1)
        q2.click()
        q3 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[9]/div[1]/label[1]')
        time.sleep(1)
        q3.click()
        q4 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[12]/div[1]/label[1]')
        time.sleep(1)
        q4.click()
        q5 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[15]/div[1]/label[1]')
        time.sleep(1)
        q5.click()
        q6 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[18]/div[1]/label[1]')
        time.sleep(1)
        q6.click()
        q7 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[21]/div[1]/label[1]')
        time.sleep(1)
        q7.click()
        q8 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[24]/div[1]/label[1]')
        time.sleep(1)
        q8.click()
        q9 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[27]/div[1]/label[1]')
        time.sleep(1)
        q9.click()
        q10 = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[3]/div[1]/div[2]/form[1]/div[30]/div[1]/label[1]')
        time.sleep(1)
        q10.click()
        
        leader_feedback_submit_btn = driver.find_element_by_class_name('Feedback-Submit-Btn')
        leader_feedback_submit_btn.send_keys(Keys.RETURN)

        time.sleep(3)
        
        # sign in as admin user to confirm feedback
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('admin')
        basic_email.send_keys('admin@gmail.com')
        basic_password.send_keys('admin')
        submit.send_keys(Keys.RETURN)
        
        time.sleep(1)

        #admin confirm form here
        reject_leader_form_btn = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/div[2]/div[2]/form[2]/button[1]')
        reject_leader_form_btn.send_keys(Keys.RETURN)
        time.sleep(3)

class GoalSettingsTest(LiveServerTestCase):
    #checks that leaders subgoal and action are being submitted and updated on coach side
    def testGoalCreateAndDelete(self):
        driver = webdriver.Chrome('emerge/chromedriver.exe')
        #driver = webdriver.Chrome(ChromeDriverManager().install())

        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')
        
        basic_username.send_keys('new-leader')
        basic_email.send_keys('new-leader@gmail.com')
        basic_password.send_keys('new-leader')
        submit.send_keys(Keys.RETURN)

        # load page
        time.sleep(2)

        # enter new sub goal
        new_subgoal_input = driver.find_element_by_name('newSubGoal')
        new_subgoal_input.send_keys('create a journal')
        create_subgoal_btn = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/div[1]/div[3]/main[1]/div[1]/div[1]/div[1]/div[2]/div[1]/span[1]/div[1]/div[2]/form[1]/div[2]/button[1]')
        create_subgoal_btn.send_keys(Keys.RETURN)

        time.sleep(1)

        # go to coach page to reload goal settings
        coach_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        time.sleep(2)

        # go back to home page
        home_page = driver.find_element_by_class_name('fas.fa-user.nav-icon-coachpage').click()

        time.sleep(1)

        new_action_input = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/div[1]/div[3]/main[1]/div[1]/div[1]/div[1]/div[2]/div[1]/span[1]/div[1]/div[2]/form[1]/div[2]/input[1]')
        new_action_input.send_keys('write 5 most important tasks for the day')
        create_action_btn = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/div[1]/div[3]/main[1]/div[1]/div[1]/div[1]/div[2]/div[1]/span[1]/div[1]/div[2]/form[1]/div[2]/button[1]')
        create_action_btn.send_keys(Keys.RETURN)

        time.sleep(1)

        # go to coach page to reload goal settings
        coach_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        time.sleep(2)

        # go back to home page
        home_page = driver.find_element_by_class_name('fas.fa-user.nav-icon-coachpage').click()

        time.sleep(1)

        new_score_input = driver.find_element_by_name('newScore')
        new_score_input.send_keys('8')
        update_score_btn = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/div[1]/div[3]/main[1]/div[1]/div[1]/div[1]/div[2]/div[1]/span[1]/div[1]/div[2]/div[1]/div[1]/form[1]/div[2]/button[1]')
        update_score_btn.send_keys(Keys.RETURN)
        
        time.sleep(1)

        # go to goal settings page for leader
        goal_setting_page = driver.find_element_by_class_name('fas.fa-bullseye.nav-icon-homepage').click()

        time.sleep(2)
 
        # confirm goal settings page is displaying the same text as leader home page
        subgoal = driver.find_element_by_name('subGoal').get_attribute('value')
        assert subgoal == 'create a journal'

        action = driver.find_element_by_name('action').get_attribute('value')
        assert action == 'write 5 most important tasks for the day'

        time.sleep(1)

        # log into sample-coach account to check if subgoal and action
        driver.get('http://127.0.0.1:8000/login')
        basic_username = driver.find_element_by_id('basic_username')
        basic_email = driver.find_element_by_id('basic_email')
        basic_password = driver.find_element_by_id('basic_password')
        submit = driver.find_element_by_class_name('ant-btn.ant-btn-primary')

        basic_username.send_keys('sample-coach')
        basic_email.send_keys('sample-coach@gmail.com')
        basic_password.send_keys('sample-coach')
        submit.send_keys(Keys.RETURN)
        
        time.sleep(1)

        # skip coach questionnaire form
        # coach_questionnaire_btn = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[1]/form[1]/button[1]')
        # coach_questionnaire_btn.send_keys(Keys.RETURN)

        # go to coachees page
        time.sleep(1)

        # go to goal settings page for leader
        coachees_page = driver.find_element_by_class_name('fas.fa-id-badge.nav-icon-homepage').click()

        time.sleep(3)

        leader_radio = driver.find_element_by_xpath('//*[@id="radio11"]').click()

        time.sleep(1)

        # close message 
        close_message = driver.find_element_by_xpath('/html[1]/body[1]/div[1]/div[4]/div[1]/div[1]/div[1]/i[1]').click()

        time.sleep(2)

        leader_subgoal = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/div[5]/div[2]/div/form/div[2]/input').get_attribute('value')
        assert leader_subgoal == 'create a journal'

        leader_action = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/div[5]/div[3]/div/form/div[2]/div[1]/input').get_attribute('value')
        assert leader_action == 'write 5 most important tasks for the day'

        # delete subgoal
        remove_subgoal = driver.find_element_by_xpath('/html/body/div/div[3]/div/div[2]/div/div/div[5]/div[5]/div[2]/div/form/div[2]/button')
        remove_subgoal.send_keys(Keys.RETURN)
        time.sleep(3)



        