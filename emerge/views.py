from re import I, T
from email import message
from django.shortcuts import render
from django.http import HttpResponse
from emerge import models
import json
import hashlib
from django.contrib.auth import get_user_model
import pusher
import stripe
import jwt
from django.conf import settings
from datetime import datetime, timedelta, date
from pytz import timezone
from django.shortcuts import redirect
from time import time
import requests
import hmac
import base64

API_KEY = 'NOmAl_DoS4CBhddzSwsh1A'
API_SEC = 'L3MINDLPWXD4VhP0RynqKXFVgXrATGJ3eOeb'

months = ['January',
'February',
'March',
'April',
'May',
'June',
'July',
'August',
'September',
'October',
'November',
'December']

pusher_client = pusher.Pusher(
  app_id='1280449',
  key='e7e6508dcf5807d7881f',
  secret='8105f71a356f9513c7b7',
  cluster='ap2',
  ssl=True
)
# Create your views here.
def getMd5(username):
    md5 = hashlib.md5()
    md5.update((username+"saltaods").encode(encoding="utf-8"))
    token = md5.hexdigest()
    return token

def generateJWT(id):
    dt = datetime.now() + timedelta(days=60)
    # print(int(dt.strftime('%S')))

    token = jwt.encode({
            'id': id,
            'exp': dt
        }, 'zzn(ivxnhl%p48y-a@xxzsgrm8n0uxhuff93$u(30i%h^z^jz8', algorithm='HS256')

    return token

def decodeJWT(token):
    try:
        payload = jwt.decode(token, 'zzn(ivxnhl%p48y-a@xxzsgrm8n0uxhuff93$u(30i%h^z^jz8', algorithms=['HS256'])
        print("payload id is: {}".format(payload['id']))
        print("payload exp is: {}".format(datetime.fromtimestamp(payload['exp'])))
        return True, payload['id']
    except jwt.ExpiredSignatureError:
        return False, 'Signature expired. Please log in again.'
    except jwt.InvalidTokenError:
        return False, 'Invalid token. Please log in again.'

def logout(request):
    ret = {"msg":"logged out"}
    ret = json.dumps(ret, ensure_ascii=False)
    response=HttpResponse(ret)
    response.delete_cookie('token')
    return response
    
    

def assignCoach(leader):
    coaches = models.Coach.objects.all()
    if len(coaches)==0:
        print("No coach")
        return
    coach=coaches[0]
    models.LeaderToCoach.objects.create(leader=leader,coach=coach)
    return

def main(request):
    return HttpResponse("Hello")


def questionnairecoach(request):
    data = json.load(request)
    username = data['username']
    token = data['token']
    try:
        user = models.User.objects.get(username=username)
    except:
        return HttpResponse("Username does not exist")
    # if(token!= getMd5(username)):
    #     return HttpResponse("wrong token")
    name=data['name']
    try:
        age = int(data['age'])
    except:
        return HttpResponse("age should be an integer")
    gender = data['gender']
    about = data['about']
    approach = data['approach']
    philosophy = data['philosophy']
    industry= data['industry']
    try :
        years_of_practice=int(data['years_of_practice'])
    except:
        return HttpResponse("years of practice should be an integer")
    highest_edu_qualification=data['highest_edu_qualification']

    coach_goal_1 = data['coach_goal_1']
    coach_goal_2 = data['coach_goal_2']
    coach_goal_3 = data['coach_goal_3']
    day = ""
    for j in ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday","Else"] :
        try:
            if(data[j]=="on"):
                day = day +","+j
        except:
            pass
    if(day !=""):
        day = day[1:]
    try:
        coach = models.Coach.objects.get(username=user)
        coach.name= name
        coach.age= age
        coach.gender = gender
        coach.about = about
        coach.approach = approach
        coach.philosophy = philosophy
        coach.industry= industry
        coach.highest_edu_qualification= highest_edu_qualification
        coach.years_of_practice= years_of_practice
        coach.coach_goal_1 = coach_goal_1
        coach.coach_goal_2 = coach_goal_2
        coach.coach_goal_3 = coach_goal_3
        coach.day = day
        coach.questionnaire_completed = 'True'
        coach.save()
        ret = {"Success":"update success"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        pass
    try:
        models.Coach.objects.create(username=user,name=name,age=age,gender=gender,about=about,industry=industry,highest_edu_qualification=highest_edu_qualification,years_of_practice=years_of_practice,coach_goal_1=coach_goal_1,coach_goal_2=coach_goal_2,coach_goal_3=coach_goal_3,day=day)
    except:
        ret = {"error":"Unknow error happened"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    ret = {"Success":"create success"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)
    
def getCoachQuestionnaireInfo(request):
    data = json.load(request)
    username = data['username']

    coachData = []

    # get coach linked to leader
    try:
        user = models.User.objects.get(username=username)
        leaderUser = models.Leader.objects.get(username=user)
        coach = models.LeaderToCoach.objects.get(leader=leaderUser).coach
    except:
        ret = {coachData}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    # append coach info to array of dictionaries
    coachData.append({"name":str(coach.name)})
    coachData.append({"age":str(coach.age)})
    coachData.append({"gender":str(coach.gender)})
    coachData.append({"about":str(coach.about)})
    coachData.append({"industry":str(coach.industry)})
    coachData.append({"highest_edu_qualification":str(coach.highest_edu_qualification)})
    coachData.append({"years_of_practice":str(coach.years_of_practice)})
    coachData.append({"coach_goal_1":str(coach.coach_goal_1)})
    coachData.append({"coach_goal_2":str(coach.coach_goal_2)})
    coachData.append({"coach_goal_3":str(coach.coach_goal_3)})
    coachData.append({"approach":str(coach.approach)})
    coachData.append({"philosophy":str(coach.philosophy)})
    coachData.append({"day":str(coach.day)})
    

    ret = coachData
    print(":::::")
    print(ret)
    print(":::::")
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


def questionnaire(request):
    data = json.load(request)
    username = data['username']
    token = request.COOKIES.get('token')
    try:
        user = models.User.objects.get(username=username)
    except:
        return HttpResponse("Username not exist")
    dec=decodeJWT(token)
    print(dec[1])
    if(dec[0]!=True):
        print('yes')
        ret = {"error":"token expired"}
        ret = json.dumps(ret, ensure_ascii=False)
        response=HttpResponse(ret)
        response.delete_cookie('token')
        return response

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")
    name = data['name']
    try:
        age = int(data['age'])
    except:
        return HttpResponse("age should be an interger")
    gender = data['gender']
    industry = data['industry']
    company = data['company']
    try:
        years_employment= int(data['years_employment'])
    except:
        return HttpResponse("years of employment should be an integer")
    try:
        months_in_role= int(data['months_in_role'])
    except:
        return HttpResponse("months_in_role should be an integer")
    coach_goal = data['coach_goal']
    try:
        #Updating Leader table
        leader = models.Leader.objects.get(username=user)
        leader.name = name
        leader.age = age
        leader.gender = gender
        leader.years_of_employment=years_employment
        leader.months_in_role = months_in_role
        leader.industry = industry
        leader.company = company
        leader.questionnaire_completed = 'True'
        # leader.coach_goal = coach_goal
        leader.save()
        #Check day already in AvailableDays

        try:
            #Create a new row inside CoachingGoals table
            models.CoachingGoals.objects.create(leader=leader, coach_goal=coach_goal)
        except:
            return HttpResponse("Error creating CoachingGoals row")

        try:
            models.AvailableDays.objects.filter(leader=leader).delete()
        except:
            return HttpResponse("day table error")
        daylist = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday","Else"]
        for i in  range(8):
            try:
                if (data[daylist[i]] == "on"):
                    models.AvailableDays.objects.create(leader=leader,day=i)
            except:
                pass
        #assign the leader to a coach
        assignCoach(leader)
        ret = {"Success":"update success"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        pass
    '''
    try:
        leader = models.Leader(username=user)
        leader.name = name
        leader.age = age
        leader.gender = gender
        leader.industry = industry
        leader.coach_goal = coach_goal
        leader.save()
        daylist = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday","Else"]
        for i in  range(8):
            try:
                if (data[daylist[i]] == "on"):
                    models.AvailableDays.objects.create(leader=leader,day=i)
            except:
                pass
    except:
        ret = {"error":"Unknow error happened"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    '''
    ret = {"Success":"create success"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def check(request):
    # will be coach's username
    username = 'sample-coach'
    print(models.Leader.objects.get(username="Bill").get_subscription_type_display())
    # try:
    #     #get users and leaders and coaches
    #     coachUser = models.User.objects.get(username=username)
    #     coach = models.Coach.objects.get(username=coachUser)

    #     #Get all coach's leaders
    #     leaders = models.LeaderToCoach.objects.filter(coach=coach)
    # except:
    #     return HttpResponse("Leader/Coach does not exist")

    # try:
    #     #Format postdata
    #         #[ {"leader": "...",  "mainGoal": "...", "subGoals": [ {"subgoal":"...", "actions": [{"action": "...", "score": "..."}, {"action": "..." etc. }] } {subgoal: etc...} ] } ] }]

    #     ret = []
    #     #Loop through all the coach's leaders
    #     for leaderToCoach in leaders:

    #         try:
    #             # testing

    #             allCoachGoals = models.CoachingGoals.objects.all()
    #             print(allCoachGoals)
    #             print(leaderToCoach.leader)
    #             #end testing
    #             mainGoalObj = models.CoachingGoals.objects.get(leader=leaderToCoach.leader)
    #             subGoalObj = models.SubGoals.objects.filter(coach_goal=mainGoalObj)
    #         except:
    #             print("MainGoalObj / subGoalObj does not exist")
    #             continue


    #         subGoalsArr = []
    #         #Loop through all subgoals here
    #         for subgoal in subGoalObj:
    #             subGoalRow = subgoal
    #             print("subgoal>>: " + subGoalRow.sub_goal)

    #             #Find all associated actions
    #             try:
    #                 #actionObj = models.Actions.objects.filter(sub_goal=subGoalRow.sub_goal)
    #                 actionObj = models.Actions.objects.filter(sub_goal=subGoalRow)
    #             except:
    #                 print("error in no action being found")
    #                 continue
    #             actionsArr = []

    #             #Add each action and score
    #             for action in actionObj:
    #                 actionRow = action
    #                 actionDict = {"action": actionRow.action, "score": actionRow.score}
    #                 actionsArr.append(actionDict)

    #             #Add all actions to the associated subgoal
    #             subGoalDict = {"subGoal": subGoalRow.sub_goal, "actions": actionsArr}
    #             print("vvv subGoalGoal dictionary")
    #             print(subGoalDict)
    #             subGoalsArr.append(subGoalDict)

    #         #Add all subgoals to the main goal and add which leader it belongs to
    #         leaderDict = {"leader": leaderToCoach.leader.username.username,"mainGoal":mainGoalObj.coach_goal, "subGoals": subGoalsArr}
    #         ret.append(leaderDict)
    #     print(ret)
    #     ret = json.dumps(ret, ensure_ascii=False)
    #     return HttpResponse(ret)
    # except:
    #     return HttpResponse("Error formatting leaders goal post data")



    return HttpResponse("unknown error in NextSchedule")


def register(request):
    # id=1
    # token= generateJWT(id)
    # print(token)
    # print(decodeJWT(token))

    data = json.load(request)
    username = data['username']
    password = data['password1']
    email = data['email']
    usertype = data['usertype']
    userid=0

    # should have check for these elements
    try:
        models.User.objects.get(username=username)
        ret = {"error":"Username used"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        pass
    try:
        models.User.objects.create(username=username,password=password,email=email,usertype=usertype)
    except:
        ret = {"error":"Unknow error happened when user created"}
        ret = json.dumps(ret, ensure_ascii=False)
        #DEBUGGING
        print(ret)
        return HttpResponse(ret)
    try:
        user = models.User.objects.get(username=username)
        if usertype == "L":
            leader = models.Leader(username=user)
            leader.save()
        elif usertype == "C":
            coach = models.Coach(username=user)
            coach.save()
    except:
        ret = {"error":"Unknow error happened when leader/coach created"}
        ret = json.dumps(ret, ensure_ascii=False)
        print(ret)
        return HttpResponse(ret)
    reg=models.User.objects.get(username=username)
    userid=reg.pk
    print(userid)
    token = generateJWT(userid)
    print(token)
    tok= token.decode('utf-8')
    
    ret = {"username":username,"token":tok,"usertype":usertype}
    ret = json.dumps(ret, ensure_ascii=False)
    print(ret)
    dec=decodeJWT(tok)
    print(dec[0])
    response=HttpResponse(ret)
    response.set_cookie('token',tok)
    return response
    # try:
    #     user_leader = models.User.objects.get(username=username)
    #     # user_coach = models.User.objects.get(username="sample-coach")

    #     # sample-coach user data
    #     #username = sample-coach
    #     #email = sample-coach@gmail.com
    #     #password = sample-coach

    #     if usertype == "L":
    #         leader = models.Leader.objects.get(username=user_leader)
    #         coach = None

    #         # try and get coach placeholder
    #         try:
    #             coach = models.Coach.objects.get(user_coach)
    #         except:
    #             print("coach is not found")
    #             coach = models.Coach(username=user_coach)
    #             coach.save()
    #             print("coach successfully saved!")
    #         #print(leader)
    #         #print(coach)
    #         leaderToCoachModel = models.LeaderToCoach(leader=leader, coach=coach)
    #         leaderToCoachModel.save()
    #         #print(leaderToCoachModel)
    # except:
    #     ret = {"error":"Unable to create LeaderToCoach object"}
    #     ret = json.dumps(ret, ensure_ascii=False)
    #     print(ret)
    #     return HttpResponse(ret)
    

def login(request):
    data = json.load(request)
    username = data['username']
    password = data['password']
    email = data['email']
    user = {}
    userid=0

    # manually set 'sample-coach' user to usertype 'C'
    try:
        user = models.User.objects.get(username=username)
    except:
        ret = {"error":"Username not exist"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    try:
        user = models.User.objects.get(username=username)
        if (user.username == 'sample-coach'):
            user.usertype = 'C'
            user.save()
    except:
        ret = {"error":"Unable to set coach placeholder with usertype 'C'"}
        return HttpResponse(ret)
    if (user.password == password and user.email == email):
        reg=models.User.objects.get(username=username)
        userid=reg.pk
        print(userid)
        token = generateJWT(userid)
        print(token)
        tok= token.decode('utf-8')
        usertype = user.usertype
        ret = {"username":username,"token":tok,"usertype":usertype}
        ret = json.dumps(ret, ensure_ascii=False)
        print(ret)
        dec=decodeJWT(tok)
        print(dec[0])
        response=HttpResponse(ret)
        response.set_cookie('token',tok)
        return response
        
    else:
        ret = {"error":"wrong password or email"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

def settings(request):
    data = json.load(request)
    username = data['username']
    token = data['token']
    try:
        user = models.User.objects.get(username=username)
    except:
        return HttpResponse("Username not exist")
    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")
    try:
        leader = models.Leader.objects.get(username=user)
        goalObj = models.CoachingGoals.objects.get(leader=leader)
    except:
        ret = {"username":user.username,"email":user.email,"phone_number":user.phone_number,"leader":"not created"}
        # ret = {"username":user.username,"email":user.email,"phone_number":"019409132","industry":"I'm unemployed :("}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    ret = {"username":user.username,"email":user.email,"phone_number":user.phone_number,"industry":leader.industry,"coach_goal":goalObj.coach_goal, "subscription_type":leader.subscription_type}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def changeSettings(request):
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)

        #Check if user has changed any personal details
        #If not, keep value to same as existing data
        email = data.get('email', user.email)
        phone_number = data.get('phone_number', user.phone_number)
        password = data.get('password', user.password)
        industry = data.get('industry', leader.industry)
        subscription_type = data.get('subscription_type', leader.subscription_type)

        try:
            models.AvailableDays.objects.filter(leader=leader).delete()
        except:
            return HttpResponse("day table error")
        daylist = ["Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday","Else"]
        for i in  range(8):
            try:
                if (data[daylist[i]] == "on"):
                    models.AvailableDays.objects.create(leader=leader,day=i)
            except:
                pass



        #Apply changes to database
        user.email = email
        user.phone_number = phone_number
        user.password = password
        user.save()

        leader.industry = industry
        leader.subscription_type = subscription_type
        leader.save()

        #DEBUGGING
        # print("User's changed email is {}".format(data.get('email', "noemailrip")))
        # print("User's email in database is {}".format(user.email))
        # print("User's changed phonenumber is {}".format(data.get('phone_number', "noemailrip")))
        # print("User's phonenumber in database is {}".format(user.phone_number))
        print("User's changed industry is {}".format(data.get('industry', "None")))
        print("User's industry in database is {}".format(leader.industry))
        print("User's changed plan is {}".format(data.get('subscription_type', "None")))
        print("User's plan in database is {}".format(leader.subscription_type))
        for i in models.AvailableDays.objects.filter(leader=leader):
            print(i.day)

    except:
        return HttpResponse("Username does not exist")
    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token, cannot change existing details.")
    try:
        leader = models.Leader.objects.get(username=user)
        goalObj = models.CoachingGoals.objects.get(leader=leader)
    except:
        ret = {"username":user.username,"email":user.email,"phone_number":user.phone_number,"leader":"not created"}
        # ret = {"username":user.username,"email":user.email,"phone_number":"019409132","industry":"I'm unemployed :("}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    ret = {"username":user.username,"email":user.email,"phone_number":user.phone_number,"industry":leader.industry,"coach_goal":goalObj.coach_goal, "subscription_type":leader.subscription_type}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def addSchedule(request):
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
        coach = models.LeaderToCoach.objects.get(leader=leader).coach
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/leader does not exist")

    #if (token != getMd5(username)):
    #    return HttpResponse("Wrong token from addSchedule")

    #user = models.User.objects.get(username=username)

    #If there is a user and leader in the database,
    # try seeing if they have a schedule associated with them and update the row
    try:
        models.Scheduling.objects.create(leader=leader,coach=coach,date=data["date"],month=data["month"],year=data["year"],time=data["time"])
        return HttpResponse("Successful created Scheduling")
    except:
        # if there is no row associated with the user/leader, create a new one
        #pass
        # models.Scheduling.objects.create(leader=leader,coach=coach,date=data["date"],month=data["month"],year=data["year"],time=data["time"])
         ret = {"unable to create or get Schedule object from User"}
        # print("unable to create or get Schedule object from User")

def getLeaderSchedules(request):
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
        leaders = models.LeaderToCoach.objects.filter(coach=coach)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist from getLeaderSchedules")
    ret =  []
    try:
        # for-loop to put information in the following format:
        # [{leader: "someleadername", "schedules": [{}, {}, {}] },
        # {leader: "someleadername", "schedules": [{}, {}, {}] }]
        all_schedules = models.Scheduling.objects.filter(coach=coach)
        print(all_schedules)
        for i in all_schedules:
            print(i.leader)

        for l in leaders:
            leader = l.leader
            scheduleTable = models.Scheduling.objects.filter(leader=leader)
            dictionary = {}
            dictionary["leader"] = leader.username.username
            dictionary["schedules"] = []
            for index, schedule  in enumerate(scheduleTable):
                dictionary["schedules"].append({"date":schedule.date,"month":schedule.month,"year":schedule.year,"time":schedule.time})
            ret.append(dictionary)

        print("leader schedules is:")
        print(ret)

        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        print("still cant get schedule row")
        return HttpResponse("Unable to update/retrieve schedule row for leader")

# for coach to see all confirmed leader's schedules on coach home page
def getLeaderNextSchedules(request):
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
        leaders = models.LeaderToCoach.objects.filter(coach=coach)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist from getLeaderSchedules")
    ret =  []
    try:
        # for-loop to put information in the following format:
        # [{leader: "someleadername", "schedules": [{}, {}, {}] },
        # {leader: "someleadername", "schedules": [{}, {}, {}] }]
        all_schedules = models.NextSchedule.objects.filter(coach=coach)
        print(all_schedules)
        for i in all_schedules:
            print(i.leader)

        for l in leaders:
            leader = l.leader
            scheduleTable = models.Scheduling.objects.filter(leader=leader)
            dictionary = {}
            dictionary["leader"] = leader.username.username
            dictionary["zoomLink"] = schedule.zoomLink
            dictionary["schedules"] = []
            for index, schedule  in enumerate(scheduleTable):
                dictionary["schedules"].append({"date":schedule.date,"month":schedule.month,"year":schedule.year,"time":schedule.time})
            ret.append(dictionary)

        print("leader next schedules is:")
        print(ret)

        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        print("still cant get schedule row")
        return HttpResponse("Unable to update/retrieve next schedule row for leader")



# On leader's side to view their next scheduled appointment and the associated zoom link
def getNextSchedule(request):
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/leader does not exist from getNextSchedule")

    #if (token != getMd5(username)):
    #    return HttpResponse("Wrong token")

    try:
        try:
            schedule = models.NextSchedule.objects.get(leader=leader)
            ret = {"Success":"create success","date":schedule.date,"month":schedule.month,"year":schedule.year,"time":schedule.time, "zoomLink": schedule.zoomLink}
        except models.NextSchedule.DoesNotExist:
            ret = {"date":"","month":"","year":"","time":"", "zoomLink":""}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    except:
        return HttpResponse("unknown error in NextSchedule")

def confirmNextSchedule(request):
    data = json.load(request)
    username = data['username']
    token = data['token']
    print("leader: " + data['leader'])
    print('coach: ' + data['username'])
    print('date: ' + data['date'])
    print("month: " + data['month'])
    print('year: ' + data['year'])
    print('time: ' + data['time'])

    leaderUsername =  data['leader']

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
        leaderUser = models.User.objects.get(username=leaderUsername)
        leader = models.Leader.objects.get(username=leaderUser)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        models.Scheduling.objects.filter(leader=leader).delete()
        models.NextSchedule.objects.filter(leader=leader).delete()
        models.NextSchedule.objects.create(leader=leader,coach=coach,date=data["date"],month=data["month"],year=data["year"],time=data["time"])
        ret = {"Success":"create success"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in confirmNextSchedule")

def addHomework(request):
    #add a new Homework
    data = json.load(request)
    username = data['username'] #coach
    token = data['token']
    leaderUsername =  data['leader']
    # debugging purposes
    print("title: " + data['title'])
    print("description: " + data['description'])
    print('username: ' + data['username']) # this should be coach?
    print('leader username: ' + data['leader'])

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
        leaderUser = models.User.objects.get(username=leaderUsername)
        leader = models.Leader.objects.get(username=leaderUser)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #models.Homework.objects.create(leader=leader,coach=coach,title=data["title"],description=data["description"],completionTime=data["completionTime"])
        models.Homework.objects.create(leader=leader,coach=coach,title=data["title"],description=data["description"],isFinished=data["isFinished"])
        ret = {"Success":"create success"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in addHomework")


def getHomework(request):
    #get all homeworks of a given leader
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #Get all rows of a leader in Homework table = all the leader's homeworks
        leaderHomeworks = models.Homework.objects.filter(leader=leader)

        #Formatting post data to return
        #Format will be [{"title": "...", "description": "..."}, {etc.}]
        ret = []
        for homework in leaderHomeworks:
            dictionary = {"title": homework.title, "description": homework.description, "isFinished": homework.isFinished}
            print(dictionary)
            ret.append(dictionary)

        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in getHomework")

def getLeaderHomework(request):
    #get all leaders' homework of the coach
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #Get all leaders assigned to the given coach
        leaders = models.LeaderToCoach.objects.filter(coach=coach)

        ret = []
        #Loop through leader by leader
        for leader in leaders:
            #Get a given leader's homeworks
            leaderHomeworks = models.Homework.objects.filter(leader=leader.leader)

            #Format post data for return
            # [ {"leader":"...", "homeworks": [ {"title": "...", "description": "..."}, {etc.} ]}]
            homeworks = []
            for homework in leaderHomeworks:
                dictionary = {"title": homework.title, "description": homework.description, "isFinished": homework.isFinished}
                homeworks.append(dictionary)
            leaderRet = {"leader": leader.leader.username.username, "homeworks": homeworks}
            ret.append(leaderRet)

        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in getLeaderHomework")


# Updates Homework model after a leader completes a homework
def completeHomework(request):
    data = json.load(request)
    username = data['username']
    token = data['token']
    title = data['title']
    description = data['description']

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
        #coach = models.Coach.objects.get(username=user)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #Get all rows of a leader in Homework table = all the leader's homeworks
        leaderHomeworks = models.Homework.objects.filter(leader=leader)

        for homework in leaderHomeworks:
            if (homework.title == title and homework.description == description):
                homework.isFinished = True
                homework.save()
                print(homework.isFinished)
                print("found a matching homework for leader from 'completeHomework")
                break

        ret = {"Success":"Homework isFinished updated to True"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in completeHomework")






# Coach view where a coach will forward some link to a specific coachee/leader
def addZoomLink(request):
    #get all leaders' homework of the coach
    data = json.load(request)
    username = data['username']
    token = data['token']
    leaderUsername =  data['leader']
    link = data['zoomLink']
    print("leader: " + data['leader'])
    print("zoomLink: " + data['zoomLink'])

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
        leaderUser = models.User.objects.get(username=leaderUsername)
        leader = models.Leader.objects.get(username=leaderUser)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    if(leader):
        print('leader is not null')

    try:
        leaderNextSchedule = models.NextSchedule.objects.get(leader=leader)
        leaderNextSchedule.zoomLink = data['zoomLink']
        leaderNextSchedule.save()

        # print all NextSchedule models  - debugging
        all_nsch = models.NextSchedule.objects.all()
        for i in all_nsch:
            print("leader: {}, coach: {}, date: {}, month: {}, zoomLink: {}".format(i.leader, i.coach, i.date, i.month, i.zoomLink))


        ret = {"Success":"successfully added zoomLink {} to leader {}".format(leaderNextSchedule.zoomLink, leaderUsername)}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in addZoomLink")

# Leader view for getting a zoom link to be displayed on their homepage
def getZoomLink(request):
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        leaderNextSchedule = models.NextSchedule.objects.get(leader=leader)
        zoomLink = leaderNextSchedule.zoomLink
        ret = {"zoomLink": zoomLink}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in addZoomLink")

# Coach view for getting all coachees' zoom links
def getLeaderZoomLinks(request):
    data = json.load(request)
    # will be coach's username
    username = data['username']
    token = data['token']

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:

        # print all NextSchedule models  - debugging
        all_nsch = models.NextSchedule.objects.all()
        for i in all_nsch:
            print("leader: {}, coach: {}, date: {}, month: {}, zoomLink: {}".format(i.leader, i.coach, i.date, i.month, i.zoomLink))


        #Get all leaders assigned to the given coach
        leaders = models.LeaderToCoach.objects.filter(coach=coach)

        ret = []
        #Loop through leader by leader
        for leader in leaders:

            try:
                #Get a given leader's zoomLink
                leaderNextSchedule = models.NextSchedule.objects.get(leader=leader.leader)
                zoomLink = leaderNextSchedule.zoomLink
                print(">>getting leader {}: zoomLink: {}".format(leader, zoomLink))
            except models.NextSchedule.DoesNotExist:
                zoomLink = ""
            #Format post data for return
            # [ {"leader":"...", "zoomLink": "..."} ]
            leaderRet = {"leader": leader.leader.username.username, "zoomLink": zoomLink}
            ret.append(leaderRet)

        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("unknown error in getLeaderZoomLinks")


#Adding subgoal - Coach's view
# def addSubGoal(request):
#     data = json.load(request)
#     # will be coach's username
#     username = data['username']
#     leaderUsername = data['leaderUsername']
#     subGoal = data['subGoal']
#     token = data['token']

#     try:
#         #get users and leaders and coaches
#         coachUser = models.User.objects.get(username=username)
#         coach = models.Coach.objects.get(username=coachUser)

#         leaderUser = models.User.objects.get(username=leaderUsername)
#         leader = models.Leader.objects.get(username=leaderUser)
#     except:
#         return HttpResponse("Leader/Coach does not exist")

#     # if (token != getMd5(username)):
#     #     return HttpResponse("Wrong token")

#     try:
#         #Try to get the leader's main goal
#         goalObj = models.CoachingGoals.objects.get(leader=leader)
#         models.SubGoals.objects.create(leader=leader, coach=coach, coach_goal=goalObj, sub_goal=subGoal)
#     except:
#         return HttpResponse("Leader does not have a main coaching goal")

#     ret = {"Success":"create success"}
#     ret = json.dumps(ret, ensure_ascii=False)
#     return HttpResponse(ret)

#Adding subgoal - Leader's view
# def leaderAddSubGoal(request):
#     data = json.load(request)
#     # will be leader's username
#     username = data['username']
#     subGoal = data['subGoal']
#     token = data['token']

#     try:
#         #get leader user
#         leaderUser = models.User.objects.get(username=username)
#         leader = models.Leader.objects.get(username=leaderUser)

#         coach = models.LeaderToCoach.objects.get(leader=leader).coach
#     except:
#         return HttpResponse("Leader / Coach does not exist")

#     # if (token != getMd5(username)):
#     #     return HttpResponse("Wrong token")

#     try:
#         #Try to get the leader's main goal
#         goalObj = models.CoachingGoals.objects.get(leader=leader)
#     except:
#         return HttpResponse("Leader does not have a main coaching goal")
#     try:
#         models.SubGoals.objects.create(leader=leader, coach=coach, coach_goal=goalObj, sub_goal=subGoal)
#     except:
#         return HttpResponse("Leader cannot create subgoal")

#     ret = {"Success":"created new leader subgoal success"}
#     ret = json.dumps(ret, ensure_ascii=False)
#     return HttpResponse(ret)

#Removing subgoal - Coach's view
# def removeSubGoal(request):
#     data = json.load(request)
#     # will be coach's username
#     username = data['username']
#     leaderUsername = data['leaderUsername']
#     subGoal = data['subGoal']
#     token = data['token']

#     try:
#         #get users and leaders and coaches
#         coachUser = models.User.objects.get(username=username)
#         coach = models.Coach.objects.get(username=coachUser)

#         leaderUser = models.User.objects.get(username=leaderUsername)
#         leader = models.Leader.objects.get(username=leaderUser)
#     except:
#         return HttpResponse("Leader/Coach does not exist")

#     # if (token != getMd5(username)):
#     #     return HttpResponse("Wrong token")

#     try:
#         #Try to get the leader's main goal
#         goalObj = models.CoachingGoals.objects.get(leader=leader)
#     except:
#         return HttpResponse("Leader does not have a main coaching goal")

#     try:
#         #Try to get the leader's sub goal
#         subGoalObj = models.SubGoals.objects.filter(coach_goal=goalObj, sub_goal=subGoal)
#     except:
#         return HttpResponse("Leader does not have a subgoal")

#     try:
#         #Delete subgoal
#         subGoalObj.delete()
#     except:
#         return HttpResponse("Unable to delete subgoal")

#     ret = {"Success":"deleted subgoal and action(s)"}
#     ret = json.dumps(ret, ensure_ascii=False)
#     return HttpResponse(ret)

#Removing subgoal - Leader's view
# def leaderRemoveSubGoal(request):
#     data = json.load(request)
#     # will be leader's username
#     username = data['username']
#     subGoal = data['subGoal']
#     token = data['token']

#     try:
#         #get users and leaders and coaches
#         leaderUser = models.User.objects.get(username=username)
#         leader = models.Leader.objects.get(username=leaderUser)

#         coach = models.LeaderToCoach.objects.get(leader=leader).coach
#     except:
#         return HttpResponse("Leader/Coach does not exist")

#     # if (token != getMd5(username)):
#     #     return HttpResponse("Wrong token")

#     try:
#         #Try to get the leader's main goal
#         goalObj = models.CoachingGoals.objects.get(leader=leader)
#     except:
#         return HttpResponse("Leader does not have a main coaching goal")

#     try:
#         #Try to get the leader's sub goal
#         subGoalObj = models.SubGoals.objects.filter(coach_goal=goalObj, sub_goal=subGoal)
#     except:
#         return HttpResponse("Leader does not have a subgoal")

#     try:
#         #Delete subgoal
#         subGoalObj.delete()
#     except:
#         return HttpResponse("Unable to delete subgoal")

#     ret = {"Success":"deleted subgoal and action(s)"}
#     ret = json.dumps(ret, ensure_ascii=False)
#     return HttpResponse(ret)

#Adding action under subgoal - Coach's view
# def addAction(request):
#     data = json.load(request)
#     # will be coach's username
#     username = data['username']
#     leaderUsername = data['leaderUsername']
#     subGoal = data['subGoal']
#     action = data['action']
#     token = data['token']

#     try:
#         #get users and leaders and coaches
#         coachUser = models.User.objects.get(username=username)
#         coach = models.Coach.objects.get(username=coachUser)

#         leaderUser = models.User.objects.get(username=leaderUsername)
#         leader = models.Leader.objects.get(username=leaderUser)
#     except:
#         return HttpResponse("Leader/Coach does not exist")

#     # if (token != getMd5(username)):
#     #     return HttpResponse("Wrong token")

#     try:
#         #Try to get the leader's sub-goal
#         subGoalObj = models.SubGoals.objects.get(sub_goal=subGoal)
#         models.Actions.objects.create(leader=leader, coach=coach, sub_goal=subGoalObj, action=action)
#     except:
#         return HttpResponse("Leader does not have a sub coaching goal")

#     ret = {"Success":"created action success"}
#     ret = json.dumps(ret, ensure_ascii=False)
#     return HttpResponse(ret)

#Adding action - Leader's view
def leaderAddAction(request):
    data = json.load(request)
    username = data['username']
    token = data['token']
    action_name = data['actionName']
    max_iterations = data['maxIterations']

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=leaderUser)

        coach = models.LeaderToCoach.objects.get(leader=leader).coach
    except:
        return HttpResponse("Leader/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        print("CODE GOT HERE 1")
        #Check for an action with the same name (do nothing for now)
        duplicate_actions = models.Actions.objects.filter(leader=leader, action_name=action_name)  
        print("CODE GOT HERE 3")

        #Update actions table with new action
        print(leader)
        print(coach)
        print(action_name)
        print(max_iterations)
        models.Actions.objects.create(leader=leader, coach=coach, action_name=action_name, current_iterations=0, max_iterations=max_iterations)

        # Return the new action
        ret = {"actionName": action_name, "maxIterations": max_iterations, "currentIterations": 0, "completed": "False"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("Something went wrong trying to add leader's new action")

    

    

#Updating progress of action - Leader's view
def leaderUpdateAction(request):
    data = json.load(request)
    username = data['username']
    token = data['token']
    action_name = data['actionName']
    current_iterations = data['currentIterations']

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=leaderUser)
        coach = models.LeaderToCoach.objects.get(leader=leader).coach
    except:
        return HttpResponse("Leader/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:

        #Update actions table with new action
        old_action = models.Actions.objects.get(leader=leader, coach=coach, action_name=action_name)
        old_action.current_iterations = current_iterations
        if current_iterations == old_action.max_iterations:
            old_action.completed = 'True'
        old_action.save()

        # DEBUGGING
        print(leader)
        print(coach)
        print(action_name)
        print(current_iterations)
        
    except:
        return HttpResponse("Something went wrong trying to add leader's new action")

    ret = {"Success":"Updated action successfully!"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


#Removing action under subgoal - Coach's view
# def removeAction(request):
#     data = json.load(request)
#     # will be coach's username
#     username = data['username']
#     leaderUsername = data['leaderUsername']
#     subGoal = data['subGoal']
#     action = data['action']
#     token = data['token']

#     try:
#         #get users and leaders and coaches
#         coachUser = models.User.objects.get(username=username)
#         coach = models.Coach.objects.get(username=coachUser)

#         leaderUser = models.User.objects.get(username=leaderUsername)
#         leader = models.Leader.objects.get(username=leaderUser)
#     except:
#         return HttpResponse("Leader/Coach does not exist")

#     # if (token != getMd5(username)):
#     #     return HttpResponse("Wrong token")

#     try:
#         #Try to get the leader's sub-goal
#         goalObj = models.CoachingGoals.objects.get(leader=leaderUsername)
#     except:
#         return HttpResponse("Leader does not have a main coaching goal")

#     try:
#         subGoalObj = models.SubGoals.objects.filter(coach_goal=goalObj, sub_goal=subGoal)
#     except:
#         return HttpResponse("Leader does not have a sub coaching goal")

#     try:
#         models.Actions.objects.filter(action=action).delete()
#     except:
#         return HttpResponse("Leader does not have a action")

#     ret = {"Success":"deleted action for leader>>>"}
#     ret = json.dumps(ret, ensure_ascii=False)
#     return HttpResponse(ret)

#Removing action under subgoal - Leader's view
def leaderRemoveAction(request):
    data = json.load(request)
    # will be coach's username
    username = data['username']
    subGoal = data['subGoal']
    action = data['action']
    token = data['token']

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=leaderUser)

        coach = models.LeaderToCoach.objects.get(leader=leader).coach
    except:
        return HttpResponse("Leader/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #Try to get the leader's sub-goal
        goalObj = models.CoachingGoals.objects.get(leader=username)
    except:
        return HttpResponse("Leader does not have a main coaching goal")

    try:
        subGoalObj = models.SubGoals.objects.filter(coach_goal=goalObj, sub_goal=subGoal)
    except:
        return HttpResponse("Leader does not have a sub coaching goal")

    try:
        models.Actions.objects.filter(action=action).delete()
    except:
        return HttpResponse("Leader does not have a action")

    ret = {"Success":"deleted action for leader>>>"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

#Updating score of an action
def updateActionScore(request):
    data = json.load(request)
    # will be leader's username
    username = data['username']
    action =  data['action']
    score = data['score']
    token = data['token']

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #Try to get the leader's action
        actionObj = models.Actions.objects.get(action=action)

        #Update the action completion score
        score = int(score)
        if score < 0 or score > 10:
            return HttpResponse("Input error - Score must be between 0 and 10")

        actionObj.score = score
        actionObj.save()
    except:
        return HttpResponse("Leader does not have a sub coaching goal")

    ret = {"Success":"create success"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Coach view - get actions of linked leaders
def getAllLeaderActions(request):
    data = json.load(request)
    username = data['username'] #coach username
    token = data['token']

    # get Coach object
    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
    except:
        ret = {"msg":"Coach user does not exist"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    # get all leaders linked to coach
    try:
        all_leaders = models.LeaderToCoach.objects.filter(coach=coach)
    except:
        ret = {"msg":"Coach user is not linked to a leader"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    all_leader_actions = []

    # format of all_leader_actions
    # action: {"name": name, "iteration": current_iterations, "max": max_iterations}
    # actions: {"name": name, "iteration": current_iterations, "max": max_iterations}, {"name": name, ...}
    # leader: {"leader": leader_name, "actions": [ {...}, {...}, {...} ]}
    # leaders: [{"leader": leader_name, "actions": [{...},{...},{...}]}, {"leader": leader_name, ...}]



    # get incomplete and complete actions from each leader 
    for i in all_leaders:

        try:
            leader_completed_actions = models.Actions.objects.filter(leader=i.leader, completed="True")

            completed_actions = []

            for j in leader_completed_actions:
                completed_actions.append(j.action_name)

        except:
            # leader has no completed actions
            continue

        try:
            leader_actions = models.Actions.objects.filter(leader=i.leader, completed="False")

            actions = []

            # printing actions of leader
            for j in leader_actions:
                action = {"actionName": j.action_name, "iteration": j.current_iterations, "max": j.max_iterations}
                actions.append(action)

            all_leader_actions.append({"leader": i.leader.username.username, "actions": actions, "completedActions": completed_actions})

        except:
            # leader has no actions
            continue
    
    # debugging
    print(all_leader_actions)

    # [{"leader": leader_name, "actions": [ {""} ] }]
    ret = {"msg": "success", "data": all_leader_actions}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


#Leader View - Get goal + actions
def getLeaderGoals(request):
    data = json.load(request)
    # will be leader's username
    username = data['username']
    token = data['token']

    try:
        #get leader
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
    except:
        return HttpResponse("Leader/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        mainGoalObj = models.CoachingGoals.objects.get(leader=leader)
    except:
        # handles no coaching goal
        ret = []
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    try:
        #mainGoalObj = models.CoachingGoals.objects.get(leader=leader)
        actionsObj = models.Actions.objects.filter(leader=leader)

        if len(actionsObj) == 0:
            ret = []
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)
        elif len(actionsObj) > 0:
            #Format postdata
            #[ {"mainGoal": "...", "actions": [ 
            # {"actionName": "...", "currentIterations": "...", "maxIterations": "...", "completed": "..."}, 
            # {"actionName": "...", "currentIterations": "...", "maxIterations": "...", "completed": "..."} etc. 
            # ] 
            # } ]
            ret = []
            actionsArr = []
            #Loop through all actions here
            for action_row in actionsObj:
                #Add action dictionary containing action info into actions array
                actionDict = {"actionName": action_row.action_name, "currentIterations": action_row.current_iterations, "maxIterations": action_row.max_iterations, "completed": action_row.completed}
                actionsArr.append(actionDict)

            # Final return data includes a main coaching goal and an array of action dictionaries
            mainGoalDict = {"mainGoal":mainGoalObj.coach_goal, "actions": actionsArr}
            ret = mainGoalDict
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)
    except:
        return HttpResponse("Error formatting leader goals and actions data")

#Coach View - Get goal + subgoal + actions
def getAllLeadersGoals(request):
    data = json.load(request)
    # will be coach's username
    username = data['username']
    token = data['token']

    try:
        #get users and leaders and coaches
        coachUser = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=coachUser)

        #Get all coach's leaders
        leaders = models.LeaderToCoach.objects.filter(coach=coach)
    except:
        return HttpResponse("Leader/Coach does not exist")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #Format postdata
            #[ {"leader": "...",  "mainGoal": "...", "subGoals": [ {"subgoal":"...", "actions": [{"action": "...", "score": "..."}, {"action": "..." etc. }] } {subgoal: etc...} ] } ] }]

        ret = []
        #Loop through all the coach's leaders
        for leaderToCoach in leaders:
            try:
                mainGoalObj = models.CoachingGoals.objects.get(leader=leaderToCoach.leader)
                subGoalObj = models.SubGoals.objects.filter(coach_goal=mainGoalObj)
            except:
                continue


            subGoalsArr = []
            #Loop through all subgoals here
            for subgoal in subGoalObj:
                subGoalRow = subgoal

                #Find all associated actions
                try:
                    #actionObj = models.Actions.objects.filter(sub_goal=subGoalRow.sub_goal)
                    actionObj = models.Actions.objects.filter(sub_goal=subGoalRow)
                except:
                    continue
                actionsArr = []

                #Add each action and score
                for action in actionObj:
                    actionRow = action
                    actionDict = {"action": actionRow.action, "score": actionRow.score}
                    actionsArr.append(actionDict)

                #Add all actions to the associated subgoal
                subGoalDict = {"subGoal": subGoalRow.sub_goal, "actions": actionsArr}
                subGoalsArr.append(subGoalDict)

            #Add all subgoals to the main goal and add which leader it belongs to
            leaderDict = {"leader": leaderToCoach.leader.username.username,"mainGoal":mainGoalObj.coach_goal, "subGoals": subGoalsArr}
            ret.append(leaderDict)
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        return HttpResponse("Error formatting leaders goal post data")

#leader/coach will click on messaging button -> pop-up
#posts leader's username + coach's usernme
#chat.channelname = 'coachusername + leaderusername'
#Messages table - filter by leader and coach (holds message history)
#message history is set Message.js


def getMessageHistory(request):
    data= json.load(request)
    username = data['username'] #Could be either leader or coach
    leaderUsername = data['leaderUsername']

    #Set leader and coach variables depending on which usertype sent the message
    try:
        user = models.User.objects.get(username=username)
        usertype = user.usertype
        if usertype == "L":
            leader = models.Leader.objects.get(username=user)
            coach = models.LeaderToCoach.objects.get(leader=leader).coach
        else:
            leaderUser = models.User.objects.get(username=leaderUsername)
            leader = models.Leader.objects.get(username=leaderUser)
            coach = models.Coach.objects.get(username=user)
    except:
        return HttpResponse("User, leader, or coach does not exist")
    try:
        messages = models.Message.objects.filter(leader=leader)
        """all_schedules = models.NextSchedule.objects.filter(coach=coach)
        print(all_schedules)
        for i in all_schedules:
            print(i.leader)

        for l in leaders:
            leader = l.leader
            scheduleTable = models.Scheduling.objects.filter(leader=leader)
            dictionary = {}
            dictionary["leader"] = leader.username.username
            dictionary["schedules"] = []
            for index, schedule  in enumerate(scheduleTable):
                dictionary["schedules"].append({"date":schedule.date,"month":schedule.month,"year":schedule.year,"time":schedule.time})
            ret.append(dictionary)

        print("leader next schedules is:")
        print(ret)"""

        messagesArray=[]


        #Format post data
        for message in messages:
            if message.sender == "L":
                #dictionary = {"name":message.leader.username.username,"message":message.message,"date":message.time,"format":"right"}
                dictionary = {"name":message.leader.username.username,"message":message.message,"format":"right"}
            else:
                #dictionary = {"name":message.coach.username.username,"message":message.message,"date":message.time,"format":"left"}
                dictionary = {"name":message.coach.username.username,"message":message.message,"format":"left"}
            messagesArray.append(dictionary)
        print(messagesArray)
    except:
        return HttpResponse("can't get messagehistory")


    ret = {"Success":"message success", "messages":messagesArray}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


def getLeaderCoachid(request):
    data= json.load(request)
    username = data['username'] #Could be either leader or coach

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
        coach = models.LeaderToCoach.objects.get(leader=leader).coach.username
        
        

        leaderid=user.pk
        coachid=coach.pk
        coachname=coach.username

    except:
        print("noot")
        return HttpResponse("User, leader, or coach does not exist")

    ret = {"leaderid": leaderid,"coachid":coachid, "coachname":coachname}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


def getLeaderid(request):
    data= json.load(request)
    leadername= data['leadername']

    try:
        user = models.User.objects.get(username=leadername)
        leaderid=user.pk

    except:
        return HttpResponse("leader does not exist")

    ret = {"leaderid": leaderid}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def getLeaderCoacheeid(request):
    data= json.load(request)
    username = data['username'] #Could be either leader or coach
    leadername= data['leaderName']

    try:
        user = models.User.objects.get(username=username)
        leader = models.User.objects.get(username=leadername)
        

        coachid=user.pk
        leaderid=leader.pk

    except:
        return HttpResponse("User, leader, or coach does not exist")

    ret = {"leaderid": leaderid,"coachid":coachid}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)





def addMessage(request):
    data= json.load(request)
    username = data['username'] #Could be either leader or coach
    leaderUsername = data['leaderUsername']

    #Set leader and coach variables depending on which usertype sent the message
    try:
        user = models.User.objects.get(username=username)
        usertype = user.usertype
        if usertype == "L":
            leader = models.Leader.objects.get(username=user)
            coach = models.LeaderToCoach.objects.get(leader=leader).coach
        else:
            leaderUser = models.User.objects.get(username=leaderUsername)
            leader = models.Leader.objects.get(username=leaderUser)
            coach = models.Coach.objects.get(username=user)
    except:
        return HttpResponse("User, leader, or coach does not exist")

    pusher_client.trigger(leaderUsername, 'message', {
            'username': data['username'],
            'message': data['message'],
        })

    #Add message to database here
    try:
        models.Message.objects.create(leader=leader, coach=coach, sender=usertype, message=data['message'])
    except:
        return HttpResponse("creating message row failed")

    ret = {"Success":"message success"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def loadMessages(request):
    data= json.load(request)
    username = data['username'] #Could be either leader or coach
    try:
        sender = models.User.get(username=username)
        models.Messages.filter(sender=sender)
    except:
        return HttpResponse("error")
    return


def saveMessages(request):
    data= json.load(request)
    sender_id=data['senderId']
    room_id=data['roomid']
    message=data['text']

    print(type(sender_id))
    print(type(room_id))
    print(type(message))

    try:
        models.Chat.objects.create(room_id=room_id,sender_id=sender_id,message=message)
    except:
        print("can't create message row")
        return HttpResponse("creating message row failed")

    ret = {"Success":"message success"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def getMessages(request):
    data= json.load(request)
    roomid = int(data['roomid']) 
    senderid=int(data['sender'])
    print(senderid)
    last_messageid=0
    print("room")
    print(roomid)
   
    try:
        messages = models.Chat.objects.filter(room_id=roomid)
        last_message= messages.order_by('-id')[:1]

        print(messages)
        print(last_message)

    except:
        return HttpResponse("error")

    messagesArray=[]

    if not messages:
        ret={"messages":messagesArray}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
        

    


        #Format post data
    for message in messages:
        dictionary = {"sender":message.sender_id,"text":message.message}
       
        messagesArray.append(dictionary)

    for las in last_message:
        last_messageid=las.id

    latestMessage= models.Chat.objects.get(id=last_messageid)
    if(latestMessage.sender_id!=senderid):
        latestMessage.received=True
        latestMessage.save()


    
    ret={"messages":messagesArray}

    print(ret)
    print(last_messageid)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def getMessageNotifications(request):
    data = json.load(request)
    roomid = int(data['roomid']) 
    userid = int(data['userid'])
    print(userid)
    last_messageid=0
   
   
    try:
        messages = models.Chat.objects.filter(room_id=roomid)
        last_message= messages.order_by('-id')[:1]

        print(last_message)

    except:
        return HttpResponse("error")

    for las in last_message:
        last_messageid=las.id

    latestMessage= models.Chat.objects.get(id=last_messageid)
    if(latestMessage.received!=True):
        if(latestMessage.sender_id!=userid):
            ret = {"new_message":"You have new messages"}
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)
    else:
        return 

def getCoachNotifications(request):
    data = json.load(request)
    user = data['user']
    userid = 0
    print(user)
    last_messageid=0
   
   
    try:
        coach= models.Coach.objects.get(username=user)
        coachname = coach.pk
        userid= models.User.objects.get(username=coachname).pk
        leadertocoach = models.LeaderToCoach.objects.filter(coach=coach)
        print(userid)
        print(leadertocoach)
        # messages = models.Chat.objects.filter(room_id=roomid)
        # last_message= messages.order_by('-id')[:1]

        # print(last_message)

    except:
        print("no coach")
        return HttpResponse("loadertocoach does not exist")

    for ltoC in leadertocoach:
        leader= ltoC.leader.pk
        roomid= models.User.objects.get(username=leader).pk
        print(roomid)
        messages = models.Chat.objects.filter(room_id=roomid)
        last_message= messages.order_by('-id')[:1]
        for las in last_message:
            last_messageid=las.id
        latestMessage= models.Chat.objects.get(id=last_messageid)
        if(latestMessage.received!=True):
            if(latestMessage.sender_id!=userid):
                ret = {"new_message":"You have new messages coach"}
                ret = json.dumps(ret, ensure_ascii=False)
                return HttpResponse(ret)






        

   






    






#this is a test api key
stripe.api_key = "sk_test_51Jk5NZLE22eIGJeai3ffzlUYKUVUkTNcvyd8Wyq7DtCxsTGAvOGtxWxoMAOyoMDx0njjB5THiZFdgEsVShIscmv300jKL4a24r"
def create_payment(request):
    data = request.body.decode('UTF-8')
    list = data.split("&")
    for i in list:
        j = i.split("=")
        if j[0] == "plan":
            planValue = j[1]
        elif j[0] == "username":
            username = j[1]
    print(planValue)
    if planValue == "1":
        price = "price_1Jrd3RLE22eIGJeazvXzBSK8"
    elif planValue == "2":
        price = "price_1Jrd4gLE22eIGJeaTAEqFCdO"
    print(planValue)
    print(username)
    YOUR_DOMAIN = "https://new-emerge-website.herokuapp.com/payment"
    #YOUR_DOMAIN = "http://www.google.com"
    try:
        checkout_session = stripe.checkout.Session.create(
            line_items=[
                {
                    # TODO: replace this with the `price` of the product you want to sell
                    'price': price,
                    'quantity': 1,
                },
            ],
            payment_method_types=[
              'card',
            ],
            mode='subscription',
            success_url=YOUR_DOMAIN + '?success=true&plan='+ planValue,
            cancel_url=YOUR_DOMAIN,#+ '?canceled=true',
            client_reference_id = username
        )
    except Exception as e:
        return HttpResponse(str(e))

    return redirect(checkout_session.url, code=303)

endpoint_secret = 'whsec_kNMRSloot1pQkjX9PawSwNfH9BGAwSUD'
#stripe
def my_webhook_view(request):
  payload = request.body
  sig_header = request.META['HTTP_STRIPE_SIGNATURE']
  event = None

  try:
    event = stripe.Webhook.construct_event(
      payload, sig_header, endpoint_secret
    )
  except ValueError as e:
    # Invalid payload
    return HttpResponse(status=400)
  except stripe.error.SignatureVerificationError as e:
    # Invalid signature
    return HttpResponse(status=400)

  # Handle the checkout.session.completed event
  if event['type'] == 'checkout.session.completed':
    session = event['data']['object']

    # Fulfill the purchase...
    fulfill_order(session)

  # Passed signature verification
  return HttpResponse(status=200)


def fulfill_order(session):
    # TODO: fill me in
    print(session.get("client_reference_id"))
    print(session.get("amount_total"))
    username = session.get("client_reference_id")
    amount = session.get("amount_total")
    if (amount == 15000):
        subscription_type = "1"
    elif(amount == 20000):
        subscription_type = "2"
    else:
        subscription_type = "0"

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=leaderUser)

        #Check for any previous LeaderToCoach row
        try:
            leader.subscription_type = subscription_type
            leader.save()
        except:
            print("Update of subscription type failed")

    except:
        return HttpResponse("Something went wrong when geting leader")


def change_subscription(request):
    # TODO: fill me in
    data = json.load(request)
    username = data['username']
    planValue = data['planValue']
    print("planValue:"+planValue);
    if (str(planValue) == "1"):
        subscription_type = "1"
    elif(str(planValue) == "2"):
        subscription_type = "2"
    else:
        subscription_type = "0"

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=leaderUser)

        #Check for any previous LeaderToCoach row
        try:
            leader.subscription_type = subscription_type
            leader.save()
        except:
            print("Update of subscription type failed")

    except:
        return HttpResponse("Something went wrong when geting leader")
    ret = {"Success": "Subscription type successfully setted!"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)
#Admin page
#Assign non-assigned leader to existing coach
def assignLeaderToCoach(request):
    data = json.load(request)
    # will be admin's username
    # username = data['username']
    # token = data['token']
    leaderUsername = data['leaderUsername']
    coachUsername = data['coachUsername']
    print(coachUsername+"------------------------------------------------------------------")

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=leaderUsername)
        leader = models.Leader.objects.get(username=leaderUser)

        coachUser = models.User.objects.get(username=coachUsername)
        coach = models.Coach.objects.get(username=coachUser)

        #Check for any previous LeaderToCoach row
        try:
            models.LeaderToCoach.objects.get(leader=leader).delete()
        except:
            print("Leader does not exist in LeaderToCoach table")

        #Assign leader to coach
        models.LeaderToCoach.objects.create(leader=leader, coach=coach)
        
        #Set leader matched_with_coach field to true
        # leader = models.Leader.objects.get(username=leader.username)
        leader.matched_with_coach = 'True'
        leader.save()
    except:
        return HttpResponse("Something went wrong assigning leader to coach...")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")
    
    ret = {"Success": "Leader successfully assigned!"}
    ret = json.dumps(ret, ensure_ascii=False)
    print(ret)
    return HttpResponse(ret)


def unlinkLeaderToCoach(request):
    data = json.load(request)
    # will be admin's username
    # username = data['username']
    # token = data['token']
    leaderUsername = data['leaderUsername']
    # coachUsername = data['coachUsername']

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=leaderUsername)
        leader = models.Leader.objects.get(username=leaderUser)

        # coachUser = models.User.objects.get(username=coachUsername)
        # coach = models.Coach.objects.get(username=coachUser)

        #Check for any previous LeaderToCoach row and set matched with coach field back to False
        try:
            models.LeaderToCoach.objects.get(leader=leader).delete()
            leader.matched_with_coach = 'False'
            leader.save()
        except:
            print("Leader does not exist in LeaderToCoach table")

    except:
        return HttpResponse("Something went wrong unassigning leader to coach...")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")
    ret = {"msg": "Leader {} successfully unlinked!".format(leaderUsername)}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)



#Get all leaders and existing coaches (dropdown box) with info on whether theyre assigned or not
def getAllLeadersAndCoaches(request):
    data = json.load(request)
    # will be admin's username
    # username = data['username']
    # usertype = data['usertype']
    # token = data['token']

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #get users and leaders and coaches
        leaders = models.Leader.objects.all()

    except:
        return HttpResponse("Leader/Coach does not exist")

    try:
        #format leader information
        # [ {"leaderUsername":  [{age: ..., gender: ..., industry: ..., days: "...", mainCoachingGoal: "...", assignedTo: ... }] } etc...  ]
        allLeaders = []
        for leader in leaders:
            if leader.age == 0:
                age = "No age specified"
            else:
                age = leader.age

            if leader.gender == "":
                gender = "No gender specified"
            else:
                gender = leader.gender

            if leader.industry == "":
                industry = "No industry specified"
            else:
                industry = leader.industry

            if leader.company == "":
                company = "No company specified"
            else:
                company = leader.company

            if leader.day == "":
                day = "Available days not specified"
            else:
                day = leader.day

            if leader.years_of_employment == -1:
                years_of_employment = "Years of employment not specified"
            elif leader.years_of_employment == 0:
                years_of_employment = "Years of employment less than a year"
            else:
                years_of_employment = leader.years_of_employment

            if leader.months_in_role == -1:
                months_in_role = "Months in role not specified"
            elif leader.months_in_role == 0:
                months_in_role = "New to role"
            else:
                months_in_role = leader.months_in_role

            subscription_type = leader.get_subscription_type_display()

            mainCoachingGoal = ""
            assignedTo = ""


            #Check if leader has chosen a main coaching goal
            try:
                mainCoachingGoal = models.CoachingGoals.objects.get(leader=leader).coach_goal
            except:
                mainCoachingGoal = "No coaching goal selected."

            #Check if leader has already been assigned to a coach
            try:
                assignedTo = models.LeaderToCoach.objects.get(leader=leader).coach.username.username
            except:
                assignedTo = "Not assigned to any coach yet."
            leaderInfoDict = {"age":age, "gender":gender, "industry":industry, "company":company, "day":day, "years_of_employment": years_of_employment, "months_in_role": months_in_role, "mainCoachingGoal":mainCoachingGoal, "assignedTo":assignedTo, "subscription_type":subscription_type}
            leadersJson = {"username": leader.username.username, "info": leaderInfoDict}
            allLeaders.append(leadersJson)
    except:
        return HttpResponse("Something went wrong formatting return post data for leaders in admin page")

    #Format post return data for all coaches information
    # [ {"leaderUsername":  [{age: ..., gender: ..., about: ..., coachGoal1: "...", coachGoal2: "...", coachGoal3: "...", day: ... , "numberOfCoachees": ...}] } etc...  ]
    try:
        allCoaches = []
        coaches = models.Coach.objects.all()
        for coach in coaches:

            if coach.age == 0:
                age = "No age specified"
            else:
                age = coach.age

            if coach.gender == "":
                gender = "No gender specified"
            else:
                gender = coach.gender

            if coach.about == "":
                about = "Coach's about info not specified"
            else:
                about = coach.about

            if coach.industry == "":
                industry = "Coach's industry not specified"
            else:
                industry = coach.industry

            if coach.highest_edu_qualification == "":
                highest_edu_qualification = "Coach's highest qualification not specified"
            else:
                highest_edu_qualification = coach.highest_edu_qualification

            if coach.years_of_practice == -1:
                years_of_practice = "Coach's years of practice not specified"
            elif coach.years_of_practice == 0:
                years_of_practice = "Coach's years of practice less than a year"
            else:
                years_of_practice = coach.years_of_practice

            if coach.day == "":
                day = "Available days not specified"
            else:
                day = coach.day

            coach_goal_1 = coach.coach_goal_1
            coach_goal_2 = coach.coach_goal_2
            coach_goal_3 = coach.coach_goal_3
            numberOfCoachees = 0

            #Check the number of coachees this coach is coaching
            numberOfCoachees = len(models.LeaderToCoach.objects.filter(coach=coach))
            if numberOfCoachees == 0:
                numberOfCoachees = "No leaders assigned to this coach."

            coachInfoDict = {"age":age, "gender":gender, "about":about, "industry":industry, "highest_edu_qualification":highest_edu_qualification, "years_of_practice":years_of_practice, "day":day, "coach_goal_1":coach_goal_1, "coach_goal_2":coach_goal_2, "coach_goal_3":coach_goal_3, "numberOfCoachees":numberOfCoachees}
            coachJson = {"username":coach.username.username, "info": coachInfoDict}
            allCoaches.append(coachJson)
    except:
        return HttpResponse("Something went wrong formatting return post data for coaches in admin page")

    ret = {"leaders": allLeaders, "coaches": allCoaches}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

def getNewLeadersAndCoaches(request):
    data = json.load(request)
    # will be admin's username
    # username = data['username']
    # usertype = data['usertype']
    # token = data['token']

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        # Get leaders who are not assigned to any coach yet
        leaders = models.Leader.objects.all()
        unassigned_leaders = [] # Contains leader objects who are not assigned
        for leader in leaders:
            if leader.matched_with_coach == 'False':
                unassigned_leaders.append(leader)
        

    except:
        return HttpResponse("Leader/Coach does not exist")

    try:
        #format leader information
        # [ {"leaderUsername":  [{age: ..., gender: ..., industry: ..., days: "...", mainCoachingGoal: "...", assignedTo: ... }] } etc...  ]
        allLeaders = []
        for leader in unassigned_leaders:
            if leader.age == 0:
                age = "No age specified"
            else:
                age = leader.age

            if leader.gender == "":
                gender = "No gender specified"
            else:
                gender = leader.gender

            if leader.industry == "":
                industry = "No industry specified"
            else:
                industry = leader.industry

            if leader.company == "":
                company = "No company specified"
            else:
                company = leader.company

            if leader.day == "":
                day = "Available days not specified"
            else:
                day = leader.day

            if leader.years_of_employment == -1:
                years_of_employment = "Years of employment not specified"
            elif leader.years_of_employment == 0:
                years_of_employment = "Years of employment less than a year"
            else:
                years_of_employment = leader.years_of_employment

            if leader.months_in_role == -1:
                months_in_role = "Months in role not specified"
            elif leader.months_in_role == 0:
                months_in_role = "New to role"
            else:
                months_in_role = leader.months_in_role

            subscription_type = leader.get_subscription_type_display()

            mainCoachingGoal = ""
            assignedTo = ""


            #Check if leader has chosen a main coaching goal
            try:
                mainCoachingGoal = models.CoachingGoals.objects.get(leader=leader).coach_goal
            except:
                mainCoachingGoal = "No coaching goal selected."

            #Check if leader has already been assigned to a coach
            try:
                assignedTo = models.LeaderToCoach.objects.get(leader=leader).coach.username.username
            except:
                assignedTo = "Not assigned to any coach yet."
            leaderInfoDict = {"age":age, "gender":gender, "industry":industry, "company":company, "day":day, "years_of_employment": years_of_employment, "months_in_role": months_in_role, "mainCoachingGoal":mainCoachingGoal, "assignedTo":assignedTo, "subscription_type":subscription_type}
            leadersJson = {"username": leader.username.username, "info": leaderInfoDict}
            allLeaders.append(leadersJson)
    except:
        return HttpResponse("Something went wrong formatting return post data for leaders in admin page")

    #Format post return data for all coaches information
    # [ {"leaderUsername":  [{age: ..., gender: ..., about: ..., coachGoal1: "...", coachGoal2: "...", coachGoal3: "...", day: ... , "numberOfCoachees": ...}] } etc...  ]
    try:
        allCoaches = []
        coaches = models.Coach.objects.all()
        for coach in coaches:

            if coach.age == 0:
                age = "No age specified"
            else:
                age = coach.age

            if coach.gender == "":
                gender = "No gender specified"
            else:
                gender = coach.gender

            if coach.about == "":
                about = "Coach's about info not specified"
            else:
                about = coach.about

            if coach.industry == "":
                industry = "Coach's industry not specified"
            else:
                industry = coach.industry

            if coach.highest_edu_qualification == "":
                highest_edu_qualification = "Coach's highest qualification not specified"
            else:
                highest_edu_qualification = coach.highest_edu_qualification

            if coach.years_of_practice == -1:
                years_of_practice = "Coach's years of practice not specified"
            elif coach.years_of_practice == 0:
                years_of_practice = "Coach's years of practice less than a year"
            else:
                years_of_practice = coach.years_of_practice

            if coach.day == "":
                day = "Available days not specified"
            else:
                day = coach.day

            coach_goal_1 = coach.coach_goal_1
            coach_goal_2 = coach.coach_goal_2
            coach_goal_3 = coach.coach_goal_3
            numberOfCoachees = 0

            #Check the number of coachees this coach is coaching
            numberOfCoachees = len(models.LeaderToCoach.objects.filter(coach=coach))
            if numberOfCoachees == 0:
                numberOfCoachees = "No leaders assigned to this coach."

            coachInfoDict = {"age":age, "gender":gender, "about":about, "industry":industry, "highest_edu_qualification":highest_edu_qualification, "years_of_practice":years_of_practice, "day":day, "coach_goal_1":coach_goal_1, "coach_goal_2":coach_goal_2, "coach_goal_3":coach_goal_3, "numberOfCoachees":numberOfCoachees}
            coachJson = {"username":coach.username.username, "info": coachInfoDict}
            allCoaches.append(coachJson)
    except:
        return HttpResponse("Something went wrong formatting return post data for coaches in admin page")

    ret = {"leaders": allLeaders, "coaches": allCoaches}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)



def getAssignedLeadersAndCoaches(request):
    data = json.load(request)
    # will be admin's username
    # username = data['username']
    # usertype = data['usertype']
    # token = data['token']

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        # Get leaders who are not assigned to any coach yet
        assigned_leaders = [] # Contains leader objects who are assigned
        assigned = models.LeaderToCoach.objects.all()
        for pair in assigned:
            if(pair.leader not in assigned_leaders):
                assigned_leaders.append(pair.leader)
       
        

    except:
        return HttpResponse("Leader/Coach does not exist")

    try:
        #format leader information
        # [ {"leaderUsername":  [{age: ..., gender: ..., industry: ..., days: "...", mainCoachingGoal: "...", assignedTo: ... }] } etc...  ]
        allLeaders = []
        for leader in assigned_leaders:
            if leader.age == 0:
                age = "No age specified"
            else:
                age = leader.age

            if leader.gender == "":
                gender = "No gender specified"
            else:
                gender = leader.gender

            if leader.industry == "":
                industry = "No industry specified"
            else:
                industry = leader.industry

            if leader.company == "":
                company = "No company specified"
            else:
                company = leader.company

            if leader.day == "":
                day = "Available days not specified"
            else:
                day = leader.day

            if leader.years_of_employment == -1:
                years_of_employment = "Years of employment not specified"
            elif leader.years_of_employment == 0:
                years_of_employment = "Years of employment less than a year"
            else:
                years_of_employment = leader.years_of_employment

            if leader.months_in_role == -1:
                months_in_role = "Months in role not specified"
            elif leader.months_in_role == 0:
                months_in_role = "New to role"
            else:
                months_in_role = leader.months_in_role

            subscription_type = leader.get_subscription_type_display()

            mainCoachingGoal = ""
            assignedTo = ""


            #Check if leader has chosen a main coaching goal
            try:
                mainCoachingGoal = models.CoachingGoals.objects.get(leader=leader).coach_goal
            except:
                mainCoachingGoal = "No coaching goal selected."

            #Check if leader has already been assigned to a coach
            try:
                assignedTo = models.LeaderToCoach.objects.get(leader=leader).coach.username.username
            except:
                assignedTo = "Not assigned to any coach yet."
            leaderInfoDict = {"age":age, "gender":gender, "industry":industry, "company":company, "day":day, "years_of_employment": years_of_employment, "months_in_role": months_in_role, "mainCoachingGoal":mainCoachingGoal, "assignedTo":assignedTo, "subscription_type":subscription_type}
            leadersJson = {"username": leader.username.username, "info": leaderInfoDict}
            allLeaders.append(leadersJson)
    except:
        return HttpResponse("Something went wrong formatting return post data for leaders in admin page")

    #Format post return data for all coaches information
    # [ {"leaderUsername":  [{age: ..., gender: ..., about: ..., coachGoal1: "...", coachGoal2: "...", coachGoal3: "...", day: ... , "numberOfCoachees": ...}] } etc...  ]
    try:
        allCoaches = []
        coaches = models.Coach.objects.all()
        for coach in coaches:

            if coach.age == 0:
                age = "No age specified"
            else:
                age = coach.age

            if coach.gender == "":
                gender = "No gender specified"
            else:
                gender = coach.gender

            if coach.about == "":
                about = "Coach's about info not specified"
            else:
                about = coach.about

            if coach.industry == "":
                industry = "Coach's industry not specified"
            else:
                industry = coach.industry

            if coach.highest_edu_qualification == "":
                highest_edu_qualification = "Coach's highest qualification not specified"
            else:
                highest_edu_qualification = coach.highest_edu_qualification

            if coach.years_of_practice == -1:
                years_of_practice = "Coach's years of practice not specified"
            elif coach.years_of_practice == 0:
                years_of_practice = "Coach's years of practice less than a year"
            else:
                years_of_practice = coach.years_of_practice

            if coach.day == "":
                day = "Available days not specified"
            else:
                day = coach.day

            coach_goal_1 = coach.coach_goal_1
            coach_goal_2 = coach.coach_goal_2
            coach_goal_3 = coach.coach_goal_3
            numberOfCoachees = 0

            #Check the number of coachees this coach is coaching
            numberOfCoachees = len(models.LeaderToCoach.objects.filter(coach=coach))
            if numberOfCoachees == 0:
                numberOfCoachees = "No leaders assigned to this coach."

            coachInfoDict = {"age":age, "gender":gender, "about":about, "industry":industry, "highest_edu_qualification":highest_edu_qualification, "years_of_practice":years_of_practice, "day":day, "coach_goal_1":coach_goal_1, "coach_goal_2":coach_goal_2, "coach_goal_3":coach_goal_3, "numberOfCoachees":numberOfCoachees}
            coachJson = {"username":coach.username.username, "info": coachInfoDict}
            allCoaches.append(coachJson)
    except:
        return HttpResponse("Something went wrong formatting return post data for coaches in admin page")

    ret = {"leaders": allLeaders, "coaches": allCoaches}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


#Ability to delete a user
def removeExistingUser(request):
    data = json.load(request)
    # will be admin's username
    # username = data['username']
    usertype = data['usertype']
    # token = data['token']
    userToDelete = data['userToDelete']
    if usertype == 'A':
        try:
            #try and get user if it exists
            models.User.objects.get(username=userToDelete).delete()

        except:
            return HttpResponse("Something went wrong deleting user...")

        # if (token != getMd5(username)):
        #     return HttpResponse("Wrong token")

        ret = {"msg": "Successfully deleted user {}".format(userToDelete)}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    else:
        ret = {"msg": "Failed to delete user {}. User does not have admin privileges.".format(userToDelete)}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)


def getLeaderIsAssigned(request):
    data = json.load(request)
    username = data['username']

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
    except:
        ret = {"msg": "User not found"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    try:
        is_matched = leader.matched_with_coach
    except:
        ret = {"msg": "Unable to get leader.matched_with_coach"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    ret = {"msg": "isMatched found", "isMatched": is_matched}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


def getQuestionnaireCompleted(request):
    data = json.load(request)
    # will be leader or coach's username
    username = data['username']
    usertype = ""
    # token = data['token']
    completed = "False"

    try:
        #try and get user if it exists
        user = models.User.objects.get(username=username)
        usertype = user.usertype

        if usertype == "L":
            leader = models.Leader.objects.get(username=user)
            if leader.questionnaire_completed == "True":
                completed = "True"
        elif usertype == "C":
            coach = models.Coach.objects.get(username=user)
            if coach.questionnaire_completed == "True":
                completed = "True"
        elif usertype == "A":
            completed = "True"
    except:
        print("APWODPJAWJDPWHDPIHAWPIDHPWHD")
        return HttpResponse("Something went wrong getting questionnaire completion...")

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    print("completed variable is: " + completed)
    ret = {"completed": completed, "usertype":usertype}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

#leader's side for submitting feedback
def submitFeedback(request):
    data = json.load(request)
    # will be leader's username
    username = data['username']
    token = data['token']
    q1 = data['q1']
    q2 = data['q2']
    q3 = data['q3']
    q4 = data['q4']
    q5 = data['q5']
    q6 = data['q6']
    q7 = data['q7']
    q8 = data['q8']
    q9 = data['q9']
    q10 = data['q10']
    comment = data['comment']

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #get users and leaders and coaches
        leaderUser = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=leaderUser)

        coach = models.LeaderToCoach.objects.get(leader=leader).coach
        print(leader)
        print(coach)

    except:
        return HttpResponse("Something went wrong getting leader and coach objects...")

    #format all feedback into one string
    try:

        feedbackJson = json.dumps({"q1":q1, "q2":q2, "q3":q3, "q4":q4, "q5":q5, "q6":q6, "q7":q7, "q8":q8, "q9":q9, "q10":q10, "comment":comment})

        #Check for any previous Feedback row
        try:
            models.Feedback.objects.get(leader=leader).delete()
        except:
            print("Leader does not exist in Feedback table")

        try:
            models.Feedback.objects.create(leader=leader, coach=coach, feedback=feedbackJson)
            ret = {"Success": "Create complete."}
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)
        except:
            ret = {"Error": "Partial create model error."}
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)

    except:
        ret = {"Error": "Something went wrong with saving feedback to database."}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    print(feedbackJson)
    ret = {"Success": "Feedback is saved successfully!"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

#coach's side for viewing feedback
def getLeadersFeedback(request):
    data = json.load(request)
    # will be coach's username
    username = data['username']
    token = data['token']

    # if (token != getMd5(username)):
    #     return HttpResponse("Wrong token")

    try:
        #get users and leaders and coaches
        coachUser = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=coachUser)

        leaders = models.LeaderToCoach.objects.filter(coach=coach)
        print(leaders)
    except:
        return HttpResponse("Something went wrong getting leader and coach objects...")

    #get feedback of a specific leader
    #format will be
    #[ {"leaderUsername": ... , "feedback": {answers to all questions here q1, q2, q3 etc.} } } {"leaderUsername": ... , "feedback": ... } etc.]
    try:
        feedbackList = []
        for leader in leaders:
            print(leader.leader)
            try:
                feedbackObj = models.Feedback.objects.get(leader=leader.leader)
                print(feedbackObj)
                if feedbackObj.confirmed == "True":
                    print('hasjhajshahsjahsjhsjah')
                    feedbackPost = json.loads(feedbackObj.feedback)
                    feedbackPostList = []
                    feedbackPostList.append(feedbackPost)

                    # convert to australia timezone
                    au_tz = timezone('Australia/Sydney')
                    au_time = feedbackObj.time.astimezone(au_tz)
                    dateTime = au_time.strftime("%d/%m/%Y, %H:%M")
                    feedbackTimestamp = dateTime + " (UTC+11)"

                    try:
                        feedbackDict = {"leaderUsername": feedbackObj.leader.username.username, "feedback": feedbackPostList, "timestamp" : feedbackTimestamp}
                    except:
                        print(">>>>>")
                    feedbackList.append(feedbackDict)

                    print(feedbackDict)
            except:
                continue

    except:
        return HttpResponse("Something went wrong formatting feedback post data...")

    print(feedbackList)
    ret = feedbackList
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)



#admin's side for viewing feedback
def getAllFeedback(request):
    data = json.load(request)

    #get feedback of a specific leader
    #format will be
    #[ {"leaderUsername": ... , "feedback": {answers to all questions here q1, q2, q3 etc.} } } {"leaderUsername": ... , "feedback": ... } etc.]
    try:
        feedbackList = []
        feedbackObj = models.Feedback.objects.all()
        print(feedbackObj)
        for feedback in feedbackObj:
            if feedback.confirmed == "False":
                feedbackPost = json.loads(feedback.feedback)
                feedbackPostList = []
                feedbackPostList.append(feedbackPost)

                # convert to australia timezone
                au_tz = timezone('Australia/Sydney')
                au_time = feedback.time.astimezone(au_tz)
                dateTime = au_time.strftime("%d/%m/%Y, %H:%M")
                feedbackTimestamp = dateTime + " (UTC+11)"

                feedbackDict = {"leaderUsername": feedback.leader.username.username, "feedback": feedbackPostList, "timestamp": feedbackTimestamp}
                feedbackList.append(feedbackDict)

        print("feedbackList")
        print(feedbackList)
    except:
        return HttpResponse("Something went wrong formatting feedback post data...")

    ret = feedbackList
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

#admin confirm feedback
def confirmFeedback(request):
    data = json.load(request)

    leaderUsername = data['leaderUsername']
    comment = data['comment']
    print("comment is {}".format(comment))

    try:
        #get the leader
        leaderUser = models.User.objects.get(username=leaderUsername)
        leader = models.Leader.objects.get(username=leaderUser)

    except:
        return HttpResponse("Something went wrong when getting leader ")
    #confirm feedback of a specific leader
    try:
        feedbackObj = models.Feedback.objects.get(leader = leader)
        feedbackJson = json.loads(feedbackObj.feedback)
        feedbackJson['comment'] = comment
        feedbackObj.feedback = json.dumps(feedbackJson)
        feedbackObj.confirmed = "True"
        feedbackObj.save()
    except:
        return HttpResponse("Something went wrong when confirming")

    ret = {"Success":"The feedback is confirmed"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

#admin delete feedback (when feedback is not nice)
def deleteFeedback(request):
    data = json.load(request)
    leaderUsername = data['leaderUsername']

    try:
        #get the leader
        leaderUser = models.User.objects.get(username=leaderUsername)
        leader = models.Leader.objects.get(username=leaderUser)
    except:
        return HttpResponse("Something went wrong when getting leader ")
    #confirm feedback of a specific leader
    try:
        feedbackObj = models.Feedback.objects.get(leader = leader).delete()
    except:
        return HttpResponse("Something went wrong when deleting")

    ret = {"Success":"The feedback is deleted"}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)



# CALENDAR/SCHEDULING FUNCTIONS START HERE

# Getting what times are available and what times are booked
def getAvailableBookedTimes(request):
    data = json.load(request)
    username = data['username']

    # try:
    try:
        user = models.User.objects.get(username=username)
        if user.usertype == "L":
            leader = models.Leader.objects.get(username=user)
            coach = models.LeaderToCoach.objects.get(leader=leader)
            coach = coach.coach
        elif user.usertype== "C":
            coach = models.Coach.objects.get(username=user)
    except:
        return HttpResponse("wrong username, no leader, or no coach found")
    
    try:
        # note: this will be empty for leader. Format = {"time": ... , "date": ...}, {"time": ... , "date": ...}, ...)
        restrictedTimeArr = [] 
        availableTimeArr = []
        bookedTimeArr = []
        if user.usertype == "L":
            availableTimes = models.AvailableTimes.objects.filter(coach=coach)
            allBookedTimes = models.BookedTimes.objects.all()
            bookedTimes = allBookedTimes.filter(coach=coach)

            for available_time in availableTimes:
                jsonString = {"available_time":available_time.available_time, "date":available_time.date}
                availableTimeArr.append(jsonString)

            for booked_time in bookedTimes:
                jsonString = {"leader":booked_time.leader.username.username,"booked_time":booked_time.booked_time, "date":booked_time.date}
                bookedTimeArr.append(jsonString)
        
        elif user.usertype == "C":
            # get all available times from all coaches
            availableTimes = models.AvailableTimes.objects.all()
            for i in availableTimes:
                if i.coach != coach:
                    # available time belongs to another coach
                    # we do not need to display the name of the coach that has this timeslot available, so name will be omitted
                    jsonString = {"time": i.available_time, "date" : i.date}
                    restrictedTimeArr.append(jsonString) 

                else:
                    # available time belongs to coach
                    jsonString = {"available_time":i.available_time, "date":i.date}
                    availableTimeArr.append(jsonString) 

            # get booked times of leaders not assigned to coach
            bookedTimes = models.BookedTimes.objects.all()
            for j in bookedTimes:
                if j.coach != coach:
                    # booked time belongs to a leader linked to another coach
                    # we do not need to display the name of the leader that has this timeslot booked, so name will be omitted
                    jsonString = {"time": j.booked_time, "date" : j.date}
                    restrictedTimeArr.append(jsonString) 
                
                else: 
                    # booked time belongs to a leader linked to coach
                    jsonString = {"leader": j.leader.username.username, "booked_time": j.booked_time, "date": j.date}
                    bookedTimeArr.append(jsonString)


        # DEBUGGING
        # print(availableTimes)
        # print(bookedTimes)
    except:
        return HttpResponse("unable to get available times / booked times")
    # if len(bookedTimes) > 0:
    #     print("time is: " + availableTimes[0].available_time)
    #     print("date is: " + availableTimes[0].date)

    ret = {"restrictedTimes": restrictedTimeArr, 'availableTimes':availableTimeArr, "bookedTimes":bookedTimeArr}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Backend function for a leader booking a time
def leaderBookTime(request):
    data = json.load(request)
    username = data['username'] # will be the leader's username
    new_booked_time = data['booked_time']
    new_booked_full_date = data['booked_full_date']

    retMsg = ""
    try:
        leader = models.Leader.objects.get(username=models.User.objects.get(username=username))
        coach = models.LeaderToCoach.objects.get(leader=leader)

        # check if the time is available for booking
        allAvailableTimes = models.AvailableTimes.objects.all()
        try:
            allAvailableTimes.filter(coach=coach.coach, available_time=new_booked_time, date=new_booked_full_date)
        except:
            retMsg = "This is timeslot is not available for booking."
            ret = {'msg':retMsg}
            # print(ret)
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)

        # get all booked times the leader has already made
        allBookedTimes = models.BookedTimes.objects.all()
        booked_times = allBookedTimes.filter(leader=leader)

        # for all booked times the leader has, keep track of the week number that they're in
        booked_weeks = []
        for booked_time_obj in booked_times:
            # isocalendar usage: date(2010, 6, 16).isocalendar()[1]
            date_breakdown = booked_time_obj.date.split()

            # booked_week = date(booked_time_obj.date).isocalendar()[1]
            booked_week = date(int(date_breakdown[3]), int(months.index(date_breakdown[2]) + 1), int(date_breakdown[1])).isocalendar()[1]
            booked_weeks.append(booked_week)

        # new date breakdown
        ndb = new_booked_full_date.split()
        meeting_link = ""
        meeting_id = ""
        meeting_password = ""
        # if the new booking is not in the same week as any other previous booking then booking successful
        # else, return an error msg string to be flashed on the frontend
        if date(int(ndb[3]), int(months.index(ndb[2]) + 1), int(ndb[1])).isocalendar()[1] not in booked_weeks:
            retMsg = "Booking successful!"
            # hardcode: models.BookedTimes.objects.create(booked_time="12:00pm-1:00pm", date="Fri, 14 January 2022")
            models.BookedTimes.objects.create(leader=leader, coach=coach.coach, booked_time=new_booked_time, date=new_booked_full_date)
            allAvailableTimes.filter(coach=coach.coach, available_time=new_booked_time, date=new_booked_full_date).delete()
            try:
                # Delete all previous existing rows for a given leader and coach
                models.MeetingLinks.objects.filter(leader=leader).delete()
                # Generate the zoom meeting object
                zoom_obj = createMeeting('')
                print(zoom_obj)
                meeting_link = zoom_obj['meetingLink']
                meeting_id = zoom_obj['meetingId']
                meeting_password = zoom_obj['meetingPassword']
                print(meeting_link)
                print(meeting_id)
                print(meeting_password)

                # Create a new row for a given leader and coach and save the zoom meeting link
                models.MeetingLinks.objects.create(leader=leader, coach=coach.coach, meeting_link=meeting_link, meeting_id=meeting_id, meeting_password=meeting_password)
            except:
                print("EXCEPTION")
        else:
            retMsg = "Cannot book more than one session in a week. \n\
            Please remove your current session for the week or book a session in a different week."
            ret = {'msg':retMsg}
            # print(ret)
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)


    except:
        return HttpResponse("Something went wrong in leaderBookTime")
    # except:
        # return HttpResponse("Leader booking post not successful")

    try:
        # might have some error
        allBookedTimes = models.AvailableTimes.objects.all()
        allBookedTimes.filter(coach=coach.coach, available_time = new_booked_time, date = new_booked_full_date).delete()
        # feedbackObj = models.Feedback.objects.get(leader = leader).delete()
    except:
        return HttpResponse("Something went wrong when deleting available time after booking")

    # return HttpResponse("Leader post successful")
    ret = {'msg':retMsg, "meetingLink":meeting_link, "meeting_id":meeting_id, "meetingPassword":meeting_password}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Mimicking the leader removing a booked time
def leaderRemoveBooking(request):
    data = json.load(request)
    username = data['username'] # will be the leader's username
    new_booked_time = data['booked_time']
    new_booked_full_date = data['booked_full_date']
    try:
        leader = models.Leader.objects.get(username=models.User.objects.get(username=username))
        coach = models.LeaderToCoach.objects.get(leader=leader)
        models.BookedTimes.objects.get(leader=leader, coach=coach.coach, booked_time=new_booked_time, date=new_booked_full_date).delete()
    except:
        return HttpResponse("Leader cannot remove booking")

    try:
        models.AvailableTimes.objects.create(coach=coach.coach, available_time=new_booked_time, date=new_booked_full_date)
    except:
        return HttpResponse("Something went wrong when creating available time object")
    
    ret = {'msg':"Successfully removed booking"}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Function for getting leader's zoom link details
def leaderGetZoomDetails(request):
    data = json.load(request)
    username = data['username'] # will be leader's username

    try:
        user = models.User.objects.get(username=username)
        leader = models.Leader.objects.get(username=user)
    except:
        ret = {'msg':"Leader does not exist"}
        print(ret)
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    # Get the earliest booking date
    all_leader_bookings = models.BookedTimes.objects.filter(leader=leader)

    if len(all_leader_bookings) == 0:
        ret = {'msg':"No upcoming bookings"}
        print(ret)
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    booking_date_list = []

    for i in all_leader_bookings:
        date_split = i.date.split()
        new_date = datetime(int(date_split[3]), int(months.index(date_split[2])) + 1, int(date_split[1]))
        booking_date_list.append(new_date)
    
    #  get earliest booking date 
    earliest_booking = min(booking_date_list)

    time_time = ""

    # get the time {9:00AM - 10:00AM}
    for i in all_leader_bookings:
        date_split = i.date.split()
        new_date = datetime(int(date_split[3]), int(months.index(date_split[2])) + 1, int(date_split[1]))
        if earliest_booking == new_date:
            time_time = i.booked_time
            break

    # format into string
    earliest_booking_date = time_time + " " + earliest_booking.strftime("%A") + ", " + earliest_booking.strftime("%d") + " " + earliest_booking.strftime("%b") + " " + earliest_booking.strftime("%Y")
    print(earliest_booking_date)

    #  Get zoom link details
    try:
        zoom_obj = models.MeetingLinks.objects.get(leader=leader)
    except:
        #  no MeetingLinks object found linked to leader
        ret = {'msg':"Leader does not have a zoom link"}
        print(ret)
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    # Get link, id, and password
    meeting_link = zoom_obj.meeting_link
    meeting_id = zoom_obj.meeting_id
    meeting_password = zoom_obj.meeting_password

    ret = {'msg':"Upcoming booking", "meetingLink":meeting_link, "meetingId":meeting_id, "meetingPassword":meeting_password, "earliestBookingDate": earliest_booking_date}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Function for getting coach's zoom link details for each leader
def coachGetZoomDetails(request):
    data = json.load(request)
    username = data['username'] # will be coach's username

    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
    except:
        ret = {'msg':"Coach does not exist"}
        print(ret)
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    # Get all leader to coach objects
    leaders = []

    leader_to_coach = models.LeaderToCoach.objects.filter(coach=coach)
    for i in leader_to_coach:
        leaders.append(i.leader)
        print(i)

    # format: [{"leader": leader_name, "meetingLink":meeting_link, "meetingId":meeting_id, "meetingPassword":meeting_password, "earliestBookingDate": earliest_booking_date}, {...}, ...]
    coach_sessions = []

    # Append earliest time for each leader. If leader has no booking, msg will be 'No upcoming bookings'
    for leader in leaders:

        # Get the earliest booking date
        all_leader_bookings = models.BookedTimes.objects.filter(leader=leader)

        if len(all_leader_bookings) == 0:
            dictionary = {'msg':"No upcoming bookings", "leader": leader.username.username}
            coach_sessions.append(dictionary)
            continue

        booking_date_list = []

        for i in all_leader_bookings:
            date_split = i.date.split()
            new_date = datetime(int(date_split[3]), int(months.index(date_split[2])) + 1, int(date_split[1]))
            booking_date_list.append(new_date)
    
        #  get earliest booking date 
        earliest_booking = min(booking_date_list)

        time_time = ""

        # get the time {9:00AM - 10:00AM}
        for i in all_leader_bookings:
            date_split = i.date.split()
            new_date = datetime(int(date_split[3]), int(months.index(date_split[2])) + 1, int(date_split[1]))
            if earliest_booking == new_date:
                time_time = i.booked_time
                break

        # format into string
        earliest_booking_date = time_time + " " + earliest_booking.strftime("%A") + ", " + earliest_booking.strftime("%d") + " " + earliest_booking.strftime("%b") + " " + earliest_booking.strftime("%Y")
        print(earliest_booking_date)

        #  Get zoom link details
        try:
            zoom_obj = None
            found_link = False
            all_zoom_obj = models.MeetingLinks.objects.all()
            for zooms in all_zoom_obj:
                if zooms.leader.username.username == leader.username.username:
                    zoom_obj = zooms
                    found_link = True
                    break
                    
            if (found_link != True):
                dictionary = {'msg':"Leader does not have a zoom link", "leader": leader.username.username}
                coach_sessions.append(dictionary)
                continue

        except:
            #  no MeetingLinks object found linked to leader
            ret = {'msg':"error in finding zoom link"}
            print(ret)
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)

        # Get link, id, and password
        meeting_link = zoom_obj.meeting_link
        meeting_id = zoom_obj.meeting_id
        meeting_password = zoom_obj.meeting_password

        # Append leader's zoom link details to coach_sessions
        dictionary = {'msg':"Upcoming booking", "leader": leader.username.username, "meetingLink":meeting_link, "meetingId":meeting_id, "meetingPassword":meeting_password, "earliestBookingDate": earliest_booking_date}
        coach_sessions.append(dictionary)

    ret = {"coachSessions": coach_sessions}
    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Function for a coach selecting an available time slot
def coachChooseAvailableTime(request):
    data = json.load(request)
    username = data['username'] # will be coach's username
    new_available_time = data['available_time']
    new_available_full_date = data['available_full_date']
    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
    except:
        return HttpResponse("Coach not found")

    try:
        # models.PreselectedTimes.objects.create(available_time="12:00pm-1:00pm", date="Fri, 14 January 2022")
        # models.PreselectedTimes.objects.create(available_time="1:00pm-2:00pm", date="Sat, 15 January 2022")
        # models.PreselectedTimes.objects.create(available_time="2:00pm-3:00pm", date="Sun, 16 January 2022")
        models.AvailableTimes.objects.create(coach=coach, available_time=new_available_time, date=new_available_full_date)
    except:
        return HttpResponse("Coach add available time not successful")
    return HttpResponse("Coach add available time successful")

# Function for a coach removing a selected available time slot
def coachRemoveAvailableTime(request):
    data = json.load(request)
    username = data['username'] # will be coach's username
    new_available_time = data['available_time']
    new_available_full_date = data['available_full_date']
    try:
        user = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=user)
        # models.PreselectedTimes.objects.create(available_time="12:00pm-1:00pm", date="Fri, 14 January 2022")
        # models.PreselectedTimes.objects.create(available_time="1:00pm-2:00pm", date="Sat, 15 January 2022")
        # models.PreselectedTimes.objects.create(available_time="2:00pm-3:00pm", date="Sun, 16 January 2022")
        models.AvailableTimes.objects.get(coach=coach, available_time = new_available_time, date = new_available_full_date).delete()
    except:
        return HttpResponse("Coach remove available time slot unsuccessful")
    return HttpResponse("Coach remove available time slot successful")

# Function for leader / coach to get a list of associated users who will be affected by a calendar update
# Note: Leader / coach who initiated the update will not be appended to the list
def getUsersForUpdate(request):
    data = json.load(request)
    username = data['username'] 
    userType = data['userType']

    # userList format example: ['leader1', 'coach5', 'leader7']
    userList = []

    if userType == 'C':
        print('a coach user')
        try:
            coachUser = models.User.objects.get(username=username)
            coachh = models.Coach.objects.get(username=coachUser)

            # get all coach users except for self
            allCoaches = models.Coach.objects.all()

            # append to userList
            for i in allCoaches:
                if i.username.username != username:
                    userList.append(i.username.username)

        except:
            ret = {'userList': 'getUsersForUpdate error - coach user does not exist.'}
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)

        # Get all coach's leaders
        try:
            leaders = models.LeaderToCoach.objects.filter(coach=coachh)
        except:
            ret = {'userList': 'getUsersForUpdate error - coach user not linked to a leader.'}
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)

        # Append to list
        for leader in leaders:
            userList.append(leader.leader.username.username)

    elif userType == 'L':
        print('a leader user')

        try:

            coach = None

            allLeaderToCoach = models.LeaderToCoach.objects.all()
            for i in allLeaderToCoach:
                if (i.leader.username.username == username):

                    coach = i.coach

                    # Append coach name to list
                    userList.append(i.coach.username.username)

        except:
            ret = {'userList': 'getUsersForUpdate error - leader is not linked to a coach.'}
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)

        # Get leaders (excluding posted leader)
        try:

            for i in allLeaderToCoach:
                if i.leader.username.username == username:
                    continue 

                if i.coach.username.username == coach.username.username:
                    userList.append(i.leader.username.username)

        except:
            ret = {'userList': 'getUsersForUpdate error - no associated leaders with coach.'}
            ret = json.dumps(ret, ensure_ascii=False)
            return HttpResponse(ret)

    else:
        ret = {'userList': 'getUsersForUpdate error - userType is not a coach or leader.'}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)

    ret = {'userList': userList}
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Deletes all data inside the PreselectedTimes and BookedTimes tables
def scheduletestRESET(request):
    try:
        models.BookedTimes.objects.all().delete()
        models.PreselectedTimes.objects.all().delete()
    except:
        return HttpResponse("Reset not successful")
    return HttpResponse("reset successful")

# CALENDAR/SCHEDULING FUNCTIONS END HERE

# Gets current count from table
def countertestGET(request):
    try:
        # models.CounterTest.objects.all().delete()
        # models.CounterTest.objects.create(count=1)
        count = models.CounterTest.objects.all()[0]
    except:
        return HttpResponse("Something went wrong getting count")

    ret = {'count':count.count}

    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)

# Returns current count + 1
def countertestPOST(request):
    try:
        count = models.CounterTest.objects.all()[0]
        count.count += 1
        count.save()
    except:
        return HttpResponse("Something went wrong getting count")
        
    ret = {'count':count.count}

    print(ret)
    ret = json.dumps(ret, ensure_ascii=False)
    return HttpResponse(ret)


# TEMP END

def getEOICoach(request):
    data = json.load(request)

    try:
        #get users and leaders and coaches
        EOICoaches = models.EOICoach.objects.all()

    except:
        return HttpResponse("EOI Coach does not exist")

    try:
        #format leader information
        # [ {"leaderUsername":  [{age: ..., gender: ..., industry: ..., days: "...", mainCoachingGoal: "...", assignedTo: ... }] } etc...  ]
        allEOICoaches = []
        for eoiCoach in EOICoaches:    
            eoiCoachInfoDict = {"name":eoiCoach.name, "email":eoiCoach.email, "message":eoiCoach.message}
            allEOICoaches.append(eoiCoachInfoDict)
        print(allEOICoaches[0])
    except:
        return HttpResponse("Something went wrong formatting return post data for eoi coach in admin page")

    ret = {"eoiCoach": allEOICoaches}
    ret = json.dumps(ret, ensure_ascii=False)
    print("it works!")
    print(ret)
    return HttpResponse(ret)

def setEOICoach(request):
    data = json.load(request)
    name = data['name']
    message = data['message']
    email = data['email']
    

    # should have check for these elements
    try:
        models.EOICoach.objects.get(name=name,message=message,email=email)
        ret = {"out":"Email and name already used"}
        ret = json.dumps(ret, ensure_ascii=False)
        return HttpResponse(ret)
    except:
        pass
    try:
        models.EOICoach.objects.create(name=name,message=message,email=email)
    except:
        ret = {"out":"Unknow error happened"}
        ret = json.dumps(ret, ensure_ascii=False)
        #DEBUGGING
        print(ret)
        return HttpResponse(ret)
    ret = {"out":"Successfully registered interest"}
    ret = json.dumps(ret, ensure_ascii=False)
    print(ret)
    return HttpResponse(ret)

def deleteEOICoach(request):
    data = json.load(request)
    email = data['email']
    message = data['message']
    name = data['name']
   
    try:
        models.EOICoach.objects.get(name=name,message=message,email=email).delete()
    except:
        ret = {"out":"Unknow error happened"}
        ret = json.dumps(ret, ensure_ascii=False)
        #DEBUGGING
        print(ret)
        return HttpResponse(ret)
    ret = {"out":"Successfully deleted"}
    ret = json.dumps(ret, ensure_ascii=False)
    print(ret)
    return HttpResponse(ret)

def getLeaderToCoach(request):
    data = json.load(request)
    username = data['username']
    token = data['token']

    try:
        coachUser = models.User.objects.get(username=username)
        coach = models.Coach.objects.get(username=coachUser)
        leaders = models.LeaderToCoach.objects.filter(coach=coach)
    except:
        # print("User/leader does not exist")
        return HttpResponse("User/Coach does not exist")

    try:
        allLeaders=[]
        for leader in leaders:
            jsonString = {"username":leader.leader.username.username}
            allLeaders.append(jsonString)
    except:
        return HttpResponse("Something went wrong")
    
    ret = {"leaders": allLeaders}
    ret = json.dumps(ret, ensure_ascii=False)
    print("it works!")
    print(ret)
    return HttpResponse(ret)

    
def generateZoomToken():
    token = jwt.encode(
 
        # Create a payload of the token containing
        # API Key & expiration time
        {'iss': API_KEY, 'exp': time() + 5000},
 
        # Secret used to generate token signature
        API_SEC,
 
        # Specify the hashing alg
        algorithm='HS256'
    )
    return token.decode('utf-8')

meetingdetails = {"topic": "Coaching Session",
                  "type": 3, # Type 3 is a recurring meeting with no fixed time
                  "start_time": "2019-06-14T10: 21: 57",
                  "duration": "45",
                  "timezone": "Europe/Madrid",
                  "agenda": "test",
 
                #   "recurrence": {"type": 1,
                #                  "repeat_interval": 1
                #                  },
                  "settings": {"host_video": "true",
                               "participant_video": "true",
                               "join_before_host": "True",
                               "mute_upon_entry": "False",
                               "watermark": "true",
                               "audio": "voip",
                               "auto_recording": "cloud"
                               }
                  }

def createMeeting(request):
    headers = {'authorization': 'Bearer ' + generateZoomToken(),
               'content-type': 'application/json'}
    api_req = requests.post(
        f'https://api.zoom.us/v2/users/me/meetings',
        headers=headers, data=json.dumps(meetingdetails))
 
    print("\n creating zoom meeting ... \n")
    # DEBUGGING: print(r.text)

    # converting the output into json and extracting the details
    api_response = json.loads(api_req.text)

    join_url = api_response["join_url"]
    meeting_id = api_response["id"]
    meeting_password = api_response["password"]
 
    print("\n Your zoom meeting link is: {join_url}\n\
        Your zoom meeting id is: {meeting_id} \n\
        Your password is: {meeting_password}".format(
            join_url = join_url,
            meeting_id = meeting_id,
            meeting_password = meeting_password
        ))

    # ret = {"out":" here is your zoom meeting link {join_URL} and your password: {meetingPassword}".format(join_URL=join_URL,meetingPassword=meetingPassword)}
    ret = {"meetingLink":join_url, "meetingId":meeting_id, "meetingPassword":meeting_password}
    # ret = json.dumps(ret, ensure_ascii=False)
    print(ret)
    return ret
    # return HttpResponse(ret)


 
 

zoom_role = 0
# Zoom SDK Functions

# Only need to post the meeting number, api, secret key and role (0 for joining) are saved
def generateSignature(request):
    data = json.load(request)
    ts = int(round(time() * 1000)) - 30000;
    msg = data['apiKey'] + str(data['meetingNumber']) + str(ts) + str(data['role']);    
    message = base64.b64encode(bytes(msg, 'utf-8'));
    # COMMENT: to see the message? message = message.decode("utf-8");    
    secret = bytes(data['apiSecret'], 'utf-8')
    hash = hmac.new(secret, message, hashlib.sha256);
    hash =  base64.b64encode(hash.digest());
    hash = hash.decode("utf-8");
    tmpString = "%s.%s.%s.%s.%s" % (data['apiKey'], str(data['meetingNumber']), str(ts), str(data['role']), hash);
    signature = base64.b64encode(bytes(tmpString, "utf-8"));
    signature = signature.decode("utf-8");

    ret = {"ret":signature.rstrip("=")}
    ret = json.dumps(ret, ensure_ascii=False)
    print(ret)
    return HttpResponse(ret)