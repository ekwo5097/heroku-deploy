from django.test import TestCase
from emerge.models import *
from emerge.views import *
from django.urls import reverse
from emerge import models
import json
from django.test import Client


class CreateUserTest(TestCase):
    #Create a User row
    def setUp(self):
        models.User.objects.create(username="test1", password="test1ert", email="test@email.com", phone_number="1234", usertype="L")

    def testUserCreated(self):
        test1 = models.User.objects.get(username="test1")
        self.assertTrue(models.User.objects.filter(username="test1").exists())
        self.assertFalse(models.User.objects.filter(username="test2").exists())
        self.assertEquals(test1.password,"test1ert")
        test1.delete()
        self.assertFalse(models.User.objects.filter(username="test1").exists())


class CreateLeaderUserTest(TestCase):
    #Create a leader user
    def setUp(self):
        user = models.User.objects.create(username="test1", password="test1", email="test@email.com", phone_number="1234", usertype="L")
        models.Leader.objects.create(username=user, name="test1", age="0", gender="test1", industry="test1", day="Monday", subscription_type="0")

    def testLeaderUserCreated(self):
        test1 = models.Leader.objects.get(username="test1")
        self.assertTrue(models.Leader.objects.filter(username="test1").exists())
        self.assertFalse(models.Leader.objects.filter(username="test2").exists())
        test1.delete()
        self.assertFalse(models.Leader.objects.filter(username="test1").exists())


class CreateCoachUserTest(TestCase):
        #Create a coach user
    def setUp(self):
        user = models.User.objects.create(
            username="test1",
            password="test1",
            email="test@email.com",
            phone_number="1234",
            usertype="C")

        models.Coach.objects.create(
            username=user, name="test1",
            age="0", gender="test1",
            about="test1",
            coach_goal_1="0",
            coach_goal_2="0",
            coach_goal_3="0",
            day="Monday")

    def testCoachUserCreated(self):
        test1 = models.Coach.objects.get(username="test1")
        self.assertTrue(models.Coach.objects.filter(username="test1").exists())
        self.assertFalse(models.Coach.objects.filter(username="test2").exists())
        test1.delete()
        self.assertFalse(models.Coach.objects.filter(username="test1").exists())

class CreateLeaderToCoachTest(TestCase):
        #Create a coach user
    def setUp(self):
        coachUser = models.User.objects.create(
            username="testcoach",
            password="testcoach",
            email="testcoach@email.com",
            phone_number="1234",
            usertype="C")

        coach = models.Coach.objects.create(
            username=coachUser, name="testcoach",
            age="0", gender="testcoach",
            about="testcoach",
            coach_goal_1="0",
            coach_goal_2="0",
            coach_goal_3="0",
            day="Monday")

        leaderUser = models.User.objects.create(
            username="testleader",
            password="testleader",
            email="testleader@email.com",
            phone_number="1234",
            usertype="L")

        leader = models.Leader.objects.create(
            username=leaderUser,
            name="testleader",
            age="0",
            gender="testleader",
            industry="testleader",
            day="Monday",
            subscription_type="0")

        models.LeaderToCoach.objects.create(
            leader = leader,
            coach = coach
        )

    def testLeaderToCoachCreated(self):
        leader = models.Leader.objects.get(username="testleader")
        coach = models.Coach.objects.get(username="testcoach")

        test1 = models.LeaderToCoach.objects.get(leader=leader)
        test2 = models.LeaderToCoach.objects.get(coach=coach)
        self.assertEquals(test1, test2)


        self.assertTrue(models.LeaderToCoach.objects.filter(leader=leader).exists())
        self.assertTrue(models.LeaderToCoach.objects.filter(coach=coach).exists())

        test1.delete()

        self.assertFalse(models.LeaderToCoach.objects.filter(leader=leader).exists())
        self.assertFalse(models.LeaderToCoach.objects.filter(coach=coach).exists())


class CreateCoachingGoalTest(TestCase):
    #Create a leader user and a coaching goal row
    def setUp(self):
        user = models.User.objects.create(username="test1", password="test1", email="test@email.com", phone_number="1234", usertype="L")
        leader = models.Leader.objects.create(username=user, name="test1", age="0", gender="test1", industry="test1", day="Monday", subscription_type="0")
        models.CoachingGoals.objects.create(leader=leader, coach_goal='Dealing with procrastination')

    def testCoachingGoalCreated(self):
        testUser = models.User.objects.get(username="test1")
        testLeader = models.Leader.objects.get(username=testUser)
        testGoal = models.CoachingGoals.objects.get(leader=testLeader)
        #self.assertTrue(testGoal.exists())
        self.assertEquals(testGoal.coach_goal, 'Dealing with procrastination')
        testGoal.delete()
        self.assertFalse(models.CoachingGoals.objects.filter(leader=testLeader).exists())

class CreateSubGoalsTest(TestCase):
    #Create a leader user and coaching goal and subgoals for the leader
    def setUp(self):
        coachUser = models.User.objects.create(
            username="testcoach",
            password="testcoach",
            email="testcoach@email.com",
            phone_number="1234",
            usertype="C")

        coach = models.Coach.objects.create(
            username=coachUser, name="testcoach",
            age="0", gender="testcoach",
            about="testcoach",
            coach_goal_1="0",
            coach_goal_2="0",
            coach_goal_3="0",
            day="Monday")

        leaderUser = models.User.objects.create(
            username="testleader",
            password="testleader",
            email="testleader@email.com",
            phone_number="1234",
            usertype="L")

        leader = models.Leader.objects.create(
            username=leaderUser,
            name="testleader",
            age="0",
            gender="testleader",
            industry="testleader",
            day="Monday",
            subscription_type="0")

        models.LeaderToCoach.objects.create(
            leader = leader,
            coach = coach
        )
        coach_goal = models.CoachingGoals.objects.create(leader=leader, coach_goal='Dealing with procrastination')
        models.SubGoals.objects.create(leader=leader, coach=coach, coach_goal=coach_goal, sub_goal='Procrastination sub goal')

    def testSubGoalsCreated(self):
        testUser = models.User.objects.get(username="testleader")
        testLeader = models.Leader.objects.get(username=testUser)
        testSubGoal = models.SubGoals.objects.get(leader=testLeader)
        #self.assertTrue(testSubGoal[0].exists())
        #self.assertTrue(len(testSubGoal) == 1)
        self.assertEquals(testSubGoal.sub_goal, 'Procrastination sub goal')
        testSubGoal.delete()
        self.assertTrue(len(models.SubGoals.objects.filter(leader=testLeader)) == 0)

class CreateActionsTest(TestCase):
    #Create a leader user and coaching goal and subgoals for the leader
    def setUp(self):
        coachUser = models.User.objects.create(
            username="testcoach",
            password="testcoach",
            email="testcoach@email.com",
            phone_number="1234",
            usertype="C")

        coach = models.Coach.objects.create(
            username=coachUser, name="testcoach",
            age="0", gender="testcoach",
            about="testcoach",
            coach_goal_1="0",
            coach_goal_2="0",
            coach_goal_3="0",
            day="Monday")

        leaderUser = models.User.objects.create(
            username="testleader",
            password="testleader",
            email="testleader@email.com",
            phone_number="1234",
            usertype="L")

        leader = models.Leader.objects.create(
            username=leaderUser,
            name="testleader",
            age="0",
            gender="testleader",
            industry="testleader",
            day="Monday",
            subscription_type="0")

        models.LeaderToCoach.objects.create(
            leader = leader,
            coach = coach
        )
        coach_goal = models.CoachingGoals.objects.create(leader=leader, coach_goal='Dealing with procrastination')
        sub_goal = models.SubGoals.objects.create(leader=leader, coach=coach, coach_goal=coach_goal, sub_goal='Procrastination sub goal')
        models.Actions.objects.create(leader=leader, coach=coach, sub_goal=sub_goal, action='Procrastination Action', score=1)

    def testActionsCreated(self):
        testUser = models.User.objects.get(username="testleader")
        testLeader = models.Leader.objects.get(username=testUser)
        testAction = models.Actions.objects.filter(leader=testLeader)
        #self.assertTrue(testAction[0].exists())
        self.assertTrue(len(testAction) == 1)
        self.assertEquals(testAction[0].action, 'Procrastination Action')
        testAction.delete()
        self.assertTrue(len(models.Actions.objects.filter(leader=testLeader)) == 0)

class CreateMessageTest(TestCase):

    def setUp(self):
        coachUser = models.User.objects.create(
            username="testcoach",
            password="testcoach",
            email="testcoach@email.com",
            phone_number="1234",
            usertype="C")

        coach = models.Coach.objects.create(
            username=coachUser, name="testcoach",
            age="0", gender="testcoach",
            about="testcoach",
            coach_goal_1="0",
            coach_goal_2="0",
            coach_goal_3="0",
            day="Monday")

        leaderUser = models.User.objects.create(
            username="testleader",
            password="testleader",
            email="testleader@email.com",
            phone_number="1234",
            usertype="L")

        leader = models.Leader.objects.create(
            username=leaderUser,
            name="testleader",
            age="0",
            gender="testleader",
            industry="testleader",
            day="Monday",
            subscription_type="0")

        models.LeaderToCoach.objects.create(
            leader = leader,
            coach = coach
        )


    def messageCreated(self):
        leader1 = models.Leader.objects.get(username="testleader")
        coach1 = models.Coach.objects.get(username="testcoach")

        models.Message.objects.create(
            leader=leader1,
            coach=coach1,
            sender="L",
            message="hi coach"
            )

        self.assertTrue(models.Message.objects.filter(leader=leader1,coach=coach1).exists())

        message1= models.Message.objects.get(leader=leader1,coach=coach1)
        message1.delete()
        self.assertFalse(models.Message.objects.filter(leader=leader1,coach=coach1).exists())


class CreateSchedulingTest(TestCase):

    def setUp(self):
        coachUser = models.User.objects.create(
            username="testcoach",
            password="testcoach",
            email="testcoach@email.com",
            phone_number="1234",
            usertype="C")

        coach = models.Coach.objects.create(
            username=coachUser, name="testcoach",
            age="0", gender="testcoach",
            about="testcoach",
            coach_goal_1="0",
            coach_goal_2="0",
            coach_goal_3="0",
            day="Monday")

        leaderUser = models.User.objects.create(
            username="testleader",
            password="testleader",
            email="testleader@email.com",
            phone_number="1234",
            usertype="L")

        leader = models.Leader.objects.create(
            username=leaderUser,
            name="testleader",
            age="0",
            gender="testleader",
            industry="testleader",
            day="Monday",
            subscription_type="0")

        models.LeaderToCoach.objects.create(
            leader = leader,
            coach = coach
        )


    def schedulingCreated(self):
        leader = models.User.objects.get(username="testleader")
        leader1 = models.Leader.objects.get(username=leader)
        coach = models.User.objects.get(username="testcoach")
        coach1 = models.Coach.objects.get(username=coach)

        models.Scheduling.objects.create(
            leader=leader1,
            coach=coach1,
            date="18",
            month="11",
            year="2021",
            time="9:00am-10:00am"
            )

        self.assertTrue(models.Scheduling.objects.filter(leader=leader1,coach=coach1).exists())

        scheduling= models.Scheduling.objects.get(leader=leader1,coach=coach1)
        scheduling.delete()
        self.assertFalse(models.Scheduling.objects.filter(leader=leader1,coach=coach1).exists())

class CreateNextScheduleTest(TestCase):

    def setUp(self):
        coachUser = models.User.objects.create(
            username="testcoach",
            password="testcoach",
            email="testcoach@email.com",
            phone_number="1234",
            usertype="C")

        coach = models.Coach.objects.create(
            username=coachUser, name="testcoach",
            age="0", gender="testcoach",
            about="testcoach",
            coach_goal_1="0",
            coach_goal_2="0",
            coach_goal_3="0",
            day="Monday")

        leaderUser = models.User.objects.create(
            username="testleader",
            password="testleader",
            email="testleader@email.com",
            phone_number="1234",
            usertype="L")

        leader = models.Leader.objects.create(
            username=leaderUser,
            name="testleader",
            age="0",
            gender="testleader",
            industry="testleader",
            day="Monday",
            subscription_type="0")

        models.LeaderToCoach.objects.create(
            leader = leader,
            coach = coach
        )


    def schedulingCreated(self):
        leader = models.User.objects.get(username="testleader")
        leader1 = models.Leader.objects.get(username=leader)
        coach = models.User.objects.get(username="testcoach")
        coach1 = models.Coach.objects.get(username=coach)


        models.NextSchedule.objects.create(
            leader=leader1,
            coach=coach1,
            date="18",
            month="11",
            year="2021",
            time="9:00am-10:00am"
            )

        self.assertTrue(models.NextSchedule.objects.filter(leader=leader1,coach=coach1).exists())

        scheduling= models.NextSchedule.objects.get(leader=leader1,coach=coach1)
        scheduling.delete()
        self.assertFalse(models.NextSchedule.objects.filter(leader=leader1,coach=coach1).exists())

class CreateNextScheduleTest(TestCase):

    def setUp(self):
        leaderUser = models.User.objects.create(
            username="testleader",
            password="testleader",
            email="testleader@email.com",
            phone_number="1234",
            usertype="L")

        leader = models.Leader.objects.create(
            username=leaderUser,
            name="testleader",
            age="0",
            gender="testleader",
            industry="testleader",
            day="Monday",
            subscription_type="0")


    def schedulingCreated(self):
        leader = models.User.objects.get(username="testleader")
        leader1 = models.Leader.objects.get(username=leader)

        models.AvailableDays.objects.create(
            leader=leader1,
            day = "2"
        )
        models.AvailableDays.objects.create(
            leader=leader1,
            day = "4"
        )

        self.assertTrue(models.AvailableDays.objects.filter(leader=leader1,coach=coach1).exists())
        models.AvailableDays.objects.filter(leader=leader1,coach=coach1).delete()
        self.assertFalse(models.AvailableDays.objects.filter(leader=leader1,coach=coach1).exists())
