from django.contrib import admin
from .models import *

admin.site.register(User)
admin.site.register(Leader)
admin.site.register(Coach)
admin.site.register(LeaderToCoach)
admin.site.register(Scheduling)
admin.site.register(NextSchedule)
admin.site.register(AvailableDays)
admin.site.register(Homework)
admin.site.register(CoachingGoals)
admin.site.register(Actions)
admin.site.register(Message)
admin.site.register(Feedback)
admin.site.register(AvailableTimes)
admin.site.register(BookedTimes)
admin.site.register(EOICoach)
admin.site.register(MeetingLinks)
admin.site.register(Chat)
# Register your models here.
