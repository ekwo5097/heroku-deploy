from django.db import models
# from django.db import models
from jsonfield import JSONField
from django.core.validators import MinValueValidator, MaxValueValidator






# Create your models here.
class User(models.Model):
    username = models.CharField(unique = True,max_length=20)
    password = models.CharField(max_length=20)
    email = models.EmailField(max_length=40)
    phone_number = models.CharField(max_length = 15, null = True, unique=False)
    USERTYPE = (
        ('L', 'Leader'),
        ('C', 'Coach'),
        ('A', 'Admin'),
    )
    usertype = models.CharField(max_length=1, choices=USERTYPE)


class Leader(models.Model):
    username = models.ForeignKey(User,to_field='username', on_delete=models.CASCADE,primary_key=True)
    name = models.CharField(max_length = 15)
    age = models.IntegerField(default = 0)
    gender = models.TextField()
    industry = models.TextField()
    day = models.TextField()
    # profile_pic = models.ImageField(upload_to='./profileimages/', null=True)
    years_of_employment = models.IntegerField(default=-1)
    SUBSCRIPTION_TYPE = (
        (0, 'Free'),
        (1, 'Basic'),
        (2, 'Premium'),
    )
    subscription_type = models.PositiveSmallIntegerField(choices=SUBSCRIPTION_TYPE, null=0, default=0)
    TRUE_FALSE_BOOLEAN = (
        ('True', 'True'),
        ('False', 'False')
    )
    questionnaire_completed = models.CharField(max_length=5, choices=TRUE_FALSE_BOOLEAN, default='False')
    matched_with_coach = models.CharField(max_length=5, choices=TRUE_FALSE_BOOLEAN, default='False')
    months_in_role = models.IntegerField(default = 0)
    company = models.TextField(default="")


class CoachingGoals(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    COACH_GOAL = (
        ('Not sure with the coaching goal', 'Not sure with the coaching goal'),
        ('Dealing with procrastination', 'Dealing with procrastination'),
        ('Building effective work habits', 'Building effective work habits'),
        ('Overcoming presentation anxiety', 'Overcoming presentation anxiety'),
        ('Building confidence and self-efficacy', 'Building confidence and self-efficacy'),
        ('Increasing ambiguity tolerance and resilience', 'Increasing ambiguity tolerance and resilience'),
        ('Delegating workload and managing teams', 'Delegating workload and managing teams'),
        ('Managing your inner critic and reducing Imposter Syndrome', 'Managing your inner critic and reducing Imposter Syndrome'),
        ('Improving leadership and perspective taking capacity', 'Improving leadership and perspective taking capacity'),
    )
    coach_goal = models.TextField(choices=COACH_GOAL, default='Not sure with the coaching goal')


class Coach(models.Model):
    username = models.ForeignKey(User,to_field='username', on_delete=models.CASCADE, primary_key=True)
    name = models.CharField(max_length = 10)
    # age = models.IntegerField(default = 0)
    age = models.TextField(default = 0)


    gender = models.TextField()
    about =models.TextField()
    industry = models.TextField(default="")
    highest_edu_qualification = models.TextField(default="")
    years_of_practice = models.IntegerField(default=-1)
    COACH_GOAL = (
        ('Not sure with the coaching goal', 'Not sure with the coaching goal'),
        ('Dealing with procrastination', 'Dealing with procrastination'),
        ('Building effective work habits', 'Building effective work habits'),
        ('Overcoming presentation anxiety', 'Overcoming presentation anxiety'),
        ('Building confidence and self-efficacy', 'Building confidence and self-efficacy'),
        ('Increasing ambiguity tolerance and resilience', 'Increasing ambiguity tolerance and resilience'),
        ('Delegating workload and managing teams', 'Delegating workload and managing teams'),
        ('Managing your inner critic and reducing Imposter Syndrome', 'Managing your inner critic and reducing Imposter Syndrome'),
        ('Improving leadership and perspective taking capacity', 'Improving leadership and perspective taking capacity'),
    )
    coach_goal_1 = models.TextField(choices=COACH_GOAL, default="")
    coach_goal_2 = models.TextField(choices=COACH_GOAL, default="")
    coach_goal_3 = models.TextField(choices=COACH_GOAL, default="")
    day = models.TextField()
    QUESTIONNAIRE_COMPLETED = (
        ('True', 'True'),
        ('False', 'False')
    )
    questionnaire_completed = models.CharField(max_length=5, choices=QUESTIONNAIRE_COMPLETED, default='False')
    philosophy = models.TextField(default="")
    approach = models.TextField(default="")
    
    
class CoachAvailableTimes(models.Model):
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    TIME_SLOTS = (
        ('8:00am-9:00am', '8:00am-9:00am'),
        ('9:00am-10:00am', '9:00am-10:00am'),
        ('10:00am-11:00am', '10:00am-11:00am'),
        ('11:00am-12:00pm', '11:00am-12:00pm'),
        ('12:00pm-1:00pm', '12:00pm-1:00pm'),
        ('1:00pm-2:00pm', '1:00pm-2:00pm'),
        ('2:00pm-3:00pm', '2:00pm-3:00pm'),
        ('3:00pm-4:00pm', '3:00pm-4:00pm'),
        ('4:00pm-5:00pm', '4:00pm-5:00pm'),
        ('5:00pm-6:00pm', '5:00pm-6:00pm'),
        ('6:00pm-7:00pm', '6:00pm-7:00pm'),
        ('7:00pm-8:00pm', '7:00pm-8:00pm'),
        ('8:00pm-9:00pm', '8:00pm-9:00pm'),
    )
    available_time = models.TextField(choices=TIME_SLOTS, default="")

# TEMP START
class AvailableTimes(models.Model):
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    TIME_SLOTS = (
        ('8:00am-9:00am', '8:00am-9:00am'),
        ('9:00am-10:00am', '9:00am-10:00am'),
        ('10:00am-11:00am', '10:00am-11:00am'),
        ('11:00am-12:00pm', '11:00am-12:00pm'),
        ('12:00pm-1:00pm', '12:00pm-1:00pm'),
        ('1:00pm-2:00pm', '1:00pm-2:00pm'),
        ('2:00pm-3:00pm', '2:00pm-3:00pm'),
        ('3:00pm-4:00pm', '3:00pm-4:00pm'),
        ('4:00pm-5:00pm', '4:00pm-5:00pm'),
        ('5:00pm-6:00pm', '5:00pm-6:00pm'),
        ('6:00pm-7:00pm', '6:00pm-7:00pm'),
        ('7:00pm-8:00pm', '7:00pm-8:00pm'),
        ('8:00pm-9:00pm', '8:00pm-9:00pm'),
    )
    available_time = models.TextField(choices=TIME_SLOTS, default="")
    # Dates will be in the format "Thu, 13 January 2022" 
    date = models.CharField(max_length=50, default = "")

class BookedTimes(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    TIME_SLOTS = (
        ('8:00am-9:00am', '8:00am-9:00am'),
        ('9:00am-10:00am', '9:00am-10:00am'),
        ('10:00am-11:00am', '10:00am-11:00am'),
        ('11:00am-12:00pm', '11:00am-12:00pm'),
        ('12:00pm-1:00pm', '12:00pm-1:00pm'),
        ('1:00pm-2:00pm', '1:00pm-2:00pm'),
        ('2:00pm-3:00pm', '2:00pm-3:00pm'),
        ('3:00pm-4:00pm', '3:00pm-4:00pm'),
        ('4:00pm-5:00pm', '4:00pm-5:00pm'),
        ('5:00pm-6:00pm', '5:00pm-6:00pm'),
        ('6:00pm-7:00pm', '6:00pm-7:00pm'),
        ('7:00pm-8:00pm', '7:00pm-8:00pm'),
        ('8:00pm-9:00pm', '8:00pm-9:00pm'),
    )
    booked_time = models.TextField(choices=TIME_SLOTS, default="")
    # Dates will be in the format "Thu, 13 January 2022"
    date = models.CharField(max_length=50, default = "")


class CounterTest(models.Model):
    count = models.SmallIntegerField(default = 0)
# TEMP END

#Sub goals belong under one overarching coaching goal
# class SubGoals(models.Model):
#     leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
#     coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
#     coach_goal = models.ForeignKey(CoachingGoals, on_delete=models.CASCADE)
#     sub_goal = models.CharField(default='', max_length=50)

#Actions for leader (limited to 3, completed actions will be saved) 
class Actions(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    action_name = models.CharField(default='', max_length=100)
    current_iterations = models.IntegerField(default=0)
    max_iterations = models.IntegerField(default=0)
    ACTION_COMPLETED = (
        ('True', 'True'),
        ('False', 'False')
    )
    completed = models.CharField(max_length=5, choices=ACTION_COMPLETED, default='False')

class LeaderToCoach(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE,primary_key=True)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)


class MeetingLinks(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE,primary_key=True)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    meeting_link = models.TextField(default = "")
    meeting_id = models.TextField(default = "")
    meeting_password = models.TextField(default = "")

class Message(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    SENDERTYPE = (
        ("0", 'None'),
        ('L', 'Leader'),
        ('C', 'Coach')
    )
    sender = models.CharField(max_length=1, choices=SENDERTYPE, default=0)
    message = models.TextField()
    time = models.DateTimeField(auto_now=True)


# Message/Messaging tables start here

# Use this table to access the id field to keep rooms private
class MessageRoom(models.Model):
    leader = models.ForeignKey(User, related_name='leaderMsg', on_delete=models.CASCADE)
    coach = models.ForeignKey(User, related_name='coachMsg', on_delete=models.CASCADE)

# Table which keeps all messages sent
class Chat(models.Model):
    room_id = models.IntegerField(default=0)
    #sender = models.ForeignKey(User, on_delete=models.CASCADE)
    sender_id= models.IntegerField(default=0)
    message = models.TextField()
    received = models.BooleanField(default=False)
    time_sent = models.DateTimeField(auto_now=True)

# Message/Messaging tables end here


class Feedback(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    feedback = models.JSONField()
    CONFIRMED = (
        ('True', 'True'),
        ('False', 'False')
    )
    confirmed = models.CharField(max_length=5, choices=CONFIRMED, default='False')
    time = models.DateTimeField(auto_now=True)

class Scheduling(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE,null=True)
    #scheduling = models.DateTimeField() #not sure
    DATE = (
        ('0', '-select-'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12'),
        ('13', '13'),
        ('14', '14'),
        ('15', '15'),
        ('16', '16'),
        ('17', '17'),
        ('18', '18'),
        ('19', '19'),
        ('20', '20'),
        ('21', '21'),
        ('22', '22'),
        ('23', '23'),
        ('24', '24'),
        ('25', '25'),
        ('26', '26'),
        ('27', '27'),
        ('28', '28'),
        ('29', '29'),
        ('30', '30'),
        ('31', '31'),
    )

    date = models.PositiveSmallIntegerField(choices=DATE,null="0")
    MONTH = (
        ('0', '-select-'),
        ('1', 'January'),
        ('2', 'February'),
        ('3', 'March'),
        ('4', 'April'),
        ('5', 'May'),
        ('6', 'June'),
        ('7', 'July'),
        ('8', 'August'),
        ('9', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
    )

    month = models.PositiveSmallIntegerField(choices=MONTH,null="0")
    YEAR = (
        ('0', '-select-'),
        ('2021', '2021'),
        ('2022', '2022'),
        ('2023', '2023'),
        ('2024', '2024'),
        ('2025', '2025'),
    )
    year = models.PositiveSmallIntegerField(choices=YEAR,null="0")
    TIME = (
        ('0', '-select-'),
        ('9:00am-10:00am', '9:00am-10:00am'),
        ('10:00am-11:00am', '10:00am-11:00am'),
        ('11:00am-12:00pm', '11:00am-12:00pm'),
        ('12:00pm-1:00pm', '12:00pm-1:00pm'),
        ('1:00pm-2:00pm', '1:00pm-2:00pm'),
        ('2:00pm-3:00pm', '2:00pm-3:00pm'),
        ('3:00pm-4:00pm', '3:00pm-4:00pm'),
        ('4:00pm-5:00pm', '4:00pm-5:00pm'),
        ('5:00pm-6:00pm', '5:00pm-6:00pm'),
    )
    # time = models.PositiveSmallIntegerField(choices=TIME,null="0")
    time = models.TextField(choices=TIME, null="0")

class NextSchedule(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE,primary_key=True)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE,null=True)
    DATE = (
        ('0', '-select-'),
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '4'),
        ('5', '5'),
        ('6', '6'),
        ('7', '7'),
        ('8', '8'),
        ('9', '9'),
        ('10', '10'),
        ('11', '11'),
        ('12', '12'),
        ('13', '13'),
        ('14', '14'),
        ('15', '15'),
        ('16', '16'),
        ('17', '17'),
        ('18', '18'),
        ('19', '19'),
        ('20', '20'),
        ('21', '21'),
        ('22', '22'),
        ('23', '23'),
        ('24', '24'),
        ('25', '25'),
        ('26', '26'),
        ('27', '27'),
        ('28', '28'),
        ('29', '29'),
        ('30', '30'),
        ('31', '31'),
    )

    date = models.PositiveSmallIntegerField(choices=DATE,null="0")
    MONTH = (
        ('0', '-select-'),
        ('1', 'January'),
        ('2', 'February'),
        ('3', 'March'),
        ('4', 'April'),
        ('5', 'May'),
        ('6', 'June'),
        ('7', 'July'),
        ('8', 'August'),
        ('9', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
    )

    month = models.PositiveSmallIntegerField(choices=MONTH,null="0")
    YEAR = (
        ('0', '-select-'),
        ('2021', '2021'),
        ('2022', '2022'),
        ('2023', '2023'),
        ('2024', '2024'),
        ('2025', '2025'),
    )
    year = models.PositiveSmallIntegerField(choices=YEAR,null="0")
    TIME = (
        ('0', '-select-'),
        ('9:00am-10:00am', '9:00am-10:00am'),
        ('10:00am-11:00am', '10:00am-11:00am'),
        ('11:00am-12:00pm', '11:00am-12:00pm'),
        ('12:00pm-1:00pm', '12:00pm-1:00pm'),
        ('1:00pm-2:00pm', '1:00pm-2:00pm'),
        ('2:00pm-3:00pm', '2:00pm-3:00pm'),
        ('3:00pm-4:00pm', '3:00pm-4:00pm'),
        ('4:00pm-5:00pm', '4:00pm-5:00pm'),
        ('5:00pm-6:00pm', '5:00pm-6:00pm'),
    )
    time = models.TextField(choices=TIME, null="0")
    zoomLink = models.TextField(default = "")

# Leader can have multiple homeworks
# 'clear all' button
# add homework button
# checkbox to display completion
class Homework(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    coach = models.ForeignKey(Coach, on_delete=models.CASCADE)
    title = models.TextField(default="")
    description = models.TextField(default="")
    isFinished = models.BooleanField(default=False)


class AvailableDays(models.Model):
    leader = models.ForeignKey(Leader, on_delete=models.CASCADE)
    day_available = (
        ('0', 'Monday'),
        ('1', 'Tuesday'),
        ('2', 'Wednesday'),
        ('3', 'Thursday'),
        ('4', 'Friday'),
        ('5', 'Saturday'),
        ('6', 'Sunday'),
        ('7', 'Else'),
    )
    day = models.PositiveSmallIntegerField(choices=day_available,default = 7)

class EOICoach(models.Model):
    name = models.CharField(unique = True,max_length=20)
    email = models.EmailField(max_length=40)
    message = models.TextField(default="")
