from .views import main
from django.urls import path
from .views import CoachAPIView

urlpatterns = [
    path('', main),
]
