import axios from 'axios';

export function questionnaire(formDatas){
    axios.post('https://new-emerge-website.herokuapp.com/api/questionnaire/',{
        username: localStorage.getItem("username"),
        token: localStorage.getItem("token"),
        name: formDatas["name"],
        age: formDatas["age"],
        gender: formDatas["gender"],
        industry: formDatas["industry"],
        company: formDatas["company"],
        coach_goal: formDatas["coach_goal"],
        years_employment: formDatas["years_employment"],
        months_in_role: formDatas["months_in_role"],
        Monday: formDatas["Monday"],
        Tuesday: formDatas["Tuesday"],
        Wednesday: formDatas["Wednesday"],
        Thursday: formDatas["Thursday"],
        Friday: formDatas["Friday"],
        Saturday: formDatas["Saturday"],
        Sunday: formDatas["Sunday"],
        Else: formDatas["Else"],
    })
    .then(res => {
        if (res.data.error!=null){
            localStorage.removeItem("token");
            localStorage.removeItem("username");
            localStorage.removeItem("usertype");
            localStorage.removeItem("messageLeader");
            localStorage.removeItem("pusherTransportTLS");
            console.log("localStorage items removed");
            localStorage.setItem("refresh","true");
                    
            alert(res.data.error); 
          }
        console.log(res);
    })
    .catch(err => {
    })
   
}


export function getSetting(){
    axios.post('https://new-emerge-website.herokuapp.com/api/getSetting/',{
        username: localStorage.getItem("username"),
        token: localStorage.getItem("token"),
    })
    .then(res => {
        console.log(res);
        return res.data
    })
    .catch(err => {
    })
}

export function coaches(formDatas){
    axios.post('https://new-emerge-website.herokuapp.com/api/coaches/',{
        username: localStorage.getItem("username"),
        token: localStorage.getItem("token"),
        date: formDatas["date"],
        month: formDatas["month"],
        year: formDatas["year"],
        time: formDatas["time"],

    })
    .then(res => {
        console.log(res);
        return res.data
    })
    .catch(err => {
    })
}
