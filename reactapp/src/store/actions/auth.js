import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = token => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token
    }
}

export const authFail = error => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    }
}
export const logout = () => {
    localStorage.removeItem('user');
    localStorage.removeItem('expirationDate');
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}

export const checkAuthTimeout = expirationTime => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expirationTime * 1000)
    }
}

// export const authLogin = (username, password) => {
export const authLogin = (username, email, password) => {
    return dispatch => {
        dispatch(authStart());
        axios.post('https://new-emerge-website.herokuapp.com/api/login/',{
            username: username,
            email: email,
            password: password
        })
        .then(res => {
          console.log(res.data.error);
          if (res.data.error!=null){
            dispatch(authFail(res.data.error));
            alert(res.data.error);
          }else{
            console.log(res);
            const token = res.data.token;
            const username = res.data.username;
            const usertype = res.data.usertype;
            localStorage.setItem('token', token);
            localStorage.setItem('username', username);
            localStorage.setItem('usertype', usertype);
            dispatch(authSuccess(token));
          }
        })
        .catch(err => {
            dispatch(authFail(err))
        })
    }
}

export const authSignup = (username, email, password1, password2) => {
    return dispatch => {
        dispatch(authStart());
        axios.post('https://new-emerge-website.herokuapp.com/api/register/',{
            username: username,
            email: email,
            password1: password1,
            password2: password2,
            usertype:"L",
        })
        .then(res => {
          if (res.data.error!=null){
            dispatch(authFail(res.data.error));
            alert(res.data.error);
          }else{
            console.log(res);
            const token = res.data.token;
            const username = res.data.username;
            const usertype = res.data.usertype;
            localStorage.setItem('token', token);
            localStorage.setItem('username', username);
            localStorage.setItem('usertype', usertype);
            dispatch(authSuccess(token));
          }
        })
        .catch(err => {
            dispatch(authFail(err))
        })
    }
}

export const coachSignup = (username, email, password1, password2) => {
    return dispatch => {
        dispatch(authStart());
        axios.post('https://new-emerge-website.herokuapp.com/api/register/',{
            username: username,
            email: email,
            password1: password1,
            password2: password2,
            usertype:"C",
        })
        .then(res => {
          if (res.data.error!=null){
            dispatch(authFail(res.data.error));
            alert(res.data.error);
          }else{
            console.log(res);
            const token = res.data.token;
            const username = res.data.username;
            const usertype = res.data.usertype;
            localStorage.setItem('token', token);
            localStorage.setItem('username', username);
            localStorage.setItem('usertype', usertype);
            dispatch(authSuccess(token));
          }
        })
        .catch(err => {
            dispatch(authFail(err))
        })
    }
}


export const coachSignupAdmin = (username, email, password1, password2) => {
    return dispatch => {
        dispatch(authStart());
        axios.post('https://new-emerge-website.herokuapp.com/api/register/',{
            username: username,
            email: email,
            password1: password1,
            password2: password2,
            usertype:"C",
        })
        .then(res => {
          if (res.data.error!=null){
            dispatch(authFail(res.data.error));
            alert(res.data.error);
          }else{
            console.log(res);
            const token = res.data.token;
            const username = res.data.username;
            const usertype = res.data.usertype;
            dispatch(authSuccess(token));
          }
        })
        .catch(err => {
            dispatch(authFail(err))
        })
    }
}

export const authCheckState = () => {
    return dispatch => {
        const token = localStorage.getItem('token');
        if(token === undefined) {
            dispatch(logout());
        }
        else {
            const expirationDate = new Date(localStorage.getItem('expirationDate'));
            if (expirationDate <= new Date() ) {
                dispatch(logout());
            }
            else {
                dispatch(authSuccess(token));
                const username = localStorage.getItem('username');
                const usertype = localStorage.getItem('usertype');
                dispatch(checkAuthTimeout((expirationDate.getTime() - new Date().getTime()) / 1000));

            }

        }
    }
}

export const authCheckUser = () => {
  return dispatch => {
      const username = localStorage.getItem('username');
      const usertype = localStorage.getItem('usertype');
  }
}
