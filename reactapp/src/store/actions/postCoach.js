import axios from 'axios';

export function questionnairecoach(formData){
    axios.post('https://new-emerge-website.herokuapp.com/api/questionnairecoach/',{
        username: localStorage.getItem("username"),
        usertype:localStorage.getItem("usertype"),
        token: localStorage.getItem("token"),
        name: formData["name"],
        age: formData["age"],
        gender: formData["gender"],
        about: formData["about"],
        approach: formData["approach"],
        philosophy: formData["philosophy"],
        industry: formData["industry"],
        years_of_practice: formData["years_of_practice"],
        highest_edu_qualification: formData["highest_edu_qualification"],
        coach_goal_1: formData["coach_goal_1"],
        coach_goal_2: formData["coach_goal_2"],
        coach_goal_3: formData["coach_goal_3"],
        Monday: formData["Monday"],
        Tuesday: formData["Tuesday"],
        Wednesday: formData["Wednesday"],
        Thursday: formData["Thursday"],
        Friday: formData["Friday"],
        Saturday: formData["Saturday"],
        Sunday: formData["Sunday"],
        Else: formData["Else"],
    })
    .then(res => {
        console.log(res);
    })
    .catch(error => {
        if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            console.log(error.response.data);
            console.log(error.response.status);
            console.log(error.response.headers);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log(error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log('Error', error.message);
          }
          console.log(error.config);
    })
}
