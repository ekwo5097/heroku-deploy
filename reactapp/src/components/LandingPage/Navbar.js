import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';
import { LoginButton } from './LoginButton';
import { useDispatch } from "react-redux";
import { connect } from 'react-redux';
import * as actions from '../../store/actions/auth';
import { RegisterButton } from './RegisterButton';
import './Navbar.css';

function Navbar(props) {
    const [click, setClick] = useState(false);
    const closeMobileMenu = () => setClick(false);
    const handleClick= () => setClick(!click);
    const [button, setButton] = useState(true);

    const showButton = () => {
        if(window.innerWidth <=960) {
            setButton(false)
        }
        else{
            setButton(true);
        }
    };

    useEffect(() => {
        showButton()
    }, []);
    const dispatch = useDispatch();


    const mapDispatchToProps =  () => { 
        dispatch(actions.logout());
    };

    window.addEventListener('resize', showButton);
    

    return (
        <>
        <nav className='navbar'>
         <div className='navbar-container'>
           {/* <Link to='/' className='navbar-logo' onClick={closeMobileMenu}>
                EMERGE
           </Link> */}
           <div className='menu-icon' onClick={handleClick}>
               <i className={click ? 'fas fa-times' : 'fas fa-bars'} />
           </div>
           <ul className={click ? 'nav-menu active' : 'nav-menu'}>
               <li className='nav-item'>
                   <Link to='/' className='nav-links' onClick={closeMobileMenu}>
                       Home
                   </Link>
               </li>
               <li className='nav-item'>
                   <Link to='/whatwedo' className='nav-links' onClick={closeMobileMenu}>
                       What We Do
                   </Link>
               </li>
               <li className='nav-item'>
                   <Link to='/ourcoaches' className='nav-links' onClick={closeMobileMenu}>
                       Our Coaches
                   </Link>
               </li>

               <li className='nav-item'>
                   <div className="loginRegisterBtnContainer">
                        {/* {button  && <Button className='log' buttonStyle='btn--outline' buttonSize='btn--large'>Login</Button>} */}
                        <LoginButton className='log' buttonStyle='btn--outline' buttonSize='btn--large'>Login</LoginButton>

                   </div>
               </li>
                
               <li className='nav-item'>
                    <div className="loginRegisterBtnContainer">
                        <RegisterButton className='btns' buttonStyle='btn--outline' buttonSize='btn--large'>
                            Register
                        </RegisterButton>
                    </div>
                </li>

           </ul>     
            <>
                {/* {button  && <Button className='log' buttonStyle='btn--outline'>Login</Button>}


                <RegisterButton className='btns'
                buttonStyle='btn--outline'
                buttonSize='btn--large'
                >
                   Register
                </RegisterButton> */}
            </>
         </div>
         </nav>
         </>
    );
}

/**const mapDispatchToProps = dispatch => {
    return {
        logout: () => dispatch(actions.logout()) 
    }
}**/

export default Navbar;