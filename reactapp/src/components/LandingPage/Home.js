import '../../App.css';
import React from 'react';
import Hero from './Hero';
import WhatWeDoCard from './WhatWeDoCard';
import Footer from './Footer';
import Navbar from './Navbar';
import { Route, useHistory } from "react-router-dom";
import axios from 'axios';

function Home(){
    let history = useHistory();
    if(localStorage.getItem("token")!=undefined){
      history.push('/userhome');

    }
    else {
      axios.post('https://new-emerge-website.herokuapp.com/api/logout/',{
              }, 
              )
              .then(response => {
                  console.log(response);
                  //history.push('/');
              })
      if (localStorage.getItem("token")!=undefined){
        localStorage.removeItem("token");
        localStorage.removeItem("username");
        localStorage.removeItem("usertype");
        localStorage.removeItem("messageLeader");
        localStorage.removeItem("pusherTransportTLS");
        console.log("localStorage items removed");
        localStorage.setItem("refresh","true");
        axios.post('https://new-emerge-website.herokuapp.com/api/logout/',{
              }, 
              )
              .then(response => {
                  console.log(response);
                  //history.push('/');
              })
        window. location. reload();
      }
      if (localStorage.getItem("refresh")=="true"){
        localStorage.removeItem("refresh");
        console.log("page refresh");
        window. location. reload();
      }
    }
    return (
        <>
        <Navbar />
        <Hero />
        <Footer />
        </>
    );
}

export default Home;
