import '../../App.css';
import React from 'react';
import WhatWeDoCard from '../LandingPage/WhatWeDoCard';
import Footer from '../LandingPage/Footer';
import OurCoachesCard from '../LandingPage/OurCoachesCard';

function OurCoachesPage(){
    return (
        <>
        <OurCoachesCard />
        <Footer />
        </>
    );
}

export default OurCoachesPage;