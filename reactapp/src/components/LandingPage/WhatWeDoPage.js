import '../../App.css';
import React from 'react';
import WhatWeDoCard from './WhatWeDoCard';
import Footer from './Footer';

function WhatWeDoPage(){
    return (
        <>
        <WhatWeDoCard />
        <Footer />
        </>
    );
}

export default WhatWeDoPage;