import React from 'react';
import './Cards.css';

function WhatWeDoCard() {
    return (
        <>
            <div className='cards'>
                <h1>What is EMERGE?</h1>
                <div className='cards_container'>
                    <div className='cards_wrapper'>
                        <ul className='cards_items'>
                        <p>EMERGE is a digital platform that connects evidence-based 
                            coaches with high potential emerging leaders. It enables 
                            emerging leaders to scale their businesses by reducing their 
                            psychological stress and increasing their mental strength.
                        </p>
                        </ul>
                    </div>
                </div>
            </div>

            <div className='cards'>
                <h1>What is evidence-based coaching?</h1>
                <div className='cards_container'>
                    <div className='cards_wrapper'>
                        <ul className='cards_items'>
                            <p>It is just like regular coaching except BETTER. 
                                Evidence-based coaching uses the latest scientific research 
                                to give you the best results in the most optimized way possible.
                            </p>
                        </ul>
                    </div>
                </div>
            </div>
        </>
        
        
    );
}

export default WhatWeDoCard;
