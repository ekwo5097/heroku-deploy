import React from 'react';
import './Cards.css';
import { Button as StyledButton }  from 'antd';
import { Link } from 'react-router-dom';

function OurCoachesCard() {
    return (
        <>
            <div className='cards'>
                <h1>Our Coaches</h1>
                <div className='cards_container'>
                    <div className='cards_wrapper'>
                        <ul className='cards_items'>
                            <p>Our coaches bring decades of corporate experience, run their own businesses 
                                and are masters in coaching psychology. They implement scientifically supported coaching methods and 
                                provide you with a hyper-personalised learning experience to help you grow as a leader.
                            </p>
                        </ul>
                        
                    </div>
            

                    <Link to ='/registerInterest' className='btn-mobile'>
                        <StyledButton type="primary" size= "large" style={{ background: "#0C1C94", borderColor: "white" }}>
                            Apply as a Coach
                        </StyledButton>
                    </Link>
                    
                </div>
            </div>
        </>
        
        
    );
}

export default OurCoachesCard;
