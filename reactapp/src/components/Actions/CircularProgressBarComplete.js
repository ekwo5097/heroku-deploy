import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import './CircularProgressBar.css';

export default function CircularProgressBarComplete({percentageProp}) {

    // const percentage_1 = 60;
    // const percentage_2 = 100;

    return(

        <>

        {/* <div class="progress-bar-container">

            In-Progress Design 1

            <CircularProgressbar value={percentage_1} text={`${percentage_1}%`} styles={buildStyles({
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(17, 40, 148)`,
                textColor: 'rgb(23, 138, 251)',
                trailColor: 'rgb(204, 204, 204)',
                backgroundColor: 'rgb(0, 0, 0)',
            })}/>
        </div> */}

        {/* <div class="progress-bar-container">

            In-Progress Design 2

            <CircularProgressbar value={percentage_1} text={`${percentage_1}%`} background backgroundPadding={6} styles={buildStyles({
                backgroundColor: 'rgb(23, 138, 251)',
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(255, 255, 255)`,
                textColor: 'rgb(255, 255, 255)',
                trailColor: 'rgb(23, 138, 251)',
            })}/>
        </div> */}

        {/* <div class="progress-bar-container">

            In-Progress Design 3

            <CircularProgressbar value={percentageProp} text={`${percentageProp}%`} styles={buildStyles({
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(23, 138, 251)`,
                textColor: 'rgb(17, 40, 148)',
                trailColor: 'rgb(204, 204, 204)',
                backgroundColor: 'rgb(0, 0, 0)',
            })}/>
        </div> */}

        {/* <div class="progress-bar-container">

            In-Progress Design 4

            <CircularProgressbar value={percentage_1} text={`${percentage_1}%`} styles={buildStyles({
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(17, 40, 148)`,
                textColor: 'rgb(17, 40, 148)',
                trailColor: 'rgb(204, 204, 204)',
                backgroundColor: 'rgb(0, 0, 0)',
            })}/>
        </div> */}

        {/* <div class="progress-bar-container">

            In-Progress Design 5

            <CircularProgressbar value={percentage_1} text={`${percentage_1}%`} background backgroundPadding={6} styles={buildStyles({
                backgroundColor: 'rgb(17, 40, 148)',
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(255, 255, 255)`,
                textColor: 'rgb(255, 255, 255)',
                trailColor: 'rgb(17, 40, 148)',
            })}/>
        </div> */}

        {/* <br/>   

        <div class="progress-bar-container">

            Completed Design 1

            <CircularProgressbar value={percentage_2} text={`${percentage_2}%`} styles={buildStyles({
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(169, 169, 169)`,
                textColor: 'rgb(219, 219, 219)',
                trailColor: 'rgb(204, 204, 204)',
                backgroundColor: 'rgb(0, 0, 0)',
            })}/>
        </div>

        <div class="progress-bar-container">

            Completed Design 2

            <CircularProgressbar value={percentage_2} text={`${percentage_2}%`} background backgroundPadding={6} styles={buildStyles({
                backgroundColor: 'rgb(219, 219, 219)',
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(255, 255, 255)`,
                textColor: 'rgb(255, 255, 255)',
                trailColor: 'rgb(219, 219, 219)',
            })}/>
        </div>

        <div class="progress-bar-container">

            Completed Design 3

            <CircularProgressbar value={percentage_2} text={`${percentage_2}%`} styles={buildStyles({
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(219, 219, 219)`,
                textColor: 'rgb(169, 169, 169)',
                trailColor: 'rgb(204, 204, 204)',
                backgroundColor: 'rgb(0, 0, 0)',
            })}/>
        </div>

        <div class="progress-bar-container">

            Completed Design 4

            <CircularProgressbar value={percentage_2} text={`${percentage_2}%`} styles={buildStyles({
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(169, 169, 169)`,
                textColor: 'rgb(169, 169, 169)',
                trailColor: 'rgb(204, 204, 204)',
                backgroundColor: 'rgb(0, 0, 0)',
            })}/>
        </div>
        */}

        <div class="progress-bar-container">

            {/* Completed Design 5 */}

            <CircularProgressbar value={percentageProp} text={`${percentageProp}%`} background backgroundPadding={6} styles={buildStyles({
                backgroundColor: 'rgb(169, 169, 169)',
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(255, 255, 255)`,
                textColor: `rgb(225, 225, 225)`,
                trailColor: 'rgb(169, 169, 169)',
            })}/>
        </div> 

        {/* Incomplete version of the progressbar*/}
        {/* <CircularProgressbar value={percentageProp} text={`${percentageProp}%`} styles={buildStyles({
                strokeLinecap: 'rounded',
                textSize: '16px',
                pathTransitionDuration: 0.5,
                pathColor: `rgb(23, 138, 251)`,
                textColor: 'rgb(17, 40, 148)',
                trailColor: 'rgb(204, 204, 204)',
                backgroundColor: 'rgb(0, 0, 0)',
            })}/> */}

        </>
    )

}