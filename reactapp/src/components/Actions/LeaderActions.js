
import { CircularProgressbar, buildStyles } from 'react-circular-progressbar';
// import { styled} from '@mui/material/styles';
// import {styled as componentStyled,} from "styled-components";
import 'react-circular-progressbar/dist/styles.css';
import {
    Grid,
    Paper,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    IconButton,
} from "@mui/material";
import { styled, createTheme, ThemeProvider} from '@mui/material/styles';
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from '@mui/icons-material/Remove';

// const ProgressWrapper = componentStyled.div`
//   .header {
//     display: flex;
//     justify-content: center;
//     align-items: center;
//   }

//   .bar-section {
//     display: flex;
//     margin: auto auto;
//     text-align: center;
//   }

//   .indiv-bar {
//   }

//   .bar {
//     width: 25%;
//     height: 25%;
//     min-width: 100px;
//     min-height: 100px;
//     margin: auto;
//   }

//   @media only screen and (min-width: 600px) {
//     .bar {
//       width: 40%;
//       height: 40%;
//     }
//   }
// `;
const theme = createTheme({
});

const Item = styled(Paper)(({ theme }) => ({
    padding: theme.spacing(1),
    textAlign: 'Center',
  }));






  const increment = (actions, setActions, actionIndex) => {
    let updatedAction = actions[actionIndex];
    updatedAction.currentIterations += 1;
    // Call API stuff NOT YET
    // const _incrementAction = async (subgoal, action) => {
    //     await incrementAction(subgoal, action);
    // };
    // _incrementAction(updatedSubgoal.subgoal, updatedAction.name);
    if (updatedAction.currentIterations === updatedAction.maxIterations) {
        updatedAction.completed = true;
        // setSnackbarOpen(true);
    }
    actions[actionIndex] = updatedAction;
    setActions(actions)
    // setTasks(updatedTasks);
    };

    const decrement = (actions, setActions, actionIndex) => {
        let updatedAction = actions[actionIndex];
        if (updatedAction.currentIterations > 0) {
          updatedAction.currentIterations -= 1;
        }
        // Call API stuff NOT YET
        // const _decrementAction = async (subgoal, action) => {
        //     await decrementAction(subgoal, action);
        // };
        // _decrementAction(updatedSubgoal.subgoal, updatedAction.name);
        if (updatedAction.currentIterations === updatedAction.maxIterations) {
            updatedAction.completed = true;
            // setSnackbarOpen(true);
        }
        actions[actionIndex] = updatedAction;
        setActions(actions)
        // setTasks(updatedTasks);
    };



const actionsMap = (actions, setActions) => {
    if (actions === undefined || actions.length === 0) {
        return(
            <p>Leader has no actions.</p>
        )
    }else {
        return (
            <>
                <ThemeProvider theme={theme}>
                    {/* <ProgressWrapper> */}
                        <Grid container wrap="wrap" columnSpacing={3} rowSpacing={2}>
                            <Item>
                                {actions.map(actionObj => {
                                    console.log(actions)
                                    console.log(actions[0])
                                    console.log(actions[0]['actionName'])
                                    const percentage = actionObj.maxIterations ? (actionObj.currentIterations / actionObj.maxIterations) * 100 : 0;
                                    return (
                                        <>
                                            <Grid item xs={12} sm={4}>
                                                <List>
                                                    <ListItem
                                                        key={actions.indexOf(actionObj)}
                                                        secondaryAction={
                                                            <>
                                                                <IconButton
                                                                    edge="start"
                                                                    aria-label="remove"
                                                                    onClick={() => decrement(actions, setActions, actions.indexOf(actionObj))}
                                                                >
                                                                    <RemoveIcon />
                                                                </IconButton>
                                                                <IconButton
                                                                    edge="end"
                                                                    aria-label="add"
                                                                    onClick={() => increment(actions, setActions, actions.indexOf(actionObj))}
                                                                >
                                                                    <AddIcon />
                                                                </IconButton>
                                                            </>
                                                        }
                                                        disablePadding
                                                    >
                                                        <ListItemButton>
                                                        <p>Action: {actionObj.actionName}</p>
                                                        <p>Progress: {actionObj.currentIterations}/{actionObj.maxIterations}</p>
                                                        </ListItemButton>
                                                    </ListItem>
                                                </List>
                                                <div className="indiv-bar">
                                                    {/* <p>Action: {actionObj.actionName}</p>
                                                    <p>Completed: {actionObj.currentIterations}/{actionObj.maxIterations}</p> */}
                                                    <br />
                                                    <div className="bar">
                                                        <CircularProgressbar
                                                        value={percentage}
                                                        text={`${Math.round(percentage)}%`}
                                                        styles={buildStyles({
                                                            strokeLinecap: 'rounded',
                                                            textSize: '16px',
                                                            pathTransitionDuration: 0.5,
                                                            pathColor: `rgb(23, 138, 251)`,
                                                            textColor: 'rgb(17, 40, 148)',
                                                            trailColor: 'rgb(204, 204, 204)',
                                                            backgroundColor: 'rgb(0, 0, 0)',
                                                        })}
                                                        />
                                                    </div>
                                                </div>
                                            </Grid>
                                        </>
                                    )
                                })}
                            </Item>
                        </Grid>
                    {/* </ProgressWrapper> */}
                </ThemeProvider>
            </>
        )
        
    }
    
}

export default function LeaderActions({actionsProp, setActionsProp}) {
    // const actionsArr = JSON.parse(JSON.stringify([...actions]))
    return (
        <>
            <div>
                {actionsMap(actionsProp, setActionsProp)}
            </div>
            
        </>
    );

}