import React, { useState, useEffect } from "react";
import Progress from './Progress'
import './ActionProgressCircle.css'
import styled from "styled-components";
import {
    TextField,
    Button,
    Grid,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    InputLabel,
    MenuItem,
    FormHelperText,
    FormControl,
    Select,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    IconButton,
    Snackbar,
    Paper,
    Alert as MuiAlert,
  } from "@mui/material";
  import AddIcon from "@mui/icons-material/Add";
  import RemoveIcon from '@mui/icons-material/Remove';
import { styled as muiStyled, createTheme, ThemeProvider} from '@mui/material/styles';
import { addAction, updateAction } from "../Common/Fetch";
import CircularProgressBar from "./CircularProgressBar";
import CircularProgressBarComplete from "./CircularProgressBarComplete";
import axios from 'axios';

const theme = createTheme({
});

const Item = muiStyled(Paper)(({ theme }) => ({
  padding: theme.spacing(1),
  textAlign: 'Center',
}));

const ActionsWrapper = styled.div`
  .header {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  && .MuiGrid-root {
    margin-left: 0;
  }

  .tasks {
    text-align: center;

    button {
      transition-duration: 0.3s;

      &:hover {
        background-color: var(--teal);
        text-decoration: line-through;
      }
    }
  }

  .add-tasks {
    text-align: center;
    padding: 10px;
  }

  .actions {
    padding: 0;
    list-style-type: none;
  }
`;

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

const flexContainer = {
  display: 'flex',
  flexDirection: 'row',
  width: '100px',
};

/**
 * @param {Object} props - props passed into react component
 * @param {Array} props.actions - array of objects representing subgoals and
 * associated actions
 * @param {Integer} props.subgoalIndex - integer representing the index of the subgoal
 * @param {Function} props.increment - function to increment action iteration
 * @returns {JSX} - React fragment with incomplete actions in a list with
 * increment buttons
 */
const showIncompleteActions = (actions, increment, decrement) => {
  if (Array.isArray(actions)){
    const incompleteActions = actions.filter((a) => a.completed == 'False');
    // const incompleteAction = actions
    console.log(incompleteActions)
    
    //   return actions.length > 0 ? (
    return incompleteActions.length > 0 ? (
      <>
        {/* <List> */}
          <div class="action-main-div">
          {incompleteActions.map((action) => {
            const percentage = Math.round(action.maxIterations ? (action.currentIterations / action.maxIterations) * 100 : 0);
            return (
              <>
              
                {/* <div className="progress"> */}
                  {/* <Progress task={task} /> */}
                  <div class="action-single-div">
                    <div class="action-name-single-div">
                      <p>{action.actionName}</p>
                    </div>
                    <div class="action-progress-single-div">
                      <p>Progress: {action.currentIterations} / {action.maxIterations}</p>
                    </div>
                    <CircularProgressBar percentageProp={percentage}/>
              
              {/* increment / decrement content */}

              <div class="incre-decre-icon-div">
                        <div class="single-icon-div">
                            <IconButton 
                                edge="start"
                                aria-label="remove"
                                onClick={() => decrement(actions.indexOf(action))}
                            >
                                <RemoveIcon />
                            </IconButton>
                          </div>
                          <div class="single-icon-div">
                            <IconButton 
                                edge="end"
                                aria-label="add"
                                onClick={() => increment(actions.indexOf(action))}
                            >
                                <AddIcon />
                            </IconButton>
                            </div>

                    </div>

                </div>
                {/* </div> */}



              {/* 
                <ListItem 
                key={actions.indexOf(action)}
                    secondaryAction={
                        <>
                        
                            <IconButton 
                                edge="start"
                                aria-label="remove"
                                onClick={() => decrement(actions.indexOf(action))}
                            >
                                <RemoveIcon />
                            </IconButton>
                          
                          
                            <IconButton 
                                edge="end"
                                aria-label="add"
                                onClick={() => increment(actions.indexOf(action))}
                            >
                                <AddIcon />
                            </IconButton>
                            
                        </>
                    }
                    disablePadding
                >
                  </ListItem> 
                 */}
        
              </>
            )})
          }

        </div>
              
        {/* </List> */}
        
      </>
    ) : (
      <p>No actions to display. Add some new ones!</p>
    );
  
  } else {
    return (
      <p>No actions to display. Add some new ones!</p>
    )
  }

  
  
  
};

const showCompletedActions = (actions) => {
  if (Array.isArray(actions)){
    const completedActions = actions.filter((a) => a.completed == 'True');
    // const incompleteAction = actions
    console.log(completedActions)
    
    //   return actions.length > 0 ? (
    return completedActions.length > 0 ? (
      <>
          <div class="action-main-div">
          {completedActions.map((action) => {
            const percentage = Math.round(action.maxIterations ? (action.currentIterations / action.maxIterations) * 100 : 0);
            return (
              <>
                <div class="action-single-div">
                  <div class="action-name-single-div">
                    <p>{action.actionName}</p>
                  </div>
                  <div class="action-progress-single-div">
                    <p>Completed: {action.currentIterations} / {action.maxIterations}</p>
                  </div>
                  <CircularProgressBarComplete percentageProp={percentage}/>
                </div>
              </>
            )})
          }

        </div>
        
      </>
    ) : (
      <p>No Completed actions to display.</p>
    );
  } else {
    return (
      <p>No Completed actions to display.</p>
    )
  }


};




export default function ActionProgressCircle() {
  const [actions, setActions] = useState([]);

  useEffect( () => {
    axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderGoals/',{
    username: localStorage.getItem("username"),
    token: localStorage.getItem("token"),
    })
    .then(response => {
        setActions(response.data['actions'])
        console.log("leader actions:")
        console.log(JSON.stringify(response.data))
    })
    .catch(err => {
    })

  },[])
  
    
    const [item, setItem] = useState("");
    const [iteration, setIteration] = useState(0);
    const [open, setOpen] = useState(false);
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarLimitOpen, setSnackbarLimitOpen] = useState(false);

    const handleSnackbarClose = (event, reason) => {
      if (reason === "clickaway") {
          return;
      }
      setSnackbarOpen(false);
    };

    const handleSnackbarLimitClose = (event, reason) => {
      if (reason === "clickaway") {
          return;
      }
      setSnackbarLimitOpen(false);
    };

    const [errors, setErrors] = useState({
    item: "",
    iteration: ""
    });

    const toggleOpen = () => {
    setItem("");
    setIteration(0);
    setOpen(!open);
    setErrors({
        item: "",
        iteration: ""
    });
    };

  const checkFields = () => {
    const errors = {
        item: "",
        iteration: ""
    };
    if (item.length === 0) {
        errors.item = "Action is required";
    }
    if (iteration <= 0) {
        errors.iteration = "Iteration must be greater than 0";
    }
    if (!Number.isInteger(Number.parseFloat(iteration))) {
        errors.iteration = "Iteration must be an integer";
    }

    return errors;
  };

  const checkActionsLimit = () => {
    if (Array.isArray(actions)){
      if ( (actions.filter((a) => a.completed == 'False')).length < 3 ) {
        return false
      } else {
        return true
      }
    } else {
      return false
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault();

    const limitReached = checkActionsLimit();

    if ( limitReached ) {
      setSnackbarLimitOpen(true);
      return
    }

    const curErrors = checkFields();
    if (
        curErrors.item !== "" ||
        curErrors.iteration !== ""
    ) {
        return setErrors(curErrors);
    }

    // Duplicate action name handling
    var highestActionNumber = 1
    var actionNumber  = 0
    var matchingActionExists = false
    if (Array.isArray(actions)){
      for (let i=0; i<actions.length; i++) {
        //Check if the new action name is equal to any previous action (while removing leading and trailing whitespaces)
        if (actions[i]['actionName'].trim() === item.trim() || actions[i]['actionName'].trim().slice(0, -3) === item.trim()) {
          matchingActionExists = true
          // If there is an action name that is the same, check if there is a number associated with that name and 
          // if it is the same or equal to the number we are about to give to the new duplicate action
          try {
            actionNumber = parseInt(actions[i]['actionName'].trim().slice(-1))
            if (actionNumber >= highestActionNumber) {
                highestActionNumber = actionNumber
            }
          } catch(err) {
              console.log(err)
          }
        }
      }
    }

    //Add one to the to-be-assigned number e.g. if existing highest is 2, new action will have a 3
    highestActionNumber += 1;
    var newActionName = item.trim()
    //Formatting new action name
    if (matchingActionExists) {
      newActionName = `${item} #${highestActionNumber}`;
    }
    

    const newAction = {
        actionName: newActionName,
        maxIterations: parseInt(iteration),
        currentIterations: 0,
        completed: 'False'
    };

    
    // Call API stuff - (Fetch.js)
    const _addAction = async (actionName, maxIterations) => {
        await addAction(actionName, maxIterations);
    };
    _addAction(newAction.actionName, newAction.maxIterations);

    //COME BACK TO THIS
    // const index = tasks.findIndex((t) => t.subgoal === goal);
    //   let updatedTasks = [...tasks];
    //   let updatedItem = { ...tasks[index] };
    // updatedTasks[index] = updatedItem;
    if (Array.isArray(actions)){
      let updatedActions = [...actions, newAction];
      setActions(updatedActions)
    }else {
      let updatedActions = [newAction];
      setActions(updatedActions)
    }
    
    setItem("");
    setOpen(!open);
  }

  const increment = (actionIndex) => {
    // let updatedTasks = [...tasks];
    // let updatedSubgoal = { ...tasks[subgoalIndex] };
    // let actions = [...tasks[subgoalIndex].actions];
    console.log("_______________________________________________________________")
    let updatedAction = actions[actionIndex];
    updatedAction.currentIterations += 1;
    // Call API stuff
    const _incrementAction = async (actionName, currentIterations) => {
        await updateAction(actionName, currentIterations);
    };
    _incrementAction(updatedAction.actionName, updatedAction.currentIterations);
    if (updatedAction.currentIterations === updatedAction.maxIterations) {
        updatedAction.completed = 'True';
        setSnackbarOpen(true);
    }
    let updatedActions = [...actions]
    updatedActions[actionIndex] = updatedAction;
    // updatedSubgoal.actions = actions;
    // updatedTasks[subgoalIndex] = updatedSubgoal;
    // setTasks(updatedTasks);
    setActions(updatedActions)
  };

  const decrement = (actionIndex) => {
    // let updatedTasks = [...tasks];
    // let updatedSubgoal = { ...tasks[subgoalIndex] };
    // let actions = [...tasks[subgoalIndex].actions];
    console.log("-------------------------------------------------------------")
    let updatedAction = actions[actionIndex];
    if (updatedAction.currentIterations > 0) {
      updatedAction.currentIterations -= 1;
    }
    // Call API stuff NOT YET
    const _decrementAction = async (actionName, currentIterations) => {
      await updateAction(actionName, currentIterations);
    };
    _decrementAction(updatedAction.actionName, updatedAction.currentIterations);
    if (updatedAction.currentIterations !== updatedAction.maxIterations) {
        updatedAction.completed = 'False';
        setSnackbarOpen(true);
    }
    let updatedActions = [...actions]
    updatedActions[actionIndex] = updatedAction;
    // updatedSubgoal.actions = actions;
    // updatedTasks[subgoalIndex] = updatedSubgoal;
    // setTasks(updatedTasks);
    setActions(updatedActions)
  };

    // const task = {
    //     subgoal: "hardcoded subgoal",
    //     actions:[{
    //         current_iterations:"5", 
    //         max_iterations:"10"
    //     }]
    // }

    return (
      <>
        <ActionsWrapper>
          <div className="add-tasks">
            <Button variant="outlined" onClick={toggleOpen}>
              + Add new action
            </Button>
            <Dialog fullWidth={true} maxWidth="md" open={open} onClose={toggleOpen}>
              <DialogTitle>Add a new action</DialogTitle>
              <DialogContent>
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <DialogContentText>
                      To add a new action, enter the name of the action and set how many times the action needs to be done to be completed.
                    </DialogContentText>
                  </Grid>
                  <Grid item xs={12} sm={8}>
                    <TextField
                      id="add-action"
                      label="New action"
                      fullWidth
                      required
                      onChange={(e) => setItem(e.target.value)}
                      error={errors.item !== ""}
                      helperText={errors.item}
                    />
                  </Grid>
                  <Grid item xs={12} sm={4}>
                    <TextField
                      id="outlined-number"
                      label="Number of Times to Complete"
                      type="number"
                      fullWidth
                      InputProps={{
                        inputProps: { min: 1 },
                      }}
                      onChange={(e) => setIteration(e.target.value)}
                      error={errors.iteration !== ""}
                      helperText={errors.iteration}
                    />
                  </Grid>
                  {/* <Grid item xs={12}>
                    <FormControl required fullWidth error={errors.goal !== ""}>
                      <InputLabel id="subgoal">Select subgoal</InputLabel>
                      <Select
                        labelId="select-subgoal-label"
                        id="select-subgoal"
                        value={goal}
                        label="Select subgoal *"
                        onChange={(e) => setGoal(e.target.value)}
                      >
                        <MenuItem value="">
                          <em>None</em>
                        </MenuItem>
                        {tasks.map((task) => (
                          // <MenuItem value={task.subgoal} key={task.subgoal}>
                          //   {task.subgoal}
                          // </MenuItem>
                        ))}
                      </Select>
                      {errors.goal !== "" && (
                        <FormHelperText>{errors.goal}</FormHelperText>
                      )}
                    </FormControl>
                  </Grid> */}
                </Grid>
              </DialogContent>
              <DialogActions>
                <Button onClick={toggleOpen}>Cancel</Button>
                <Button onClick={handleSubmit}>Add</Button>
              </DialogActions>
            </Dialog>
          </div>
          <div className="snackbar">
            <Snackbar
              open={snackbarOpen}
              autoHideDuration={6000}
              onClose={handleSnackbarClose}
            >
              <Alert
                onClose={handleSnackbarClose}
                severity="success"
                sx={{ width: "100%" }}
              >
                Completed an action!
              </Alert>
            </Snackbar>
          </div>
          <div className="snackbar">
            <Snackbar
              open={snackbarLimitOpen}
              autoHideDuration={6000}
              onClose={handleSnackbarLimitClose}
            >
              <Alert
                onClose={handleSnackbarLimitClose}
                severity="error"
                sx={{ width: "100%" }}
              >
                You can only have up to 3 actions at a time. Please complete an action to add more!
              </Alert>
            </Snackbar>
          </div>
          <Grid container wrap="wrap" columnSpacing={3} rowSpacing={2}>
              <Grid item xs={12} sm={12}>
                <div className="tasks">
                  <ul className="actions">
                    {showIncompleteActions(actions, increment, decrement)}
                  </ul>
                </div>
              </Grid>
          </Grid>

          <br/>
          <br/>
          <br/>
          <br/>

          <Grid container wrap="wrap" columnSpacing={3} rowSpacing={2}>
            <Grid item xs={12} sm={12}>
              <Item>Completed Actions</Item>
            </Grid>

              <Grid item xs={12} sm={12}>
                <div className="tasks">
                  <ul className="actions">
                    {showCompletedActions(actions)}
                  </ul>
                </div>
                
              </Grid>
              
          </Grid>
          
        </ActionsWrapper>
        
        </>
      );

}

