import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import './NavbarAdmin.css';
import { Route, useHistory } from "react-router-dom";


function NavbarAdmin() {
    let history = useHistory();
    const logout = event => {
        event.preventDefault();
        localStorage.removeItem("token");
        localStorage.removeItem("username");
        localStorage.removeItem("usertype");
        localStorage.removeItem("messageLeader");
        localStorage.removeItem("pusherTransportTLS");
        console.log("localStorage items removed");
        localStorage.setItem("refresh","true");
        history.push('/');
      }
    

    return (
        <>
                <div className="sidebar-coachpage" id="sidebarContainer">
                <nav className="navContainer-coachpage">
                    <div>

                        <div class="sidebar-menu-coachpage">
                            <a href="#" class="nav-links-coachpage">
                                <Link to='/adminpage'>
                                 <i class="fas fa-user nav-icon-coachpage"></i>
                                {/* <i class='bx bxs-user nav-icon-coachpage'></i> */}
                                <span class="nav-name-coachpage">Home</span></Link>
                            </a>

                            <a href="#" class="nav-links-coachpage">
                                <Link to='/adminlinkpage'>
                                <i class="fas fa-id-badge nav-icon-coachpage"></i>
                                {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                <span class="nav-name-coachpage">Linking</span></Link>
                            </a>

                            <a href="#" class="nav-links-coachpage">
                                <Link to='/adminunlinkpage'>
                                <i class="fas fa-id-badge nav-icon-coachpage"></i>
                                {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                <span class="nav-name-coachpage">Unlinking</span></Link>
                            </a>

                            <a href="#" class="nav-links-coachpage">
                                <Link to='/eoicoach'>
                                <i class="fas fa-id-badge nav-icon-coachpage"></i>
                                {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                <span class="nav-name-coachpage">EOI notifications</span></Link>
                            </a>

                            <a href="#" class="nav-links-coachpage">
                                <Link to='/registerCoachAdmin'>
                                <i class="fas fa-id-badge nav-icon-coachpage"></i>
                                {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                <span class="nav-name-coachpage">Register coach</span></Link>
                            </a>

                            <a href="#" class="nav-links-coachpage">
                                <Link to='/adminfeedback'>
                                <i class="fas fa-id-badge nav-icon-coachpage"></i>
                                {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                <span class="nav-name-coachpage">Feedback</span></Link>
                            </a>

                            <a href="#" class="nav-links-coachpage" onClick={logout}>
                                <Link to='/'>
                                <i class='bx bxs-comment-edit nav-icon-coachpage'></i>
                                <span class="nav-name-coachpage">Logout</span></Link>
                            </a>
                        </div>

                    </div>
                </nav>
            </div>
       
         </>
    );
}



export default NavbarAdmin;