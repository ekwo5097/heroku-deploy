import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import './NavbarLeader.css';
import { Route, useHistory } from "react-router-dom";
import axios from 'axios';


function NavbarLeader() {
    let history = useHistory();
    const logout = event => {
        event.preventDefault();
        localStorage.removeItem("token");
        localStorage.removeItem("username");
        localStorage.removeItem("usertype");
        localStorage.removeItem("messageLeader");
        localStorage.removeItem("pusherTransportTLS");
        console.log("localStorage items removed");
        localStorage.setItem("refresh","true");
       
        
      }
      const [matchedState, setMatchedState] = useState(false);

      useEffect(() => {
        // get leader is matched status
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderIsAssigned/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['msg'] === "isMatched found"){ 
                    if (response.data['isMatched'] === "True"){
                        setMatchedState(true);
                        matchedComplete();
                    }else{
                        matchedIncomplete();
                    }
                }else{
                    // Some error occurred when retrieving leader's matched_with_coach
                    console.log(response.data['msg'])

                }
                
            })
            .catch(err => {
            })
    }, [])

    

    function matchedIncomplete(){
        document.getElementById("coach-link").style.pointerEvents = "none";
        document.getElementById("feedback-link").style.pointerEvents = "none";
        document.getElementById("goals-link").style.pointerEvents = "none";

        document.getElementById("coach-lock").style.color = "gray";
        document.getElementById("feedback-lock").style.color = "gray";
        document.getElementById("goals-lock").style.color = "gray";

        document.getElementById("coach-text").style.color = "gray";
        document.getElementById("feedback-text").style.color = "gray";
        document.getElementById("goals-text").style.color = "gray";

        document.getElementById("coach-standard-icon").style.color = "gray";
        document.getElementById("feedback-standard-icon").style.color = "gray";
        document.getElementById("goals-standard-icon").style.color = "gray";
    }

    function matchedComplete(){
        document.getElementById("coach-link").style.pointerEvents = "auto";
        document.getElementById("feedback-link").style.pointerEvents = "auto";
        document.getElementById("goals-link").style.pointerEvents = "auto";

        document.getElementById("coach-lock").style.display = "none";
        document.getElementById("feedback-lock").style.display = "none";
        document.getElementById("goals-lock").style.display = "none";

        document.getElementById("coach-text").style.color = "#FFFFFF";
        document.getElementById("feedback-text").style.color = "#FFFFFF";
        document.getElementById("goals-text").style.color = "#FFFFFF";

        document.getElementById("coach-standard-icon").style.color = "#FFFFFF";
        document.getElementById("feedback-standard-icon").style.color = "#FFFFFF";
        document.getElementById("goals-standard-icon").style.color = "#FFFFFF";
    }
    

    return (
        <>
          <div className="sidebar-homepage" id="sidebarContainer">
                    <nav className="navContainer-homepage">
                        <div>
                            <a href="#" className="nav-links-homepage nav-logo-homepage">
                                {/*<i className='fas fa-th-list'></i>*/}
                                <span className="nav-logo-name-homepage">{/*Dashboard*/}</span>
                            </a>

                            <div class="sidebar-menu-homepage">
                                <a href="#" class="nav-links-homepage">
                                    <Link to='/userhome'>
                                    <i class="fas fa-user nav-icon-homepage"></i>
                                    {/* <i class='bx bxs-user nav-icon-coachpage'></i> */}
                                    <span class="nav-name-homepage">Home</span></Link>
                                </a>
                                <a href="#" class="nav-links-homepage" id="coach-link">
                                    <Link to='/coaches'>
                                    <i class="fas fa-id-badge nav-icon-homepage" id="coach-standard-icon"></i>
                                    {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                    <span class="nav-name-homepage" id="coach-text">Coach</span>
                                    <div class="nav-lock-div"><i class="fas fa-solid fa-lock nav-icon-homepage" id="coach-lock"></i></div></Link>
                                </a>
                                <a href="#" class="nav-links-homepage" id="feedback-link">
                                    <Link to='/feedback'>
                                    <i class="fas fa-clipboard nav-icon-homepage" id="feedback-standard-icon"></i>
                                    <span class="nav-name-homepage" id="feedback-text">Feedback</span>
                                    <div class="nav-lock-div"><i class="fas fa-solid fa-lock nav-icon-homepage" id="feedback-lock"></i></div></Link>
                                </a>
                                <a href="#" class="nav-links-homepage" id="goals-link">
                                    <Link to='/questionnaire'>
                                    <i class="fas fa-bullseye nav-icon-homepage" id="goals-standard-icon"></i>
                                    {/* <i class='bx bx-target-lock nav-icon-coachpage'></i> */}
                                    <span class="nav-name-homepage" id="goals-text">Update my Info</span>
                                    <div class="nav-lock-div"><i class="fas fa-solid fa-lock nav-icon-homepage" id="goals-lock"></i></div></Link>
                                </a>
                                {/* <a href="#" class="nav-links-homepage">
                                    <Link to='/questionnaire'>
                                     <i class="fas fa-bullseye nav-icon-homepage"></i> 
                                    <i class="fas fa-question-circle nav-icon-homepage"></i>
                                    <span class="nav-name-homepage">Questionnaire</span></Link>
                                </a> */}


                                {/* <a href="#" class="nav-links-homepage">
                                    <Link to='/settings'>
                                    <i class="fas fa-cog nav-icon-homepage"></i>
                                    <span class="nav-name-homepage">Settings</span></Link>
                                </a>

                                <a href="#" class="nav-links-homepage" onClick={logout}>
                                    <Link to='/'>
                                    <i class='bx bxs-comment-edit nav-icon-homepage'></i>
                                    <span class="nav-name-homepage-log">Logout</span></Link>
                                </a>  */}
                            </div>

                        </div>
                    </nav>
                </div>
       
        </>
    );
}



export default NavbarLeader;