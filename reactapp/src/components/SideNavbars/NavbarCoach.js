import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import './NavbarLeader.css';
import { Route, useHistory } from "react-router-dom";


function NavbarCoach() {
    let history = useHistory();
    const logout = event => {
        event.preventDefault();
        localStorage.removeItem("token");
        localStorage.removeItem("username");
        localStorage.removeItem("usertype");
        localStorage.removeItem("messageLeader");
        localStorage.removeItem("pusherTransportTLS");
        console.log("localStorage items removed");
        localStorage.setItem("refresh","true");
        history.push('/');
      }
    

    return (
        <>
          <div className="sidebar-coachpage" id="sidebarContainer">
                <nav className="navContainer-coachpage">
                    <div>


                        <div class="sidebar-menu-coachpage">
                            <a href="#" class="nav-links-coachpage">
                                <Link to='/userhome'>
                                 <i class="fas fa-user nav-icon-coachpage"></i>
                                <span class="nav-name-coachpage">Home</span></Link>
                            </a>
                            <a href="#" class="nav-links-coachpage">
                                <Link to='/coachees'>
                                <i class="fas fa-id-badge nav-icon-coachpage"></i>
                                <span class="nav-name-coachpage">Leaders</span></Link>
                            </a>
                            <a href="#" class="nav-links-coachpage">
                                <Link to='/coachfeedback'>
                                <i class="fas fa-clipboard nav-icon-coachpage"></i>
                                <span class="nav-name-coachpage">Feedback</span></Link>
                            </a>
                            <a href="#" class="nav-links-coachpage">
                                <Link to='/questionnairecoach'>
                                <i class="fas fa-bullseye nav-icon-homepage" id="goals-standard-icon"></i>
                                <span class="nav-name-coachpage">Update my Info</span></Link>
                            </a>

                            {/* <a href="#" class="nav-links-coachpage">
                                <Link to='/questionnaire'>
                                <i class="fas fa-question-circle nav-icon-coachpage"></i>
                                <span class="nav-name-coachpage">Questionnaire</span></Link>
                            </a> */}
                            {/* <a href="#" class="nav-links-coachpage">
                                <Link to='/settings'>
                                <i class="fas fa-cog nav-icon-coachpage"></i>
                                <span class="nav-name-coachpage">Settings</span></Link>
                            </a>
                            <a href="#" class="nav-links-coachpage" onClick={logout}>
                                <Link to='/'>
                                <i class='bx bxs-comment-edit nav-icon-coachpage'></i>
                                <span class="nav-name-coachpage">Logout</span></Link>
                            </a> */}
                        </div>

                    </div>
                </nav>
            </div>
        </>
    );
}



export default NavbarCoach;