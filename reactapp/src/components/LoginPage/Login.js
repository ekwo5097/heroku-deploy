import React, { useState } from "react";
import { useDispatch } from "react-redux";
import validator from "validator";
import { useHistory } from "react-router-dom";
import * as actions from '../../store/actions/auth';
import {
  Avatar,
  Button,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
  Alert,
} from "@mui/material";
import { NavLink } from "react-router-dom";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
// import {
//   login,
//   setFirstName,
//   setLastName,
//   setUserCoach,
//   setUserLeader,
//   setUserAdmin,
//   setUserId,
// } from "Reducers/UserSlice";
import validateDetails from "./ValidateLogin";


const Login = () => {
  const [failedAuth, setFailedAuth] = useState(false);
  const [details, setDetails] = useState({
    username: "", 
    email: "",
    password: "",
  });
  const [errors, setErrors] = useState({
    username: "", 
    email: "",
    password: "",
  });
  const dispatch = useDispatch();
  let history = useHistory()

  const checkEmail = (email) => {
    if (email === "" || validator.isEmail(email)) {
      setErrors({ ...errors, email: "" });
    } else {
      setErrors({ ...errors, email: "Not a valid email address" });
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const curErrors = validateDetails(details);
    if (curErrors.username !== "" || curErrors.email !== "" || curErrors.password !== "") {
      return setErrors(curErrors);
    }
    console.log("logging in");
    dispatch(actions.authLogin(details.username, details.email, details.password))
    setTimeout(function() {
      if(localStorage.getItem("usertype")=="A"){
        history.push('/userhome');
      }
      else if(localStorage.getItem("token")!=null){
        history.push('/userhome');
      }
    }, 2000);
  

   
};

  return (
    <div style={{
      backgroundColor: "#0C1C94", 
      height: "100%", 
      padding: "13%", 
      paddingTop: "2%",
      margin:"0"}}>
    <Container component="main" maxWidth="xs">
      <Box
        mt={{ xs: 0, sm: 4, md: 8, lg: 12 }}
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          border: 1,
          borderColor: "grey.500",
          padding: 6,
          bgcolor: "white",
          borderRadius: 6,
          boxShadow: 20
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "#2589f7" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log in
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="off"
              autoFocus
              onChange={(e) => setDetails({ ...details, username: e.target.value })}
              error={errors.username !== ""}
              helperText={errors.username}
            />
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="off"
            autoFocus
            onChange={(e) => setDetails({ ...details, email: e.target.value })}
            onBlur={(e) => checkEmail(e.target.value)}
            error={errors.email !== ""}
            helperText={errors.email}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) =>
              setDetails({ ...details, password: e.target.value })
            }
            onBlur={(e) => setErrors({ ...errors, password: e.target.value })}
            error={errors.password !== "" && details.password === ""}
            helperText={details.password === "" ? errors.password : ""}
          />
          {failedAuth && (
            <Alert severity="error">Invalid login credentials</Alert>
          )}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Log In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link component={NavLink} to="/register" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
            <Grid item>
              <Link component={NavLink} to="/forgot" variant="body2">
                {"I forgot my password"}
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
      loading: state.loading,
      error: state.error
  }
}

const mapDispatchToProps = dispatch => {
  return {
      onAuth: (username, email, password) => dispatch(actions.authLogin(username, email, password))
  }
}

export default Login;
