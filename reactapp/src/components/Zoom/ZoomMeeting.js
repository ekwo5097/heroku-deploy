import React, { useState, useEffect } from "react";
import axios from 'axios';
import { Button as StyledButton }  from 'antd';
import '../LandingPage/Cards.css';

import ZoomSDKTest from './ZoomSDKTest';


export default function ZoomMeeting(){

    const [meetingLink, setMeetingLink] = React.useState("");
    const [meetingId, setMeetingId] = React.useState("");
    const [meetingPassword, setMeetingPassword] = React.useState("");

    const getZoomMeeting = event => {
        event.preventDefault();
        axios.post('https://new-emerge-website.herokuapp.com/api/createMeeting/',{
            }, 
            )
            .then(response => {
                console.log(JSON.stringify(response.data.meetingLink));
                console.log(JSON.stringify(response.data.meetingId));
                console.log(JSON.stringify(response.data.meetingPassword));

                setMeetingLink(response.data.meetingLink);
                setMeetingId(response.data.meetingId)
                setMeetingPassword(response.data.meetingPassword)

                //history.push('/');
            })
    }


  return (
    <>
    
  


    <div className='cards'>
                <h1>Zoom Meeting</h1>
                <div className='cards_container'>
                    <div className='cards_wrapper'>
                        <ul className='cards_items'>
                            <p>
                                Click on this <a href={meetingLink}>
                                 meeting </a> to join with password {meetingPassword}
                                 Meeting ID: {meetingId}
                            </p>
                        </ul>
                        
                    </div>
            

                <StyledButton type="primary" size= "large" style={{ background: "#white", borderColor: "#0C1C94" }} onClick={getZoomMeeting}>
                   Get Link
                </StyledButton>

                {/* <ZoomSDKTest meetingId={meetingId} meetingUsername="Test1" meetingPassword={meetingPassword} /> */}
                    
                </div>
            </div>
    
    </>
);


}
