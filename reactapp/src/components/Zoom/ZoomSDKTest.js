import React, { useState, useEffect } from "react"
import axios from 'axios';
import ZoomMtgEmbedded from '@zoomus/websdk/embedded';




    

export default function ZoomSDKTest({meetingId, meetingUsername, meetingPassword}) {

  // var apiKey = 'CPAOC0DAQ4eBFPTXOx5sjw'
  // var apiSecret = 'sDwFiYqVRdAyOzd0k4qZeSS3joVpA2h0XNMp'


  var apiKey = 'NOmAl_DoS4CBhddzSwsh1A'
  var apiSecret = 'L3MINDLPWXD4VhP0RynqKXFVgXrATGJ3eOeb'

  // var meetingNumber = 85236780914
  // var role =0;
  // var userName = 'WebSDK'
  // var password = 'T2rLrg'

  const [signature, setSignature] = useState();

  const generateSignature = () => {
    console.log(meetingId)
    console.log(meetingUsername)
    console.log(meetingPassword)


    axios.post('https://new-emerge-website.herokuapp.com/api/generateSignature/', {
      apiKey: apiKey,
      apiSecret: apiSecret,
      role: 0, //0 for joining
      meetingNumber: meetingId
    })
    .then(response => {
        console.log("signature is:" + JSON.stringify(response.data.ret))
        setSignature(response.data.ret)
    })
    .catch(err => {
    });
  }


  const client = ZoomMtgEmbedded.createClient();
  const [meetingSDKElement, setMeetingSDKElement] = useState();
  useEffect( () => {
    setMeetingSDKElement(document.getElementById('meetingSDKElement'));
  }, []);
    

    
  const joinMeeting = () => {
    generateSignature()

    client.init({
      debug: true,
      zoomAppRoot: meetingSDKElement,
      language: 'en-US',
      customize: {
        meetingInfo: ['topic', 'host', 'mn', 'pwd', 'telPwd', 'invite', 'participant', 'dc', 'enctype'],
        toolbar: {
          buttons: [
            {
              text: 'Custom Button',
              className: 'CustomButton',
              onClick: () => {
                console.log('custom button');
              }
            }
          ]
        }
      }
    });
      
    client.join({
      apiKey: apiKey,
      signature: signature,
      meetingNumber: meetingId,
      password: meetingPassword,
      userName: meetingUsername
    });
    
  }


    

  return (
      <>
          <button onClick={joinMeeting}>Join Meeting</button>

          <div id="meetingSDKElement"></div>
      </>
  )
}