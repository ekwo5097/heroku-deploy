
import React, { useState } from "react";
import validator from "validator";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useEffect } from "react";
import axios from 'axios';
import {
  Avatar,
  Button,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
//import { login, setUserCoach, setUserLeader } from "Reducers/UserSlice";
import validateDetails from "./ValidateInterest";

function RegisterInterest() {

 
    const [details, setDetails] = useState({
        fields: ["name","email", "message"],
        message: "",
        email: "",
        name: "",
      });
    

    
      const [errors, setErrors] = useState({
        name: "",
        message: "",
        email: "",
      });
      const dispatch = useDispatch();
      let history = useHistory();
     
    
      const labels = {
        message: "Description of your interest",
        email: "Email Address",
        name: "Name",
      };
    
      const checkEmail = (email) => {
        if (email != "" || validator.isEmail(email)) {
          setErrors({ ...errors, email: "" });
        } else {
          setErrors({ ...errors, email: "Not a valid email address" });
        }
      };

      const checkName = (name) => {
        if (name != "") {
          setErrors({ ...errors, name: "" });
        } else {
          setErrors({ ...errors, name: "Name shouldn't be empty" });
        }
      };
    
      const checkMessage = (message) => {
        if (message === "" || message.length >= 6) {
          setErrors({ ...errors, message: "" });
        } else {
          setErrors({
            ...errors,
            message: "Message must not be empty",
          });
        }
      };

    
      const checkFields = (field, e) => {
        const val = e.target.value;
        if(field == "name"){
            checkName(val);
        }
        else if (field === "email") {
          checkEmail(val);
        } else if (field === "message") {
          checkMessage(val);
        } else {
          setErrors({
            ...errors,
            [field]: "",
          });
        }
      };
    
      // creating input fields for signup form
      const inputFields = details.fields.map((field, key) => {
        const sm = field === "firstName" || field === "lastName" ? 6 : 12;
        const confidential = ["password", "passwordAgain"];
        return (
          <React.Fragment key={key}>
            <Grid item xs={12} sm={sm}>
              <TextField
                autoComplete={confidential.includes(field) ? "new-password" : "off"}
                name={field}
                label={labels[field]}
                type={confidential.includes(field) ? "password" : "text"}
                id={field}
                required
                fullWidth
                onChange={(e) =>
                  setDetails({ ...details, [field]: e.target.value })
                }
                onBlur={(e) => checkFields(field, e)}
                error={errors[field] !== ""}
                helperText={errors[field]}
              />
            </Grid>
          
          </React.Fragment>
        );
      });


    const handleSubmit = e => {
        e.preventDefault();
        const curErrors = validateDetails(details);
        if (
        curErrors.name !== "" ||
        curErrors.message !== "" ||
        curErrors.email !== "" 
        ) {
        return setErrors(curErrors);
        }
      
        
        axios.post('https://new-emerge-website.herokuapp.com/api/setEOICoach/',{
        name: details.name,
        email: details.email,
        message: details.message
        },
        )
        .then(response => {
            console.log(response.data);
            alert(response.data.out);
            history.push('/');
        })
        .catch(err => {
        })
    }
    
    


      
       
    
        

    //render() {
    return (
      <div style={{
        backgroundColor: "#0C1C94", 
        height: "100%", 
        padding: "7%", 
        paddingTop: "2%",
        margin:"0"}}>
      <Container component="main" maxWidth="xs">
      <Box
        mt={{ xs: 0, sm: 4, md: 8, lg: 12 }}
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          border: 1,
          borderColor: "grey.500",
          padding: 5,
          bgcolor: "white",
          borderRadius: 5,
          boxShadow: 20
        }}
      >
        <Avatar sx={{ m: 1, bgcolor: "#2589f7" }}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Register Interest
        </Typography>
        <Box
          component="form"
          noValidate
          onSubmit={handleSubmit}
          mt={{ xs: 2, sm: 3 }}
        >
          <Grid container spacing={{ xs: 1, sm: 2 }}>
            {inputFields}
          </Grid>

          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Register Interest as a Coach
          </Button>
          <Grid container>
            <Grid item xs>
              <Link component={NavLink} to="/login" variant="body2">
                Already have an account? Log in
              </Link>
            </Grid>
            <Grid item>
              <Link component={NavLink} to="/register" variant="body2">
                Interested as a leader?
              </Link>
            </Grid>
          </Grid>
        </Box>
      </Box>
    </Container>
    </div>
    );
    };





export default RegisterInterest;

