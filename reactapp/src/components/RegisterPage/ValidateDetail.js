import validator from "validator";

// function to error check submission details
const validateDetails = (details) => {
  const curErrors = {
    password: "",
    passwordAgain: "",
    email: "",
    username:"",
  };

  if (details.password === "") {
    curErrors.password = "Password is required";
  } else if (details.password.length < 6) {
    curErrors.password = "Password must be at least 6 characters long";
  }
  if (details.passwordAgain === "") {
    curErrors.passwordAgain = "Matching password is required";
  } else if (details.password !== details.passwordAgain) {
    curErrors.passwordAgain = "Passwords did not match";
  }
  if (details.username == "") {
    curErrors.username ="Username is required";
  }
  if (details.email === "") {
    curErrors.email = "Email is required";
  } else if (!validator.isEmail(details.email)) {
    curErrors.email = "Not a valid email address";
  }
  return curErrors;
};

export default validateDetails;
