
import React, { useState } from "react";
import PasswordStrengthBar from "react-password-strength-bar";
import validator from "validator";
import { useDispatch } from "react-redux";
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { connect } from 'react-redux';
import NavbarAdmin from '../SideNavbars/NavbarAdmin';
import * as actions from '../../store/actions/auth';
import {
  Avatar,
  Button,
  TextField,
  Link,
  Grid,
  Box,
  Typography,
  Container,
} from "@mui/material";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
//import { login, setUserCoach, setUserLeader } from "Reducers/UserSlice";
import validateDetails from "./ValidateDetail";

function RegisterCoachAdmin(props){


    const [details, setDetails] = useState({
        fields: ["username","email", "password", "passwordAgain"],
        password: "",
        passwordAgain: "",
        email: "",
        username: "",
      });
    
      const [errors, setErrors] = useState({
        password: "",
        passwordAgain: "",
        email: "",
        username:"",
      });
      //const dispatch = useDispatch();
      let history = useHistory();
    
      const labels = {
        password: "Password",
        passwordAgain: "Re-enter password",
        email: "Email Address",
        username: "Username",
      };
    
      const checkEmail = (email) => {
        if (email === "" || validator.isEmail(email)) {
          setErrors({ ...errors, email: "" });
        } else {
          setErrors({ ...errors, email: "Not a valid email address" });
        }
      };
    
      const checkPassword = (password) => {
        if (password === "" || password.length >= 6) {
          setErrors({ ...errors, password: "" });
        } else {
          setErrors({
            ...errors,
            password: "Password must be at least 6 characters long",
          });
        }
      };
    
      const checkFields = (field, e) => {
        const val = e.target.value;
        if (field === "email") {
          checkEmail(val);
        } else if (field === "password") {
          checkPassword(val);
        } else {
          setErrors({
            ...errors,
            [field]: "",
          });
        }
      };
    
      // creating input fields for signup form
      const inputFields = details.fields.map((field, key) => {
        const sm = field === "firstName" || field === "lastName" ? 6 : 12;
        const confidential = ["password", "passwordAgain"];
        return (
          <React.Fragment key={key}>
            <Grid item xs={12} sm={sm}>
              <TextField
                autoComplete={confidential.includes(field) ? "new-password" : "off"}
                name={field}
                label={labels[field]}
                type={confidential.includes(field) ? "password" : "text"}
                id={field}
                required
                fullWidth
                onChange={(e) =>
                  setDetails({ ...details, [field]: e.target.value })
                }
                onBlur={(e) => checkFields(field, e)}
                error={errors[field] !== ""}
                helperText={errors[field]}
              />
            </Grid>
            {field === "password" && (
              <Grid item xs={12}>
                <PasswordStrengthBar password={details.password} />
              </Grid>
            )}
          </React.Fragment>
        );
      });

    const handleSubmit = e => {
        e.preventDefault();
        const curErrors = validateDetails(details);
        if (
        curErrors.password !== "" ||
        curErrors.passwordAgain !== "" ||
        curErrors.email !== "" ||
        curErrors.username !== ""
        ) {
        return setErrors(curErrors);
        }
      
        props.onAuth(
          details.username,
          details.email,
          details.password,
          details.passwordAgain
      );
      history.push('/adminpage');

      };

    //render() {
    return (
      <div style={{
        backgroundColor: "#0C1C94", 
        height: "100%", 
        padding: "13%", 
        paddingTop: "2%",
        margin:"0"}}> 
        <NavbarAdmin />

        <Container component="main" maxWidth="xs">
        <Box
          mt={{ xs: 0, sm: 4, md: 8, lg: 12 }}
          sx={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            border: 1,
            borderColor: "grey.500",
            padding: 6,
            bgcolor: "white",
            borderRadius: 6,
            boxShadow: 20
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: "#2589f7" }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign up a coach
          </Typography>
          <Box
            component="form"
            noValidate
            onSubmit={handleSubmit}
            mt={{ xs: 2, sm: 3 }}
          >
            <Grid container spacing={{ xs: 1, sm: 2 }}>
              {inputFields}
            </Grid>
  
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Create Coach user
            </Button>
            
          </Box>
        </Box>
      </Container>
      </div>
    );
    }
    const mapStateToProps = (state) => {
      return {
          loading: state.loading,
          error: state.error
      }
  }
  
  const mapDispatchToProps = dispatch => {
      return {
          onAuth: (username, email, password1, password2) => dispatch(actions.coachSignupAdmin(username, email, password1, password2)) 
      }
  }
  
  
  // export default Register;
  export default connect(mapStateToProps, mapDispatchToProps)(RegisterCoachAdmin);