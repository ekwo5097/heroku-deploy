import validator from "validator";

const validateDetails = (details) => {
  const curErrors = {
    name: "",
    email: "",
    message: "",
  };
  if (details.email === "") {
    curErrors.email = "Email is required";
  } else if (!validator.isEmail(details.email)) {
    curErrors.email = "Not a valid email address";
  }
  if (details.message === "") {
    curErrors.message = "Description of interest is required";
  }
  if(details.name == ""){
    curErrors.name= "Name is required";
  }
  return curErrors;
};

export default validateDetails;