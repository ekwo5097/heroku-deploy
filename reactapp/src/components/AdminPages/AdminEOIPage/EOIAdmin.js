import axios from 'axios';
import React, { useState, useEffect } from "react"
import NavbarAdmin from "../../SideNavbars/NavbarAdmin";
import { useHistory } from "react-router-dom";
import './EOIAdmin.css';



function EOIAdmin() {


    const [allEOICoaches, setAllEOICoaches] = useState([]);
    const[indexDisplayed,setIndexDisplayed]=useState(0);
    let history = useHistory();

    useEffect(() => {
        
        axios.post('https://new-emerge-website.herokuapp.com/api/getEOICoach/', {
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            userType: "A"
        })
            .then(response => {
                setAllEOICoaches(response.data.eoiCoach);
                console.log(JSON.stringify(response.data.eoiCoach));
            })
            .catch(err => {
            });
        },[]);

        function handleDelete(eoiCoach) {
            axios.post('https://new-emerge-website.herokuapp.com/api/deleteEOICoach/', {
            email: eoiCoach.email,
            name:eoiCoach.name,
            message:eoiCoach.message
            })
            .then(response => {
                // history.push('/');
            })
            .catch(err => {
            });       
        }
        

    return (
        <>
        <NavbarAdmin/>

        <div class="eoiCoachContainer">
            <h2 class="eoiCoachTitle">Expression of Interest</h2>
            <h4 class="eoiCoachDesc">The following shows a list of people who are interested in registering as a coach</h4>

                <form name="eoiCoachForm">
                    
                        {allEOICoaches.map(eoiCoach => (
                            <>
                                <div className="eoiInfo">
                                    <p>Name: {eoiCoach.name} </p>
                                    <p>Email: {eoiCoach.email}</p>   
                                    <p>Message: {eoiCoach.message} </p>                 
                                    {/* <button type="submit" class="btn deleteBtnEoiAdminPage" onClick={handleSubmit(eoiCoach)} >Delete Coach</button> */}
                                </div>
                                <button type="submit" class="btn deleteBtnEoiAdminPage" onClick={() => handleDelete(eoiCoach)} >Delete Coach</button> 
                            </>
                        ))}
                         {/* <button onClick={addToDisplay}>Load MORE</button> */}
                </form>
                
                 {/* <form name="leader-name-form">
                    <fieldset>
                        {allEOICoaches.map(eoiCoach => (
                            <>
                            <h4>{eoiCoach.name} {eoiCoach.email} {eoiCoach.message} </h4>                    
                            <button type="submit" class="btn deleteBtnAdminPage">Delete Coach</button>
                                    
                            </>
                        ))}
                    </fieldset>
                </form> */}
        </div>
                                        

        </>
    )
    }


export default EOIAdmin;