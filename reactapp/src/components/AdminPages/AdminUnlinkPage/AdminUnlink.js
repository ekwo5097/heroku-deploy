
import React, { useState, useEffect } from "react"
import axios from 'axios';
import './AdminUnlink.css';
import { Link } from 'react-router-dom';
import { Route, useHistory } from "react-router-dom";
import NavbarAdmin from "../../SideNavbars/NavbarAdmin";

export default function AdminUnlink() {


    const [allLeaders, setAllLeaders] = useState([]);
    const [allCoaches, setAllCoaches] = useState([]);

    useEffect(() => {
        // if (localStorage.getItem('usertype')=="L") {
        // first post for leader's next schedule
        axios.post('https://new-emerge-website.herokuapp.com/api/getAssignedLeadersAndCoaches/', {
            // username: localStorage.getItem("username"),
            // token: localStorage.getItem("token"),
            // userType: "A"
        })
            .then(response => {
                setAllLeaders(response.data.leaders)
                setAllCoaches(response.data.coaches)
                console.log(JSON.stringify(response.data.leaders))
                console.log(JSON.stringify(response.data.coaches))
            })
            .catch(err => {
            });

    }, [])

     /*    Functions for displaying coachee name      */

        // generate id based on coachee count
    function idFunction()
    {
            if ( typeof idFunction.c == 'undefined' ) {
                idFunction.c = 0;
            }


    }



    // generate id based on coachee count
    /*}
    function idFunction() {
        if (typeof idFunction.c == 'undefined') {
            idFunction.c = 0;
        }

        ++idFunction.c;
        var nId = "radio" + idFunction.c.toString()

        return nId;
    }*/

    function idFunctionCoach() {
        if (typeof idFunctionCoach.c == 'undefined') {
            idFunctionCoach.c = 0;
        }

        ++idFunctionCoach.c;
        var nId = "radio" + idFunctionCoach.c.toString()

        return nId;
    }


    function setLeader(leaderName) {
        localStorage.setItem('selectedLeader', leaderName);
    }

    // generate value based on coachee count
    function valueFunction() {
        if (typeof valueFunction.c == 'undefined') {
            valueFunction.c = 0;
        }

        ++valueFunction.c;
        var vFunction = "radio" + valueFunction.c.toString()

        return vFunction;
    }

    function valueFunctionCoach() {
        if (typeof valueFunctionCoach.c == 'undefined') {
            valueFunctionCoach.c = 0;
        }

        ++valueFunctionCoach.c;
        var vFunction = "radio" + valueFunctionCoach.c.toString()

        return vFunction;
    }

    // generate name based on coachee count
    function nameFunction() {
        if (typeof nameFunction.c == 'undefined') {
            nameFunction.c = 0;
        }

        ++nameFunction.c;
        var nm = "radio" + nameFunction.c.toString()

        return nm;
    }

    function nameFunctionCoach() {
        if (typeof nameFunctionCoach.c == 'undefined') {
            nameFunctionCoach.c = 0;
        }

        ++nameFunctionCoach.c;
        var nm = "radio" + nameFunctionCoach.c.toString()

        return nm;
    }

    function getLeaderName(leaderName) {
        localStorage.setItem('selectedLeader', leaderName);
        var rbs = document.querySelectorAll('div[className="coachee-info-block"]');
        let selectedValue;
        for (const rb of rbs) {
            // if (rb.checked) {
            //     selectedValue = rb.value;
            console.log(rb)
            //     break;
            // }
        }
        try {
            var selected = selectedValue.replace('radio', '');
        }
        catch
        {
            //console.log("error: radio button string unable to be replaced.");
            return;
        }

        // locate and match coachee info number from array
        var blocks = document.getElementsByClassName("coachee-info-block");
        var blockName = "";
        var matchedBlock = "";
        for (const block of blocks) {
            blockName = block.getAttribute('name')
            try {
                var blockNum = blockName.replace('coachee-block-', '');
                if (blockNum == selected) {
                    matchedBlock = blockName;
                    break;
                }
            }
            catch
            {
                //console.log("error: coachee-block-x string unable to be replaced.");
                continue;
            }
        }

        // set unmatched coachee div to 'none', matched div with 'block'
        //console.log("BLOCKNAME: " + blockName)
        var allCoacheeBlock = document.querySelectorAll('.coachee-info-block');
        for (const item of allCoacheeBlock) {
            //console.log(item.getAttribute('name'));
            if (item.getAttribute('name') == blockName) {
                item.style.display = 'block';
            }
            else {
                item.style.display = 'none';
            }
        }
    }

    function getCoachName(coachName) {
        localStorage.setItem('selectedCoach', coachName);
        var rbs = document.querySelectorAll('div[className="coach-info-block"]');
        let selectedValue;
        for (const rb of rbs) {
            // if (rb.checked) {
            //     selectedValue = rb.value;
            console.log(rb)
            //     break;
            // }
        }
        try {
            var selected = selectedValue.replace('radio', '');
        }
        catch
        {
            //console.log("error: radio button string unable to be replaced.");
            return;
        }

        // locate and match coachee info number from array
        var blocks = document.getElementsByClassName("coach-info-block");
        var blockName = "";
        var matchedBlock = "";
        for (const block of blocks) {
            blockName = block.getAttribute('name')
            try {
                var blockNum = blockName.replace('coach-block-', '');
                if (blockNum == selected) {
                    matchedBlock = blockName;
                    break;
                }
            }
            catch
            {
                //console.log("error: coachee-block-x string unable to be replaced.");
                continue;
            }
        }

        // set unmatched coachee div to 'none', matched div with 'block'
        //console.log("BLOCKNAME: " + blockName)
        var allCoachBlock = document.querySelectorAll('.coach-info-block');
        for (const item of allCoachBlock) {
            //console.log(item.getAttribute('name'));
            if (item.getAttribute('name') == blockName) {
                item.style.display = 'block';
            }
            else {
                item.style.display = 'none';
            }
        }


    }


    // retrieve button data on selected coachee, which is used to link with leader-info
    function setLeaderDataActive() {


        // var leaderInfoDisplay = document.querySelector('div[name="leader-info-list"]').style.visibility;
        // console.log(leaderInfoDisplay);

        var rbs = document.querySelectorAll('input[name="coachee"]');
        let selectedValue;
        for (const rb of rbs) {
            if (rb.checked) {
                selectedValue = rb.value;
                break;
            }
        }

        // get coachee info number
        try {
            var selected = selectedValue.replace('radio', '');
        }
        catch
        {
            //console.log("error: radio button string unable to be replaced.");
            return;
        }

        // locate and match coachee info number from array
        var blocks = document.getElementsByClassName("coachee-info-block");
        var blockName = "";
        var matchedBlock = "";
        for (const block of blocks) {
            blockName = block.getAttribute('name')
            try {
                var blockNum = blockName.replace('coachee-block-', '');
                if (blockNum == selected) {
                    matchedBlock = blockName;
                    break;
                }
            }
            catch
            {
                //console.log("error: coachee-block-x string unable to be replaced.");
                continue;
            }
        }

        // set unmatched coachee div to 'none', matched div with 'block'
        //console.log("BLOCKNAME: " + blockName)
        var allCoacheeBlock = document.querySelectorAll('.coachee-info-block');
        for (const item of allCoacheeBlock) {
            //console.log(item.getAttribute('name'));
            if (item.getAttribute('name') == blockName) {
                item.style.display = 'block';
            }
            else {
                item.style.display = 'none';
            }
        }

    }

    function setCoachDataActive() {

        var rbs = document.querySelectorAll('input[name="coach"]');
        let selectedValue;
        for (const rb of rbs) {
            if (rb.checked) {
                selectedValue = rb.value;
                break;
            }
        }

        // get coachee info number
        try {
            var selected = selectedValue.replace('radio', '');
        }
        catch
        {
            console.log("error: radio button string unable to be replaced.");
            return;
        }

        // locate and match coachee info number from array
        var blocks = document.getElementsByClassName("coach-info-block");
        var blockName = "";
        var matchedBlock = "";
        for (const block of blocks) {
            blockName = block.getAttribute('name')
            try {
                var blockNum = blockName.replace('coach-block-', '');
                if (blockNum == selected) {
                    matchedBlock = blockName;
                    break;
                }
            }
            catch
            {
                console.log("error: coachee-block-x string unable to be replaced.");
                continue;
            }
        }

        // set unmatched coachee div to 'none', matched div with 'block'
        //console.log("BLOCKNAME: " + blockName)
        var allCoachBlock = document.querySelectorAll('.coach-info-block');
        for (const item of allCoachBlock) {
            console.log(item.getAttribute('name'));
            if (item.getAttribute('name') == blockName) {
                item.style.display = 'block';
            }
            else {
                item.style.display = 'none';
            }
        }

    }


    // sets coachee-info display block based on counter
    function coacheeBlockDisplay(name) {
        if (typeof coacheeBlockDisplay.c == 'undefined' || coacheeBlockDisplay.dict == 'undefined') {
            coacheeBlockDisplay.c = 0;
            coacheeBlockDisplay.dict = {};
        }

        // function to store which coachee blocks are already displayed
        // check if name already exists in dictionary
        if (name in coacheeBlockDisplay.dict) {
            // return the coachee-block with the number associated with the leader name
            var cBlock = "coachee-block-" + coacheeBlockDisplay.dict[name];
            //console.log("EXISTING leader block name: " + coacheeBlockDisplay.dict[name]);
            //console.log("cBlock: " + cBlock);
            return cBlock;
        }

        ++coacheeBlockDisplay.c;
        coacheeBlockDisplay.dict[name] = coacheeBlockDisplay.c;
        var cBlock = "coachee-block-" + coacheeBlockDisplay.dict[name];
        //console.log("NEW leader block name: " + coacheeBlockDisplay.dict[name]);
        //console.log("cBlock: " + cBlock);
        return cBlock;
    }

    function coachBlockDisplay(name) {
        if (typeof coachBlockDisplay.c == 'undefined' || coachBlockDisplay.dict == 'undefined') {
            coachBlockDisplay.c = 0;
            coachBlockDisplay.dict = {};
        }

        // function to store which coachee blocks are already displayed
        // check if name already exists in dictionary
        if (name in coachBlockDisplay.dict) {
            // return the coachee-block with the number associated with the leader name
            var cBlock = "coach-block-" + coachBlockDisplay.dict[name];
            //console.log("EXISTING leader block name: " + coacheeBlockDisplay.dict[name]);
            console.log("cBlock: " + cBlock);
            return cBlock;
        }

        ++coachBlockDisplay.c;
        coachBlockDisplay.dict[name] = coachBlockDisplay.c;
        var cBlock = "coach-block-" + coachBlockDisplay.dict[name];
        //console.log("NEW leader block name: " + coacheeBlockDisplay.dict[name]);
        console.log("cBlock: " + cBlock);
        return cBlock;
    }

    function handleUnlinkCoach() {

        axios.post('https://new-emerge-website.herokuapp.com/api/unlinkLeaderToCoach/', {
            // username: localStorage.getItem("username"),
            // token: localStorage.getItem("token"),
            leaderUsername: localStorage.getItem("selectedLeader")
        })
            .then(response => {

                console.log(JSON.stringify(response.data.Success))
            })
            .catch(err => {
            })

    }
    
    if(localStorage.getItem('usertype')=="A") {
    return (
        <>
        <NavbarAdmin/>
            <div class="adminPageContainer">
                <h1>Admin Unlinking Page</h1>
                {/* <div className="logoutBtnContainer">
                  <a href="#" onClick={logout}>
                      <i class='bx bxs-comment-edit nav-icon-homepage'></i>
                      <span class="adminpage-logout">Logout</span>
                  </a>
                </div> */}

                <div class="leaderCoachContainer">

                    <div class="leaderColumn">
                        <h2 class="columnTitle">Leaders</h2>
                        <form name="leader-name-form" onClick={setLeaderDataActive}>

                            <fieldset>
                                {allLeaders.map(leader => (
                                    <>

                                        <div className="leader leaderContent" id="leaderContent">
                                            <input type="radio" name="coachee" id={idFunction()} onClick={() => getLeaderName(leader.username)} value={valueFunction()} />
                                            <label for={nameFunction()}> {leader.username} </label>
                                        </div>

                                        {/* <div className="coachee-info-block" name={localStorage.getItem('selectedLeader')}> */}
                                        <div className="coachee-info-block" id="infoContainer" name={coacheeBlockDisplay(leader.username)}>
                                            <p class="leaderCoachText">Age: {leader.info.age}</p>
                                            <p class="leaderCoachText">Gender: {leader.info.gender}</p>
                                            <p class="leaderCoachText">Industry: {leader.info.industry}</p>
                                            <p class="leaderCoachText">Days Available: {leader.info.day}</p>
                                            <p class="leaderCoachText">Company: {leader.info.company}</p>
                                            <p class="leaderCoachText">Years in Current Role at Work: {leader.info.years_of_employment}</p>
                                            <p class="leaderCoachText">Months in Current Role: {leader.info.months_in_role}</p>
                                            <p class="leaderCoachText">Coaching Goal: {leader.info.mainCoachingGoal}</p>
                                            <p class="leaderCoachText">Assigned Coach: {leader.info.assignedTo}</p>
                                            <p class="leaderCoachText">Subscription Level: {leader.info.subscription_type}</p>

                                            <div class="coachList">
                                                {/* <p class="listOfCoachesSubHeading">Assign to a Coach</p>

                                                <select id="assignedCoach">
                                                    {allCoaches.map(coach => (
                                                        <>
                                                            <option value={coach.username}> {coach.username} </option>
                                                        </>
                                                    ))}

                                                </select> */}


                                                <button type="submit" class="btn submitAdminPage" onClick={handleUnlinkCoach}>Unlink</button>

                                            </div>
                                        </div>

                                        {/* <div class="coachList">
                                            <p>List of Coaches</p>

                                            <select id="assignedCoach">
                                                {allCoaches.map(coach => (
                                                    <>
                                                        <option value={coach.username}> {coach.username} </option>
                                                    </>
                                                ))}

                                            </select>


                                            <button type="submit" class="btn scheduleSubmit" onClick={handleAssignCoach}>Submit</button>

                                        </div>

                                        <button type="submit" class="btn scheduleSubmit" onClick={handleDeleteLeader}>Delete Leader</button> */}

                                    </>
                                ))}
                            </fieldset>




                        </form>
                    </div>

                    <div class="coachColumn">
                        <h2 class="columnTitle">Coaches</h2>
                        <form name="leader-name-form" onClick={setCoachDataActive}>


                            <fieldset>
                                {allCoaches.map(coach => (
                                    <>

                                        <div className="leader leaderContent" id="leaderContent">
                                            {/* <input type="radio" name="coachee" id={idFunction()} onClick={() => getCoachName(coach.username)} value={valueFunction()} />
                                            <label for={nameFunction()}> {coach.username} </label> */}
                                            <input type="radio" name="coach" id={idFunctionCoach()} onClick={() => getCoachName(coach.username)} value={valueFunctionCoach()} />
                                            <label for={nameFunctionCoach()}> {coach.username} </label>
                                        </div>



                                        {/* <div className="coach-info-block" name={localStorage.getItem('selectedCoach')}> */}
                                        <div className="coach-info-block" id="infoContainer" name={coachBlockDisplay(coach.username)}>
                                            <p class="leaderCoachText">Age: {coach.info.age}</p>
                                            <p class="leaderCoachText">Gender: {coach.info.gender}</p>
                                            <p class="leaderCoachText">About: {coach.info.about}</p>
                                            <p class="leaderCoachText">Industry: {coach.info.industry}</p>
                                            <p class="leaderCoachText">Highest Coaching Qualification: {coach.info.highest_edu_qualification}</p>
                                            <p class="leaderCoachText">Coaching Practice (Years): {coach.info.years_of_practice}</p>
                                            <p class="leaderCoachText">Days Available: {coach.info.day}</p>
                                            <p class="leaderCoachText">Coaching Goal 1: {coach.info.coach_goal_1}</p>
                                            <p class="leaderCoachText">Coaching Goal 2: {coach.info.coach_goal_2}</p>
                                            <p class="leaderCoachText">Coaching Goal 3: {coach.info.coach_goal_3}</p>
                                            <p class="leaderCoachText">Number of Assigned Leaders: {coach.info.numberOfCoachees}</p>

                                        </div>

                                        {/* <button type="submit" class="btn scheduleSubmit" onClick={handleDeleteCoach}>Delete Coach</button> */}

                                    </>
                                ))}
                            </fieldset>

                        </form>
                    </div>

                </div>

                <br/>
                <br/>

        </div>
    </>

  )}

}

