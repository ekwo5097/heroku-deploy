
import React, { useState, useEffect } from "react"
import axios from 'axios';
import './AdminFeedback.css';
import { Link } from 'react-router-dom';
import { Route, useHistory } from "react-router-dom";
import NavbarAdmin from "../../SideNavbars/NavbarAdmin";
import {
    TextField,
    Grid,
    Paper,
    Button,
} from "@mui/material";
import { styled, createTheme, ThemeProvider} from '@mui/material/styles';
import DoneIcon from '@mui/icons-material/Done';
import ClearIcon from '@mui/icons-material/Clear';

const theme = createTheme({
    palette: {
      primaryConfirm: {
        main: '#669D41',
      },
      white: {
        main: '#ffffff',
      },
      primaryDelete: {
        main: '#ec1313',
      },
    },
  });

const Item = styled(Paper)(({ theme }) => ({
    // ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'Center',
    // color: theme.palette.text.secondary,
  }));

export default function AdminFeedback() {

    const [feedbacks, getAllFeedbacks] = useState([]);
    const[feedbacksCopy, setFeedbacksCopy] = useState([]);
    var randomvariable = "this is a test";
    const [newFeedback, confirmFeedbacks] = useState({});
    const [dFeedback, deleteFeedbacks] = useState({});

    useEffect(() => {

        axios.post('https://new-emerge-website.herokuapp.com/api/getAllFeedback/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                getAllFeedbacks(response.data)
                setFeedbacksCopy(response.data)
                //console.log(response.data)

            })
            .catch(err => {
            })

    }, [])

    const handleOnChangeComment = (index, commentValue, feedbacksCopy) => {
        var copyObj = feedbacksCopy
        copyObj[index]['comment'] = commentValue
        setFeedbacksCopy(copyObj)
        
    }

    const handleConfirmFeedback = (leaderUsername, index) => {
        // const leaderUsername = event.target.elements.leaderName;
        axios.post('https://new-emerge-website.herokuapp.com/api/confirmFeedback/',{
            token: localStorage.getItem("token"),
            leaderUsername:leaderUsername,
            comment:feedbacksCopy[index]['comment'],

            }, console.log(leaderUsername),

            )
            .then(response => {
                confirmFeedbacks(response.data)
                console.log(response.data)
                getAllFeedbacks(feedbacks.filter(feedback => feedback.leaderUsername !== leaderUsername));
                setFeedbacksCopy(feedbacks.filter(feedback => feedback.leaderUsername !== leaderUsername))
            })
            .catch(err => {
            })
    }

    const handleDeleteFeedback = (leaderUsername) => {
        ///event.preventDefault();
        // const leaderUsername = event.target.elements.leaderName;
        axios.post('https://new-emerge-website.herokuapp.com/api/deleteFeedback/',{
            token: localStorage.getItem("token"),
            leaderUsername:leaderUsername,

            }, console.log(leaderUsername),

            )
            .then(response => {
                deleteFeedbacks(response.data)
                //console.log(response.data)
                getAllFeedbacks(feedbacks.filter(feedback => feedback.leaderUsername !== leaderUsername));
            })
            .catch(err => {
            })
    };

    // retrieve button data on selected coachee, which is used to link with leader-info
    
    
if(localStorage.getItem('usertype')=="A") {
return (
    <>
        <ThemeProvider theme={theme}>
        <NavbarAdmin/>
            <div class="adminPageContainer">
                <h1>Unconfirmed Feedback</h1>
                {/* <div className="logoutBtnContainer">
                <a href="#" onClick={logout}>
                    <i class='bx bxs-comment-edit nav-icon-homepage'></i>
                    <span class="adminpage-logout">Logout</span>
                </a>
                </div> */}


                <div className="feedback-container">
                        {feedbacks.map( (feedback, index) => (
                            <>
                                <div className="feedback-confirmation-form">
                                    {feedback.feedback.map(answer => (
                                        <>
                                            <div class="feedback-body-adminside">
                                                <Grid container wrap="wrap" columnSpacing={3} rowSpacing={2}>
                                                    <Grid item xs={12} sm={12}>
                                                        <Item><h4>{feedback.leaderUsername}'s Feedback</h4></Item>
                                                    </Grid>
                                                    <Grid item xs={12} sm={12}>
                                                        <Item><div class="feedback-time">{feedback.timestamp}</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q1.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8}>
                                                        <Item>The coaching was effective in helping me reach my goals.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q1}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q2.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>I valued the time we spent having a coaching conversation.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q2}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q3.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>In the coaching session I felt able to present my own ideas.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q3}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q4.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>The coach showed that they understood my feelings.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q4}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q5.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>By the end of the coaching session I had greater clarity about the issues I face.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q5}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q6.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>The goals we set during coaching were very important to me.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q6}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q7.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>The coach was very good at helping me develop clear, simple and achievable action plans.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q7}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q8.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>When coaching, the coach spent more time analysing the problem rather than developing solutions.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q8}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q9.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>My coach asked me about my progress towards my goals.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q9}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Q10.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={8} zeroMinWidth>
                                                        <Item>We discussed any performance shortfalls or failure to complete actions steps.</Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><strong>{answer.q10}</strong></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={2}>
                                                        <Item><div class="question-text"> Comments.</div></Item>
                                                    </Grid>
                                                    <Grid item xs={4} sm={10} zeroMinWidth>
                                                        <Item>{answer.comment}</Item>
                                                        
                                                    </Grid>
                                                    <Grid item xs={12} sm={12} zeroMinWidth>
                                                        <TextField
                                                            inputContainerStyle={{ alignItems: 'center' }}
                                                            titleTextStyle={{ textAlign: 'center' }}
                                                            name='comment'
                                                            id="outlined-multiline-static"
                                                            label="Edit Comment For Confirming"
                                                            style = {{width: 500}}
                                                            multiline
                                                            rows={4}
                                                            defaultValue={answer.comment}
                                                            onChange={(e) => handleOnChangeComment(index, e.target.value, feedbacksCopy)}
                                                        />
                                                    </Grid>
                                                    <Grid item xs={12} sm={2}>
                                                        <Button onClick={()=>handleDeleteFeedback(feedback.leaderUsername)} variant="outlined" color="primaryDelete" endIcon={<ClearIcon />}
                                                        sx={{
                                                            '&:hover': {
                                                            backgroundColor: '#ec1313',
                                                            color: '#ffffff',
                                                            },
                                                        }}
                                                        >
                                                            Delete Feedback
                                                        </Button>
                                                    </Grid>
                                                    <Grid item xs={12} sm={2}>
                                                        <Button onClick={()=>handleConfirmFeedback(feedback.leaderUsername, index)} type="submit" variant="outlined" color="primaryConfirm" endIcon={<DoneIcon />}
                                                        sx={{
                                                            '&:hover': {
                                                            bgcolor: '#669D41',
                                                            color: '#ffffff',
                                                            },
                                                        }}
                                                        >
                                                            Confirm Feedback
                                                        </Button>
                                                    </Grid>
                                                </Grid>
                                            </div>
                                            <br/>
                                            <br/>
                                            <br/>
                                            <br/>
                                        </>
                                    ))}
                                </div>
                            </>
                        ))}

                </div>
            </div>
        </ThemeProvider>
    </>

  )}

}
