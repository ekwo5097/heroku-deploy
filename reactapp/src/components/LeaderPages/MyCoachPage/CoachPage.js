import 'material-icons/iconfont/material-icons.css';

//installed boxicons under reactapp -> npm install
// import 'boxicons/css/boxicons.css';

import './CoachPage.css';
import './CoachPageDesign.css';
import React, {useState,useEffect} from 'react';
import { useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';
import * as actions from '../../../store/actions/auth';
import * as post from '../../../store/actions/post';
import Pusher from "pusher-js";
import axios from 'axios';
import NavbarLeader from '../../SideNavbars/NavbarLeader';
import QuestNotComplete from '../../pages/QuestNotComplete';
import Calendar from '../../Scheduling/Calendar';
import TopHeaderComponent from '../../pages/TopHeaderComponent';
import Notification from '../../Scheduling/Notification';
import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import VideoCameraFrontIcon from '@mui/icons-material/VideoCameraFront';
import CloseIcon from '@mui/icons-material/Close';
import styled from "styled-components";
import { Fab, Collapse } from "@mui/material";
import MessageRoundedIcon from "@mui/icons-material/MessageRounded";
import MessagePopup from '../../Messaging/MessagePopup';


function scrollUp() {
    var chatHistory = document.getElementById("scroll-element");
    if(chatHistory)
    {
        chatHistory.scrollTop = chatHistory.scrollHeight;
    }
}

function CoachPage() {
    
    const [questionnaireState, setQuestionnaireState] = useState(false);
    const [openCalendar, setOpenCalendar] = useState(false);
    const [matchingState, setCompletedMatchingState] = useState(false); /* leader matched with coach */
    const [coachName, setCoachName] = useState('')

    var coachInfo = {
        name: "",
        gender: "",
        age: "",
        about: "",
        industry: "",
        highest_edu_qualification: "",
        years_of_practice: "",
        goal_1: "",
        goal_2: "",
        goal_3: "",
        day: ""
      };


    const onFocus = (event => {
        event.target.setAttribute('autocomplete', 'off');
        console.log(event.target.autocomplete);
      }
    )

    const { current: formDatas } = React.useRef({});
    const [post, setPost] = React.useState({})


    let history = useHistory();

    localStorage.setItem('messageLeader', localStorage.getItem("username"));


    //MESSAGING CONSTS START
    const [username, setUsername] = useState(localStorage.getItem("username"));

    const [showMessage, setShowMessages] = useState(false);
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const[messageHistory,setMessageHistory]=useState([]);
    let allMessages = [];

    //new messaging

    const MessageWrapper = styled.div`
    position: fixed;
    bottom: 4vh;
    right: 4vw;
    text-align: right;
    `;

    const [leaderid, setleaderid]=useState();
    const [coachid, setcoachid]=useState();

    const [open, setOpen] = useState(false);

    const toggleOpen = () => {
        setOpen(!open);
    };
    // let coachName = "sample-leader";

    //ends

    const pusher = new Pusher('e7e6508dcf5807d7881f', {
        cluster: 'ap2'
    });
    //MESSAGING CONSTS END
    function hideAlert() {
        setMessages([]);
        setMessageHistory([]);
        setShowMessages(false);
      }


      useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getQuestionnaireCompleted/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['completed'] === "True"){
                    setQuestionnaireState(true);
                }
            })
            .catch(err => {
            })
    }, [])

    useEffect(() => {
        // get leader is matched status
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderIsAssigned/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['msg'] == "isMatched found"){ 
                    // setQStatus(true);
                    if (response.data['isMatched'] == "True"){
                        setCompletedMatchingState(true);
                    }
                }else{
                    // Some error occurred when retrieving leader's matched_with_coach
                    console.log(response.data['msg'])
                }
                
            })
            .catch(err => {
            })
    }, [])




    const handleOnChange = React.useCallback(event => {
      formDatas[event.target.name] = event.target.value;
    }, []);

    // const handleOnSubmit = React.useCallback(event => {
    //   console.log('formDatas: ', formDatas);
    //   post.coaches(formDatas);
    //   setState({formDatas})
    //   event.preventDefault();
    // //   history.push(
    // //     history.push({
    // //         pathname: '/userhome',
    // //         state: {formDatas}
    // //     })
    // // , [])
    // })
    const handleOnSubmit = React.useCallback(event => {
        // post.getSetting();
        axios.post('https://new-emerge-website.herokuapp.com/api/addSchedule/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            date: formDatas["date"],
            month: formDatas["month"],
            year: formDatas["year"],
            time: formDatas["time"],
            },
            console.log(formDatas["date"]),
            console.log(formDatas["month"]),
            console.log(formDatas["year"]),
            console.log(formDatas["time"])
            )
            .then(response => {
                setPost(response.data)
                console.log(response.data)
                // setSettings({username: post.username, password: post.password, email: post.email, phone_number: post.phone_number})
            })
            .catch(err => {
            })
            event.preventDefault();
            history.push('/userhome');
    }, []);


    const submit = event => {
        event.preventDefault();
        if(localStorage.getItem('usertype')=="C"){
            axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
            username: localStorage.getItem("username"),
             leaderUsername: localStorage.getItem('messageLeader'),
            message: message,

            },
            console.log("message " + message + " username: " + localStorage.getItem("username") )
            )
            .then(response => {
                setMessage('');
                console.log(response.data)
                scrollUp();
            })
            .catch(err => {
            })

        }
        else {

            axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
                username: localStorage.getItem("username"),
                leaderUsername: localStorage.getItem("username"),
                message: message,

                },
                console.log("message " + message + " username: " + localStorage.getItem("username") )
                )
                .then(response => {
                    setMessage('');
                    console.log(response.data)
                    scrollUp();
                })
                .catch(err => {
                })
        }
    }


    function handleClick() {
        setShowMessages(false);
        setMessages([]);
        setMessageHistory([]);

        //MESSAGING STUFF BEGINS HERE
        allMessages = [];
        console.log("allMessages IS THIS ---------------------" + allMessages)
        Pusher.logToConsole = true;
        var channel;
        channel = pusher.subscribe(localStorage.getItem('messageLeader'));

        channel.bind('message', function (data) {
            allMessages.push(data);
            setMessages(allMessages);

        })
        console.log("After pushing allMessages IS THIS ---------------------" + allMessages)

        //MESSAGING STUFF ENDS HERE
        //MESSAGING BEGINS HERE
        axios.post('https://new-emerge-website.herokuapp.com/api/getMessageHistory/',{
            username: localStorage.getItem("username"),
            leaderUsername: localStorage.getItem("messageLeader"),
            },console.log("username " + localStorage.getItem("username") + " leader: " + localStorage.getItem("messageLeader"))
            )
            .then(response => {
                setMessageHistory(response.data.messages)
                console.log(response.data.messages)
                scrollUp();

            })
            .catch(err => {
            })
        // MESSAGING ENDS HERE
        setShowMessages(true)
    }

    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getCoachQuestionnaireInfo/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                var data = response.data
                coachInfo["name"] = data[0]["name"]
                coachInfo["age"] = data[1]["age"] + " y/o"
                coachInfo["gender"] = data[2]["gender"]
                coachInfo["about"] = data[3]["about"]
                coachInfo["industry"] = data[4]["industry"]
                coachInfo["highest_edu_qualification"] = data[5]["highest_edu_qualification"]
                coachInfo["years_of_practice"] = data[6]["years_of_practice"]
                coachInfo["goal_1"] = data[7]["coach_goal_1"]
                coachInfo["goal_2"] = data[8]["coach_goal_2"]
                coachInfo["goal_3"] = data[9]["coach_goal_3"]
                coachInfo["approach"] = data[10]["approach"]
                coachInfo["philosophy"] = data[11]["philosophy"]
                coachInfo["day"] = data[12]["day"]

                document.getElementById("name").innerHTML = coachInfo["name"]
                document.getElementById("gender").innerHTML = coachInfo["gender"]
                document.getElementById("age").innerHTML = coachInfo["age"]
                document.getElementById("years-in-industry").innerHTML = coachInfo["industry"]
                document.getElementById("goal-1").innerHTML = coachInfo["goal_1"]
                document.getElementById("goal-2").innerHTML = coachInfo["goal_2"]
                document.getElementById("goal-3").innerHTML = coachInfo["goal_3"]
                document.getElementById("about-me-header").innerHTML = "Introduction"
                // document.getElementById("about-me-body").innerHTML = "Hi my name is " + coachInfo["name"] + "! I have a Master of Science in Coaching Psychology and enjoy working with leaders who want to successfully manage an increased scope of responsibility at work! I bring 10 years of experience in leadership coaching and have worked in different management responsibilities in technology for 15 years."
                document.getElementById("about-me-body").innerHTML = coachInfo['about']
                document.getElementById("philosophy-header").innerHTML = "My Philosophy"
                // document.getElementById("philosophy-body").innerHTML = "Every challenge provides an opportunity to learn something new."
                document.getElementById("philosophy-body").innerHTML = coachInfo['philosophy']
                document.getElementById("approach-header").innerHTML = "My Approach"
                // document.getElementById("approach-body").innerHTML = "I use a solution-focused, cognitive-behavioural approach to coaching."
                document.getElementById("approach-body").innerHTML = coachInfo['approach']

            })
            .catch(err => {
            })
    }, [])

    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/leaderGetZoomDetails/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                var data = response.data
                if (data['msg'] === "No upcoming bookings"){
                    document.getElementById("booking-date-notice").innerHTML = "No Upcoming Sessions"

                    document.getElementById("disable-join-session").style.display = "block";
                    document.getElementById("enable-join-session").style.display = "none";
                }
                else if (data['msg'] === "Upcoming booking"){
 
                    document.getElementById("booking-date-notice").innerHTML = data['earliestBookingDate']
                    document.getElementById("booking-meeting-id").innerHTML = "Meeting ID: " + data['meetingId'];
                    document.getElementById("booking-meeting-password").innerHTML = "Meeting Password: " + data['meetingPassword'];
                    document.getElementById("zoom-link-hyperlink").href = data['meetingLink'];

                    document.getElementById("disable-join-session").style.display = "none";
                    document.getElementById("enable-join-session").style.display = "block";

                }

            })
            .catch(err => {
            })
    },
        useEffect(() => {
            
            axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderCoachid/', {
                username: localStorage.getItem("username"),
                
            })
                .then(response => {
                    setleaderid(response.data.leaderid);
                    setcoachid(response.data.coachid);
                    setCoachName(response.data.coachname); 

                    
                })
                .catch(err => {
                });
            },
    []))

    function handleOpenCalendar(){
        setOpenCalendar(true);
        document.getElementById("calendar-outer").style.display = "block";
        document.getElementById("calendar-outer").style.zIndex = "10";
        document.getElementById("close-calendar-outer").style.display="block";
        console.log("calendar open state: " + openCalendar)
    }


    function handleCloseCalendar(){
        setOpenCalendar(false);
        document.getElementById("calendar-outer").style.display = "none";
        document.getElementById("calendar-outer").style.zIndex = "-1";
        document.getElementById("close-calendar-outer").style.display="none";
        console.log("calendar open state: " + openCalendar)
    }

    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getMessageHistory/',{
            username: localStorage.getItem("username"),
            leaderUsername: localStorage.getItem("messageLeader"),
            },console.log("username " + localStorage.getItem("username") + " leader: " + localStorage.getItem("messageLeader"))
            )
            .then(response => {
                setMessageHistory(response.data.messages)
                console.log(response.data.messages)

            })
            .catch(err => {
            })
    }, [])


    // const listBookedTimes = Object.entries(state).map(([key, value]) => {
    //     return(
    //         <p key={key}>
    //             {value.date} {value.month} {value.year} {value.time}
    //         </p>
    //     )
    // })

    if (questionnaireState == false){
        console.log("questionnaireComplete: " + questionnaireState)
        return (
            <>
            
            <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

            </div>

            <NavbarLeader/>
            {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
            <div class="no-access-outer">
                <div class="no-access-inner">
                    You don't have access to this page!
                </div>
            </div>

            {/* Leader to coach matching placeholder */}
                {/* <div class="no-access-outer">
                    <div class="no-access-inner">
                        Hi there! This page will display details about your coach once you have been matched! :)
                    </div>
                </div> */}
            
            </>
        )
    }

    if (matchingState == false){
        return (
        <>
        
        <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

            </div>

            <NavbarLeader/>
            {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
            <div class="no-access-outer">
                <div class="no-access-inner">
                    You don't have access to this page!
                </div>
            </div>

        </>
        );
    }

    else{


    return (
        <>
            {/* Insert react code here to display on the /coaches page */}
            {/* need to convert html code from Templates/CoachPage.html into reactjs code (only some html tags are the compatible) */}

            {/* Load coach info */}
            {/* <div class="load-coach-info-div" id="loadCoachInfo" onClick={LoadCoachInfo}></div> */}


            {/* Calendar */}
            <div class="calendar-outer-div" id="calendar-outer">
                <div class="calendar-coach-page-div" id="calendar-mycoach-div">
                    <div class="close-calendar-outer-div" id="close-calendar-outer">
                        <div class="close-calendar-inner-div">
                            <div class="close-calendar-btn-div" id="calendar-close-btn" onClick={handleCloseCalendar}>
                                <CloseIcon/>
                            </div>
                        </div>
                    </div>
                    <Calendar userType={'L'} userName={localStorage.getItem('username')}/>
                </div>
            </div>

            <TopHeaderComponent/>

            {/* =====sidebar====== */}
            <NavbarLeader/>

            <div class="full-background">
                <div class="my-coach-main-content">

                    <div class="main-coach-information">
                        <div class="profile-image-div"/>
                        <div class="name-desc-div" id="name"/>
                        <div class="gender-age-div">
                            <div class="gender-age-text" id="gender"/>
                            <div class="gender-age-text" id="age"/>
                        </div>
                        <div class="years-in-industry-div" id="years-in-industry"/>

                        <div class="booking-schedule-div">
                            <div class="schedule-booking-btn">
                                <div class="book-video-icon-div" onClick={handleOpenCalendar}>
                                    BOOK SESSION <EventAvailableIcon/>
                                </div>
                            </div>
                            <div class="booking-date-desc-div">Your Next Session</div>
                            <div class="booking-date-div" id="booking-date-notice"></div><br/>
                            <div class="enable-join-session-div" id="enable-join-session">
                                <a class="zoom-link-hyperlink-a" id="zoom-link-hyperlink" href="">
                                    <div class="book-video-icon-div" id="book-video-hyperlink">
                                            JOIN SESSION <VideoCameraFrontIcon/>
                                    </div>
                                </a>           
                                <div class="booking-date-div" id="booking-meeting-id"></div><br/>
                                <div class="booking-date-div" id="booking-meeting-password"></div>                     
                            </div>

                            <div class="disable-join-session-div" id="disable-join-session">
                                <div class="disable-enter-video-call-btn" id="disable-enter-video-call">
                                    <div class="book-video-icon-div">
                                        JOIN SESSION <VideoCameraFrontIcon/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="additional-information-wrapper">
                        <div class="main-coaching-goals">
                            <h3>My Coaching Goals</h3>
                            <div class="coaching-goal-div" id="goal-1"/>
                            <div class="coaching-goal-div" id="goal-2"/>
                            <div class="coaching-goal-div" id="goal-3"/>
                        </div>
                        <div class="main-about-me">
                            <h3>About Me</h3>
                            <div class="about-me-div" id="about-me">
                                <div class="about-me-header-div" id="about-me-header"/>
                                <div class="about-me-body-div" id="about-me-body"/>
                            </div>
                            <div class="about-me-div" id="about-me">
                                <div class="about-me-header-div" id="philosophy-header"/>
                                <div class="about-me-body-div" id="philosophy-body"/>
                            </div>
                            <div class="about-me-div" id="about-me">
                                <div class="about-me-header-div" id="approach-header"/>
                                <div class="about-me-body-div" id="approach-body"/>
                            </div>
                        </div>
                    </div>
                </div>

            {/* Calendar notifications */}
            <div class="notification-coach-page-div" id="notification-mycoach-div">
                <Notification></Notification>
            </div>


            {/* new message */}
            <MessageWrapper>
                <Collapse in={open} sx={{ flexDirection: "column-reverse" }}>
                <MessagePopup
                    name={coachName}
                    toggleOpen={toggleOpen}
                    popup={true}
                    recipientId={coachid}
                    senderId={leaderid}
                />
                
                </Collapse>

                <Fab
                color="primary"
                aria-label="message"
                onClick={() => toggleOpen()}
                sx={{ mt: 1 }}
                >
                <MessageRoundedIcon />
                </Fab>
            </MessageWrapper>
        
            </div>

            
            
            {/* MESSAGE CONTENT START BELOW */}


                {/* {showMessage ?
                    <>
                        <div class="wrapper">

                            <div class="main">

                                <div>
                                    <i class="fas fa-times-circle message-icon-coachpage2" onClick={hideAlert}></i>
                                </div>


                                <div class="px-2 scroll" id="scroll-element">
                                    

                                    {messageHistory.map(prevMessage => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==prevMessage.name ?
                                                    <>
                                                        <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{prevMessage.name}</span>
                                                                <p class="msg">{prevMessage.message}</p>
                                                            </div>
                                                        </div>

                                                    </>
                                                    :
                                                    <>
                                                        <div class="d-flex align-items-center">
                                                            <div class="text-left pr-1">
                                                                <div class="pr-2 pl-1"> <span class="name">{prevMessage.name}</span>
                                                                    <p class="msg">{prevMessage.message}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>
                                                }
                                        </>
                                        )
                                    })}
                                    {messages.map(message => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==message.username ?
                                                <>
                                                    <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                </>
                                                :
                                                <>
                                                    <div class="d-flex align-items-center">
                                                        <div class="text-left pr-1">
                                                            <div class="pr-2 pl-1"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                }
                                            </>
                                            )
                                        })}
                                    

                                </div>
                                
                                <nav class="message-input bg-white navbar-expand-sm d-flex justify-content-between">
                                    <form onSubmit={e => submit(e)}>
                                        <input type="text number" name="text" class="form-control" placeholder="Type a message..."
                                        value={message}  onChange={e => setMessage(e.target.value)} onFocus={onFocus}/>
                                        <div class="icondiv d-flex justify-content-end align-content-center text-center ml-2">  <i class="fa fa-arrow-circle-right icon2"></i> </div>
                                    </form>
                                </nav>
                            </div>
                        </div>

                    </>
                    :
                    <>
                        <div class="message-icon-coachpage">
                            <i class="fas fa-comment" onClick={handleClick}> Message </i>
                        </div>
                    </> */}
                {/* } */}
            





        </>
    );
}

}

export default CoachPage;
