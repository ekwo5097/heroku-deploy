import 'material-icons/iconfont/material-icons.css';
import './CoachPageDesign.css';
import React, {useState,useEffect} from 'react';
import axios from 'axios';
import NavbarLeader from '../../SideNavbars/NavbarLeader';
import QuestNotComplete from '../../pages/QuestNotComplete';
import Calendar from '../../Scheduling/Calendar';
import TopHeaderComponent from '../../pages/TopHeaderComponent';
import Notification from '../../Scheduling/Notification';


import EventAvailableIcon from '@mui/icons-material/EventAvailable';
import VideoCameraFrontIcon from '@mui/icons-material/VideoCameraFront';


function CoachPageDesign() {
    
    // const [coachInfo, setCoachInfo] = useState([]);
    const [openCalendar, setOpenCalendar] = useState(false);

    var coachInfo = {
        name: "",
        gender: "",
        age: "",
        about: "",
        industry: "",
        highest_edu_qualification: "",
        years_of_practice: "",
        goal_1: "",
        goal_2: "",
        goal_3: "",
        day: ""
      };

    useEffect(() => {
        // hard-coded username for testing
        axios.post('https://new-emerge-website.herokuapp.com/api/getCoachQuestionnaireInfo/',{
            username: "leader555",
            })
            .then(response => {
                var data = response.data
                coachInfo["name"] = data[0]["name"]
                coachInfo["age"] = data[1]["age"]
                coachInfo["gender"] = data[2]["gender"]
                coachInfo["about"] = data[3]["about"]
                coachInfo["industry"] = data[4]["industry"]
                coachInfo["highest_edu_qualification"] = data[5]["highest_edu_qualification"]
                coachInfo["years_of_practice"] = data[6]["years_of_practice"]
                coachInfo["goal_1"] = data[7]["coach_goal_1"]
                coachInfo["goal_2"] = data[8]["coach_goal_2"]
                coachInfo["goal_3"] = data[9]["coach_goal_3"]
                coachInfo["day"] = data[10]["day"]

                document.getElementById("name").innerHTML = coachInfo["name"]
                document.getElementById("gender").innerHTML = coachInfo["gender"]
                document.getElementById("age").innerHTML = coachInfo["age"]
                document.getElementById("years-in-industry").innerHTML = coachInfo["years_of_practice"] + " years in " + coachInfo["industry"]
                document.getElementById("goal-1").innerHTML = coachInfo["goal_1"]
                document.getElementById("goal-2").innerHTML = coachInfo["goal_2"]
                document.getElementById("goal-3").innerHTML = coachInfo["goal_3"]
                document.getElementById("about-me-header").innerHTML = "Introduction"
                document.getElementById("about-me-body").innerHTML = coachInfo["about"]
                document.getElementById("philosophy-header").innerHTML = "My Philosophy"
                document.getElementById("philosophy-body").innerHTML = "Make improvements, not excuses!"
                document.getElementById("approach-header").innerHTML = "My Approach"
                document.getElementById("approach-body").innerHTML = "Start slow, then build momentum!"


            })
            .catch(err => {
            })
    }, [])

    function handleOpenCalendar(){
        setOpenCalendar(true);
        document.getElementById("calendar-outer").style.display = "block";
        document.getElementById("calendar-close-btn").style.display = "block";
        document.getElementById("full-background").style.background = "RGB(105, 105, 105)";
        console.log("calendar open state: " + openCalendar)
    }

    function handleCloseCalendar(){
        setOpenCalendar(false);
        document.getElementById("calendar-outer").style.display = "none";
        document.getElementById("calendar-close-btn").style.display = "none";
        document.getElementById("full-background").style.background = "RGB(240, 240, 240)";
        console.log("calendar open state: " + openCalendar)
    }

    return (
        <>
        
            {/* Calendar notifications */}
            <div class="notification-coach-page-div" id="notification-mycoach-div">
                <Notification></Notification>
            </div>

            {/* Calendar */}
            <div class="calendar-outer-div" id="calendar-outer">
                <div class="calendar-coach-page-div" id="calendar-mycoach-div">
                    <div class="close-calendar-btn-div" id="calendar-close-btn" onClick={handleCloseCalendar}>X</div>
                    <Calendar userType={'L'} userName={localStorage.getItem('username')}/>
                </div>
            </div>

            <TopHeaderComponent/>

            {/* =====sidebar====== */}
            <NavbarLeader/>
            <div class="full-background">
                <div class="my-coach-main-content">

                    <div class="main-coach-information">
                        <div class="profile-image-div"/>
                        <div class="name-desc-div" id="name"/>
                        <div class="gender-age-div">
                            <div class="gender-age-text" id="gender"/>
                            <div class="gender-age-text" id="age"/>
                        </div>
                        <div class="years-in-industry-div" id="years-in-industry"/>

                        <div class="booking-schedule-div">
                            <div class="schedule-booking-btn">
                                <div class="book-video-icon-div" onClick={handleOpenCalendar}>
                                    BOOK SESSION <EventAvailableIcon/>
                                </div>
                            </div>
                            <div class="booking-date-desc-div">Your Next Session</div>
                            <div class="booking-date-div">Tuesday, 15 February 2022</div>
                            <div class="enter-video-call-btn">
                                <div class="book-video-icon-div">
                                    JOIN SESSION <VideoCameraFrontIcon/>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="additional-information-wrapper">
                        <div class="main-coaching-goals">
                            <h3>My Coaching Goals</h3>
                            <div class="coaching-goal-div" id="goal-1"/>
                            <div class="coaching-goal-div" id="goal-2"/>
                            <div class="coaching-goal-div" id="goal-3"/>
                        </div>
                        <div class="main-about-me">
                            <h3>About Me</h3>
                            <div class="about-me-div" id="about-me">
                                <div class="about-me-header-div" id="about-me-header"/>
                                <div class="about-me-body-div" id="about-me-body"/>
                            </div>
                            <div class="about-me-div" id="about-me">
                                <div class="about-me-header-div" id="philosophy-header"/>
                                <div class="about-me-body-div" id="philosophy-body"/>
                            </div>
                            <div class="about-me-div" id="about-me">
                                <div class="about-me-header-div" id="approach-header"/>
                                <div class="about-me-body-div" id="approach-body"/>
                            </div>
                        </div>
                    </div>


                </div>

                


            </div>






        </>
    );
}

export default CoachPageDesign;
