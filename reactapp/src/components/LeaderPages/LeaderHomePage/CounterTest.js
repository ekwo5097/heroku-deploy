


import {
    TextField,
    Button,
    Grid,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    InputLabel,
    MenuItem,
    FormHelperText,
    FormControl,
    Select,
    List,
    ListItem,
    ListItemButton,
    ListItemText,
    IconButton,
    Snackbar,
    Alert as MuiAlert,
} from "@mui/material";

import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from '@mui/icons-material/Remove';
import React, { useState, useEffect } from "react";
import axios from 'axios';
  

export default function CounterTest() {

    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/countertestGET/',{
            // username: localStorage.getItem("username"),
            // token: localStorage.getItem("token"),
            })
            .then(response => {
                setCount(response.data.count)
                console.log(JSON.stringify(response.data))
            })
            .catch(err => {
            })
        // Add more lines here
        }
        , [])


    // var count = 0;
    var [count, setCount] = useState(0);
    var [actions, setActions] = useState('<p>New Action</p>');
    // const countDiv = <p>{count}</p>

    // ReactDOM.render(
    //     countDiv,
    //     document.getElementById('counter')
    // )

    // This works
    // const decrement = () => {
    //     console.log("decrementing...");
    //     setCount(count - 1);
    //     console.log(count)
    // };
    // const increment = () => {
    //     console.log("incrementing...");
    //     setCount(count + 1);
    //     console.log(count)
    // };

    // This also works
    // function decrement() {
    //     console.log("decrementing...");
    //     setCount(count - 1);
    //     console.log(count)
    // };
    // function increment() {
    //     console.log("incrementing...");
    //     setCount(count + 1);
    //     console.log(count)
    // };

    // This updates the var properly but doesnt update on-screen
    // const decrement = () => {
    //     console.log("decrementing...");
    //     count -= 1;
    //     console.log(count)
    // };
    // const increment = () => {
    //     console.log("incrementing...");
    //     count += 1;
    //     console.log(count)
    // };


    const decrement = () => {
        console.log("decrementing...");
        setCount(count - 1);
        console.log(count)
    };
    const increment = () => {
        console.log("incrementing...");
        axios.post('https://new-emerge-website.herokuapp.com/countertestPOST/',{
            // username: localStorage.getItem("username"),
            // token: localStorage.getItem("token"),
            })
            .then(response => {
                setCount(response.data.count)
                setActions(actions + '<p>New Action</p>')
                console.log(JSON.stringify(response.data))
            })
            .catch(err => {
            })
        
        console.log(count)
    };
    


    return(
        <>  
            <IconButton
                edge="end"
                aria-label="add"
                onClick={() => decrement()}
            >
                <RemoveIcon />
            </IconButton>
            {/* <div onClick={decrement()}>MINUS</div> */}
            {/* <div>{count}</div> */}
            <div id="counter">{count}</div>
            {/* <div onClick={increment()}>PLUS</div> */}
            <IconButton
                edge="end"
                aria-label="add"
                onClick={() => increment()}
            >
                <AddIcon />
            </IconButton>

            <div id="actions">{actions}</div>
        </>
    );



    // const [count, setCount] = useState(0);

    // return (
    // <div>
    //     <p>You clicked {count} times</p>
    //     <button onClick={() => setCount(count + 1)}>
    //     Click me
    //     </button>
    // </div>
    // );
}