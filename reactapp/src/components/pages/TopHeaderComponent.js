import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import './TopHeaderComponent.css';
import { Route, useHistory } from "react-router-dom";
import LogoutTwoToneIcon from '@mui/icons-material/LogoutTwoTone';
import MiscellaneousServicesIcon from '@mui/icons-material/MiscellaneousServices';


function TopHeaderComponent() {
    let history = useHistory();
    const logout = event => {
        event.preventDefault();
        localStorage.removeItem("token");
        localStorage.removeItem("username");
        localStorage.removeItem("usertype");
        localStorage.removeItem("messageLeader");
        localStorage.removeItem("pusherTransportTLS");
        console.log("localStorage items removed");
        localStorage.setItem("refresh","true");
        history.push('/');
      }
    

    return (
        <>
          <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

                <div className="acc-feature">
                    <i class="fas fa-user top-icon-coachpage"></i>
                    <div className="acc-dropdown">
                        <a href="#"><Link to='/settings'><MiscellaneousServicesIcon color="primary"/> Settings</Link></a>
                        <a href="#" onClick={logout}><Link to='/'> <LogoutTwoToneIcon color="primary"/> Sign Out</Link></a>
                    </div>
                </div>
            </div>
        </>
    );
}



export default TopHeaderComponent;