// install material icons -> npm install material-icons@latest
import 'material-icons/iconfont/material-icons.css';

//installed boxicons under reactapp -> npm install
// import 'boxicons/css/boxicons.css';

// import './CoacheesPage.css';
import './CoacheePageMod';
import React, {useEffect, useState} from 'react';
import { Route, useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';
import * as actions from '../../store/actions/auth';
import * as post from '../../store/actions/post';
import axios from 'axios';
import Messages from './Messages';
import Pusher from "pusher-js";
import NavbarCoach from '../SideNavbars/NavbarCoach';
import MessagePopup from '../Messaging/MessagePopup'
import TopHeaderComponent from './TopHeaderComponent';
import CoachLeaderActions from './CoachLeaderActions';

function scrollUp() {
    var chatHistory = document.getElementById("scroll-element");
    if(chatHistory)
    {
        chatHistory.scrollTop = chatHistory.scrollHeight;
    }
}

function CoacheesPage() {
     let history = useHistory();

     const logout = event => {
       event.preventDefault();
       localStorage.removeItem("token");
       localStorage.removeItem("username");
       localStorage.removeItem("usertype");
       localStorage.removeItem("messageLeader");
       localStorage.removeItem("pusherTransportTLS");
       console.log("localStorage items removed");
       localStorage.setItem("refresh","true");
       history.push('/');
     }

    const [allLeaders, setAllLeaders] = useState([])

     const [showMessage, setShowMessages] = useState(false);
     //const [showMessage, setShowMessages] = useState(false);
    // const { current: formDatas } = React.useRef({});

    // const [post, setPost] = React.useState({})
    const [appts, setAppts] =  React.useState({}); {/*useState({date:1,month:10,year:2021,time:'9:00am-10:00am',leader:'sam'});*/}
    const [post, setPost] = useState([]);
    const [hw, setHw] = React.useState({})
    const [zm, setZm] = React.useState({})
    const [leaderHws, setLeaderHws] = useState([]);
    const [leaderZoomLinks, setLeaderZoomLinks] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [goals, setGoals] = useState([]);
    const [subgoals, setSubGoal] = useState([]);
    const [rsubgoals, setRemoveSubGoal] = useState([]);
    const [action, setAddAction] = useState([]);
    const [raction, setRemoveAction] = useState([]);
    const [isMessage, setisMessage] = useState(false);

    const[clickedLeader, setClickedLeader] = useState();
    var leaderSchedulesDiv = "";
    // const handleOnChange = React.useCallback(event => {
    //   formDatas[event.target.name] = event.target.value;
    // }, []);
    const [leaderid, setleaderid]=useState();
    const [coachid, setcoachid]=useState();
    const [open, setOpen] = useState(false);

    const toggleOpen = () => {
        setOpen(!open);
    };

    //MESSAGING CONSTS START
    // const [username, setUsername] = useState(localStorage.getItem("username"));
    // const [messages, setMessages] = useState([]);
    // const [message, setMessage] = useState('');
    // const[messageHistory,setMessageHistory]=useState([]);
    // let allMessages = [];
    //MESSAGING CONSTS END
    // function hideAlert() {
    //     setMessages([]);
    //     setMessageHistory([]);
    //     setShowMessages(false);
    //   }

    // const pusher = new Pusher('e7e6508dcf5807d7881f', {
    //     cluster: 'ap2'
    // });
    function handleClick(leaderName) {
        // setShowMessages(false);
        // setMessages([]);
        // setMessageHistory([]);
        // pusher.unsubscribe(localStorage.getItem('messageLeader'));
        // console.log("----------------------UNSUBSCRIBING--------------------")

        setClickedLeader(leaderName)
        setisMessage(true)

        // Set actions of selected leader
        // document.querySelector("#leader-actions-main").value = leaderName;
        // document.getElementById("leader-actions-main").value = leaderName;
        // console.log(document.querySelector("#leader-actions-main").value);
        // document.querySelector("#set-leader-action-active-id").click()
        // document.getElementsByClassName("leader-actions-main-container")[0].value = leaderName; // testing this
        const ele = document.getElementsByClassName("leader-actions-main-container")[0];
        console.log("found ele-ele-ele");

        setTimeout(function() { 

            document.querySelector("#leader-actions-main").value = leaderName;
            console.log(document.querySelector("#leader-actions-main").value)
            document.querySelector("#set-leader-action-active-id").click();

         }, 1000);
        // const value = document.querySelector("#leader-actions-main").value;
        // console.log(value)
        // document.getElementById("set-leader-action-active-id").click();
        // console.log(document.getElementsByClassName("leader-actions-main-container")[0].value);

        localStorage.setItem('messageLeader', leaderName);
        console.log(clickedLeader)
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderCoacheeid/', {
            username: localStorage.getItem("username"),
            leaderName:leaderName
            
        })
            .then(response => {
                setleaderid(response.data.leaderid);
                setcoachid(response.data.coachid);
                console.log(response.data.leaderid);
                console.log("leaderid after clicking" + leaderid);

            })
            .catch(err => {
            });

        
    }
    //confirm a specific leader's schedule upon clicking 'confirm button'
    const handleApptSubmit = React.useCallback(event => {
        // post.getSetting();
        event.preventDefault();
        const leaderInput = event.target.elements.leaderName;
        const dateInput = event.target.elements.date;
        const monthInput = event.target.elements.month;
        const yearInput = event.target.elements.year;
        const timeInput = event.target.elements.time;
        axios.post('https://new-emerge-website.herokuapp.com/api/confirmNextSchedule/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leader:leaderInput.value,
            date:dateInput.value,
            month:monthInput.value,
            year:yearInput.value,
            time:timeInput.value,
            }, console.log(leaderInput.value),
            {/*console.log("coachee name: " + leader + " username: " + localStorage.getItem("username") )*/}

            )
            .then(response => {
                setAppts(response.data)
                console.log(response.data)
                // setSettings({username: post.username, password: post.password, email: post.email, phone_number: post.phone_number})
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []);

    const handleZoomSubmit = React.useCallback(event => {
        event.preventDefault();
        const zoomInput = event.target.elements.zoom; // accessing directly // accessing via `form.elements`
        const leaderInput = event.target.elements.leaderName; // get leader name for new zoom link
        axios.post('https://new-emerge-website.herokuapp.com/api/addZoomLink/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leader: leaderInput.value,
            zoomLink: zoomInput.value,
            // leader: leaderInput.value ---> have to save leader data along with form
            },
            console.log(zoomInput.value),
            )
            .then(response => {
                setZm(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);

    const handleHomeworkSubmit = React.useCallback(event => {
        event.preventDefault();
        const leaderInput = event.target.elements.leaderName; // get leader name from form (this is already manually set)
        const titleInput = event.target.elements.title; // accessing directly
        const descInput = event.target.elements.description; // accessing via `form.elements`
        axios.post('https://new-emerge-website.herokuapp.com/api/addHomework/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leader: leaderInput.value,
            title: titleInput.value,
            description: descInput.value,
            isFinished: false
            },
            console.log(titleInput.value),
            console.log(descInput.value),
            )
            .then(response => {
                setHw(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);

    //Handle when coach adds a subgoal to a selected leader
    const handleAddSubgoal = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.newSubGoal;
        axios.post('https://new-emerge-website.herokuapp.com/api/addSubGoal/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value

            }, console.log(subGoal.value),

            )
            .then(response => {
                setSubGoal(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);

    //Handle when coach removes a subgoal to a selected leader
    const handleRemoveSubgoal = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.subGoal;
        axios.post('https://new-emerge-website.herokuapp.com/api/removeSubGoal/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value

            }, console.log(subGoal.value),

            )
            .then(response => {
                setRemoveSubGoal(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);

    //Handle when coach adds an action to a selected leader
    const handleAddAction = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.subGoal;
        const action = event.target.elements.action;
        axios.post('https://new-emerge-website.herokuapp.com/api/addAction/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value,
            action:action.value

            }, console.log(action.value),

            )
            .then(response => {
                setAddAction(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);

    //Handle when coach removes an action to a selected leader
    const handleRemoveAction = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.subGoal;
        const action = event.target.elements.action;
        axios.post('https://new-emerge-website.herokuapp.com/api/removeAction/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value,
            action:action.value

            }, console.log(action.value),

            )
            .then(response => {
                setRemoveAction(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);

    // const handleMessage = event => {
    //     event.preventDefault();
    //     localStorage.setItem('messageLeader', clickedLeader);
    //     console.log("leader is " + localStorage.getItem('messageLeader'))
    //     //history.push('/messages');

    // }


    //get all leaders and their unconfirmed schedules
    useEffect(() => {
        if(localStorage.getItem('usertype')=="C") {
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderSchedules/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setPost(response.data)
                console.log("post is: " + post)
                console.log(JSON.stringify(response.data))

                var result = [];

                for(var i in response.data)
                {
                    if(Object.keys(response.data[i]).length != 0)
                    {
                        //var dict = response[i][1]

                        //for (var key in dict)
                        //{
                        //    if (dict[key]["date"] == "") {console.log("empty date")}
                        //}
                        result.push([i, response.data[i]]);
                    }
                }

                //for every leader
                // let leaderSchedulesDiv = "";
                for (var i in result)
                {
                    //dict is a list of all the leader's schedules
                    var dict = result[i][1]
                    console.log(dict)
                    for (var key in dict)
                    {
                        var leaderName = result[i][0];
                        leaderSchedulesDiv += "<div> coachee name: " + leaderName + " date: " + dict[key]["date"] + " month: " + dict[key]["month"] + " year: " + dict[key]["year"] + " time: " + dict[key]["time"] + "</div>"
                        console.log(leaderSchedulesDiv[0])
                        console.log(leaderSchedulesDiv[1])

                        //console.log("the leaderName is: "+ leaderName)

                        //console.log("the month is: "+ dict[key]["month"])

                        //need to add to appts schedule by schedule
                        setAppts({date:dict[key]["date"],month:dict[key]["month"],year:dict[key]["year"],time:dict[key]["time"],leader:leaderName})
                        // () => {
                        //     console.log("coachee name: {" + leaderName + "} date: {" + dict[key]["date"]+ "} month: {" + dict[key]["month"] + "} year: {" + dict[key]["year"] + "} time: {" + dict[key]["time"] + "}")
                        // });

                        //console.log("coachee name: {" + appts.leader + "} date: {" + appts.date+ "} month: {" + appts.month + "} year: {" + appts.year + "} time: {" + appts.time + "}")


                    }
                }
                //console.log("coachee name: {" + appts.leader + "} date: {" + appts.date+ "} month: {" + appts.month + "} year: {" + appts.year + "} time: {" + appts.time + "}")
            })
            .catch(err => {
            });

            // post for getting all coachees homeworks for coach view
            axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderHomework/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setLeaderHws(response.data)
                console.log("leader homework is")
                console.log(JSON.stringify(response.data))
            })
            .catch(err => {
            });

            // post for getting all coachees homeworks for coach view
            axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderZoomLinks/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setLeaderZoomLinks(response.data)
                console.log("leader zoom links is:")
                console.log(JSON.stringify(response.data))
            })
            .catch(err => {
            });


            // post for getting all coachees homeworks for coach view
            axios.post('https://new-emerge-website.herokuapp.com/api/getAllLeadersGoals/',{
                username: localStorage.getItem("username"),
                token: localStorage.getItem("token"),
                })
                .then(response => {
                    setGoals(response.data)
                    //console.log(JSON.stringify(response.data))
                })
                .catch(err => {
                });
            
            // gets list of assigned coachees for dropdown 
            axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderToCoach/', {
                username: localStorage.getItem("username"),
                token: localStorage.getItem("token")
            })
                .then(response => {
                    setAllLeaders(response.data.leaders)
                    console.log(JSON.stringify(response.data.leaders))
                })
                    .catch(err => {
            });
            
            


        }}, [])

        // const submit = event => {
        //     event.preventDefault();
        //     if(localStorage.getItem('usertype')=="C"){
        //         axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
        //         username: localStorage.getItem("username"),
        //          leaderUsername: localStorage.getItem('messageLeader'),
        //         message: message,

        //         },
        //         console.log("message " + message + " username: " + localStorage.getItem("username") )
        //         )
        //         .then(response => {
        //             setMessage('');
        //             console.log(response.data)
        //             scrollUp();
        //         })
        //         .catch(err => {
        //         })

        //     }
        //     else {

        //         axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
        //             username: localStorage.getItem("username"),
        //             leaderUsername: localStorage.getItem("username"),
        //             message: message,

        //             },
        //             console.log("message " + message + " username: " + localStorage.getItem("username") )
        //             )
        //             .then(response => {
        //                 setMessage('');
        //                 console.log(response.data)
        //                 scrollUp();
        //             })
        //             .catch(err => {
        //             })
        //     }
        // }




 /*    Functions for displaying coachee name      */

    // generate id based on coachee count
   function idFunction()
   {
        if ( typeof idFunction.c == 'undefined' ) {
            idFunction.c = 0;
        }

        ++idFunction.c;
        var nId = "radio" + idFunction.c.toString()

       return nId;
   }

    // generate value based on coachee count
   function valueFunction()
   {
        if ( typeof valueFunction.c == 'undefined' ) {
            valueFunction.c = 0;
        }

        ++valueFunction.c;
        var vFunction = "radio" + valueFunction.c.toString()

       return vFunction;
   }

    // generate name based on coachee count
   function nameFunction()
   {
    if ( typeof nameFunction.c == 'undefined' ) {
        nameFunction.c = 0;
    }

    ++nameFunction.c;
    var nm = "radio" + nameFunction.c.toString()

   return nm;
   }


   /* Functions for displaying coachee info */


   // generate coachee info name based on coachee count
   function coacheeInfoFunction(name)
   {

    // save to static array. check if the same coachee is still being executed. if so, get last coachee

    if ( typeof coacheeInfoFunction.c == 'undefined' ) {
        coacheeInfoFunction.c = 0;
    }

    if ( typeof coacheeInfoFunction.names == 'undefined') {
        coacheeInfoFunction.names = [];
    }

    if (coacheeInfoFunction.names.includes(name))
    {
        return "coachee-info-" + coacheeInfoFunction.c.toString();
    }

    coacheeInfoFunction.names.push(name);
    ++coacheeInfoFunction.c;
    var cInfo = "coachee-info-" + coacheeInfoFunction.c.toString()

   return cInfo;
   }

    // retrieve button data on selected coachee, which is used to link with leader-info
   function setLeaderDataActive()
   {

    // set image to 'none', and leader info to 'block'
    document.querySelector('div[name="image-wrapper-2"]').style.visibility = 'hidden';
    document.querySelector('div[name="leader-info-list"]').style.visibility = 'visible';

    var leaderInfoDisplay = document.querySelector('div[name="leader-info-list"]').style.visibility;
    console.log(leaderInfoDisplay);

    var rbs = document.querySelectorAll('input[name="coachee"]');
    let selectedValue;
    for (const rb of rbs) {
        if (rb.checked) {
            selectedValue = rb.value;
            break;
        }
    }

    // get coachee info number
    try
    {
        var selected = selectedValue.replace('radio','');
    }
    catch
    {
        //console.log("error: radio button string unable to be replaced.");
        return;
    }

    // locate and match coachee info number from array
    var blocks = document.getElementsByClassName("coachee-info-block");
    var blockName = "";
    var matchedBlock = "";
    for (const block of blocks) {
        blockName = block.getAttribute('name')
        try
        {
            var blockNum = blockName.replace('coachee-block-','');
            if (blockNum == selected)
            {
                matchedBlock = blockName;
                break;
            }
        }
        catch
        {
            //console.log("error: coachee-block-x string unable to be replaced.");
            continue;
        }
    }

    // set unmatched coachee div to 'none', matched div with 'block'
    //console.log("BLOCKNAME: " + blockName)
    var allCoacheeBlock = document.querySelectorAll('.coachee-info-block');
    for (const item of allCoacheeBlock)
    {
        //console.log(item.getAttribute('name'));
        if (item.getAttribute('name') == blockName)
        {
            item.style.display='block';
        }
        else
        {
            item.style.display='none';
        }
    }

   }

   // sets coachee-info display block based on counter
   function coacheeBlockDisplay(name)
    {
        if ( typeof coacheeBlockDisplay.c == 'undefined' || coacheeBlockDisplay.dict == 'undefined') {
            coacheeBlockDisplay.c = 0;
            coacheeBlockDisplay.dict = {};
        }

        // function to store which coachee blocks are already displayed
        // check if name already exists in dictionary
        if (name in coacheeBlockDisplay.dict)
        {
            // return the coachee-block with the number associated with the leader name
            var cBlock = "coachee-block-" + coacheeBlockDisplay.dict[name];
            //console.log("EXISTING leader block name: " + coacheeBlockDisplay.dict[name]);
            //console.log("cBlock: " + cBlock);
            return cBlock;
        }

        ++coacheeBlockDisplay.c;
        coacheeBlockDisplay.dict[name] = coacheeBlockDisplay.c;
        var cBlock = "coachee-block-" + coacheeBlockDisplay.dict[name];
        //console.log("NEW leader block name: " + coacheeBlockDisplay.dict[name]);
        //console.log("cBlock: " + cBlock);
        return cBlock;
    }

    function isComplete(homework) {
        if (homework.isFinished == true){
            // set class to strike-through
            return "strike-through"
        }
        return "standard-homework"

    }



    return (


        <>

           
            <TopHeaderComponent/>


            {/* =====sidebar====== */}
            
            <NavbarCoach/>


            {/*===========NEW COACHEE PAGE DESIGN===========*/}

            <div class="leader-container">

                <div class="choose-leader">
                    <h1 id="title-text">Select Leader </h1>
                    
                    <select id="selectedCoachee" class="dropdownbox-coachee" value={clickedLeader ? clickedLeader : ''} onChange={(event) => handleClick(event.target.value)}>
                    <option value=''>Select a leader</option>
                        {allLeaders.map(leader => (
                            
                            <option value={leader.username}> {leader.username}</option>

                            
                        ))}
                    </select>
                </div>

                {isMessage ?
                <>

                <div className="coachee-info" >
                    <h2 id="coacheeInfoHeading"> Leader Incomplete Actions </h2>
                    <div className="coachee-leader-action-outer-container">
                        <CoachLeaderActions coachName={(localStorage.getItem('username'))}/>
                    </div>
                </div>


                {/* <div className="coachee-info" >
                    <h2 id="coacheeInfoHeading"> Leader Goal </h2>

                    <h3>Main Goal</h3>
                    <div className="coacheeInfoGoals">
                        <b>Increasing ambiguity tolerance and resilience</b>
                    </div>

                    <h3>Sub Goals</h3>

                    <form class="form-container" id="subGoalForm" >
                        <div class="newScoreContentContainer">
                            <input type="text" name="subGoal"  value="Subgoal 1"/>
                            <button type="submit" className="removeSubGoalBtn">Remove Sub Goal</button>
                        </div>
                </form>

                <div class="solid-border"/>


                    <h3 id="sub-title">Add New Sub Goals</h3>
                    <form class="form-container" id="subGoalForm">
                        <div class="newScoreContentContainer">
                            <input type="text" name="newSubGoal" placeholder="Add a subgoal..." required/>
                            <button type="submit" class="newSubGoalBtn">Add Subgoal</button>
                        </div>
                    </form>
                </div> */}

                {/* <div className="coachee-info" >
                    <h2 id="coacheeInfoHeading-HW"> Leader's Homework </h2> */}

                        {/* <div class="coacheeHwContainer">
                            <button className="toggleCoacheeHw" onClick={() => setIsOpen(!isOpen)}>
                                <label>Homework title: </label>
                                <input type="text" for="cbox" name="title" value="Meditate"/>
                            </button>
                            {isOpen && <div className="coacheeHwContent">
                                <label>Homework description: </label>
                                <input type="text" for="cbox" name="description" value="Meditate for 10 minutes every morning"/>
                            </div>}
                        </div> */}
                        
                    
{/* 
                    <div>
                        <h3 id="sub-title-hw">Add New Homework</h3>
                        <form class="hwFormStyle">
                            <label class="hwLabelStyle"><b>Title</b></label>
                            <input class="hwInputStyle" type="text" name="title" placeholder="Homework Title..." required/>
                            <label class="hwLabelStyle"><b>Description</b></label>
                            <textarea type="text" name="description" placeholder="Homework Description..." required/>
                            <button class="hwSubmitStyle" type="submit">Submit</button>
                        </form>
                    </div> */}


                {/* </div> */}

                
                <div className="coachee-info" >
                <h2 id="coacheeInfoHeading">Messages </h2>
                <MessagePopup
                                name={localStorage.getItem("messageLeader")}
                                toggleOpen={toggleOpen}
                                popup={false}
                                recipientId={leaderid}
                                senderId={coachid}
                            />


                </div>
                </>
                :
                <>
                </>
                }



            </div>

            {/*=====================================*/}
            

            {/* <div class="leader-container">

                <div class="leader-template">

                    <div className="leader-list">
                        <div className="leader leaderHeading">
                            <h2>List of Coachees</h2>
                        </div>
                        <div className="leader leaderNameContainer">


                            <form name="leader-name-form" onClick={setLeaderDataActive}>
                                <fieldset>
                                {post.map(scheduleObject => (
                                                    <>

                                                        <div className="leader leaderContent" id="leaderContent">
                                                                <input type="radio" name="coachee" id={idFunction()} value={valueFunction()} onClick={ () => handleClick(scheduleObject.leader)}/>
                                                                    <label for={nameFunction()}> {scheduleObject.leader} </label>
                                                        </div>
                                                    </>
                                                ))}
                                </fieldset>
                            </form>
                        </div>
                    </div>

                <div className="leader-info-container">
                    <div class="image-wrapper-2" name="image-wrapper-2">
                        <div className="leader-info-list" name="leader-info-list">

                            <div className="leader-info leaderBookings" id="leader-info">
                                <h2 id="coacheeInfoHeading"> Unconfirmed Schedules </h2> */}

                                {/* {post.map(scheduleObject => (
                                                <>
                                                    <div className="coachee-info-block" name={coacheeBlockDisplay(scheduleObject.leader)}> */}

                                                    {/* <h3> { scheduleObject.leader }'s Booking </h3>   */}

                                                    {/* <h3> Coachee Name </h3>   */}


                                                    {/* <form onSubmit={handleApptSubmit}>

                                                        <div class="leaderNameBooking">
                                                            Coachee Name: <input type="text" name="leaderName" value={scheduleObject.leader}/>
                                                        </div>

                                                        <ul>
                                                            {scheduleObject.schedules.map(schedule => (
                                                                <>
                                                                    <div className="coachee-info-block" name={coacheeBlockDisplay(scheduleObject.leader)}> */}

                                                                        {/*<input type="text" name="leaderName" value={scheduleObject.leader}/>*/}
                                                                        {/* <div class="coachee-info-element">
                                                                            <label>
                                                                                <input type="radio" name={coacheeInfoFunction(scheduleObject.leader)} id={idFunction()} value={valueFunction()}/>
                                                                                <span className="bookingInfo">Date: {schedule.date}/{schedule.month}/{schedule.year}                       Time:{schedule.time}</span>

                                                                            </label>
                                                                        </div>

                                                                        <div class="bookingDetails">
                                                                            <input type="text" name="date" value={schedule.date}/>
                                                                            <input type="text" name="month" value={schedule.month}/>
                                                                            <input type="text" name="year" value={schedule.year}/>
                                                                            <input type="text" name="time" value={schedule.time}/>
                                                                        </div>
                                                                    </div>

                                                                </>
                                                                ))
                                                            }
                                                        </ul>

                                                        <button class="bookingConfirmationButton" type="submit">Submit</button>


                                                    </form>

                                                </div>





                                                </>
                                            ))}


                            </div>

                            <div class="solid-border"/> */}


                            {/* ZOOM LINK FOR EACH COACHEE*/}
                            {/* <div className="leader-info leaderZoomLinks" id="leader-info">
                                <h2 id="coacheeInfoHeading"> Zoom Link </h2>
                                    {leaderZoomLinks.map(leaderZoomLink=> (
                                                            <>

                                                                <h4 className="coachee-info-block" name={coacheeBlockDisplay(leaderZoomLink.leader)} id="coacheeZmLinkStyle">{leaderZoomLink.leader}'s current zoom link: {leaderZoomLink.zoomLink}</h4>


                                                                <form className="coachee-info-block" name={coacheeBlockDisplay(leaderZoomLink.leader)} onSubmit={handleZoomSubmit} id="zmLinkFormStyle">
                                                                    <h4 className="zmLinkHeaderStyle"><b>Input {leaderZoomLink.leader}'s new zoom link:</b></h4>
                                                                    <input type="text" name="leaderName" value={leaderZoomLink.leader}/>
                                                                    <br/>
                                                                    <input type="text" name="zoom" placeholder="Zoom Link..." required/>
                                                                    <button class="zmLinkSubmitStyle" type="submit">Submit</button>
                                                                </form>

                                                            </>
                                   ))}
                            </div>

                            <div class="solid-border"/> */}

                            {/* GOALS FOR COACHEE */}

                            {/* <div className="leader-info leaderGoal">
                                <h2 id="coacheeInfoHeading"> Coachee Goals </h2> */}
                                {/* This can be replaced with coachee goal view in future development */}
                                        {/* {goals.map(goalObject => (
                                                                <>
                                                                    <div className="coachee-info-block" name={coacheeBlockDisplay(goalObject.leader)}>

                                                                        <h4 className="zmLinkHeaderStyle">Main Goal </h4>
                                                                        <div className="coacheeInfoGoals">
                                                                            <b>{goalObject.mainGoal} </b>
                                                                        </div>

                                                                        <br/>


                                                                            {goalObject.subGoals.map(subGoal =>(
                                                                                <>
                                                                                    <h4 className="zmLinkHeaderStyle">Sub Goals </h4>

                                                                                    <div className="subGoalContainer">
                                                                                        <div id="subGoalFormStyle">

                                                                                            <form class="form-container" id="subGoalForm" onSubmit={handleRemoveSubgoal}> */}
                                                                                                {/* wrap leader input in div so that it can be hidden */}
                                                                                                {/* <div className="leaderNameSubgoalContainer">
                                                                                                    <input type="text" name="leaderName" value={goalObject.leader}/>
                                                                                                </div>

                                                                                                <div class="newScoreContentContainer">
                                                                                                    <input type="text" name="subGoal" placeholder={subGoal.subGoal} value={subGoal.subGoal}/>
                                                                                                    <button type="submit" className="removeSubGoalBtn">Remove Sub Goal</button> */}

                                                                                                {/* </div>
                                                                                            </form>
                                                                                        </div>
                                                                                    </div> */}





                                                                                    {/* {subGoal.actions.map(action => (
                                                                                    <>

                                                                                    <h4 className="zmLinkHeaderStyle">Actions </h4>

                                                                                        <div>
                                                                                            <div class="newSubGoalContainer" id="myForm">
                                                                                                <form class="form-container" id="subGoalForm" onSubmit={handleRemoveAction}>


                                                                                                    <div className="leaderNameSubgoalContainer">
                                                                                                        <input type="text" name="leaderName" value={goalObject.leader}/>
                                                                                                        <input type="text" name="subGoal" placeholder={subGoal.subGoal} value={subGoal.subGoal}/>
                                                                                                    </div>

                                                                                                    <div className="actionScoreContainer">

                                                                                                        <div className="coacheeInfoGoals actionBox">
                                                                                                            <input type="text" name="action" placeholder={action.action} value={action.action}/>
                                                                                                        </div>

                                                                                                        <div className="coacheeInfoGoals scoreBox">
                                                                                                            Score: {action.score}
                                                                                                        </div>
                                                                                                    </div>



                                                                                                    <button type="actionBtnStyle" onclass="btn scheduleSubmit" id="removeActionBtn">Remove Action</button>


                                                                                                </form>
                                                                                            </div>
                                                                                        </div> */}
                                                                                            {/* Temporary button for adding and removing actions */}
                                                                                            {/*}
                                                                                            <div className="actionBtnContainer">
                                                                                                <button class="actionBtnStyle" type="submit"> Add Action </button>
                                                                                                <button class="actionBtnStyle" type="submit"> Remove Action </button>
                                                                                            </div>
                                                                                            */}

                                                                                        {/* </>
                                                                                        ))} */}

                                                                                        {/* Add action to subgoal*/}
                                                                                        {/* <div class="newSubGoalContainer" id="myForm" >

                                                                                            <h4 className="zmLinkHeaderStyle">Add Action to Sub Goal </h4>

                                                                                            <form class="form-container"  id="subGoalForm" onSubmit={handleAddAction}>

                                                                                                <div className="leaderNameSubgoalContainer">
                                                                                                    <input type="text" name="leaderName" value={goalObject.leader}/>
                                                                                                    <input type="text" name="subGoal" placeholder={subGoal.subGoal} value={subGoal.subGoal}/>
                                                                                                </div>

                                                                                                <div class="newScoreContentContainer">
                                                                                                    <input type="text" name="action" placeholder="Add an action..." required/>
                                                                                                    <button type="submit" class="newSubGoalBtn">Add Action</button> */}
                                                                                                    {/* <button type="zmLinkSubmitStyle" class="btn scheduleSubmit">Add Action</button>  */}
                                                                                                {/* </div>
                                                                                            </form>
                                                                                        </div> */}

                                                                                        {/*<button type="actionBtnStyle" onSubmit={handleAddAction} onclass="btn scheduleSubmit">Add Action</button> */}



                                                                            {/* <br/> */}

                                                                                {/* Subgoal divider */}
                                                                                {/* <hr class="dash"/>


                                                                                </>
                                                                            ))} */}

                                                                    {/* Adding subgoals  moving this upwards*/}
                                                                    {/* <div class="newSubGoalContainer">
                                                                        <h4 class="addNewSubGoalHeader">Add New Sub Goals </h4>



                                                                        <form class="form-container" id="subGoalForm" onSubmit={handleAddSubgoal}>

                                                                            <div className="leaderNameSubgoalContainer">
                                                                                <input type="text" name="leaderName" value={goalObject.leader}/>
                                                                            </div>

                                                                            <div class="newScoreContentContainer">
                                                                                <input type="text" name="newSubGoal" placeholder="Add a subgoal..." required/>
                                                                                <button type="submit" class="newSubGoalBtn">Add Subgoal</button>
                                                                            </div>
                                                                        </form>
                                                                    </div>


                                                                </div> */}

{/* 
                                                                </>
                                    ))}
                            </div> */}


                            {/* <div class="solid-border"/> */}



                            {/* GOAL PROGRESS FOR COACHEE */}

                            {/*<div className="leader-info leaderProgress">
                                <h2 id="coacheeInfoHeading"> Coachee Goal Progress </h2>*/}
                                {/* This can be replaced with coachee goal progress view in future development */}
                                      {/*}  {post.map(scheduleObject => (
                                                                <>

                                                                <div className="coachee-info-block" name={coacheeBlockDisplay(scheduleObject.leader)}>
                                                                    <h4> Psychological Wellbeing </h4>
                                                                    <div class="w3-border">
                                                                        <div class="w3-grey psychologicalGraph"></div>
                                                                    </div>

                                                                    <h4> Transformational Leadership </h4>
                                                                    <div class="w3-border">
                                                                        <div class="w3-grey transformationalGraph"></div>
                                                                    </div>

                                                                    <h4> Goal Attainment </h4>
                                                                    <div class="w3-border">
                                                                        <div class="w3-grey goalGraph"></div>
                                                                    </div>
                                                                </div>


                                                                </>
                                      ))} */}

                           {/* </div>*/}

                            {/* <div class="solid-border"/> */}

                            {/* HOMEWORK FOR COACHEE */}

                            {/* <div className="leader-info leaderCreateHomework">
                                <h2 id="coacheeInfoHeading"> Create Homework </h2>

                                {post.map(scheduleObject => (
                                                                <>
                                                                    <div className="coachee-info-block" name={coacheeBlockDisplay(scheduleObject.leader)}>
                                                                        <div class="form" id="homeworkForm" onSubmit={handleHomeworkSubmit}>
                                                                            <form class="hwFormStyle">
                                                                                <label class="hwLabelStyle"><b>Coachee's Name</b></label>
                                                                                    <input class="hwInputStyle" type="text" name="leaderName" value={scheduleObject.leader}/>
                                                                                <label class="hwLabelStyle"><b>Title</b></label>
                                                                                    <input class="hwInputStyle" type="text" name="title" placeholder="Homework Title..." required/>
                                                                                <label class="hwLabelStyle"><b>Description</b></label>
                                                                                    <input type="text" name="description" placeholder="Homework Description..." required/>
                                                                                <button class="hwSubmitStyle" type="submit">Submit</button>
                                                                            </form>

                                                                        </div>
                                                                    </div>


                                                                </>
                                    ))}

                            </div>


                            <div className="leader-info leaderHomework">
                                <h2 id="coacheeInfoHeading"> Coachee Homework </h2>
                                    {leaderHws.map(leaderHomework => (
                                                    <>
                                                        <div className="coachee-info-block" name={coacheeBlockDisplay(leaderHomework.leader)}>
                                                            <span> {leaderHomework.leader} </span>
                                                            {leaderHomework.homeworks.map(homework => (
                                                                    <>
                                                                    <br/>
                                                                    <div class="coacheeHwContainer">

                                                                        <button className="toggleCoacheeHw" onClick={() => setIsOpen(!isOpen)}>
                                                                            <label>Homework title: </label>
                                                                            <input  className={isComplete(homework)} id={homework.title} type="text" for="cbox" name="title" value={homework.title}/>
                                                                        </button>
                                                                        {isOpen && <div className="coacheeHwContent">
                                                                            <label>Homework description: </label>
                                                                            <input  className={isComplete(homework)} id={homework.description} type="text" for="cbox" name="description" value={homework.description}/>
                                                                        </div>}
                                                                    </div>
                                                                    </>
                                                                ))
                                                            }
                                                            <br/>
                                                        </div>
                                                    </>
                                                ))
                                                }
                            </div>


                            <div> */}
                                 {/*Doesn't work btw  */}
                                {/* {leaderZoomLinks.map(leaderZoomLink => (
                                    <>
                                        <div className="coachee-info-block" name={coacheeBlockDisplay(leaderZoomLink.leader)}>
                                        <form >
                                        <fieldset>
                                        <div className="coachee-info-block coachee-info-element">
                                            
                                            <input type="text" name={coacheeInfoFunction(leaderZoomLink.leader)} id={idFunction()} value={valueFunction()} defaultValue={leaderZoomLink.zoomLink} />

                                        </div>
                                        </fieldset>
                                        </form>
                                        </div>
                                    </>
                                ))
                                }

                            </div>


                            <div class="solid-border"/> */}
                            {/* <div class="solid-border"/>
                            <div className="leader-info">
                            <MessagePopup
                                name={localStorage.getItem("messageLeader")}
                                toggleOpen={toggleOpen}
                                popup={false}
                                recipientId={leaderid}
                                senderId={coachid}
                            />
                            </div> */}



                        {/* </div>
                    </div>
                    
                    </div>
                   
                </div>


            </div> */}
            {/* <div>
                {showMessage ?
                    <>

                        <div class="wrapper">

                            <div class="main">
                            
                                <div>
                                    <i class="fas fa-times-circle message-icon-coacheespage" onClick={hideAlert}></i>
                                </div>


                                <div class="px-2 scroll" id="scroll-element">


                                    {messageHistory.map(prevMessage => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==prevMessage.name ?
                                                    <>
                                                        <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{prevMessage.name}</span>
                                                                <p class="msg">{prevMessage.message}</p>
                                                            </div>
                                                        </div>

                                                    </>
                                                    :
                                                    <>
                                                        <div class="d-flex align-items-center">
                                                            <div class="text-left pr-1">
                                                                <div class="pr-2 pl-1"> <span class="name">{prevMessage.name}</span>
                                                                    <p class="msg">{prevMessage.message}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>
                                                }
                                        </>
                                        )
                                    })}
                                    {messages.map(message => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==message.username ?
                                                <>
                                                    <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                </>
                                                :
                                                <>
                                                    <div class="d-flex align-items-center">
                                                        <div class="text-left pr-1">
                                                            <div class="pr-2 pl-1"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                }
                                            </>
                                            )
                                        })}
                                </div>
                                <nav class="message-input bg-white navbar-expand-sm d-flex justify-content-between">
                                    <form onSubmit={e => submit(e)}>
                                        <input type="text number" name="text" class="form-control" placeholder="Type a message..."
                                        value={message}  onChange={e => setMessage(e.target.value)} />
                                        <div class="icondiv d-flex justify-content-end align-content-center text-center ml-2">  <i class="fa fa-arrow-circle-right icon2"></i> </div>
                                    </form>
                                </nav>
                            </div>
                        </div>
                    </>
                    :
                    <>
                        <div class="message-icon-coachpage">
                            <i class="fas fa-comment" onClick={ () => handleClick(localStorage.getItem("messageLeader"))}> Message </i>
                        </div>
                    </>
                }
            </div> */}

{/* ENDS HEREEEEEEEEEE */}
        </>
    );
}

export default CoacheesPage;