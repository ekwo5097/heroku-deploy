import React, { useState, useEffect } from "react";
import "./Payment.css";
import axios from 'axios';

const Message = ({ message }) => (
  <section>
    <p>{message}</p>
  </section>
);
export default function Payment() {
  const [message, setMessage] = useState("");
  useEffect(() => {
    // Check to see if this is a redirect back from Checkout
    const query = new URLSearchParams(window.location.search);
    if (query.get("success")) {
      setMessage("Order placed! You will receive an email confirmation.");
      var planValue = query.get("plan");
      axios.post('https://new-emerge-website.herokuapp.com/api/change_subscription/',{
          username: localStorage.getItem("username"),
          token:localStorage.getItem("token"),
          planValue:planValue
          },
          console.log("planValue " + planValue + " username: " + localStorage.getItem("username") )
      )
      .then(response => {
          console.log(response.data)
      })
      .catch(err => {
      })
    }
    if (query.get("canceled")) {
      setMessage(
        "Order canceled -- continue to shop around and checkout when you're ready."
      );
    }
  }, []);
  return message ? (
    <Message message={message} />
  ) : (
    <></>
  );
}
