import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import './NavbarLeader.css';
import { Route, useHistory } from "react-router-dom";


function NavbarLeader() {
    let history = useHistory();
    const logout = event => {
        event.preventDefault();
        localStorage.removeItem("token");
        localStorage.removeItem("username");
        localStorage.removeItem("usertype");
        localStorage.removeItem("messageLeader");
        localStorage.removeItem("pusherTransportTLS");
        console.log("localStorage items removed");
        localStorage.setItem("refresh","true");
        history.push('/');
      }
    

    return (
        <>
          <div className="sidebar-homepage" id="sidebarContainer">
                    <nav className="navContainer-homepage">
                        <div>
                            <a href="#" className="nav-links-homepage nav-logo-homepage">
                                {/*<i className='fas fa-th-list'></i>*/}
                                <span className="nav-logo-name-homepage">{/*Dashboard*/}</span>
                            </a>

                            <div class="sidebar-menu-homepage">
                                <a href="#" class="nav-links-homepage">
                                    <Link to='/userhome'>
                                    <i class="fas fa-user nav-icon-homepage"></i>
                                    {/* <i class='bx bxs-user nav-icon-coachpage'></i> */}
                                    <span class="nav-name-homepage">Home</span></Link>
                                </a>
                                <a href="#" class="nav-links-homepage">
                                    <Link to='/coaches'>
                                    <i class="fas fa-id-badge nav-icon-homepage"></i>
                                    {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                    <span class="nav-name-homepage" name="abcd">Coach</span></Link>
                                </a>
                                <a href="#" class="nav-links-homepage">
                                    <Link to='/feedback'>
                                    <i class="fas fa-clipboard nav-icon-homepage"></i>
                                    <span class="nav-name-homepage">Feedback</span></Link>
                                </a>
                                <a href="#" class="nav-links-homepage">
                                    <Link to='/goalsettings'>
                                    <i class="fas fa-bullseye nav-icon-homepage"></i>
                                    {/* <i class='bx bx-target-lock nav-icon-coachpage'></i> */}
                                    <span class="nav-name-homepage">Goal Settings</span></Link>
                                </a>
                                <a href="#" class="nav-links-homepage">
                                    <Link to='/questionnaire'>
                                    {/* <i class="fas fa-bullseye nav-icon-homepage"></i> */}
                                    <i class="fas fa-question-circle nav-icon-homepage"></i>
                                    <span class="nav-name-homepage">Questionnaire</span></Link>
                                </a>


                                <a href="#" class="nav-links-homepage">
                                    <Link to='/settings'>
                                    <i class="fas fa-cog nav-icon-homepage"></i>
                                    {/* <i class='bx bxs-cog nav-icon-coachpage'></i> */}
                                    <span class="nav-name-homepage">Settings</span></Link>
                                </a>

                                <a href="#" class="nav-links-homepage" onClick={logout}>
                                    <Link to='/'>
                                    <i class='bx bxs-comment-edit nav-icon-homepage'></i>
                                    <span class="nav-name-homepage-log">Logout</span></Link>
                                </a>
                            </div>

                        </div>
                    </nav>
                </div>
       
        </>
    );
}



export default NavbarLeader;