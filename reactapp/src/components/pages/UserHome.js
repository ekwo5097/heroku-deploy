import 'material-icons/iconfont/material-icons.css';
import './UserHome.css';
import React from 'react';
import * as actions from '../../store/actions/auth';
import { Route, useHistory } from "react-router-dom";
import Progressbar from '../Progressbar'
import { Link, useLocation } from 'react-router-dom';
import Footer from '../LandingPage/Footer';
import { useEffect, useState, useRef } from 'react';
import Pusher from "pusher-js";
import axios from 'axios';
import NavbarLeader from '../SideNavbars/NavbarLeader';
import Calendar from '../Scheduling/Calendar';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import QuestNotComplete from './QuestNotComplete';
import NavbarCoach from '../SideNavbars/NavbarCoach';
import TopHeaderComponent from './TopHeaderComponent';
import Notification from '../Scheduling/Notification';
import AdminPage from '../AdminPages/AdminHomePage/AdminPage'
import VideoCameraFrontIcon from '@mui/icons-material/VideoCameraFront';
import './UserHomeCoach.css';
import { Grid, Paper } from "@mui/material";
import { styled, createTheme} from '@mui/material/styles';
import { Button, useRadioGroup } from '@mui/material';
import { Fab, Collapse } from "@mui/material";
import MessageRoundedIcon from "@mui/icons-material/MessageRounded";
import MessagePopup from "../Messaging/MessagePopup";
import '../LeaderPages/MyCoachPage/CoachPageDesign.css';
import CloseIcon from '@mui/icons-material/Close';
import LeaderActions from '../Actions/LeaderActions';
import ActionProgressCircle from '../Actions/ActionProgressCircle';

//   Regular Grid styling
const Item1 = styled(Paper)(({ theme }) => ({
    // ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'Center',
    borderRadius: '10px',
    // color: theme.palette.text.secondary,
    }));

//   Resources in evidence-based insights grid styling
const Item2 = styled(Paper)(({ theme }) => ({
    // ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'Center',
    borderShadow: 'none',
    border: 'RGB(204, 204, 204) solid 1px',
    background: 'RGB(255, 255, 255)',
    color: 'black',
    borderRadius: '10px',
    height: '60px',
    lineHeight: '50px',
    "&:hover": {
        border: 'RGB(204, 204, 204) solid 1px',
        color: 'RGB(255, 255, 255)',
        background: 'RGB(204, 204, 204)',
        cursor: 'pointer',
      },
    "&:active": {
    border: 'RGB(204, 204, 204) solid 1px',
    background: 'RGB(255, 255, 255)',
    color: 'RGB(204, 204, 204)',
    },
    // color: theme.palette.text.secondary,
  }));


//   Calendar grid styling
const Item3 = styled(Paper)(({ theme }) => ({
// ...theme.typography.body2,
padding: theme.spacing(1),
textAlign: 'Center',
background: 'blue',
color: 'RGB(255, 255, 255)',
border: 'blue solid 1px',
borderRadius: '10px',
height: '60px',
lineHeight: '50px',
"&:hover": {
    border: 'blue solid 1px',
    background: 'RGB(255, 255, 255)',
    color: 'blue',
    cursor: 'pointer',
    },
"&:active": {
    border: 'RGB(255, 255, 255) solid 1px',
    background: 'blue',
    color: 'RGB(255, 255, 255)',
    },
// color: theme.palette.text.secondary,
}));

//   Next coaching schedule grid styling
const Item4 = styled(Paper)(({ theme }) => ({
    // ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'Center',
    background: 'RGB(17, 40, 148)',
    color: 'RGB(255, 255, 255)',
    border: 'RGB(17, 40, 148) solid 1px',
    borderRadius: '10px',
    height: '60px',
    lineHeight: '50px',
    "&:hover": {
        border: 'RGB(17, 40, 148) solid 1px',
        background: 'RGB(255, 255, 255)',
        color: 'RGB(17, 40, 148)',
        cursor: 'pointer',
        },
    "&:active": {
        border: 'RGB(255, 255, 255) solid 1px',
        background: 'RGB(17, 40, 148)',
        color: 'RGB(255, 255, 255)',
        },
    // color: theme.palette.text.secondary,
    }));


var usertype = localStorage.getItem('usertype');

function scrollUp() {
    var chatHistory = document.getElementById("scroll-element");
    if(chatHistory)
    {
        chatHistory.scrollTop = chatHistory.scrollHeight;
    }
}

function UserHome() {
    const location = useLocation()


    const [appts, setAppts] =  useState({date:1,month:10,year:2021,time:'9:00am-10:00am',led:'sam'});
    const [app, setDay] = useState();
    const [post, setPost] =  useState([]);
    const [nextSchedule, setNextSchedule] =  useState([]);
    const [homeworks, setHomeworks] = useState([]);
    const [goals, setGoals] = useState([]);
    const [actions, setActions] = useState([]);
    const [hw, setHwComplete] = useState([]);
    const [isOpen, setIsOpen] = useState(false);
    const [score, setScore] =  useState([]);
    const [subgoals, setSubGoal] = useState([]);
    const [rsubgoals, setRemoveSubGoal] = useState([]);
    const [action, setAddAction] = useState([]);
    const [raction, setRemoveAction] = useState([]);
    const [openQStatus, setQStatus] = React.useState(false);
    const [questionnaireState, setQuestionnaireState] = useState(false);
    const [matchingState, setCompletedMatchingState] = useState(false); /* leader matched with coach */
    var leaderZoomLinksOpen = false;  // toggle display list of zoom links for each leader
    const socketRef = useRef();
    const[userid,setUserid]=useState();
    const [chatNotif, setChatNotif] = useState(false); 
    const [chatNotifCoach, setChatNotifCoach] = useState(false); 
    const [openCalendar, setOpenCalendar] = useState(false);
    const [coachName, setCoachName] = useState();



    const [leaderid, setleaderid]=useState();
    const [coachid, setcoachid]=useState();

    const [open, setOpen] = useState(false);

    const toggleOpen = () => {
        setOpen(!open);
    };

    const Alert = React.forwardRef(function Alert(props, ref) {
        return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
      });

    let history = useHistory();

    const onFocus = (event => {
        event.target.setAttribute('autocomplete', 'off');
        console.log(event.target.autocomplete);
      }
    )

    var usertype = localStorage.getItem('usertype');
    if(localStorage.getItem('usertype')=="L"){
        localStorage.setItem('messageLeader', localStorage.getItem("username"));
    }

    const [showMessage, setShowMessages] = useState(false);
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const[messageHistory,setMessageHistory]=useState([]);
    let allMessages = [];

    const pusher = new Pusher('e7e6508dcf5807d7881f', {
        cluster: 'ap2'
    });
    //MESSAGING CONSTS END
    function hideAlert() {
        setMessages([]);
        setMessageHistory([]);
        setShowMessages(false);
      }

    const logout = event => {
      event.preventDefault();
      localStorage.removeItem("token");
      localStorage.removeItem("username");
      localStorage.removeItem("usertype");
      localStorage.removeItem("messageLeader");
      localStorage.removeItem("pusherTransportTLS");
      console.log("localStorage items removed");
      localStorage.setItem("refresh","true");
     
      
      


    }

    const submit = event => {
        event.preventDefault();
        if(localStorage.getItem('usertype')=="C"){
            axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
            username: localStorage.getItem("username"),
             leaderUsername: localStorage.getItem('messageLeader'),
            message: message,

            },
            console.log("message " + message + " username: " + localStorage.getItem("username") )
            )
            .then(response => {
                setMessage('');
                console.log(response.data)
                scrollUp();
            })
            .catch(err => {
            })

        }
        else {

            axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
                username: localStorage.getItem("username"),
                leaderUsername: localStorage.getItem("username"),
                message: message,

                },
                console.log("message " + message + " username: " + localStorage.getItem("username") )
                )
                .then(response => {
                    setMessage('');
                    console.log(response.data)
                    scrollUp();
                })
                .catch(err => {
                })
        }
    }

    function handleClick() {
        setShowMessages(false);
        setMessages([]);
        setMessageHistory([]);

        //MESSAGING STUFF BEGINS HERE
        allMessages = [];
        console.log("allMessages IS THIS ---------------------" + allMessages)
        Pusher.logToConsole = true;
        var channel;
        channel = pusher.subscribe(localStorage.getItem('messageLeader'));

        channel.bind('message', function (data) {
            allMessages.push(data);
            setMessages(allMessages);

        })
        console.log("After pushing allMessages IS THIS ---------------------" + allMessages)

        //MESSAGING STUFF ENDS HERE
        //MESSAGING BEGINS HERE
        axios.post('https://new-emerge-website.herokuapp.com/api/getMessageHistory/',{
            username: localStorage.getItem("username"),
            leaderUsername: localStorage.getItem("messageLeader"),
            },console.log("username " + localStorage.getItem("username") + " leader: " + localStorage.getItem("messageLeader"))
            )
            .then(response => {
                setMessageHistory(response.data.messages)
                console.log(response.data.messages)
                scrollUp();

            })
            .catch(err => {
            })
        // MESSAGING ENDS HERE
        setShowMessages(true)
    }


    function handleNotifyy(){
        socketRef.current.send(JSON.stringify({notify:"a new message", }));
        console.log("button called");

    }

    useEffect(() => {


        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderid/', {
            leadername: localStorage.getItem("messageLeader"),})
            .then(response => {

                    if(localStorage.getItem('usertype')=='L'){
                        setUserid(response.data.leaderid)
                        console.log(userid)
                        
                    }
                    else {
                        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderCoacheeid/', {
                            username: localStorage.getItem("username"),
                            leaderName:localStorage.getItem("messageLeader"),      
                        })
                            .then(response => {
                                setUserid(response.data.coachid)
                                console.log(userid)
                                
                            })
                            .catch(err => {
                            });

                }})

                .catch(err => {
                });
        
                

        
        if(localStorage.getItem('usertype')=='L') {
            
            setTimeout(()=> {axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderid/', {
                leadername: localStorage.getItem("messageLeader"),})
                .then(response => {
                    //setConversation(response.data.leaderid);
                    console.log("conversation is "+ response.data.leaderid);
                    console.log("user id is "+userid)
                    
            

                    
                axios.post('https://new-emerge-website.herokuapp.com/api/getMessageNotifications/',{
                    roomid: response.data.leaderid,
                    userid: userid,
                    })
                    .then(response => {
                        // alert(response.data.new_message)
                        setChatNotif(true)
                        console.log(response.data.new_message)

                    })
                    .catch(err => {
                    });
                })
                .catch(err => {
                })},10000)
            }
            else if(localStorage.getItem('usertype')=='C') {
                setTimeout(()=>{axios.post('https://new-emerge-website.herokuapp.com/api/getCoachNotifications/',{
                        user: localStorage.getItem("username"),
                        userid: userid,
                        })
                        .then(response => {
                            // alert(response.data.new_message)
                            setChatNotifCoach(true); 
                            console.log(response.data.new_message)

                        })
                        .catch(err => {
                        })},10000)



            }

        

        // socketRef.current.send(JSON.stringify({notify:"a new message", }));
        // socketRef.current = new WebSocket('ws://'+ window.location.host)
        // socketRef.current.onopen = e => {
        // console.log('open', e)
        // }
        // socketRef.current.onmessage = e => {
        //     console.log(JSON.parse(e.data))
        // }
        // socketRef.current.onerror = e => {
        // console.log('error', e)
        // }

        axios.post('https://new-emerge-website.herokuapp.com/api/getMessageHistory/',{
            username: localStorage.getItem("username"),
            leaderUsername: localStorage.getItem("messageLeader"),
            },console.log("username " + localStorage.getItem("username") + " leader: " + localStorage.getItem("messageLeader"))
            )
            .then(response => {
                setMessageHistory(response.data.messages)
                console.log(response.data.messages)

            })
            .catch(err => {
            })

        axios.post('https://new-emerge-website.herokuapp.com/api/getQuestionnaireCompleted/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['completed'] == "True"){ 
                    // setQStatus(true);
                    setQuestionnaireState(true);
                }else{
                    // setQStatus(true);
                }
                
            })
            .catch(err => {
            })


        if (localStorage.getItem('usertype')=="L") {
        // first post for leader's next schedule
        axios.post('https://new-emerge-website.herokuapp.com/api/getNextSchedule/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setNextSchedule(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })

        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderCoachid/', {
                username: localStorage.getItem("username"),
                
            })
                .then(response => {
                    setleaderid(response.data.leaderid);
                    setcoachid(response.data.coachid);
                    setCoachName(response.data.coachname); 
    
                    
                })
                .catch(err => {
                })
            
        // second post for leader's homeworks
        axios.post('https://new-emerge-website.herokuapp.com/api/getHomework/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setHomeworks(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })

        // third post for leader's goals and actions
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderGoals/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setGoals(response.data['mainGoal'])
                setActions(response.data['actions'])
                console.log("leader goals and actions:")
                console.log(JSON.stringify(response.data))
            })
            .catch(err => {
            })

        // get leader questionnaire completion status
        axios.post('https://new-emerge-website.herokuapp.com/api/getQuestionnaireCompleted/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['completed'] == "True"){ 
                    // setQStatus(true);
                    setQuestionnaireState(true);
                }else{
                    // commented because homepage will not display incomplete questionnaire popup
                    // setQStatus(true);
                }
                
            })
            .catch(err => {
            })

        // get leader is matched status
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderIsAssigned/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['msg'] == "isMatched found"){ 
                    // setQStatus(true);
                    if (response.data['isMatched'] == "True"){
                        setCompletedMatchingState(true);
                    }
                }else{
                    // Some error occurred when retrieving leader's matched_with_coach
                    console.log(response.data['msg'])
                }
                
            })
            .catch(err => {
            })

            

        }}
        , [userid])

        const handleHomeworkComplete = React.useCallback(event => {
            event.preventDefault();
            const titleInput = event.target.elements.title; // accessing directly // accessing via `form.elements`
            const descriptionInput = event.target.elements.description; // get leader name for new zoom link
            axios.post('https://new-emerge-website.herokuapp.com/api/completeHomework/',{
                username: localStorage.getItem("username"),
                token: localStorage.getItem("token"),
                title: titleInput.value,
                description: descriptionInput.value,
                },
                console.log("titleInput: " + titleInput.value),
                console.log("descriptionInput: " + descriptionInput.value)
                )
                .then(response => {
                    setHwComplete(response.data)
                    console.log(response.data)
                })
                .catch(err => {
                })
                event.preventDefault();
        }, []);

        const handleScoreSubmit = React.useCallback(event => {
            event.preventDefault();
            const action = event.target.elements.action;
            const score = event.target.elements.newScore;
            const leaderInput = event.target.elements.leaderName;
            axios.post('https://new-emerge-website.herokuapp.com/api/updateActionScore/',{
                username: localStorage.getItem("username"),
                token: localStorage.getItem("token"),
                action: action.value,
                score: score.value,
                // leader: leaderInput.value ---> have to save leader data along with form
                },
                //console.log(zoomInput.value),
                )
                .then(response => {
                    setScore(response.data)
                    console.log(response.data)
                })
                .catch(err => {
                })
                event.preventDefault();

        }, []);

        //Handle when leader adds a subgoal
        const handleAddSubgoal = React.useCallback(event => {
            event.preventDefault();
            const leaderName = event.target.elements.leaderName;
            const subGoal = event.target.elements.newSubGoal;
            axios.post('https://new-emerge-website.herokuapp.com/api/leaderAddSubGoal/',{
                username: localStorage.getItem("username"),
                token: localStorage.getItem("token"),
                leaderUsername:leaderName.value,
                subGoal:subGoal.value

                }, console.log(subGoal.value),

                )
                .then(response => {
                    setSubGoal(response.data)
                    console.log(response.data)
                })
                .catch(err => {
                })
                event.preventDefault();

        }, []);

        //Handle when leader removes a subgoal
        const handleRemoveSubgoal = React.useCallback(event => {
            event.preventDefault();
            const leaderName = event.target.elements.leaderName;
            const subGoal = event.target.elements.subGoal;
            axios.post('https://new-emerge-website.herokuapp.com/api/leaderRemoveSubGoal/',{
                username: localStorage.getItem("username"),
                token: localStorage.getItem("token"),
                leaderUsername:leaderName.value,
                subGoal:subGoal.value

                }, console.log(subGoal.value),

                )
                .then(response => {
                    setRemoveSubGoal(response.data)
                    console.log(response.data)
                })
                .catch(err => {
                })
                event.preventDefault();

        }, []);

    //Handle when leader adds an action
    const handleAddAction = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.subGoal;
        const action = event.target.elements.action;
        axios.post('https://new-emerge-website.herokuapp.com/api/leaderAddAction/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value,
            action:action.value

            }, console.log(action.value),

            )
            .then(response => {
                setAddAction(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);


    //Handle when leader removes an action
    const handleRemoveAction = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.subGoal;
        const action = event.target.elements.action;
        axios.post('https://new-emerge-website.herokuapp.com/api/leaderRemoveAction/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value,
            action:action.value

            }, console.log(action.value),

            )
            .then(response => {
                setRemoveAction(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();

    }, []);

    function isComplete(homework) {
        if (homework.isFinished == true){
            // set class to strike-through
            return "strike-through"
        }
        return "standard-homework"

    }

    // Create upcoming and no upcoming sessions
    function createUpcomingSessions(sessions){
        for (var i = 0; i < sessions.length ; i++) {
            
            const outerDiv = document.createElement("div");
            outerDiv.className = "single-leader-session-container-div";
            outerDiv.id = "leader-div-" + i;

            // Append outer div to the leader-session container
            document.getElementById("leader-session-container").appendChild(outerDiv); 

            const name = document.createElement("div");
            name.className = "leader-session-description-div";
            name.innerHTML = sessions[i]['leader'];

            document.getElementById(outerDiv.id).appendChild(name); 

            // If leader has a booking
            if (sessions[i]['msg'] == "Upcoming booking"){

                const time = document.createElement("div");
                time.className = "single-leader-time";
                time.innerHTML = "Time: " + sessions[i]['earliestBookingDate'];

                const aLink = document.createElement("a");
                aLink.className = "single-leader-active-session-div";
                aLink.href = sessions[i]['meetingLink']
                aLink.innerHTML = "JOIN SESSION"

                const timeLinkContainer = document.createElement("div");
                timeLinkContainer.className = "leader-time-join-btn-holder";
                timeLinkContainer.id = "leader-div-time-join-btn-" + i;

                // Append time and join session button container to parent div, then append time and join session button to timeLinkContainer
                document.getElementById(outerDiv.id).appendChild(timeLinkContainer); 
                document.getElementById(timeLinkContainer.id).appendChild(time); 
                document.getElementById(timeLinkContainer.id).appendChild(aLink);

                const meetingId = document.createElement("div");
                meetingId.className = "single-leader-meeting-id-password";
                meetingId.innerHTML = "Meeting ID: " + sessions[i]['meetingId'];

                const password = document.createElement("div");
                password.className = "single-leader-meeting-id-password";
                password.innerHTML = "Password: " + sessions[i]['meetingPassword'];

                const meetingIdPasswordContainer = document.createElement("div");
                meetingIdPasswordContainer.className = "leader-meeting-id-password-holder";
                meetingIdPasswordContainer.id = "leader-div-meeting-id-password-" + i;

                // Append meeting id and password container to parent div, then append meeting id and password to meetingIdPasswordContainer
                document.getElementById(outerDiv.id).appendChild(meetingIdPasswordContainer); 
                document.getElementById(meetingIdPasswordContainer.id).appendChild(meetingId); 
                document.getElementById(meetingIdPasswordContainer.id).appendChild(password); 

            } else{
                // Else if user has no active sessions
                const noSession = document.createElement("div");
                noSession.className = "single-leader-no-session";
                noSession.innerHTML = "No Upcoming Session";

                // Append child to outerDiv
                document.getElementById(outerDiv.id).appendChild(name);
                document.getElementById(outerDiv.id).appendChild(noSession); 
            }

          }
    }


    // Opens list of sessions booked by each leader for coach userhome

    useEffect(() => {

        if (localStorage.getItem('usertype')=="C"){
            setTimeout(function() { 
                axios.post('https://new-emerge-website.herokuapp.com/api/coachGetZoomDetails/',{
                    username: localStorage.getItem("username"),
                    token: localStorage.getItem("token"),
                    },)
                    .then(response => {

                        const parentElement = document.getElementById("leader-session-container")

                        // Remove every child element of parent 
                        while (parentElement.firstChild) {
                            parentElement.firstChild.remove()
                          }

                        document.getElementById("leader-session-container").style.display = "block";
            
                        // Create join session buttons for each leader
                        createUpcomingSessions(response.data['coachSessions']);
                    })
                    .catch(err => {
                    })
            },2000)
        }

    })

    function handleOpenCalendar(){
        setOpenCalendar(true);
        document.getElementById("calendar-outer").style.display = "block";
        document.getElementById("calendar-outer").style.zIndex = "10";
        document.getElementById("close-calendar-outer").style.display="block";
        console.log("calendar open state: " + openCalendar)
    }

    function handleCloseCalendar(){
        setOpenCalendar(false);
        document.getElementById("calendar-outer").style.display = "none";
        document.getElementById("calendar-outer").style.zIndex = "-1";
        document.getElementById("close-calendar-outer").style.display="none";
        console.log("calendar open state: " + openCalendar)
    }

    const handleQStatusClose = (event) => {
        setQStatus(false);
    };

    const handleChatNotif = (event) => {
        setChatNotif(false);
    };

    const handleChatNotifCoach = (event) => {
        setChatNotifCoach(false);
    };

    if(localStorage.getItem('usertype')=="L") {

        if (questionnaireState == false){
            return (
                <>
                
                    {/* <div className="top-header">
                        <div className="logo-lettering">
                            <h1>EMERGE</h1>
                        </div>
                    </div> */}
                    <TopHeaderComponent/>

                    <NavbarLeader/>
                    {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
                    <div class="no-access-outer">
                        <div class="no-access-inner">
                            <h1 >Welcome {localStorage.getItem('username')}!</h1>
                            <div class="questionnaire-prompt-text">
                                <span>Please answer the questionnaire in order to be matched with a coach!</span>
                            </div>
                            <QuestNotComplete userType={(localStorage.getItem('usertype'))}/>
                        </div>
                    </div>

                    {/* <Snackbar open={openQStatus} autoHideDuration={5000} onClose={handleQStatusClose}>
                        <Alert onClose={handleQStatusClose} severity="info" sx={{ width: '70%' }}>
                    <Snackbar open={openQStatus} autoHideDuration={5000} onClose={handleQStatusClose}>
                        <Alert onClose={handleQStatusClose} severity="info" sx={{ width: '100%' }}>
                            You are not currently linked to a coach, please answer the questionnaire.
                            <div class="visit-questionnaire-outer">
                                <a href="#">
                                    <Link to='/questionnaire'>
                                        <div class="visit-questionnaire-div">
                                            <span>Visit Questionnaire</span>
                                        </div>
                                    </Link>
                                </a>
                            </div>
                        </Alert>
                    </Snackbar> */}

                </>
            );
        }
        
        if (matchingState == false){
            return (
            <>
            {/* <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>
            </div> */}
            <TopHeaderComponent/>


            <NavbarLeader/>

            <div class="no-access-outer">
                <div class="no-access-inner">
                    <h1 >Hi {localStorage.getItem('username')}!</h1>
                    <div class="questionnaire-prompt-text">
                        <span>Thank you for completing the questionnaire!</span><br/>
                        <span>Please wait as we are currently in the process of matching you with a coach.</span>
                    </div>
                </div>
            </div>

            </>
            );

        }

        // else{

    return (

        <>

            {/* Questionnaire Incomplete Flash */}

            <div>

            <div className="userhome">
                <TopHeaderComponent/>

                {/* =====sidebar====== */}
                <NavbarLeader/>

                {/* =====leader chat notification====== */}
                <Snackbar open={chatNotif} autoHideDuration={5000} onClose={handleChatNotif}>
                    <Alert onClose={handleChatNotif} severity="info" sx={{ width: '100%' }}>
                        You have a new message from your coach
                    </Alert>
                </Snackbar>

                

                <div class="main-cont">
                    <main>
                        {/* <button onClick={handleNotifyy}>notify</button> */}
                        <div class="personal-cont-wrapper">
                            <div class="roww">
                                <div class="col-left">
                                    <div class="col-sub2">
                                        <div class="row-mod">

                                            <h1 >Welcome {localStorage.getItem('username')}!</h1>
                                            <p>Your coach is here to help you achieve your goal one step at a time! <br/> Navigate to the coach tab on the sidebar to find all your coaching needs!</p>

                                        </div>
                                    </div>
                                    {/* <div class="col-sub2-actions">
                                        <div class="row-mod">

                                            -YOUR HOMEWORK COMPONENT-

                                            <h1 >Your Actions</h1>
                                            <span>
                                                

                                                    
                                                <>
                                                    <div>

                                                        <h4>Main Goal </h4>
                                                        <div className="coacheeInfoGoals">
                                                            <b>Main Goal: {goals} </b>
                                                        </div>
                                                        <br/>
                                                    </div>
                                                </>
                                                <LeaderActions actionsProp={actions} setActionsProp={setActions} />
                                                <ActionProgressCircle />
                                                            
                                            </span>
                                            
                                        </div>
                                    </div> */}
                                    
                                </div>
                                    <div class="col-right">
                                        <div class="col-sub3">
                                            <div class="row-mod">
                                                <div>
                                                    <h1 class="smaller-head">Why Evidence-based Coaching?</h1>
                                                    <span class="evidence-text">
                                                        Evidence-based coaching uses the latest scientific research
                                                        to give you the best results in the most optimized way possible.
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>                                

                            </div>

                            <div class="col-sub2-actions">
                                <div class="row-mod-leader-actions">

                                    {/* -YOUR HOMEWORK COMPONENT- */}
                                    <div class="your-actions-outer-div">
                                        <h1>Your Actions</h1>  
                                    </div>
                                    <span>
                                        <>
                                            <div>

                                                <h4>Main Goal </h4>
                                                <div className="coacheeInfoGoals">
                                                    <b>Main Goal: {goals} </b>
                                                </div>
                                                <br/>
                                            </div>
                                        </>
                                        {/* <LeaderActions actionsProp={actions} setActionsProp={setActions} /> */}
                                        <ActionProgressCircle />        
                                    </span>
                                            
                                </div>
                            </div>

                            <div class="messageWrapper">
                                <Collapse in={open} sx={{ flexDirection: "column-reverse" }}>
                                <MessagePopup
                                    name={coachName}
                                    toggleOpen={toggleOpen}
                                    popup={true}
                                    recipientId={coachid}
                                    senderId={leaderid}
                                /> 
                                </Collapse>
                                <Fab
                                color="primary"
                                aria-label="message"
                                onClick={() => toggleOpen()}
                                sx={{ mt: 1 }}
                                >
                                <MessageRoundedIcon />
                                </Fab>
                            </div>

        

                    


                            {/* <div>
                {showMessage ?
                    <>
                        <div class="wrapper">

                            <div class="main">

                                <div>
                                    <i class="fas fa-times-circle message-icon-coachpage2" onClick={hideAlert}></i>
                                </div>


                                <div class="px-2 scroll" id="scroll-element">


                                    {messageHistory.map(prevMessage => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==prevMessage.name ?
                                                    <>
                                                        <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{prevMessage.name}</span>
                                                                <p class="msg">{prevMessage.message}</p>
                                                            </div>
                                                        </div>

                                                    </>
                                                    :
                                                    <>
                                                        <div class="d-flex align-items-center">
                                                            <div class="text-left pr-1">
                                                                <div class="pr-2 pl-1"> <span class="name">{prevMessage.name}</span>
                                                                    <p class="msg">{prevMessage.message}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>
                                                }
                                        </>
                                        )
                                    })}
                                    {messages.map(message => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==message.username ?
                                                <>
                                                    <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                </>
                                                :
                                                <>
                                                    <div class="d-flex align-items-center">
                                                        <div class="text-left pr-1">
                                                            <div class="pr-2 pl-1"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                }
                                            </>
                                            )
                                        })}


                                </div>

                                <nav class="message-input bg-white navbar-expand-sm d-flex justify-content-between">
                                    <form onSubmit={e => submit(e)}>
                                        <input type="text number" name="text" class="form-control" placeholder="Type a message..."
                                        value={message}  onChange={e => setMessage(e.target.value)} onFocus={onFocus}/>
                                        <div class="icondiv d-flex justify-content-end align-content-center text-center ml-2">  <i class="fa fa-arrow-circle-right icon2"></i> </div>
                                    </form>
                                </nav>
                            </div>
                        </div>

                    </>
                    :
                    <>
                        <div class="message-icon-coachpage">
                            <i class="fas fa-comment" onClick={handleClick}> Message </i>
                        </div>
                    </>
                }
            </div> */}




                        </main>
                    </div>
                </div>
            </div>
            <div>
                <Footer />
            </div>
        </>

            );

        // }
    }
    else if (localStorage.getItem('usertype')=="C") {

        if (questionnaireState == false){
            return (
                <>
                
                    <TopHeaderComponent/>

                    <NavbarCoach/>
                    {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
                    <div class="no-access-outer">
                        <div class="no-access-inner">
                            <h1 >Welcome {localStorage.getItem('username')}!</h1>
                            <div class="questionnaire-prompt-text">
                                <span>Please answer the questionnaire to kickstart your first coaching session!</span>
                            </div>
                            <QuestNotComplete userType={(localStorage.getItem('usertype'))}/>
                        </div>
                    </div>

                    <Snackbar open={openQStatus} autoHideDuration={5000} onClose={handleQStatusClose}>
                        <Alert onClose={handleQStatusClose} severity="info" sx={{ width: '70%' }}>
                            Please fill in the questionnaire to be matched with a leader.
                            <div class="visit-questionnaire-outer">
                                <a href="#">
                                    <Link to='/questionnairecoach'>
                                        <div class="visit-questionnaire-div">
                                            <span>Visit Questionnaire</span>
                                        </div>
                                    </Link>
                                </a>
                            </div>
                        </Alert>
                    </Snackbar>

                    {/* <Snackbar open={chatNotifCoach} autoHideDuration={5000} onClose={handleChatNotifCoach}>
                        <Alert onClose={handleChatNotifCoach} severity="info" sx={{ width: '100%' }}>
                            You have a new message
                            <div class="visit-questionnaire-outer">
                                <a href="#">
                                    <Link to='/coachees'>
                                        <div class="visit-questionnaire-div">
                                            <span>Open Message</span>
                                        </div>
                                    </Link>
                                </a>
                            </div>
                        </Alert>
                    </Snackbar> */}

                </>
            );
        }else if(questionnaireState == true) {


        return (

            <>

                <div>
                <div className="userhome">
                    <div className="top-header">
                       <TopHeaderComponent/>

                    </div>

                    <Snackbar open={chatNotifCoach} autoHideDuration={5000} onClose={handleChatNotifCoach}>
                        <Alert onClose={handleChatNotifCoach} severity="info" sx={{ width: '100%' }}>
                            You have a new message from a leader
                            <div class="visit-questionnaire-outer">
                                <a href="#">
                                    <Link to='/coachees'>
                                        <div class="visit-questionnaire-div">
                                            <span>Open Message</span>
                                        </div>
                                    </Link>
                                </a>
                            </div>
                        </Alert>
                    </Snackbar>

                    {/* =====sidebar====== */}
                    <div className="sidebar-homepage" id="sidebarContainer">
                        <nav className="navContainer-homepage">
                            <div>


                                <div class="sidebar-menu-homepage">
                                    <a href="#" class="nav-links-homepage">
                                        <Link to='/userhome'>
                                        <i class="fas fa-user nav-icon-homepage"></i>
                                        {/* <i class='bx bxs-user nav-icon-coachpage'></i> */}
                                        <span class="nav-name-homepage">Home</span></Link>
                                    </a>
                                    <a href="#" class="nav-links-homepage">
                                        <Link to='/coachees'>
                                        <i class="fas fa-id-badge nav-icon-homepage"></i>
                                        {/* <i class='bx bxs-user-badge nav-icon-coachpage'></i> */}
                                        <span class="nav-name-homepage" name="coachee-name">Leaders</span></Link>
                                    </a>
                                    <a href="#" class="nav-links-homepage">
                                        <Link to='/coachfeedback'>
                                        <i class="fas fa-clipboard nav-icon-homepage"></i>
                                        <span class="nav-name-homepage">Feedback</span></Link>
                                    </a>
                                    <a href="#" class="nav-links-homepage">
                                        <Link to='/questionnairecoach'>
                                        <i class="fas fa-bullseye nav-icon-homepage" id="goals-standard-icon"></i>
                                        <span class="nav-name-homepage">Update my Info</span></Link>
                                    </a>
{/* 
                                    <a href="#" class="nav-links-homepage">
                                        <Link to='/settings'>
                                        <i class="fas fa-cog nav-icon-homepage"></i>
                                        <span class="nav-name-homepage">Settings</span></Link>
                                    </a> */}

                                    {/* <a href="#" class="nav-links-homepage">
                                    <Link to='/questionnairecoach'>
                                    <i class="fas fa-bullseye nav-icon-homepage"></i> 
                                    <i class="fas fa-question-circle nav-icon-homepage"></i>
                                    <span class="nav-name-homepage">Questionnaire</span></Link>
                                </a> */}

                                    {/* <a href="#" class="nav-links-homepage" onClick={logout}>
                                        <Link to='/'>
                                        <i class='bx bxs-comment-edit nav-icon-homepage'></i>
                                        <span class="nav-name-homepage-log">Logout</span>
                                        </Link>
                                </a> */}
                                </div> 

                            </div>
                        </nav>
                    </div>

                    {/* Calendar */}
                    <div class="calendar-outer-div" id="calendar-outer">
                        <div class="calendar-coach-page-div" id="calendar-mycoach-div">
                            <div class="close-calendar-outer-div" id="close-calendar-outer">
                                <div class="close-calendar-inner-div">
                                    <div class="close-calendar-btn-div" id="calendar-close-btn" onClick={handleCloseCalendar}>
                                        <CloseIcon/>
                                    </div>
                                </div>
                            </div>
                            <Calendar userType={'C'} userName={localStorage.getItem('username')}/>
                        </div>
                    </div>

                    {/* Calendar notifications */}
                    <div class="notification-coach-page-div">
                        <Notification></Notification>
                    </div>
                    <Snackbar open={chatNotifCoach} autoHideDuration={5000} onClose={handleChatNotifCoach}>
                        <Alert onClose={handleChatNotifCoach} severity="info" sx={{ width: '100%' }}>
                            You have a new message from a leader
                            <div class="visit-questionnaire-outer">
                                <a href="#">
                                    <Link to='/coachees'>
                                        <div class="visit-questionnaire-div">
                                            <span>Open Message</span>
                                        </div>
                                    </Link>
                                </a>
                            </div>
                        </Alert>
                    </Snackbar>

                    


                    <div class="main-cont">
                        <main>
                            {/* <div class="personal-cont-wrapper"> */}
                                {/* <div class="roww">  */}
                                    {/* <div class="col-left"> */}
                                        {/* <div class="col-sub2">
                                            <div class="row-mod">
                                            <h1 name="coach-name">Welcome  {localStorage.getItem('username')}!</h1> */}
                                            {/* <h1> Your next supervision </h1> */}

                                            {/* <div className="bookingTimeContainer"> */}
                                                {/* <div className="bookingDate flex1">
                                                    <small>DATE</small>
                                                    <h2>{appts.date}</h2>
                                                </div>
                                                <div className="bookingDate flex1">
                                                    <small>MONTH</small>
                                                    {<h2>{appts.month}</h2>}
                                                </div>
                                                <div className="bookingDate flex1">
                                                    <small>YEAR</small>
                                                    {<h2>{appts.year}</h2>}
                                                </div>
                                                <div className="bookingDate flex2">
                                                    <small>TIME</small>
                                                    {<h2>{appts.time}</h2>}
                                                </div> */}
                                                {/*<div className="bookingDate flex2">
                                                    <small>STATUS</small>
                                                    {<h2>{appts.led}</h2>}
                                                </div>*/}
                                            {/* </div> */}

                                            {/* <div className="zoomLinkContainer">
                                                <div className="bookingDate flex2">
                                                    <small>ZOOM LINK</small>
                                                </div>
                                            </div> */}

                                            {/* </div> */}
                                        {/* </div> */}
                                        {/* <div class="col-sub2-actions">
                                            <div class="row-mod">
                                            <h1 >Your Actions</h1>
                                                <span>
                                                    <ul>
                                                        <li>Homework 1</li>
                                                        <li>Homework 2</li>
                                                        <li>Homework 3</li>
                                                        <li>Homework 4</li>
                                                    </ul>
                                                </span>
                                            </div>
                                        </div> */}
                                    {/* </div> */}
                                        {/* <div class="col-right"> */}
                                            {/* <div class="col-sub3">
                                                <div class="row-mod">
                                                    <div> */}
                                                        {/*<h1 class="smaller-head">Evidenced-based Insights</h1>*/}
                                                        {/* <h1> Evidenced-based Insights </h1>
                                                        <div className="evidence-btn-group">
                                                            <a href='https://www.researchgate.net/publication/322856328_Lifestyle_coaching_for_mental_health_difficulties_scoping_review'><button className="evidence-btn">Article 1</button></a>
                                                            <a href='https://www.researchgate.net/publication/233682110_The_impact_of_life_coaching_on_goal_attainment_metacognition_and_mental_health'><button className="evidence-btn">Article 2</button></a>
                                                        </div> */}

                                                    {/* </div>
                                                </div>  */}
                                            {/* </div> */}
                                            {/* <div class="col-sub3">
                                                <div class="row-mod">
                                                    <div> */}
                                                        {/* <h1 class="smaller-head"><i class="fa fa-calendar" aria-hidden="true"></i> Meetings </h1>
                                                        <h1> Your next supervision </h1> */}

                                                        {/* <div className="bookingTimeContainer">
                                                            <div className="bookingDate flex1">
                                                                <small>DATE</small>
                                                                <h2>11</h2>
                                                            </div>
                                                            <div className="bookingDate flex1">
                                                                <small>MONTH</small>
                                                                {<h2>12</h2>}
                                                            </div>
                                                            <div className="bookingDate flex1">
                                                                <small>YEAR</small>
                                                                {<h2>2021</h2>}
                                                            </div>
                                                            <div className="bookingDate flex2">
                                                                <small>TIME</small>
                                                                {<h2>9:00am-10:00am</h2>}
                                                            </div>
                                                        </div> */}
                                                    {/* </div>
                                                    </div>
                                                    </div> */}

                                                    {/* <div class="col-sub3">
                                                        <div class="row-mod">
                                                            <div> */}

                                                        {/* <h1> Your next peer to peer meeting </h1>

                                                        <div className="bookingTimeContainer">
                                                            <div className="bookingDate flex1">
                                                                <small>DATE</small>
                                                                <h2>11</h2>
                                                            </div>
                                                            <div className="bookingDate flex1">
                                                                <small>MONTH</small>
                                                                {<h2>12</h2>}
                                                            </div>
                                                            <div className="bookingDate flex1">
                                                                <small>YEAR</small>
                                                                {<h2>2021</h2>}
                                                            </div>
                                                            <div className="bookingDate flex2">
                                                                <small>TIME</small>
                                                                {<h2>10:00am-11:00am</h2>}
                                                            </div>
                                                        </div> */}

                                                    {/* </div>
                                                </div>
                                            </div>        */}
                                        {/* </div> */}




                                    {/* Updated coach homepage content */}

                                <div class="coach-home-page-grid-div">
                                    <div class="coach-home-page-grid-inner-div">
                                        <Grid container wrap="wrap" columnSpacing={3} rowSpacing={2}>
                                            <Grid item xs={12} sm={12}>
                                                <Item1>
                                                    <h2>Welcome {localStorage.getItem('username')}!</h2>
                                                    <h7>Don't forget to check your leaders' messages</h7>
                                                    <br/>
                                                    <h7></h7>
                                                </Item1>
                                            </Grid>

                                            <Grid item xs={12} sm={12}>
                                                <Item1>
                                                    <h2>Evidence-based Insights</h2>
                                                    <Grid container wrap="wrap" columnSpacing={3} rowSpacing={2}>
                                                        <Grid item xs={4} sm={4}>
                                                            <Item2>Article</Item2>
                                                        </Grid>
                                                        <Grid item xs={4} sm={4}>
                                                            <Item2>Survey</Item2>
                                                        </Grid>
                                                        <Grid item xs={4} sm={4}>
                                                            <Item2>Poll</Item2>
                                                        </Grid>
                                                    </Grid>
                                                </Item1>
                                            </Grid> 

                                            {/* Calendar and Next Schedule */}

                                            <Grid item xs={12} sm={12}>
                                                <Item3 onClick={handleOpenCalendar}>VIEW CALENDAR</Item3>
                                            </Grid>


                                            <Grid item xs={12} sm={12}>
                                                <Item1>
                                                    <h2>Your Next Coaching Sessions</h2> 

                                                    {/* These are hard-coded for design purposes */}
                                                    
                                                    <div class="leader-session-container-div" id="leader-session-container"></div>
                                                </Item1>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </div>

                                        {/* Leave for reference */}
                                        {/* <h1> Evidenced-based Insights </h1>
                                        <div className="evidence-btn-group">
                                            <a href='https://www.researchgate.net/publication/322856328_Lifestyle_coaching_for_mental_health_difficulties_scoping_review'><button className="evidence-btn">Article 1</button></a>
                                            <a href='https://www.researchgate.net/publication/233682110_The_impact_of_life_coaching_on_goal_attainment_metacognition_and_mental_health'><button className="evidence-btn">Article 2</button></a>
                                        </div> */}



                                    
                                        {/* <div className="calendar-userhome-container">

                                            <div className="all-leader-sessions-div">
                                                <div class="open-coaching-sessions-div" id="open-coaching-session" onClick={handleOpenLeaderSessions}>OPEN COACHING SESSIONS</div>
                                                <div class="leader-session-container-div" id="leader-session-container">
                                                    
                                                </div>
                                            </div>

                                            <h1>Your Schedule</h1>
                                            <Calendar userType={(localStorage.getItem('usertype'))} userName={localStorage.getItem('username')} />
                                        </div> */}
                                        

                                    {/* </div>

                                </div> */}
                            

                                <div>
                {/* {showMessage ?
                    <>
                        <div class="wrapper">

                            <div class="main">

                                <div>
                                    <i class="fas fa-times-circle message-icon-coachpage2" onClick={hideAlert}></i>
                                </div>


                                <div class="px-2 scroll" id="scroll-element">


                                    {messageHistory.map(prevMessage => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==prevMessage.name ?
                                                    <>
                                                        <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{prevMessage.name}</span>
                                                                <p class="msg">{prevMessage.message}</p>
                                                            </div>
                                                        </div>

                                                    </>
                                                    :
                                                    <>
                                                        <div class="d-flex align-items-center">
                                                            <div class="text-left pr-1">
                                                                <div class="pr-2 pl-1"> <span class="name">{prevMessage.name}</span>
                                                                    <p class="msg">{prevMessage.message}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>
                                                }
                                        </>
                                        )
                                    })}
                                    {messages.map(message => {
                                        return (
                                            <>
                                                { localStorage.getItem('username')==message.username ?
                                                <>
                                                    <div class="d-flex align-items-center text-right justify-content-end ">
                                                            <div class="pr-2"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                </>
                                                :
                                                <>
                                                    <div class="d-flex align-items-center">
                                                        <div class="text-left pr-1">
                                                            <div class="pr-2 pl-1"> <span class="name">{message.username}</span>
                                                                <p class="msg">{message.message}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </>
                                                }
                                            </>
                                            )
                                        })}


                                </div>

                                <nav class="message-input bg-white navbar-expand-sm d-flex justify-content-between">
                                    <form onSubmit={e => submit(e)}>
                                        <input type="text number" name="text" class="form-control" placeholder="Type a message..."
                                        value={message}  onChange={e => setMessage(e.target.value)} onFocus={onFocus}/>
                                        <div class="icondiv d-flex justify-content-end align-content-center text-center ml-2">  <i class="fa fa-arrow-circle-right icon2"></i> </div>
                                    </form>
                                </nav>
                            </div>
                        </div>

                    </>
                    :
                    <>
                        <div class="message-icon-coachpage">
                            <i class="fas fa-comment" onClick={handleClick}> Message </i>
                        </div>
                    </>
                } */}
            </div>



                            </main>
                        </div>
                    </div>
                </div>
                <div>
                    <Footer />
                </div>
            </>

            );
        }
    }else if (localStorage.getItem('usertype')=="A") {
        return (
            <>
                <AdminPage />
            </>
        );
    }
}

export default UserHome;