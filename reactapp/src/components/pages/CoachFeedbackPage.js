import React from 'react';
import './CoachFeedbackPage.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as actions from '../../store/actions/auth';
import { useEffect, useState } from 'react';
import NavbarCoach from '../SideNavbars/NavbarCoach';
import TopHeaderComponent from './TopHeaderComponent';
import {
    Grid,
    Paper,
} from "@mui/material";
import { styled, createTheme} from '@mui/material/styles';

const theme = createTheme({
    palette: {
      primaryConfirm: {
        main: '#669D41',
      },
      white: {
        main: '#ffffff',
      },
      primaryDelete: {
        main: '#ec1313',
      },
    },
  });

const Item = styled(Paper)(({ theme }) => ({
    // ...theme.typography.body2,
    padding: theme.spacing(1),
    textAlign: 'Center',
    // color: theme.palette.text.secondary,
  }));


function CoachFeedbackPage() {

    const [feedbacks, setAllFeedback] = React.useState([])

    useEffect(() => {
    axios.post('https://new-emerge-website.herokuapp.com/api/getLeadersFeedback/',{
        username: localStorage.getItem("username"),
        token: localStorage.getItem("token"),
        },
        )
        .then(response => {
            setAllFeedback(response.data)
            //console.log(response.data)
        })
        .catch(err => {
        })
    }
    , [])

    return (
        <>
            {/* =====sidebar====== */}
            
            <NavbarCoach/>
            <TopHeaderComponent/>

            

            <div class="feedback-main-content">
                <div class="feedback-border">
                    <div class="box-container">
                        <div class="Feedback-title">Feedback of Coachees</div>
                            <div class="Instruction-container">
                                
                                <br/>
                                <br/>



                            </div>
                    </div>

                    <hr class="dash"/>
                    
                    {/* Feedback of Coachees start here */}

                    <div class="box-container">
                        <div className="feedback-container">
                            

                            {feedbacks.map(feedback => (
                                <>
                                    

                                    {feedback.feedback.map(answer => (
                                        <>
                                        <div class="feedback-body-adminside">
                                            <Grid container wrap="wrap" columnSpacing={3} rowSpacing={2}>
                                                <Grid item xs={12} sm={12}>
                                                    <Item><h4>{feedback.leaderUsername}'s Feedback</h4></Item>
                                                </Grid>
                                                <Grid item xs={12} sm={12}>
                                                    <Item><div class="feedback-time">{feedback.timestamp}</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q1.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8}>
                                                    <Item>The coaching was effective in helping me reach my goals.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q1}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q2.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>I valued the time we spent having a coaching conversation.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q2}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q3.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>In the coaching session I felt able to present my own ideas.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q3}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q4.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>The coach showed that they understood my feelings.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q4}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q5.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>By the end of the coaching session I had greater clarity about the issues I face.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q5}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q6.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>The goals we set during coaching were very important to me.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q6}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q7.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>The coach was very good at helping me develop clear, simple and achievable action plans.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q7}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q8.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>When coaching, the coach spent more time analysing the problem rather than developing solutions.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q8}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q9.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>My coach asked me about my progress towards my goals.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q9}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Q10.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={8} zeroMinWidth>
                                                    <Item>We discussed any performance shortfalls or failure to complete actions steps.</Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><strong>{answer.q10}</strong></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={2}>
                                                    <Item><div class="question-text"> Comments.</div></Item>
                                                </Grid>
                                                <Grid item xs={4} sm={10} zeroMinWidth>
                                                    <Item>{answer.comment}</Item>
                                                </Grid>
                                            </Grid>
                                        </div>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </>
                                    ))}

                                    <hr class="small-dash"/>
                                </>
                                
                            ))}
                    
                        
                        </div>
                    
                    </div>

                </div>
            </div>

        </>
    );
}

export default CoachFeedbackPage;