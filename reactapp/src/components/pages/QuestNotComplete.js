import { Link } from 'react-router-dom';
import { Button } from "@mui/material";
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import './QuestNotComplete.css';

export default function QuestNotComplete({ userType }){

    var userLink = '/questionnaire'
    var msg = 'FIND A COACH!'

    if (userType == "C"){
        userLink = '/questionnairecoach'
        msg = 'VIEW QUESTIONNAIRE!'
    }

    const Item = styled(Paper)(({ theme }) => ({
        ...theme.typography.body2,
        padding: theme.spacing(1),
        textAlign: 'center',
        color: theme.palette.text.secondary,
      }));


    return(
        <>

            <div class="not-complete-outer-div">
                <div class="not-complete-inner-div">
                    <a href="#">
                        <Link to={userLink}>
                            <Button 
                            type="submit"
                            fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                            style={{ width: '200px', background: 'RGB(17, 40, 148)'}}>
                                {msg}
                            </Button>
                        </Link>
                    </a>
                </div>
            </div>

        </>
    );

}