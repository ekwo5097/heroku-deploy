// install material icons -> npm install material-icons@latest
import 'material-icons/iconfont/material-icons.css';

//installed boxicons under reactapp -> npm install
// import 'boxicons/css/boxicons.css';

import './CoacheePageMod.css';
import React, {useEffect, useState} from 'react';
import { Route, useHistory } from "react-router-dom";
import { Link } from 'react-router-dom';
import * as actions from '../../store/actions/auth';
import * as post from '../../store/actions/post';
import axios from 'axios';
import Messages from './Messages';
import Pusher from "pusher-js";
import NavbarCoach from '../SideNavbars/NavbarCoach';



function CoacheePageMod() {
    const [isOpen, setIsOpen] = useState(false);
    const [allLeaders, setAllLeaders] = useState([])

    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderToCoach/', {
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token")
        })
            .then(response => {
                setAllLeaders(response.data.leaders)
                console.log(JSON.stringify(response.data.leaders))
            })
            .catch(err => {
            });
    
    }, [])

   

    return (

        <>

            <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

            </div>


            <NavbarCoach/>

            <div class="leader-container">

                <div class="choose-leader">
                    <h1 id="title-text">Select Coachee </h1>
                    <select id="selectedCoachee" class="dropdownbox-coachee" >
                        {allLeaders.map(leader => (
                            <>
                            <option value={leader.username}> {leader.username}</option>

                            </>
                        ))}
                    </select>
                </div>

                <div className="coachee-info" >
                    <h2 id="coacheeInfoHeading"> Coachee Goal </h2>

                    <h3>Main Goal</h3>
                    <div className="coacheeInfoGoals">
                        <b>Increasing ambiguity tolerance and resilience</b>
                    </div>

                    <h3>Sub Goals</h3>

                    <form class="form-container" id="subGoalForm" >
                        <div class="newScoreContentContainer">
                            <input type="text" name="subGoal"  value="Subgoal 1"/>
                            <button type="submit" className="removeSubGoalBtn">Remove Sub Goal</button>
                        </div>
                   </form>
                
                   <div class="solid-border"/>


                    <h3 id="sub-title">Add New Sub Goals</h3>
                    <form class="form-container" id="subGoalForm">
                        <div class="newScoreContentContainer">
                            <input type="text" name="newSubGoal" placeholder="Add a subgoal..." required/>
                            <button type="submit" class="newSubGoalBtn">Add Subgoal</button>
                        </div>
                    </form>
                </div>

                <div className="coachee-info" >
                    <h2 id="coacheeInfoHeading-HW"> Coachee's Homework </h2>

                        <div class="coacheeHwContainer">
                            <button className="toggleCoacheeHw" onClick={() => setIsOpen(!isOpen)}>
                                <label>Homework title: </label>
                                <input type="text" for="cbox" name="title" value="Meditate"/>
                            </button>
                            {isOpen && <div className="coacheeHwContent">
                                <label>Homework description: </label>
                                <input type="text" for="cbox" name="description" value="Meditate for 10 minutes every morning"/>
                            </div>}
                        </div>
                        
                    

                    <div>
                        <h3 id="sub-title-hw">Add New Homework</h3>
                        <form class="hwFormStyle">
                            <label class="hwLabelStyle"><b>Title</b></label>
                            <input class="hwInputStyle" type="text" name="title" placeholder="Homework Title..." required/>
                            <label class="hwLabelStyle"><b>Description</b></label>
                            <textarea type="text" name="description" placeholder="Homework Description..." required/>
                            <button class="hwSubmitStyle" type="submit">Submit</button>
                        </form>
                    </div>
                </div>



            </div>

                

                
                           
                

        </>
    );
}

export default CoacheePageMod;
