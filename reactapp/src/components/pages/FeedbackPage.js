import React from 'react';
import './FeedbackPage.css';
import { Link } from 'react-router-dom';
import axios from 'axios';
import * as actions from '../../store/actions/auth';
import NavbarLeader from '../SideNavbars/NavbarLeader';
import {
    TextField,
} from "@mui/material";
import { useHistory } from "react-router-dom";


import { useEffect, useState } from 'react';
import { questionnaire } from '../../store/actions/post';
import QuestNotComplete from './QuestNotComplete';
import TopHeaderComponent from './TopHeaderComponent';

function FeedbackPage() {

    let history = useHistory();
    const [questionnaireState, setQuestionnaireState] = useState(false);
    const [matchingState, setCompletedMatchingState] = useState(false); /* leader matched with coach */

    const [post, setFeedback] = React.useState({})
    // const [comment, setComment] = React.useState()

    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getQuestionnaireCompleted/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['completed'] === "True"){
                    setQuestionnaireState(true);
                }
            })
            .catch(err => {
            })
    }, [])

    useEffect(() => {
        // get leader is matched status
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderIsAssigned/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['msg'] == "isMatched found"){ 
                    // setQStatus(true);
                    if (response.data['isMatched'] == "True"){
                        setCompletedMatchingState(true);
                    }
                }else{
                    // Some error occurred when retrieving leader's matched_with_coach
                    console.log(response.data['msg'])
                }
                
            })
            .catch(err => {
            })
    }, [])



    const handleFeedbackSubmit = React.useCallback(event => {
        event.preventDefault();
        //const leaderInput = event.target.elements.leaderName; // get leader name from form (this is already manually set)
        const q1 = event.target.elements.q1;
        const q2 = event.target.elements.q2;
        const q3 = event.target.elements.q3;
        const q4 = event.target.elements.q4;
        const q5 = event.target.elements.q5;
        const q6 = event.target.elements.q6;
        const q7 = event.target.elements.q7;
        const q8 = event.target.elements.q8;
        const q9 = event.target.elements.q9;
        const q10 = event.target.elements.q10;
        const comment = event.target.elements.comment;
        //const titleInput = event.target.elements.title; // accessing directly
        //const descInput = event.target.elements.description; // accessing via `form.elements`
        axios.post('https://new-emerge-website.herokuapp.com/api/submitFeedback/', {
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            q1: q1.value,
            q2: q2.value,
            q3: q3.value,
            q4: q4.value,
            q5: q5.value,
            q6: q6.value,
            q7: q7.value,
            q8: q8.value,
            q9: q9.value,
            q10: q10.value,
            comment: comment.value,
        },
            console.log(q1.value),
            console.log(q2.value),
            console.log(q3.value),
            console.log(q4.value),
            console.log(q5.value),
            console.log(q6.value),
            console.log(q7.value),
            console.log(q8.value),
            console.log(q9.value),
            console.log(q10.value),
            console.log(comment.value),
        )
            .then(response => {
                setFeedback(response.data)
                console.log(response.data)
                event.preventDefault();
                history.push('/userhome');
            })
            .catch(err => {
            })
        event.preventDefault();
    }, []);

    if (questionnaireState == false){
        return (
            <>
            
            <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

            </div>

            <NavbarLeader/>
            {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
            <div class="no-access-outer">
                <div class="no-access-inner">
                    You don't have access to this page!
                </div>
            </div>
            
            </>
        )
    }

    if (matchingState == false){
        return (
        <>
        
        <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

            </div>

            <NavbarLeader/>
            {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
            <div class="no-access-outer">
                <div class="no-access-inner">
                    You don't have access to this page!
                </div>
            </div>
        </>
        );
    }

    return (
        <>
            <NavbarLeader/>
            <TopHeaderComponent/>


            <div class="feedback-main-content">
                <div class="feedback-border">

                    <div class="box-container">

                        <div class="Feedback-title">Feedback</div>

                        <div class="Instruction-container">
                            
                            <div class="Instruction-text">
                                Please be frank and open in your response, as this will form the basis for accurate feedback on your coach’s coaching skills.
                            </div>

                            <div class="Instruction-text">
                                Please indicate the extent to which you agree or disagree with the following statements. Do not spend too much time on any question.
                            </div>

                            <br />
                            <div class="Instruction-text">
                                Think about the coaching session you just had with your coach as you answer these questions.
                            </div>
                            <br />
                            <br />

                        </div>
                    </div>

                    <hr class="dash" />

                    {/* Feedback form starts here */}

                    <div class="box-container">
                        <form className="feedback-container" onSubmit={handleFeedbackSubmit}>

                            <div class="feedback-text">
                                1. The coaching was effective in helping me reach my goals
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            {/* <BorderLinearProgress variant="determinate" value={50} />
                            <br/> */}
                            <div class="radio-body">
                                <div class="radio" name="q1">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-1" type="radio" value="1" name="q1" id="q1-myRadio1" required />
                                    <label class="radio__label-1" for="q1-myRadio1">1</label>
                                    <input class="radio__input-1" type="radio" value="2" name="q1" id="q1-myRadio2" />
                                    <label class="radio__label-1" for="q1-myRadio2">2</label>
                                    <input class="radio__input-1" type="radio" value="3" name="q1" id="q1-myRadio3" />
                                    <label class="radio__label-1" for="q1-myRadio3">3</label>
                                    <input class="radio__input-1" type="radio" value="4" name="q1" id="q1-myRadio4" />
                                    <label class="radio__label-1" for="q1-myRadio4">4</label>
                                    <input class="radio__input-1" type="radio" value="5" name="q1" id="q1-myRadio5" />
                                    <label class="radio__label-1" for="q1-myRadio5">5</label>
                                    <input class="radio__input-1" type="radio" value="6" name="q1" id="q1-myRadio6" />
                                    <label class="radio__label-1" for="q1-myRadio6">6</label>
                                    <input class="radio__input-1" type="radio" value="7" name="q1" id="q1-myRadio7" />
                                    <label class="radio__label-1" for="q1-myRadio7">7</label>
                                    <input class="radio__input-1" type="radio" value="8" name="q1" id="q1-myRadio8" />
                                    <label class="radio__label-1" for="q1-myRadio8">8</label>
                                    <input class="radio__input-1" type="radio" value="9" name="q1" id="q1-myRadio9" />
                                    <label class="radio__label-1" for="q1-myRadio9">9</label>
                                    <input class="radio__input-1" type="radio" value="10" name="q1" id="q1-myRadio10" />
                                    <label class="radio__label-1" for="q1-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>

                            <hr class="small-dash" />

                            <div class="feedback-text">
                                2. I valued the time we spent having a coaching conversation
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q2">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-2" type="radio" value="1" name="q2" id="q2-myRadio1" required />
                                    <label class="radio__label-2" for="q2-myRadio1">1</label>
                                    <input class="radio__input-2" type="radio" value="2" name="q2" id="q2-myRadio2" />
                                    <label class="radio__label-2" for="q2-myRadio2">2</label>
                                    <input class="radio__input-2" type="radio" value="3" name="q2" id="q2-myRadio3" />
                                    <label class="radio__label-2" for="q2-myRadio3">3</label>
                                    <input class="radio__input-2" type="radio" value="4" name="q2" id="q2-myRadio4" />
                                    <label class="radio__label-2" for="q2-myRadio4">4</label>
                                    <input class="radio__input-2" type="radio" value="5" name="q2" id="q2-myRadio5" />
                                    <label class="radio__label-2" for="q2-myRadio5">5</label>
                                    <input class="radio__input-2" type="radio" value="6" name="q2" id="q2-myRadio6" />
                                    <label class="radio__label-2" for="q2-myRadio6">6</label>
                                    <input class="radio__input-2" type="radio" value="7" name="q2" id="q2-myRadio7" />
                                    <label class="radio__label-2" for="q2-myRadio7">7</label>
                                    <input class="radio__input-2" type="radio" value="8" name="q2" id="q2-myRadio8" />
                                    <label class="radio__label-2" for="q2-myRadio8">8</label>
                                    <input class="radio__input-2" type="radio" value="9" name="q2" id="q2-myRadio9" />
                                    <label class="radio__label-2" for="q2-myRadio9">9</label>
                                    <input class="radio__input-2" type="radio" value="10" name="q2" id="q2-myRadio10" />
                                    <label class="radio__label-2" for="q2-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>

                            <hr class="small-dash" />

                            <div class="feedback-text">
                                3. In the coaching session I felt able to present my own ideas
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q3">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-3" type="radio" value="1" name="q3" id="q3-myRadio1" required />
                                    <label class="radio__label-3" for="q3-myRadio1">1</label>
                                    <input class="radio__input-3" type="radio" value="2" name="q3" id="q3-myRadio2" />
                                    <label class="radio__label-3" for="q3-myRadio2">2</label>
                                    <input class="radio__input-3" type="radio" value="3" name="q3" id="q3-myRadio3" />
                                    <label class="radio__label-3" for="q3-myRadio3">3</label>
                                    <input class="radio__input-3" type="radio" value="4" name="q3" id="q3-myRadio4" />
                                    <label class="radio__label-3" for="q3-myRadio4">4</label>
                                    <input class="radio__input-3" type="radio" value="5" name="q3" id="q3-myRadio5" />
                                    <label class="radio__label-3" for="q3-myRadio5">5</label>
                                    <input class="radio__input-3" type="radio" value="6" name="q3" id="q3-myRadio6" />
                                    <label class="radio__label-3" for="q3-myRadio6">6</label>
                                    <input class="radio__input-3" type="radio" value="7" name="q3" id="q3-myRadio7" />
                                    <label class="radio__label-3" for="q3-myRadio7">7</label>
                                    <input class="radio__input-3" type="radio" value="8" name="q3" id="q3-myRadio8" />
                                    <label class="radio__label-3" for="q3-myRadio8">8</label>
                                    <input class="radio__input-3" type="radio" value="9" name="q3" id="q3-myRadio9" />
                                    <label class="radio__label-3" for="q3-myRadio9">9</label>
                                    <input class="radio__input-3" type="radio" value="10" name="q3" id="q3-myRadio10" />
                                    <label class="radio__label-3" for="q3-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>


                            <hr class="small-dash" />

                            <div class="feedback-text">
                                4. The coach showed that they understood my feelings
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q4">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-4" type="radio" value="1" name="q4" id="q4-myRadio1" required />
                                    <label class="radio__label-4" for="q4-myRadio1">1</label>
                                    <input class="radio__input-4" type="radio" value="2" name="q4" id="q4-myRadio2" />
                                    <label class="radio__label-4" for="q4-myRadio2">2</label>
                                    <input class="radio__input-4" type="radio" value="3" name="q4" id="q4-myRadio3" />
                                    <label class="radio__label-4" for="q4-myRadio3">3</label>
                                    <input class="radio__input-4" type="radio" value="4" name="q4" id="q4-myRadio4" />
                                    <label class="radio__label-4" for="q4-myRadio4">4</label>
                                    <input class="radio__input-4" type="radio" value="5" name="q4" id="q4-myRadio5" />
                                    <label class="radio__label-4" for="q4-myRadio5">5</label>
                                    <input class="radio__input-4" type="radio" value="6" name="q4" id="q4-myRadio6" />
                                    <label class="radio__label-4" for="q4-myRadio6">6</label>
                                    <input class="radio__input-4" type="radio" value="7" name="q4" id="q4-myRadio7" />
                                    <label class="radio__label-4" for="q4-myRadio7">7</label>
                                    <input class="radio__input-4" type="radio" value="8" name="q4" id="q4-myRadio8" />
                                    <label class="radio__label-4" for="q4-myRadio8">8</label>
                                    <input class="radio__input-4" type="radio" value="9" name="q4" id="q4-myRadio9" />
                                    <label class="radio__label-4" for="q4-myRadio9">9</label>
                                    <input class="radio__input-4" type="radio" value="10" name="q4" id="q4-myRadio10" />
                                    <label class="radio__label-4" for="q4-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>


                            <hr class="small-dash" />

                            <div class="feedback-text">
                                5. By the end of the coaching session I had greater clarity about the issues I face
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q5">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-5" type="radio" value="1" name="q5" id="q5-myRadio1" required />
                                    <label class="radio__label-5" for="q5-myRadio1">1</label>
                                    <input class="radio__input-5" type="radio" value="2" name="q5" id="q5-myRadio2" />
                                    <label class="radio__label-5" for="q5-myRadio2">2</label>
                                    <input class="radio__input-5" type="radio" value="3" name="q5" id="q5-myRadio3" />
                                    <label class="radio__label-5" for="q5-myRadio3">3</label>
                                    <input class="radio__input-5" type="radio" value="4" name="q5" id="q5-myRadio4" />
                                    <label class="radio__label-5" for="q5-myRadio4">4</label>
                                    <input class="radio__input-5" type="radio" value="5" name="q5" id="q5-myRadio5" />
                                    <label class="radio__label-5" for="q5-myRadio5">5</label>
                                    <input class="radio__input-5" type="radio" value="6" name="q5" id="q5-myRadio6" />
                                    <label class="radio__label-5" for="q5-myRadio6">6</label>
                                    <input class="radio__input-5" type="radio" value="7" name="q5" id="q5-myRadio7" />
                                    <label class="radio__label-5" for="q5-myRadio7">7</label>
                                    <input class="radio__input-5" type="radio" value="8" name="q5" id="q5-myRadio8" />
                                    <label class="radio__label-5" for="q5-myRadio8">8</label>
                                    <input class="radio__input-5" type="radio" value="9" name="q5" id="q5-myRadio9" />
                                    <label class="radio__label-5" for="q5-myRadio9">9</label>
                                    <input class="radio__input-5" type="radio" value="10" name="q5" id="q5-myRadio10" />
                                    <label class="radio__label-5" for="q5-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>



                            <hr class="small-dash" />

                            <div class="feedback-text">
                                6. The goals we set during coaching were very important to me
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q6">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-6" type="radio" value="1" name="q6" id="q6-myRadio1" required />
                                    <label class="radio__label-6" for="q6-myRadio1">1</label>
                                    <input class="radio__input-6" type="radio" value="2" name="q6" id="q6-myRadio2" />
                                    <label class="radio__label-6" for="q6-myRadio2">2</label>
                                    <input class="radio__input-6" type="radio" value="3" name="q6" id="q6-myRadio3" />
                                    <label class="radio__label-6" for="q6-myRadio3">3</label>
                                    <input class="radio__input-6" type="radio" value="4" name="q6" id="q6-myRadio4" />
                                    <label class="radio__label-6" for="q6-myRadio4">4</label>
                                    <input class="radio__input-6" type="radio" value="5" name="q6" id="q6-myRadio5" />
                                    <label class="radio__label-6" for="q6-myRadio5">5</label>
                                    <input class="radio__input-6" type="radio" value="6" name="q6" id="q6-myRadio6" />
                                    <label class="radio__label-6" for="q6-myRadio6">6</label>
                                    <input class="radio__input-6" type="radio" value="7" name="q6" id="q6-myRadio7" />
                                    <label class="radio__label-6" for="q6-myRadio7">7</label>
                                    <input class="radio__input-6" type="radio" value="8" name="q6" id="q6-myRadio8" />
                                    <label class="radio__label-6" for="q6-myRadio8">8</label>
                                    <input class="radio__input-6" type="radio" value="9" name="q6" id="q6-myRadio9" />
                                    <label class="radio__label-6" for="q6-myRadio9">9</label>
                                    <input class="radio__input-6" type="radio" value="10" name="q6" id="q6-myRadio10" />
                                    <label class="radio__label-6" for="q6-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>



                            <hr class="small-dash" />

                            <div class="feedback-text">
                                7. The coach was very good at helping me develop clear, simple and achievable action plans
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q7">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-7" type="radio" value="1" name="q7" id="q7-myRadio1" required />
                                    <label class="radio__label-7" for="q7-myRadio1">1</label>
                                    <input class="radio__input-7" type="radio" value="2" name="q7" id="q7-myRadio2" />
                                    <label class="radio__label-7" for="q7-myRadio2">2</label>
                                    <input class="radio__input-7" type="radio" value="3" name="q7" id="q7-myRadio3" />
                                    <label class="radio__label-7" for="q7-myRadio3">3</label>
                                    <input class="radio__input-7" type="radio" value="4" name="q7" id="q7-myRadio4" />
                                    <label class="radio__label-7" for="q7-myRadio4">4</label>
                                    <input class="radio__input-7" type="radio" value="5" name="q7" id="q7-myRadio5" />
                                    <label class="radio__label-7" for="q7-myRadio5">5</label>
                                    <input class="radio__input-7" type="radio" value="6" name="q7" id="q7-myRadio6" />
                                    <label class="radio__label-7" for="q7-myRadio6">6</label>
                                    <input class="radio__input-7" type="radio" value="7" name="q7" id="q7-myRadio7" />
                                    <label class="radio__label-7" for="q7-myRadio7">7</label>
                                    <input class="radio__input-7" type="radio" value="8" name="q7" id="q7-myRadio8" />
                                    <label class="radio__label-7" for="q7-myRadio8">8</label>
                                    <input class="radio__input-7" type="radio" value="9" name="q7" id="q7-myRadio9" />
                                    <label class="radio__label-7" for="q7-myRadio9">9</label>
                                    <input class="radio__input-7" type="radio" value="10" name="q7" id="q7-myRadio10" />
                                    <label class="radio__label-7" for="q7-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>



                            <hr class="small-dash" />

                            <div class="feedback-text">
                                8. When coaching, the coach spent more time analysing the problem rather than developing solutions
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q8">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-8" type="radio" value="1" name="q8" id="q8-myRadio1" required />
                                    <label class="radio__label-8" for="q8-myRadio1">1</label>
                                    <input class="radio__input-8" type="radio" value="2" name="q8" id="q8-myRadio2" />
                                    <label class="radio__label-8" for="q8-myRadio2">2</label>
                                    <input class="radio__input-8" type="radio" value="3" name="q8" id="q8-myRadio3" />
                                    <label class="radio__label-8" for="q8-myRadio3">3</label>
                                    <input class="radio__input-8" type="radio" value="4" name="q8" id="q8-myRadio4" />
                                    <label class="radio__label-8" for="q8-myRadio4">4</label>
                                    <input class="radio__input-8" type="radio" value="5" name="q8" id="q8-myRadio5" />
                                    <label class="radio__label-8" for="q8-myRadio5">5</label>
                                    <input class="radio__input-8" type="radio" value="6" name="q8" id="q8-myRadio6" />
                                    <label class="radio__label-8" for="q8-myRadio6">6</label>
                                    <input class="radio__input-8" type="radio" value="7" name="q8" id="q8-myRadio7" />
                                    <label class="radio__label-8" for="q8-myRadio7">7</label>
                                    <input class="radio__input-8" type="radio" value="8" name="q8" id="q8-myRadio8" />
                                    <label class="radio__label-8" for="q8-myRadio8">8</label>
                                    <input class="radio__input-8" type="radio" value="9" name="q8" id="q8-myRadio9" />
                                    <label class="radio__label-8" for="q8-myRadio9">9</label>
                                    <input class="radio__input-8" type="radio" value="10" name="q8" id="q8-myRadio10" />
                                    <label class="radio__label-8" for="q8-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>



                            <hr class="small-dash" />

                            <div class="feedback-text">
                                9. My coach asked me about my progress towards my goals
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q9">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-9" type="radio" value="1" name="q9" id="q9-myRadio1" required />
                                    <label class="radio__label-9" for="q9-myRadio1">1</label>
                                    <input class="radio__input-9" type="radio" value="2" name="q9" id="q9-myRadio2" />
                                    <label class="radio__label-9" for="q9-myRadio2">2</label>
                                    <input class="radio__input-9" type="radio" value="3" name="q9" id="q9-myRadio3" />
                                    <label class="radio__label-9" for="q9-myRadio3">3</label>
                                    <input class="radio__input-9" type="radio" value="4" name="q9" id="q9-myRadio4" />
                                    <label class="radio__label-9" for="q9-myRadio4">4</label>
                                    <input class="radio__input-9" type="radio" value="5" name="q9" id="q9-myRadio5" />
                                    <label class="radio__label-9" for="q9-myRadio5">5</label>
                                    <input class="radio__input-9" type="radio" value="6" name="q9" id="q9-myRadio6" />
                                    <label class="radio__label-9" for="q9-myRadio6">6</label>
                                    <input class="radio__input-9" type="radio" value="7" name="q9" id="q9-myRadio7" />
                                    <label class="radio__label-9" for="q9-myRadio7">7</label>
                                    <input class="radio__input-9" type="radio" value="8" name="q9" id="q9-myRadio8" />
                                    <label class="radio__label-9" for="q9-myRadio8">8</label>
                                    <input class="radio__input-9" type="radio" value="9" name="q9" id="q9-myRadio9" />
                                    <label class="radio__label-9" for="q9-myRadio9">9</label>
                                    <input class="radio__input-9" type="radio" value="10" name="q9" id="q9-myRadio10" />
                                    <label class="radio__label-9" for="q9-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>




                            <hr class="small-dash" />

                            <div class="feedback-text">
                                10. We discussed any performance shortfalls or failure to complete actions steps
                            </div>
                            {/* <div class="rating-container">
                                <div class="row">
                                    <div class="column">
                                        Strongly Disagree
                                    </div>
                                    <div class="column">
                                        Strongly Agree
                                    </div>
                                </div>
                            </div> */}
                            <div class="radio-body">
                                <div class="radio" name="q10">
                                    <p class="measureFeedback">Strongly Disagree</p>
                                    <input class="radio__input-10" type="radio" value="1" name="q10" id="q10-myRadio1" required />
                                    <label class="radio__label-10" for="q10-myRadio1">1</label>
                                    <input class="radio__input-10" type="radio" value="2" name="q10" id="q10-myRadio2" />
                                    <label class="radio__label-10" for="q10-myRadio2">2</label>
                                    <input class="radio__input-10" type="radio" value="3" name="q10" id="q10-myRadio3" />
                                    <label class="radio__label-10" for="q10-myRadio3">3</label>
                                    <input class="radio__input-10" type="radio" value="4" name="q10" id="q10-myRadio4" />
                                    <label class="radio__label-10" for="q10-myRadio4">4</label>
                                    <input class="radio__input-10" type="radio" value="5" name="q10" id="q10-myRadio5" />
                                    <label class="radio__label-10" for="q10-myRadio5">5</label>
                                    <input class="radio__input-10" type="radio" value="6" name="q10" id="q10-myRadio6" />
                                    <label class="radio__label-10" for="q10-myRadio6">6</label>
                                    <input class="radio__input-10" type="radio" value="7" name="q10" id="q10-myRadio7" />
                                    <label class="radio__label-10" for="q10-myRadio7">7</label>
                                    <input class="radio__input-10" type="radio" value="8" name="q10" id="q10-myRadio8" />
                                    <label class="radio__label-10" for="q10-myRadio8">8</label>
                                    <input class="radio__input-10" type="radio" value="9" name="q10" id="q10-myRadio9" />
                                    <label class="radio__label-10" for="q10-myRadio9">9</label>
                                    <input class="radio__input-10" type="radio" value="10" name="q10" id="q10-myRadio10" />
                                    <label class="radio__label-10" for="q10-myRadio10">10</label>
                                    <p class="measureFeedback">Strongly Agree</p>
                                </div>
                            </div>
                            <div class="Feedback-Text-Container">
                            <br />
                            {/* <div class="radio-body"> */}
                                <TextField
                                    //fullWidth
                                    //class='radio'
                                    inputContainerStyle={{ alignItems: 'center' }}
                                    titleTextStyle={{ textAlign: 'center' }}
                                    name='comment'
                                    id="outlined-multiline-static"
                                    label="Comment"
                                    style = {{width: 500}}
                                    multiline
                                    rows={4}
                                    
                                    // onChange={(e) => setComment(e.target.value)}
                                />
                            {/* </div> */}
                            
                            </div>

                            <div class="Feedback-Btn-Container">
                                <button class="Feedback-Submit-Btn">Submit Feedback</button>
                            </div>

                        </form>

                    </div>

                </div>
            </div>

        </>
    );
}

export default FeedbackPage

