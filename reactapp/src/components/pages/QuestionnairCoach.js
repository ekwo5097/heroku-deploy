import React from 'react';
import './Questionnaire.css'
import { useHistory } from "react-router-dom";

import * as postCoach from '../../store/actions/postCoach';

function QuestionnaireCoach() {
    let history = useHistory();
  const { current: formData } = React.useRef({});
  

  const handleSkip = React.useCallback(event => {
    event.preventDefault();
    history.push('/userhome');
  }, []);

  const handleOnChange = React.useCallback(event => {
    formData[event.target.name] = event.target.value;
  }, []);

  const handleOnSubmit = React.useCallback(event => {
    console.log('formData: ', formData);
    postCoach.questionnairecoach(formData);
    event.preventDefault();
    history.push('/userhome'); 
  }, []);

    return (

        <div class="QuestionnaireRoot">
            <form className="QuestionnaireContainer" onChange={handleOnChange} onSubmit={handleOnSubmit}>

                <div className="rectangle"></div>

                <div className="QuestionnaireTitle">
                    <h1>Tell us more about yourself</h1>
                    <p>The following questionnaire will help us to match you with leaders!</p>
                </div>

                <div className="QuestionnaireItems">
                    <p>What's your name?</p>
                    <input type="text" name="name" />
                </div>

                <div class="QuestionnaireItems">
                    <p>What's your age?</p>
                    <input type="text" name="age" />
                </div>
                <div class="QuestionnaireItems">
                    <p>What's your gender?</p>
                    <select name="gender">
                        <option value="not_willing_to_say">Not willing to say</option>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="else">Else</option>
                    </select>
                </div>

                <div class="QuestionnaireItems">
                    <p>What's your introduction to a leader?</p>
                    <input type="text" name="about" />
                </div>

                <div class="QuestionnaireItems">
                    <p>What is your approach to coaching?</p>
                    <input type="text" name="approach" />
                </div>

                <div class="QuestionnaireItems">
                    <p>What is your coaching philosophy?</p>
                    <input type="text" name="philosophy" />
                </div>

                <div class="QuestionnaireItems">
                    <p>Your industry?</p>
                    <input type="text" name="industry" />
                </div>

                <div class="QuestionnaireItems">
                    <p>Years in practice?</p>
                    {/* <div className="years-in-practice"> */}
                        <input className="years-in-practice" type="number" name="years_of_practice" />
                    {/* </div> */}
                </div>

                <div class="QuestionnaireItems">
                    <p>Most recent coaching qualification?</p>
                    <input type="text" name="highest_edu_qualification"  />
                </div>

                

                <div class="QuestionnaireItems">
                    <p>Coach goal 1</p>
                    <select name="coach_goal_1">
                    <option value="Not sure with the coaching goal">Not sure with the coaching goal</option>
                        <option value="Dealing with procrastination">Dealing with procrastination</option>
                        <option value="Building effective work habits">Building effective work habits</option>
                        <option value="Overcoming presentation anxiety">Overcoming presentation anxiety</option>
                        <option value="Building confidence and self-efficacy">Building confidence and self-efficacy </option>
                        <option value="Increasing ambiguity tolerance and resilience">Increasing ambiguity tolerance and resilience</option>
                        <option value="Delegating workload and managing teams">Delegating workload and managing teams</option>
                        <option value="Managing your inner critic and reducing Imposter Syndrome">Managing your inner critic and reducing Imposter Syndrome</option>
                    </select>
                </div>
                <div class="QuestionnaireItems">
                    <p>Coach goal 2</p>
                    <select name="coach_goal_2">
                    <option value="Not sure with the coaching goal">Not sure with the coaching goal</option>
                        <option value="Dealing with procrastination">Dealing with procrastination</option>
                        <option value="Building effective work habits">Building effective work habits</option>
                        <option value="Overcoming presentation anxiety">Overcoming presentation anxiety</option>
                        <option value="Building confidence and self-efficacy">Building confidence and self-efficacy </option>
                        <option value="Increasing ambiguity tolerance and resilience">Increasing ambiguity tolerance and resilience</option>
                        <option value="Delegating workload and managing teams">Delegating workload and managing teams</option>
                        <option value="Managing your inner critic and reducing Imposter Syndrome">Managing your inner critic and reducing Imposter Syndrome</option>
                    </select>
                </div>
                <div class="QuestionnaireItems">
                    <p>Coach goal 3</p>
                    <select name="coach_goal_3">
                    <option value="Not sure with the coaching goal">Not sure with the coaching goal</option>
                        <option value="Dealing with procrastination">Dealing with procrastination</option>
                        <option value="Building effective work habits">Building effective work habits</option>
                        <option value="Overcoming presentation anxiety">Overcoming presentation anxiety</option>
                        <option value="Building confidence and self-efficacy">Building confidence and self-efficacy </option>
                        <option value="Increasing ambiguity tolerance and resilience">Increasing ambiguity tolerance and resilience</option>
                        <option value="Delegating workload and managing teams">Delegating workload and managing teams</option>
                        <option value="Managing your inner critic and reducing Imposter Syndrome">Managing your inner critic and reducing Imposter Syndrome</option>
                    </select>
                </div>

                <div class="AvailableTimeContainer">
                    <p>What days are you available for coaching?</p>

                    <div class="AvailableTime">
                        <label><input name="Monday" type="checkbox" /> <span> Monday </span> </label>
                        <label><input name="Tuesday" type="checkbox" /> <span>Tuesday</span></label>
                        <label><input name="Wednesday" type="checkbox" /><span>Wednesday</span></label>
                        <label><input name="Thursday" type="checkbox" /><span>Thursday</span></label>
                        <label><input name="Friday" type="checkbox" /><span>Friday</span></label>
                        <label><input name="Saturday" type="checkbox" /><span>Saturday</span></label>
                        <label><input name="Sunday" type="checkbox" /><span>Sunday</span></label>
                        <label><input name="Else" type="checkbox" /><span>Different for every week</span></label>
                    </div>
                </div>

                <input type="submit" value="Submit" />
                <div className="skip-for-now-btn" onClick={handleSkip}>Skip For Now</div>
                    {/* <button onClick={handleSkip}>Skip For Now</button> */}
            </form>
        </div>

    );
}

export default QuestionnaireCoach;
