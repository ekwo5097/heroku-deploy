import {useEffect, useState} from "react";
import Pusher from "pusher-js";
import './Messages.css';
import axios from 'axios';
 
function Messages() {
    const [username, setUsername] = useState(localStorage.getItem("username"));
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const[messageHistory,setMessageHistory]=useState([]);
    let allMessages = [];

    
    //const [lead, setLead] = useState('');

    useEffect(() => {
        Pusher.logToConsole = true;

        const pusher = new Pusher('e7e6508dcf5807d7881f', {
            cluster: 'ap2'
        });
        var channel;
       
        if(localStorage.getItem('usertype')=="C"){
         channel = pusher.subscribe(localStorage.getItem('messageLeader')); 
        }
        else{
            channel = pusher.subscribe(localStorage.getItem('username')); 
        }
        channel.bind('message', function (data) {
            allMessages.push(data);
            setMessages(allMessages);
        })
        var lead;
        if(localStorage.getItem('usertype')=="C"){
            lead=localStorage.getItem('messageLeader');
        }
        else if (localStorage.getItem('usertype')=="L"){
            lead=localStorage.getItem('username');
        }
        axios.post('https://new-emerge-website.herokuapp.com/api/getMessageHistory/',{
            username: localStorage.getItem("username"),
            leaderUsername: lead,
            },console.log("username " + localStorage.getItem("username") + " leader: " + lead)
            )
            .then(response => {
                setMessageHistory(response.data.messages)
                console.log(response.data.messages)      
                var chatHistory = document.getElementById("scroll-element");
                if(chatHistory)
                {
                    chatHistory.scrollTop = chatHistory.scrollHeight;
                }
            })
            .catch(err => {
            })

            }, []);

    const submit = event => {
        event.preventDefault();
        if(localStorage.getItem('usertype')=="C"){
            axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
            username: localStorage.getItem("username"),
             leaderUsername: localStorage.getItem('messageLeader'),
            message: message,

            },
            console.log("message " + message + " username: " + localStorage.getItem("username") )
            )
            .then(response => {
                setMessage('');
                console.log(response.data)         
            })
            .catch(err => {
            })

        }
        else {

            axios.post('https://new-emerge-website.herokuapp.com/api/addMessage/',{
                username: localStorage.getItem("username"),
                leaderUsername: localStorage.getItem("username"),
                message: message,

                },
                console.log("message " + message + " username: " + localStorage.getItem("username") )
                )
                .then(response => {
                    setMessage('');
                    console.log(response.data)         
                })
                .catch(err => {
                })

        //setMessage('');
        }
    }

    return (
        /**<div className="container">
            <div className="d-flex flex-column align-items-stretch flex-shrink-0 bg-white">
                <div
                    className="d-flex align-items-center flex-shrink-0 p-3 link-dark text-decoration-none border-bottom">
                    <input className="fs-5 fw-semibold" value={username}/>
                </div>
                <div className="list-group list-group-flush border-bottom scrollarea">
                    {messages.map(message => {
                        return (
                            <div className="list-group-item list-group-item-action py-3 lh-tight">
                                <div className="d-flex w-100 align-items-center justify-content-between">
                                    <strong className="mb-1">{message.username}</strong>
                                </div>
                                <div className="col-10 mb-1 small">{message.message}</div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <form onSubmit={e => submit(e)}>
                <input className="form-control" placeholder="Write a message" value={message}
                       onChange={e => setMessage(e.target.value)}
                />
            </form>
        </div>**/
        <>
            <div class="wrapper">
                <div class="main">
                
                    <div class="px-2 scroll">
                
                        {messageHistory.map(prevMessage => {
                            return (
                                <>
                                    { localStorage.getItem('username')==prevMessage.name ?
                                        <>
                                            <div class="d-flex align-items-center text-right justify-content-end ">
                                                <div class="pr-2"> <span class="name">{prevMessage.name}</span>
                                                    <p class="msg">{prevMessage.message}</p>
                                                </div>
                                            </div>

                                        </>
                                        :
                                        <>
                                            <div class="d-flex align-items-center">
                                                <div class="text-left pr-1">
                                                    <div class="pr-2 pl-1"> <span class="name">{prevMessage.name}</span>
                                                        <p class="msg">{prevMessage.message}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </>
                                    }   
                            </>
                            )
                        })}
                        {messages.map(message => {
                            return (
                                <>
                                    { localStorage.getItem('username')==message.username ?
                                    <>
                                        <div class="d-flex align-items-center text-right justify-content-end ">
                                                <div class="pr-2"> <span class="name">{message.username}</span>
                                                    <p class="msg">{message.message}</p>
                                                </div>
                                            </div>
                                    </>
                                    :
                                    <>
                                        <div class="d-flex align-items-center">
                                            <div class="text-left pr-1">
                                                <div class="pr-2 pl-1"> <span class="name">{message.username}</span>
                                                    <p class="msg">{message.message}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </>
                                    } 
                                </>
                                )
                            })}
                    </div>
                    <nav class="message-input bg-white navbar-expand-sm d-flex justify-content-between">
                        <div className="sample-here">
                            <form onSubmit={e => submit(e)}> 
                                <input type="text number" name="text" class="form-control" placeholder="Type a message..."
                                value={message}  onChange={e => setMessage(e.target.value)} />
                                <div class="icondiv d-flex justify-content-end align-content-center text-center ml-2">  <i class="fa fa-arrow-circle-right icon2"></i> </div>
                            </form>
                        </div>
                    </nav>
                </div>
            </div>
        </>
    );
}


export default Messages;