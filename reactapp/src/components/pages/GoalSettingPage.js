import 'material-icons/iconfont/material-icons.css';
import './GoalSettingPage.css';
import * as actions from '../../store/actions/auth';
import { Link, useLocation } from 'react-router-dom';
import React, { useState, useEffect } from "react"
import Footer from '../LandingPage/Footer';
import axios from 'axios';
import NavbarLeader from '../SideNavbars/NavbarLeader';
import QuestNotComplete from './QuestNotComplete';
import TopHeaderComponent from './TopHeaderComponent';



function GoalSettingPage(){

    const [questionnaireState, setQuestionnaireState] = useState(false);
    const [matchingState, setCompletedMatchingState] = useState(false); /* leader matched with coach */

    const [goal, setGoal] = useState([]);
    const [subgoal, setSubGoal] = useState([]);
    const [rsubgoal, removeSubGoal] = useState([]);
    const [action, setAction] = useState([]);
    const [raction, removeAction] = useState([]);
    const [score, setScore] = useState([]);

    
    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getQuestionnaireCompleted/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['completed'] === "True"){
                    setQuestionnaireState(true);
                }
            })
            .catch(err => {
            })
    }, [])

    useEffect(() => {
        // get leader is matched status
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderIsAssigned/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                if(response.data['msg'] == "isMatched found"){ 
                    // setQStatus(true);
                    if (response.data['isMatched'] == "True"){
                        setCompletedMatchingState(true);
                    }
                }else{
                    // Some error occurred when retrieving leader's matched_with_coach
                    console.log(response.data['msg'])
                }
                
            })
            .catch(err => {
            })
    }, [])

    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderGoals/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setGoal(response.data)
                console.log("leader goals:")
                console.log(JSON.stringify(response.data))
            })
            .catch(err => {
            })
        }
        , [])

        //Handle when leader removes a subgoal 
        const handleRemoveSubgoal = React.useCallback(event => {
            event.preventDefault();
            const leaderName = event.target.elements.leaderName;
            const subGoal = event.target.elements.subGoal;
            axios.post('https://new-emerge-website.herokuapp.com/api/leaderRemoveSubGoal/',{
                username: localStorage.getItem("username"),
                token: localStorage.getItem("token"),
                leaderUsername:leaderName.value,
                subGoal:subGoal.value
                
                }, console.log(subGoal.value),
                
                )
                .then(response => {
                    removeSubGoal(response.data)
                    console.log(response.data)
                })
                .catch(err => {
                })
                event.preventDefault();
        }, []); 

    //Handle when leader adds an action
    const handleAddAction = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.subGoal;
        const action = event.target.elements.action;
        axios.post('https://new-emerge-website.herokuapp.com/api/leaderAddAction/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value,
            action:action.value
            
            }, console.log(action.value),
            
            )
            .then(response => {
                setAction(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []); 

    //Handle when leader removes an action 
    const handleRemoveAction = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.subGoal;
        const action = event.target.elements.action;
        axios.post('https://new-emerge-website.herokuapp.com/api/leaderRemoveAction/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value,
            action:action.value
            
            }, console.log(action.value),
            
            )
            .then(response => {
                removeAction(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []); 

    const handleScoreSubmit = React.useCallback(event => {
        event.preventDefault(); 
        const action = event.target.elements.action;
        const score = event.target.elements.newScore; 
        const leaderInput = event.target.elements.leaderName; 
        axios.post('https://new-emerge-website.herokuapp.com/api/updateActionScore/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            action: action.value,
            score: score.value,
            // leader: leaderInput.value ---> have to save leader data along with form
            },
            //console.log(zoomInput.value),
            )
            .then(response => {
                setScore(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []);

    //Handle when leader adds a subgoal 
    const handleAddSubgoal = React.useCallback(event => {
        event.preventDefault();
        const leaderName = event.target.elements.leaderName;
        const subGoal = event.target.elements.newSubGoal;
        axios.post('https://new-emerge-website.herokuapp.com/api/leaderAddSubGoal/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            leaderUsername:leaderName.value,
            subGoal:subGoal.value
            
            }, console.log(subGoal.value),
            
            )
            .then(response => {
                setSubGoal(response.data)
                console.log(response.data)
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []); 





    if (questionnaireState == false){
        return (
            <>
            
            <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

            </div>

            <NavbarLeader/>
            {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
            <div class="no-access-outer">
                <div class="no-access-inner">
                    You don't have access to this page!
                </div>
            </div>

            </>
        )
    }

    if (matchingState == false){
        return (
        <>
        
        <div className="top-header">
                <div className="logo-lettering">
                    <h1>EMERGE</h1>
                </div>

            </div>

            <NavbarLeader/>
            {/* <QuestNotComplete userType={(localStorage.getItem('usertype'))}/> */}
            <div class="no-access-outer">
                <div class="no-access-inner">
                    You don't have access to this page!
                </div>
            </div>
        </>
        );
    }

    return(
        <>

        <div>
            
            <TopHeaderComponent/>

            <NavbarLeader/>
            
            

            {/* Goal Setting Section */}

            <div class="main-cont">
                <div class="personal-cont-wrapper">
                    <div class="row-content">
                        <div class="col-full-content">
                            <div class="col-content">
                                <div class="col-sub">
                                    <div class="row-col-content">
                                        <h1>Goal Settings</h1>

                                        {goal.map(goalObject => (
                                            <>
                                            <h4 className="title-header-style"> Main Coaching Goal</h4>
                                            <div className="coacheeInfoGoals">
                                                <b>{goalObject.mainGoal} </b>
                                            </div>

                                            <br/>

                                                {/* Subgoals */}
                                                {goalObject.subGoals.map(subGoal =>(
                                                    <>
                                                        <h4 className="title-header-style"> Subgoal </h4>
                                                            <form class="form-container" id="subGoalForm" onSubmit={handleRemoveSubgoal}>

                                                                <div className="leaderNameSubgoalContainer">      
                                                                    <input type="text" name="leaderName" value={localStorage.getItem('username')}/>
                                                                </div>
                                                                    <div class="newScoreContentContainer">
                                                                        <input type="text" name="subGoal" placeholder={subGoal.subGoal} value={subGoal.subGoal}/>
                                                                        <button type="submit" class="removeSubGoalBtn">Remove Subgoal</button> 
                                                                    </div>
                                                            </form>

                                                            {/* Actions for the above subgoal */}
                                                            <h4 className="title-header-style"> Actions </h4>
                                                            <div class="content-form newSubGoalContainer" id="subGoalForm">  {/* */}
                                                            {subGoal.actions.map(action => ( 
                                                            <> 
                                                            
                                                                <form class="form-container" id="subGoalForm" onSubmit={handleRemoveAction}>
                                                                    <div className="leaderUpdateActionContainer">    
                                                                        <input type="text" name="leaderName" value={localStorage.getItem('username')}/>
                                                                        <input type="text" name="subGoal" placeholder={subGoal.subGoal} value={subGoal.subGoal}/>
                                                                    </div>

                                                                    {/*<div className="subGoalContentContainer">*/}
                                                                        <div className="newScoreContentContainer">
                                                                            <input type="text" name="action" placeholder={action.action} value={action.action}/>
                                                                            <button type="submit" className="removeSubGoalBtn">Remove Action</button> 
                                                                        </div>
                                                                    {/*</div>*/}
                                                                                                    
                                                                </form>

                                                                {/* Handles updating score */}
                                                                    <form class="form-container" id="subGoalForm" onSubmit={handleScoreSubmit}>
                                                                        <div className="leaderUpdateActionContainer">
                                                                            <input type="text" name="leaderName" value={localStorage.getItem('username')}/>
                                                                            <input type="text" name="action" placeholder={action.action} value={action.action}/>
                                                                        </div>
                                                                        Current score: {action.score}
                                                                        <div class="newScoreContentContainer">
                                                                            <input type="text" name="newScore" placeholder="Enter a new score..." required/>
                                                                            <button type="submit" className="removeSubGoalBtn">Submit Score</button> 
                                                                        </div>
                                                                    </form>

                                                                    <hr class="small-dash"/>
                                                                
                                                            </> )) }

                                                            </div>

                                                            <br/>

                                                                {/* Add action to subgoal */}
                                                                <h4 className="title-header-style"> Add Action to Subgoal </h4>
                                                                <form class="form-container" id="subGoalForm" onSubmit={handleAddAction}>
                                                                    <div className="leaderNameSubgoalContainer">
                                                                        <input type="text" name="leaderName" value={localStorage.getItem('username')}/>
                                                                        <input type="text" name="subGoal" placeholder={subGoal.subGoal} value={subGoal.subGoal}/>
                                                                    </div>
                                                                    <div class="newScoreContentContainer">
                                                                        <input type="text" name="action" placeholder="Add an action..." required/>
                                                                        <button type="submit" class="removeSubGoalBtn">Add Action</button> 
                                                                    </div>
                                                                </form>
                                                                

                                                            {/* Subgoal divider */}
                                                            <hr class="dash"/>
                                                    </>
                                                ))}

                                                {/* Add subgoal */}
                                                    <h4 className="title-header-style">Add Subgoal </h4>
                                                    <form class="form-container" id="subGoalForm" onSubmit={handleAddSubgoal}>
                                                        <div className="leaderNameSubgoalContainer">
                                                            <input type="text" name="leaderName" value={localStorage.getItem('username')}/>
                                                        </div>
                                                        <div class="newScoreContentContainer">
                                                            <input type="text" name="newSubGoal" placeholder="Add a subgoal..." required/>
                                                            <button type="submit" class="removeSubGoalBtn">Add Subgoal</button> 
                                                        </div>
                                                    </form>

                                            </>
                                        ))}

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>







            

        </div>

        </>

    )

}

export default GoalSettingPage; 