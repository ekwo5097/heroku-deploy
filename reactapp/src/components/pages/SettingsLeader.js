import React, {useState, useEffect} from 'react';
import './SettingsLeader.css';
import { Link } from 'react-router-dom';
import * as actions from '../../store/actions/auth';
import 'material-icons/iconfont/material-icons.css';
import * as post from '../../store/actions/post';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import NavbarLeader from '../SideNavbars/NavbarLeader';
import TopHeaderComponent from './TopHeaderComponent';

function SettingsLeader(){

    const [message, setMessage] = useState("");
    let history = useHistory();

    const Message = ({ message }) => (
      <section>
        <p>{message}</p>
      </section>
    );

    const handleOnClick = React.useCallback(event => {
      event.preventDefault();
      var planValue= document.querySelector('input[name="plan"]:checked').value;
      axios.post('https://new-emerge-website.herokuapp.com/api/pay/',{
          planValue: planValue,
          },
          console.log("post the pay"),
          )
          .then(response => {

          })
          .catch(err => {
          })
          event.preventDefault();
          //history.push('/payment');
    }, []);

    const[state, setState] = useState('');

    const { current: formDatas } = React.useRef({});
    // const[settings, setSettings] = useState({username: post.username, password: post.password, email: post.email, phone_number: post.phone_number})

    const handleOnChange = React.useCallback(event => {
        formDatas[event.target.name] = event.target.value;
    }, []);

    const handleOnSubmit = React.useCallback(event => {
        // post.getSetting();
        axios.post('https://new-emerge-website.herokuapp.com/api/changeSettings/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            email: formDatas["email"],
            phone_number: formDatas["phone_number"],
            password: formDatas["password"],
            industry: formDatas["industry"],
            Monday: formDatas["Monday"],
            Tuesday: formDatas["Tuesday"],
            Wednesday: formDatas["Wednesday"],
            Thursday: formDatas["Thursday"],
            Friday: formDatas["Friday"],
            Saturday: formDatas["Saturday"],
            Sunday: formDatas["Sunday"],
            Else: formDatas["Else"],
            subscription_type: formDatas["plan"],
            },
            console.log(formDatas["email"]),
            console.log(formDatas["phone_number"]),
            console.log(formDatas["password"]),
            console.log(formDatas["plan"])
            )
            .then(response => {
                setPost(response.data)
                console.log(response.data)
                // setSettings({username: post.username, password: post.password, email: post.email, phone_number: post.phone_number})
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []);

    const [post, setPost] = React.useState(null);
    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/settings/',{
            username: localStorage.getItem("username"),
            token: localStorage.getItem("token"),
            })
            .then(response => {
                setPost(response.data)
                console.log(response.data)

                // setSettings({username: post.username, password: post.password, email: post.email, phone_number: post.phone_number})
            })
            .catch(err => {
            })
          const query = new URLSearchParams(window.location.search);
          if (query.get("success")) {
            setMessage("Order placed! You will receive an email confirmation.");
          }
          if (query.get("canceled")) {
            setMessage(
              "Order canceled -- continue to shop around and checkout when you're ready."
            );
          }
          }, [])

    function profile(){
        setState(

                <form className="profile-content" onChange={handleOnChange} onSubmit={handleOnSubmit}>
                    <div class="headingSettings profile-heading">
                        <h1 className="profiletxt">Profile</h1>
                    </div>
                    <div class="profile-picture">
                        <div class="picture-container"> </div>
                        <div class="picture-buttons">
                            <button class="change-pic">Change Picture</button>
                            <button class="delete-pic">Delete Picture </button>
                        </div>
                    </div>

                    <div class="profile-edit">
                        <p class="profile-text">Username</p>
                        <input type="text" name="username" defaultValue ={post.username} />

                        <p class="profile-text">Email</p>
                        <input type="email" name="email" defaultValue={post.email} />

                        <p class="profile-text">Phone number </p>

                        <input type="number" name="phone_number" defaultValue={post.phone_number} />

                        <p class="profile-text">Password</p>
                        <input type="password" name="password" />

                        <input type="submit" value="Save" id="submit-button" />
                    </div>


                </form>

        )
    }

    function availableDays(){
        setState(

            <form className="profile-content" onChange={handleOnChange} onSubmit={handleOnSubmit}>
                <div class="headingSettings availableDays-heading">
                    <h1 className="profiletxt">Available Days</h1>
                </div>

                <div class="AvailableTimeSettings">
                    <label><input name="Monday" type="checkbox" /> <span> Monday </span> </label>
                    <label><input name="Tuesday" type="checkbox" /> <span>Tuesday</span></label>
                    <label><input name="Wednesday" type="checkbox" /><span>Wednesday</span></label>
                    <label><input name="Thursday" type="checkbox" /><span>Thursday</span></label>
                    <label><input name="Friday" type="checkbox" /><span>Friday</span></label>
                    <label><input name="Saturday" type="checkbox" /><span>Saturday</span></label>
                    <label><input name="Sunday" type="checkbox" /><span>Sunday</span></label>
                    <label><input name="Else" type="checkbox" /><span>Different for every week</span></label>
                </div>

                <input type="submit" value="Save" id="submit-button" />

            </form>

        )
    }

    function industry(){
        setState(
            <form className="profile-content" onChange={handleOnChange} onSubmit={handleOnSubmit}>
                <div class="headingSettings availableDays-heading">
                    <h1 className="profiletxt">Industry</h1>
                </div>

                <div class="profile-edit">
                    <p class="profile-text">Are you expanding or changing your business?</p>

                    <div class="AvailableTimeSettings industrybox">
                        <label> <input type="radio" name="radio" /> <span>Expanding business</span> </label>
                        <label><input type="radio" name="radio"/> <span>Changing business</span></label>
                    </div>

                    <p class="profile-text">What industry will your business be in?</p>
                    <input type="text" name="industry" defaultValue={post.industry}/>

                </div>

                <input type="submit" value="Save" id="submit-button" />

            </form>
        )
    }

    function subscriptionService(){
        setState(
            <form class="form" action="/api/pay" method="POST">

                <div class="headingSettings profile-heading">
                    <h1 className="profiletxt">Payment</h1>
                </div>

                <h3>Choose a subscription plan that is the most ideal for you. You can upgrade or change your plan anytime!</h3>
                <div class="headingSettings profile-heading usernameContainer">
                    <p>Username:</p><input id="username" name="username" value={localStorage.getItem("username")} readonly/>
                </div>
                <div className="subscriptionPlan">
                    <label class="subscription">
                        <input type="radio" name="plan" value="0" />
                        <div class="subscription-details">
                            <div class="subscription-content">
                                <span>Free {post.subscription_type==0 ? <span> (in use)</span> : <></>}</span>
                                <h1>$0</h1>
                                <p>Experience a free coaching trial!</p>
                            </div>
                        </div>
                    </label>

                    <label class="subscription">
                        <input id="planValue" type="radio" name="plan" value="1" />
                        <div class="subscription-details">
                            <div class="subscription-content">
                                <span>Basic {post.subscription_type==1 ? <span> (in use)</span> : <></>}</span>
                                <h1>$250</h1>
                                <p>Basic access to personalised coaching facilities.</p>
                            </div>
                        </div>
                    </label>

                    <label class="subscription">
                        <input id="planValue" type="radio" name="plan" value="2" />
                        <div class="subscription-details">
                            <div class="subscription-content">
                                <span>Premium {post.subscription_type==2 ? <span> (in use)</span> : <></>}</span>
                                <h1>$300</h1>
                                <p>Access to personalised coaching facilities and premium coaching services.</p>
                            </div>
                        </div>
                    </label>

                </div>


                <input type="submit" value="Pay" id="submit-button"/>
                {message ? (
                  <Message message={message} />
                ) : (
                  <></>
                )
                }
            </form>
        )
    }


    {/*function payment(){
        setState(<h1>Payment</h1>)
    }*/}

    {/*function additionalResources(){
        setState(<h1>Additional Services</h1>)
    }*/}
    return (

        <>
           <TopHeaderComponent/>
           <NavbarLeader/>

            {/* MAIN CONTENT */}

            <div className="main-content-settings">

                {/* SETTINGS HEADER */}
                <div className="settings-header">
                    <h1>Settings</h1>
                </div>
                {/* SETTINGS MAIN */}
                <div className="settings-container">
                    {/* SETTINGS OPTIONS */}
                    <div className="settings-options-container">
                        <div className="setting-option" onClick={profile}>
                            <h2>Profile</h2>
                            <i class="fas fa-chevron-right"></i>
                        </div>

                        <div className="setting-option" onClick={availableDays}>
                            <h2>Available Days</h2>
                            <i class="fas fa-chevron-right"></i>
                        </div>

                        <div className="setting-option" onClick={industry}>
                            <h2>Industry</h2>
                            <i class="fas fa-chevron-right"></i>
                        </div>
                        <a className="que" href="/questionnaire">
                        <div className="setting-option" >
                       
                            <h2>Update my Info</h2>
                            <i class="fas fa-chevron-right"></i>
                           
                        </div>
                        </a>

                        <div className="setting-option" onClick={subscriptionService}>
                            <h2>Payment</h2>
                            <i class="fas fa-chevron-right"></i>
                        </div>

                        {/*<div className="setting-option" onClick={payment}>
                            <h2>Payment</h2>
                            <i class="fas fa-chevron-right"></i>
                        </div>*/}

                        {/*<div className="setting-option" onClick={additionalResources}>
                            <h2>Additional Resources</h2>
                            <i class="fas fa-chevron-right"></i>
                        </div>*/}
                    </div>

                    {/* SETTINGS CONTENT */}

                    <div className="settings-content">
                        <>{state}</>
                        <>{profile}</>
                    </div>

                </div>
            </div>



        </>

    )
}

export default SettingsLeader;
