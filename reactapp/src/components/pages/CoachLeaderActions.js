import axios from 'axios';
import React, { useState, useEffect } from "react";
import './CoachLeaderActions.css';
import '../Actions/CircularProgressBar.css';
import CircularProgressBar from '../Actions/CircularProgressBar';

function CoachLeaderActions({coachName}) {

    var leaderActions = [];

    const [percentage_1, setPercentage1] = useState(0);
    const [percentage_2, setPercentage2] = useState(0);
    const [percentage_3, setPercentage3] = useState(0);

    useEffect(() => {
        
        // post for getting all coachees actionsn for coach view
        axios.post('https://new-emerge-website.herokuapp.com/api/getAllLeaderActions/',{
        username: coachName,
        token: localStorage.getItem("token"),
        })
        .then(response => {
            leaderActions = response.data['data']
        })
        .catch(err => {
        });

    }, []);

    function handleGetCoachLeaderActions() {
        
        // post for getting all coachees actionsn for coach view
        axios.post('https://new-emerge-website.herokuapp.com/api/getAllLeaderActions/',{
        username: localStorage.getItem("username"),
        token: localStorage.getItem("token"),
        })
        .then(response => {
            leaderActions = response.data['data']
        })
        .catch(err => {
        });

    }

    function handleSetLeaderActions(){

        // Retrieve coach leader actions
        document.getElementById("retrieve-coach-leader-actions-id").click();

        // Wait a second for data to be retrieved from database
        setTimeout(function() { 

            const leaderName = document.getElementById("leader-actions-main").value;
            setLeaderActionToNone();
            const selectedActions = getLeaderActions(leaderName);
            const completedActions = getLeaderCompletedActions(leaderName);
    
            // check if selectedActions is an array
            if(Array.isArray(selectedActions) === false){
                setLeaderNoActions(leaderName);
                return;
            } 
    
            // Leader has no actions
            if (selectedActions.length === 0){
                setLeaderNoActions(leaderName);
    
            }
    
            if(selectedActions.length !== 0){
                setLeaderActions(selectedActions);
            }

            // check if selectedActions is an array
            if(Array.isArray(completedActions) === false){
                setLeaderNoCompletedActions(leaderName);
                return;
            } 

            if(completedActions.length === 0){
                setLeaderNoCompletedActions(leaderName);
            }

            if(completedActions.length !== 0){
                setLeaderCompletedActions(completedActions);
            }

         }, 1000);


    }

    function getLeaderActions(leaderName){
        console.log(leaderActions)
        for (var i = 0; i < leaderActions.length; i++){
            if (leaderActions[i]['leader'] === leaderName){
                return leaderActions[i]['actions'];
            }
        }

        // If no leader is found
        return [];
    }

    // Set all action container to none
    function setLeaderActionToNone(){

        const allActionContainer = document.getElementsByClassName("leader-actions-single-container");
        for (var i = 0; i < allActionContainer.length; i++){
            allActionContainer[i].style.display = 'none';
        }

        document.getElementsByClassName("leader-actions-main-container")[0].style.height = "350px";
        document.getElementsByClassName("leader-actions-main-container")[0].style.width = "100%";

        document.getElementById("leader-no-action-desc").style.display = "none";
    }

    function setLeaderNoActions(name){  

        document.getElementById("leader-no-action-desc").innerHTML = name + " has no incomplete actions";
        document.getElementById("leader-no-action-desc").style.display = "block";
        document.getElementsByClassName("leader-actions-main-container")[0].style.height = "50px";
        document.getElementsByClassName("leader-actions-main-container")[0].style.width = "100%";
    }

    function setLeaderActions(actions){

        for (var i = 0; i < actions.length; i++){

            document.getElementsByClassName("leader-actions-single-container")[i].style.display = "inline-block";

            // Set action name
            document.getElementById("action-name" + (i + 1).toString()).innerHTML = actions[i]['actionName'];

            // Set action iter
            document.getElementById("action-iter" + (i + 1).toString()).innerHTML = parseInt(actions[i]['iteration']) + " / " + parseInt(actions[i]['max']);

            // Set action percentage
            const progressPercentage = ( parseInt(actions[i]['iteration']) / parseInt(actions[i]['max']) ) * 100 ;
            if (i === 0){
                setPercentage1(parseInt(progressPercentage));
            } else if (i === 1){
                setPercentage2(parseInt(progressPercentage));
            } else if (i === 2){
                setPercentage3(parseInt(progressPercentage));
            }

        }

        return;
    }

    // Completed actions functions

    function setLeaderNoCompletedActions(name){  

        //  remove children from parent
        const parentElement = document.getElementById("leader-completed-actions")

        // Remove every child element of parent 
        while (parentElement.firstChild) {
            parentElement.firstChild.remove()
          }

        document.getElementById("leader-no-completed-action-desc").innerHTML = name + " has no completed actions";
        document.getElementById("leader-no-completed-action-desc").style.display = "block";
        return;
    }

    function getLeaderCompletedActions(leaderName){

        console.log(leaderActions)
        for (var i = 0; i < leaderActions.length; i++){
            if (leaderActions[i]['leader'] === leaderName){
                return leaderActions[i]['completedActions'];
            }
        }

        // If no leader is found
        return [];
    }

    function setLeaderCompletedActions(completedActions){

        document.getElementById("leader-no-completed-action-desc").innerHTML = "";
        document.getElementById("leader-no-completed-action-desc").style.display = "none";

        //  remove children from parent
        const parentElement = document.getElementById("leader-completed-actions")

        // Remove every child element of parent 
        while (parentElement.firstChild) {
            parentElement.firstChild.remove()
          }

        for (var i = 0; i < completedActions.length; i++){

            const text = document.createElement("h6");
            text.innerHTML = completedActions[i].toString();
    
            // // Append outer div to the leader-completed-actions
            parentElement.appendChild(text); 
        }

    }



    return (
        <>

            <div class="set-leader-action-active-container" id="set-leader-action-active-id" onClick={handleSetLeaderActions}></div>
            
            <div class="retrieve-coach-leader-actions-container" id="retrieve-coach-leader-actions-id" onClick={handleGetCoachLeaderActions}></div>

            <div className="coach-leader-actions-div">
                <div className="leader-actions-main-container" id="leader-actions-main" value="default-value">
                    <div className="leader-no-action-desc-div" id="leader-no-action-desc"></div>
                    <div className="leader-actions-single-container" id="action-single-container-1">
                        <div className="leader-actions-action-name" id="action-name1"/>
                        <div className="leader-actions-iter" id="action-iter1"/>
                        <CircularProgressBar percentageProp={percentage_1} id="percentage-id-1"/>
                    </div>

                    <div className="leader-actions-single-container" id="action-single-container-2">
                        <div className="leader-actions-action-name" id="action-name2"/>
                        <div className="leader-actions-iter" id="action-iter2"/>
                        <CircularProgressBar percentageProp={percentage_2} id="percentage-id-2"/>
                    </div>

                    <div className="leader-actions-single-container" id="action-single-container-3">
                        <div className="leader-actions-action-name" id="action-name3"/>
                        <div classsName="leader-actions-iter" id="action-iter3"></div>
                        <CircularProgressBar percentageProp={percentage_3} id="percentage-id-3" />
                    </div>
                </div>

                <div className="leader-completed-actions-outer-div">
                    <h2 className="completed-actions-heading">Leader Completed Actions</h2>
                    <div className="leader-no-completed-action-desc-div" id="leader-no-completed-action-desc"></div>
                    <div className="leader-completed-actions-container" id="leader-completed-actions"></div>
                </div>

            </div>

        </>
    );

}

export default CoachLeaderActions;