import './Calendar.css';
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import CalendarClient from './CalendarClient'
import { Button} from "@mui/material";

export default function Calendar({ userType, userName, activateSession, deactivateSession}) {

    var timeslotBtns = []
    var sessionData = []

    const days = [
        "Mon",
        "Tue",
        "Wed",
        "Thu",
        "Fri",
        "Sat",
        "Sun",
    ]
    const fulldays = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ]
    const short_months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
    ];
    const startEndTime = [
        "8:00am-9:00am",
        "9:00am-10:00am",
        "10:00am-11:00am",
        "11:00am-12:00pm",
        "12:00pm-1:00pm",
        "1:00pm-2:00pm",
        "2:00pm-3:00pm",
        "3:00pm-4:00pm",
        "4:00pm-5:00pm",
        "5:00pm-6:00pm",
        "6:00pm-7:00pm",
        "7:00pm-8:00pm",
        "8:00pm-9:00pm",
    ];
    const months = [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
    ];

    // localStorage date format in string: ["Mon, 31 January 2022", ..., ... ]
    if (localStorage.getItem("currentWeek") == null){
        getSelectedWeek(new Date())
    }else{
        const arr = JSON.parse(localStorage.getItem("currentWeek"))
    }



    // param: "Mon, 31 January 2022" return: "31"
    function retDate(date){
        return date.split(" ")[1];
    }

    // param: "Mon, 31 January 2022" return: "Mon"
    function retShortDay(date){
        return date.split(",")[0];
    }

    // param: "Mon, 31 January 2022" return: "Monday"
    function retLongDay(date){
        return fulldays[days.indexOf(date.split(",")[0])];
    }

    // param: "Mon, 31 January 2022" return: "January"
    function retMonth(date){
        return date.split(" ")[2];
    }

    // param: "Mon, 31 January 2022" return: "2022"
    function retYear(date){
        return date.split(" ")[3];
    }

    

    {/* DEBUGGING */}
    // var date = new Date();
    // const today = new Date();
    // var type = "";
    // var dateString = days[date.getDay()]+ ", " + date.getDate() + " " + months[date.getMonth()] + " " + date.getFullYear();
    // var newDate = new Date(dateString);
    // console.log("[DEBUGGING] " + newDate)

    function getSelectedWeek(date){

        // Unable to access global variables. Variables re-declared here.
        const days = [
            "Mon",
            "Tue",
            "Wed",
            "Thu",
            "Fri",
            "Sat",
            "Sun",
        ]
        const fulldays = [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
        ]
        const short_months = [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
        ];

        const startEndTime = [
            "8:00am-9:00am",
            "9:00am-10:00am",
            "10:00am-11:00am",
            "11:00am-12:00pm",
            "12:00pm-1:00pm",
            "1:00pm-2:00pm",
            "2:00pm-3:00pm",
            "3:00pm-4:00pm",
            "4:00pm-5:00pm",
            "5:00pm-6:00pm",
            "6:00pm-7:00pm",
            "7:00pm-8:00pm",
            "8:00pm-9:00pm",
        ];

        const months = [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December",
        ];

        const setWeek = []; 
        let curr = date

        for (let i = 1; i <= 7; i++) {
            let first = curr.getDate() - curr.getDay() + i
            let DATE = new Date(curr.setDate(first))

            // Get day, date, month, and year to save into string
            // Format to: "Mon, 31 January 2022"
            if (DATE.getDay() == 0){
                // SUNDAY
                const day = days[6]
                const date = DATE.getDate().toString()
                const month = months[DATE.getMonth()]
                const year = DATE.getFullYear().toString()

                setWeek.push(day + ", " + date + " " + month + " " + year);
            }else{
                const day = days[DATE.getDay() - 1]
                const date = DATE.getDate().toString()
                const month = months[DATE.getMonth()]
                const year = DATE.getFullYear().toString()

                setWeek.push(day + ", " + date + " " + month + " " + year);
            }
        }  

        localStorage.setItem("currentWeek", JSON.stringify(setWeek))
        const arr = JSON.parse(localStorage.getItem("currentWeek"))
        // console.log(arr) // DEBUGGING

    }

    function getPreviousWeek(){
        const current_week = JSON.parse(localStorage.getItem("currentWeek"))
        const curr_date = new Date(current_week[0]) //test
        var last_week = new Date(curr_date.getFullYear(),curr_date.getMonth(), curr_date.getDate()-7); //test
        getSelectedWeek(last_week)
        generateCalendar()
        saveTimeslotBtns()
        setTimeslots()
      }

    function getNextWeek(){
        const current_week = JSON.parse(localStorage.getItem("currentWeek"))
        const curr_date = new Date(current_week[0]) //test
        var next_week = new Date(curr_date.getFullYear(),curr_date.getMonth(), curr_date.getDate()+7); //test
        getSelectedWeek(next_week)
        generateCalendar()
        saveTimeslotBtns()
        setTimeslots()

    }

    function saveTimeslotBtns(){
        timeslotBtns = document.querySelectorAll('[id=cal-btn]');
        // console.log("Entered: saveTimeslotBtns")  /* DEBUGGING */
    }

    function setTimeslots(){
        if (userType == "L"){
            // set all timeslots to gray bg and inactive
            timeslotBtns.forEach(function(btn) {
                btn.className = "timeslot-button";
                btn.innerText = ""
              });
        }
        else{
            // set all timeslots to white bg and active 
            timeslotBtns.forEach(function(btn) {
                btn.className = "ch-timeslot-button";
                btn.innerText = ""
              });
        }

        // set the timeslots of available and booked sessions
        setAvailableTimeslots()
        setBookedTimeslots()
        setRestrictedTimeslots()

    }

    function setAvailableTimeslots(){
        try{

            saveTimeslotBtns() 
            const current_week = JSON.parse(localStorage.getItem("currentWeek")) 

            const avTimes = sessionData['availableTimes']
            var time_curr = "-1"
            var day_curr = "-1"
            var full_day_curr = new Date()
            var appended_val = "-1"

            // preselected available times 
            var av_day = -1
            var av_date = -1
            var av_month = -1
            var av_year = -1

            // current week times
            var cw_day = -1
            var cw_date = -1
            var cw_month = -1
            var cw_year = -1

            for ( var i = 0; i < avTimes.length; i++){
                full_day_curr = new Date(avTimes[i]['date'])

                day_curr = full_day_curr.getDay()
                if (day_curr == 0){
                    day_curr = 6;
                }else{
                    day_curr = (day_curr - 1).toString()
                }

                // day_curr = (full_day_curr.getDay() - 1).toString() 
                time_curr = startEndTime.indexOf(avTimes[i]['available_time']).toString()

                av_day = full_day_curr.getDay();
                av_date = full_day_curr.getDate();
                av_month = full_day_curr.getMonth();
                av_year = full_day_curr.getFullYear();

                appended_val = day_curr + "-" + time_curr // check this value with the values of timeslotBtns

                for (var j = 0; j < timeslotBtns.length; j++){
                    const stored_wk = new Date(current_week[parseInt(day_curr)])

                    cw_day = stored_wk.getDay();
                    cw_date = stored_wk.getDate();
                    cw_month = stored_wk.getMonth();
                    cw_year = stored_wk.getFullYear();

                    if (timeslotBtns[j].value == appended_val && av_day == cw_day && av_date == cw_date && av_month == cw_month && av_year == cw_year){
                        if (userType == "L"){
                            timeslotBtns[j].className = "timeslot-button"
                        }else{
                            timeslotBtns[j].className = "ch-timeslot-button"
                        }
                        timeslotBtns[j].className += " available-slot"
                        timeslotBtns[j].innerText = ""
                        break;
                    }
                }
            }
        
        } catch (err){
            // console.log("no available times to set")
        }
    }

    function setBookedTimeslots(){
        try{
            const current_week = JSON.parse(localStorage.getItem("currentWeek"))

            const bkTimes = sessionData['bookedTimes']
            var time_curr = "-1"
            var day_curr = "-1"
            var full_day_curr = new Date()
            var appended_val = "-1"

            // preselected available times 
            var bk_day = -1
            var bk_date = -1
            var bk_month = -1
            var bk_year = -1

            // current week times
            var cw_day = -1
            var cw_date = -1
            var cw_month = -1
            var cw_year = -1

            for ( var i = 0; i < bkTimes.length; i++){
                
                // skip if booking belongs to another leader
                if (userType == "L"){
                    if (bkTimes[i]['leader'] != userName){
                        continue;
                    }
                }

                full_day_curr = new Date(bkTimes[i]['date'])
                day_curr = full_day_curr.getDay()
                if (day_curr == 0){
                    day_curr = 6;
                }else{
                    day_curr = (day_curr - 1).toString()
                }
                time_curr = startEndTime.indexOf(bkTimes[i]['booked_time']).toString()

                bk_day = full_day_curr.getDay();
                bk_date = full_day_curr.getDate();
                bk_month = full_day_curr.getMonth();
                bk_year = full_day_curr.getFullYear();

                appended_val = day_curr + "-" + time_curr // check this value with the values of timeslotBtns
  
                for (var j = 0; j < timeslotBtns.length; j++){
                    const stored_wk = new Date(current_week[parseInt(day_curr)])
                    cw_day = stored_wk.getDay();
                    cw_date = stored_wk.getDate();
                    cw_month = stored_wk.getMonth();
                    cw_year = stored_wk.getFullYear();

                    if (timeslotBtns[j].value == appended_val && bk_day == cw_day && bk_date == cw_date && bk_month == cw_month && bk_year == cw_year){
                        // const stored_wk = current_week[parseInt(day_curr)]
                        
                        if (userType == "L"){
                            timeslotBtns[j].className = "timeslot-button"
                            timeslotBtns[j].innerText = "Booked" 
                        }else{
                            timeslotBtns[j].className = "ch-timeslot-button"
                            timeslotBtns[j].innerText = bkTimes[i]['leader'] // modify to contain leader name
                        }
                        timeslotBtns[j].className += " booked-slot"
                        break;
                    }
                }

            }
        
        } catch (err){
            // console.log("no booked times to set")
        }
    }

    function setRestrictedTimeslots(){
        try{
            const current_week = JSON.parse(localStorage.getItem("currentWeek")) 

            const avTimes = sessionData['restrictedTimes']
            var time_curr = "-1"
            var day_curr = "-1"
            var full_day_curr = new Date()
            var appended_val = "-1"

            // preselected available times 
            var av_day = -1
            var av_date = -1
            var av_month = -1
            var av_year = -1

            // current week times
            var cw_day = -1
            var cw_date = -1
            var cw_month = -1
            var cw_year = -1

            for ( var i = 0; i < avTimes.length; i++){
                full_day_curr = new Date(avTimes[i]['date'])

                day_curr = full_day_curr.getDay()
                if (day_curr == 0){
                    day_curr = 6;
                }else{
                    day_curr = (day_curr - 1).toString()
                }

                // day_curr = (full_day_curr.getDay() - 1).toString() 
                time_curr = startEndTime.indexOf(avTimes[i]['time']).toString()

                av_day = full_day_curr.getDay();
                av_date = full_day_curr.getDate();
                av_month = full_day_curr.getMonth();
                av_year = full_day_curr.getFullYear();

                appended_val = day_curr + "-" + time_curr // check this value with the values of timeslotBtns

                for (var j = 0; j < timeslotBtns.length; j++){
                    const stored_wk = new Date(current_week[parseInt(day_curr)])

                    cw_day = stored_wk.getDay();
                    cw_date = stored_wk.getDate();
                    cw_month = stored_wk.getMonth();
                    cw_year = stored_wk.getFullYear();

                    if (timeslotBtns[j].value == appended_val && av_day == cw_day && av_date == cw_date && av_month == cw_month && av_year == cw_year){
                        if (userType == "L"){
                            timeslotBtns[j].className = "timeslot-button"
                        }else{
                            timeslotBtns[j].className = "ch-timeslot-button"
                        }
                        timeslotBtns[j].className += " restricted-slot"
                        timeslotBtns[j].innerText = ""
                        break;
                    }
                }
            }
        
        } catch (err){
            // console.log("no available times to set")
        }
    }

    /*  Generates table header data for date and month of the loaded week  */
    function generateCalendar(){

        const current_week = JSON.parse(localStorage.getItem("currentWeek"))

        // set current month
        if (retMonth(current_week[0]) != retMonth(current_week[6])){
            document.getElementById("month-text").innerHTML = retMonth(current_week[0]) + " - " + retMonth(current_week[6]) + " " + retYear(current_week[6]);
        }
        else{
            document.getElementById("month-text").innerHTML = retMonth(current_week[6]) + " " + retYear(current_week[6]);
        }

        // set dates of the week
        document.getElementById("monday-date").innerHTML = retDate(current_week[0])
        document.getElementById("tuesday-date").innerHTML = retDate(current_week[1])
        document.getElementById("wednesday-date").innerHTML = retDate(current_week[2])
        document.getElementById("thursday-date").innerHTML = retDate(current_week[3])
        document.getElementById("friday-date").innerHTML = retDate(current_week[4])
        document.getElementById("saturday-date").innerHTML = retDate(current_week[5])
        document.getElementById("sunday-date").innerHTML = retDate(current_week[6])

        // remove '.active' from all dates' classname
        for (var i = 0; i < current_week.length; i++){
            document.getElementById(fulldays[i].toLowerCase() + "-date").className = "day";
        }

        // set current date to active
        const todayDate = new Date();
        for (var i = 0; i < current_week.length; i++){
            var curr_date = new Date(current_week[i]) 
            if (curr_date.getDate() == todayDate.getDate() &&
                curr_date.getMonth() == todayDate.getMonth() &&
                curr_date.getFullYear() == todayDate.getFullYear()){
                    var dayNum = curr_date.getDay();
                    // console.log(dayNum)
                    if (dayNum == 0){
                        dayNum = 6;
                    }else{
                        dayNum -= 1;
                    }
                    document.getElementById(fulldays[dayNum].toLowerCase() + "-date").className += " active";
                    // console.log(fulldays[dayNum].toLowerCase())
                }
        } 
    }

    function saveValue(val){
        document.getElementById("value-holder").value = val;
        // console.log("value-holder from someValue: " + document.getElementById("value-holder").value)
    }

    function togglePopup(time){

        saveValue(time)

        // DEBUGGING
        // console.log("value-holder from togglePopup: " + document.getElementById("value-holder").value)

        const slotType = getTimeslotType(time)

        // check user type and execute from type of timeslot
        if (userType == "L"){
            if (slotType == 1){
                // leader clicks on an available slot 
                leaderPopup(time, "1")
            }
            else if (slotType == 2){
                // leader clicks on a booked slot
                leaderPopup(time, "0")
            }
        }
        else{
            if (slotType == 0){
                // coach clicks on inactive slot 
                coachPopup(time, "1")
            }
            else if (slotType == 1){
                // coach clicks on an available slot
                coachPopup(time, "0")
                
            }
            else if (slotType == 2){
                // coach clicks on a booked slot
                // WORK IN PROGRESS
            }
            else if (slotType == 3){
                // coach clicks on a restricted slot
                document.querySelector("#global-unavailable-noti-btn").click()
            }
        }
    }

    // Returns -1 for error, 0 for inactive, 1 for available, 2 for booked
    function getTimeslotType(time){
        for (var i = 0; i < timeslotBtns.length; i++){
            if (timeslotBtns[i].value == time){

                const c_name = timeslotBtns[i].classList
                try{
                    if (c_name.contains("available-slot")){
                        return 1
                    }
                    else if (c_name.contains("booked-slot")){
                        return 2
                    }
                    else if (c_name.contains("restricted-slot")){
                        return 3
                    }
                    else{
                        return 0
                    }
                } catch(err){
                    return -1
                }
            }
        }
        return -1
    }

    function closePopup(){
        // check which user is active and set popup to inactive
        if (userType == "L"){
            document.getElementById("popup-1").classList.remove("active");
        }
        else{
            document.getElementById("popup-2").classList.remove("active");
        }
    }

    /* adds or removes a booking for leader. state 0 for remove. state 1 for add */
    function leaderPopup(time, state){

        const current_week = JSON.parse(localStorage.getItem("currentWeek"))

        // document.getElementById("popup-1").classList.toggle("active"); 

        const start = time.split("-")[0]
        const end = time.split("-")[1]

        const curr_day = days[parseInt(start)];
        const curr_time = startEndTime[parseInt(end)];

        const current_week_date = new Date(current_week[parseInt(start)])

        const curr_date = current_week_date.getDate();
        const curr_month = months[current_week_date.getMonth()];
        const curr_year = current_week_date.getFullYear();

        const fullTimeDisplay = curr_day + ", " + curr_date + " " + curr_month + " " + curr_year;

        document.getElementById("leader-confirm-booking-day").innerHTML = curr_day;
        document.getElementById("leader-confirm-booking-time").innerHTML = curr_time;

        document.getElementById("leader-confirm-booking-date").innerHTML = fullTimeDisplay;
        document.getElementById("leader-confirm-accepted-btn").value = curr_time + "-" + fullTimeDisplay + "-" + state;

        const button_time = document.getElementById("leader-confirm-accepted-btn").value;

        document.getElementById("leader-confirm-accepted-btn").click(); 

    }

    /* adds or removes an available time for coach. state 0 for remove. state 1 for add */
    function coachPopup(time, state){
        // document.getElementById("popup-2").classList.toggle("active"); 

        // getSelectedWeek(first_day)

        const current_week = JSON.parse(localStorage.getItem("currentWeek"))

        // DEBUGGING
        // for(var i = 0; i < current_week.length; i++){
        //     console.log(current_week[i].getDate())
        //   }

        console.log(time)
        const start = time.split("-")[0]
        const end = time.split("-")[1]

        const curr_day = days[parseInt(start)];
        const curr_time = startEndTime[parseInt(end)];

        const current_week_date = new Date(current_week[parseInt(start)])

        // const curr_date = current_week[parseInt(start)].getDate();
        // const curr_month = months[current_week[parseInt(start)].getMonth()];
        // const curr_year = current_week[parseInt(start)].getFullYear();

        const curr_date = current_week_date.getDate();
        const curr_month = months[current_week_date.getMonth()];
        const curr_year = current_week_date.getFullYear();

        const fullTimeDisplay = curr_day + ", " + curr_date + " " + curr_month + " " + curr_year;

        document.getElementById("coach-confirm-booking-day").innerHTML = curr_day;
        document.getElementById("coach-confirm-booking-time").innerHTML = curr_time;

        document.getElementById("coach-confirm-booking-date").innerHTML = fullTimeDisplay;
        document.getElementById("coach-confirm-accepted-btn").value = curr_time + "-" + fullTimeDisplay + "-" + state;

        // console.log(fullTimeDisplay)

        const button_time = document.getElementById("coach-confirm-accepted-btn").value;

        document.getElementById("coach-confirm-accepted-btn").click(); 
    }


    // example param values: "0-1", "5-6"
    // return example: "9:00am-10:00am"
    function getTimeForBooking(val){
        return val.split("-")[0] + "-" + val.split("-")[1];
    }

    // example param values: "0-1", "5-6"
    // return example: "Wed, 19 January 2022"
    function getFullDateForBooking(val){
        return val.split("-")[2];
    }

    function addBookedSlot(time, full_date, state){

        const newDay = days.indexOf(full_date.split(",")[0])
        const newTime = startEndTime.indexOf(time)

        const newBooking = newDay + "-" + newTime

        for(var i = 0; i < timeslotBtns.length; i++){
            if (timeslotBtns[i].value == newBooking){
                // modify UI display and text to booking state
                if (userType == "L"){
                    timeslotBtns[i].className = "timeslot-button"
                }else{
                    timeslotBtns[i].className = "ch-timeslot-button"
                }
                // console.log("state: " + state)
                if(state == "1"){
                    timeslotBtns[i].className += " booked-slot"
                    timeslotBtns[i].innerText = "Booked"
                }else{
                    timeslotBtns[i].className += " available-slot"
                    timeslotBtns[i].innerText = ""
                }
                
            }
        }
    }

    function addAvailableSlot(time, full_date, state){
        const newDay = days.indexOf(full_date.split(",")[0])
        const newTime = startEndTime.indexOf(time)

        const newBooking = newDay + "-" + newTime

        // for(var i = 0; i < current_week.length; i++){
        //     console.log(current_week[i].getDate())
        //   }

        for(var i = 0; i < timeslotBtns.length; i++){
            if (timeslotBtns[i].value == newBooking){
                // modify UI display and text to booking state
                if (userType == "L"){
                    timeslotBtns[i].className = "timeslot-button"
                }else{
                    timeslotBtns[i].className = "ch-timeslot-button"
                }
                // console.log("state: " + state)
                if(state == "1"){
                    timeslotBtns[i].className += " available-slot"
                    timeslotBtns[i].innerText = ""
                }else{
                    timeslotBtns[i].innerText = ""
                }
            }
        }

                        
    }

    /* Returns if state is 0 or 1 from string */
    function getState(val){
        return val.split("-")[3];
    }


    /*  -------- {USEEFFECT, AXIOS START} -------  */



    // get all booking times and preselected bookings upon loading page
    useEffect(() => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
             username: userName,
            }).then(response => {
                sessionData = response.data
                console.log(sessionData)
                // DEBUGGING
                // const AVAILABLE_TIMES = (sessionData['availableTimes'])
                // const BOOKED_TIMES = (sessionData['bookedTimes'])
                // for (var i = 0; i < AVAILABLE_TIMES.length; i++){
                //     console.log(AVAILABLE_TIMES[i]['available_time'])
                // } 
                // for (var i = 0; i < BOOKED_TIMES.length; i++){
                //     console.log(BOOKED_TIMES[i]['leader'] + " " + BOOKED_TIMES[i]['booked_time'])
                        
                // }
                console.log("Getting available / booked sessions...")
                try {
                    generateCalendar()
                    console.log("generated calendar.")
                    // check if the calendar is correctly displaying the dates, if not reload the calendar again
                    if (document.getElementById("monday-date").innerHTML == ""){
                        // window.location.reload(); 
                    }
                } catch (err) {
                    // console.log("error occured when generating dates and months")
                }
        
                try{
                        saveTimeslotBtns() 
                        setTimeslots()
                } catch(err) {
                    // console.log("error occured when saving timeslot btns, and setting time slot btns")
                }

                setTimeslots();

        }).catch(err => { console.log("scheduletestGET error running") });


        axios.post('https://new-emerge-website.herokuapp.com/api/getUsersForUpdate/',{
            username: userName,
            userType: userType,
           }).then(response => {

            // save to local storage

            localStorage.setItem("recipientList", JSON.stringify(response.data['userList']))
            // console.log(JSON.parse(localStorage.getItem("recipientList")))
            }).catch(err => { console.log("Something went wrong trying to retrieve associated users for update") 
        });

    }, [])

    // Checks if there are newly linked users when page is not reloaded and updates recipient list
    const getUsersForUpdate = React.useCallback(event => {
        axios.post('https://new-emerge-website.herokuapp.com/api/getUsersForUpdate/',{
            username: userName,
            userType: userType,
           }).then(response => {

            // save to local storage

            localStorage.setItem("recipientList", JSON.stringify(response.data['userList']))
            console.log("reupdated recipientList")
            }).catch(err => { console.log("Something went wrong trying to retrieve associated users for update") 
        });
    })

    // reset all booking times and preselected bookings
    const handleReset = React.useCallback(event => {
        event.preventDefault();
        axios.post('https://new-emerge-website.herokuapp.com/api/scheduletestRESET/',{
        }).then(response => {
            console.log("scheduletestRESET success from axios")
            // console.log(JSON.stringify(response.data))

    }).catch(err => { console.log("scheduletestRESET error running") });
    }, [])

    // Move back a week
    const handlePreviousWeek = React.useCallback(event => {
        event.preventDefault();
        // const leaderInput = event.target.elements.leaderName;
        axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
            username: userName,
            },).then(response => {
                sessionData = response.data
                // console.log("scheduletestGET: got some booking data from coach")
                getPreviousWeek();
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []);

    // Move forward a week
    const handleNextWeek = React.useCallback(event => {
        event.preventDefault();
        axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
            username: userName,
            },).then(response => {
                sessionData = response.data
                // console.log("scheduletestGET: got some booking data from coach")
                getNextWeek();
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []);

    // Reload calendar after detecting an update
    const handleUpdateReload = React.useCallback(event => {
        event.preventDefault();
        axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
            username: userName,
            },).then(response => {
                document.querySelector("#reload-noti-btn").click()
                sessionData = response.data
                setTimeslots();
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []);

    // Mimicking get coach booking sessions
    const handleGetCoachingSessionTimes = React.useCallback(event => {
        event.preventDefault();
        axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
            username: userName,
            },).then(response => {
                sessionData = response.data
                setTimeslots();
            })
            .catch(err => {
            })
            event.preventDefault();
    }, []);

    // Updates JOIN SESSION on Coach Page
    const handleLeaderBookingChange = React.useCallback(event => {
        event.preventDefault();

        axios.post('https://new-emerge-website.herokuapp.com/api/leaderGetZoomDetails/',{
            username: localStorage.getItem("username"),
            })
            .then(response => {
                var data = response.data
                console.log(data)
                console.log(data['msg'])
                if (data['msg'] === "No upcoming bookings"){
                    document.getElementById("booking-date-notice").innerHTML = "No Upcoming Sessions"

                    document.getElementById("disable-join-session").style.display = "block";
                    document.getElementById("enable-join-session").style.display = "none";
                }
                else if (data['msg'] === "Upcoming booking"){
 
                    document.getElementById("booking-date-notice").innerHTML = data['earliestBookingDate']
                    document.getElementById("booking-meeting-id").innerHTML = "Meeting ID: " + data['meetingId'];
                    document.getElementById("booking-meeting-password").innerHTML = "Meeting Password: " + data['meetingPassword'];
                    document.getElementById("zoom-link-hyperlink").href = data['meetingLink'];

                    document.getElementById("disable-join-session").style.display = "none";
                    document.getElementById("enable-join-session").style.display = "block";

                }

            })
            .catch(err => {
            })
    }, [])

    const handleLeaderBookingTimeSubmit = React.useCallback(event => {
        event.preventDefault();
        var successBooking = -1;
        togglePopup(document.getElementById("value-holder").value);
        if(getState(document.getElementById("leader-confirm-accepted-btn").value) == "1") {
        axios.post('https://new-emerge-website.herokuapp.com/api/leaderBookTime/',{
            username: userName,
             booked_time:getTimeForBooking(document.getElementById("leader-confirm-accepted-btn").value),
             booked_full_date: getFullDateForBooking(document.getElementById("leader-confirm-accepted-btn").value)
            },).then(response => {
                if (response.data['msg'] == "Booking successful!"){
                    document.querySelector("#booked-noti-btn").click()
                    addBookedSlot( getTimeForBooking(document.getElementById("leader-confirm-accepted-btn").value), 
                    getFullDateForBooking(document.getElementById("leader-confirm-accepted-btn").value ),
                    getState(document.getElementById("leader-confirm-accepted-btn").value))

                    // update recipientList and send update message to server
                    document.getElementById("leaderBookingChange").click()
                    document.getElementById("update-recipient-users").click()
                    document.querySelector("#calendar-update-btn").click()
                    

                }else{
                    // leader unable to make more than one booking per week
                    document.querySelector("#warning-noti-btn").click()
                }
            })
            .catch(err => {
            }); 

            axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
                username: userName,
                },).then(response => {
                    sessionData = response.data
                })
                .catch(err => {
                });

        }else{
            axios.post('https://new-emerge-website.herokuapp.com/api/leaderRemoveBooking/',{
                username: userName,
                 booked_time:getTimeForBooking(document.getElementById("leader-confirm-accepted-btn").value),
                 booked_full_date: getFullDateForBooking(document.getElementById("leader-confirm-accepted-btn").value)
                },).then(response => {

                    document.querySelector("#not-booked-noti-btn").click()
                    addBookedSlot( getTimeForBooking(document.getElementById("leader-confirm-accepted-btn").value), 
                    getFullDateForBooking(document.getElementById("leader-confirm-accepted-btn").value ),
                    getState(document.getElementById("leader-confirm-accepted-btn").value))

                    // update recipientList and send update message to server
                    document.getElementById("leaderBookingChange").click()
                    document.getElementById("update-recipient-users").click()
                    document.querySelector("#calendar-update-btn").click()
                })
                .catch(err => {
                }); 

            axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
                username: userName,
                },).then(response => {
                    sessionData = response.data
                })
                .catch(err => {
                });
        }
            event.preventDefault();
            
    }, []);


    const handleCoachSelectedTimeSubmit = React.useCallback(event => {
        event.preventDefault();
        togglePopup(document.getElementById("value-holder").value);
        if(getState(document.getElementById("coach-confirm-accepted-btn").value) == "1") {
        axios.post('https://new-emerge-website.herokuapp.com/api/coachChooseAvailableTime/',{
            username: userName,
             available_time:getTimeForBooking(document.getElementById("coach-confirm-accepted-btn").value),
             available_full_date: getFullDateForBooking(document.getElementById("coach-confirm-accepted-btn").value)
            },).then(response => {

                document.querySelector("#available-noti-btn").click()
                addAvailableSlot( getTimeForBooking(document.getElementById("coach-confirm-accepted-btn").value), 
                getFullDateForBooking(document.getElementById("coach-confirm-accepted-btn").value ),
                getState(document.getElementById("coach-confirm-accepted-btn").value) )

                // update recipientList and send update message to server
                document.getElementById("update-recipient-users").click()
                document.querySelector("#calendar-update-btn").click()

                event.preventDefault();
                
            })
            .catch(err => {
            }); 

        axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
            username: userName,
            },).then(response => {
                sessionData = response.data
            })
            .catch(err => {
            });

        }else{
            axios.post('https://new-emerge-website.herokuapp.com/api/coachRemoveAvailableTime/',{
                username: userName,
                 available_time:getTimeForBooking(document.getElementById("coach-confirm-accepted-btn").value),
                 available_full_date: getFullDateForBooking(document.getElementById("coach-confirm-accepted-btn").value)
                },).then(response => {

                    document.querySelector("#not-available-noti-btn").click()
                    addAvailableSlot( getTimeForBooking(document.getElementById("coach-confirm-accepted-btn").value), 
                    getFullDateForBooking(document.getElementById("coach-confirm-accepted-btn").value ),
                    getState(document.getElementById("coach-confirm-accepted-btn").value) )

                    // update recipientList and send update message to server
                    document.getElementById("update-recipient-users").click()
                    document.querySelector("#calendar-update-btn").click()

                    event.preventDefault();
                })
                .catch(err => {
                }); 

            axios.post('https://new-emerge-website.herokuapp.com/api/getAvailableBookedTimes/',{
                username: userName,
            },).then(response => {
                sessionData = response.data
            })
            .catch(err => {
            });
        }

            event.preventDefault();
    }, []);


    if (document.readyState === "complete") {
        // console.log("already loaded page")
        if (typeof first_day == 'undefined'){ }

        try {
             generateCalendar()

             // check if the calendar is correctly displaying the dates, if not reload the calendar again
             if (document.getElementById("monday-date").innerHTML == ""){
                //  window.location.reload();
                // console.log("page is trying to reload")
             }
        } catch (err) {
        }

        try{
              saveTimeslotBtns() 
              setTimeslots()
        } catch(err) {
        }

        setTimeout(function(){
            document.getElementById("record").click()
        },2000);

    } else {
            // console.log("newly loaded page")

            try {
                generateCalendar()
                if (document.getElementById("monday-date").innerHTML == ""){
                    // window.location.reload(); 
                }
            } catch (err) {
                // console.log("error occured when generating dates and months")
            }
    
            try{
                  saveTimeslotBtns() 
                  setTimeslots()
            } catch(err) {
            }
    }

    
    /*  -------- {USEEFFECT, AXIOS} END -------  */

    function dummyFunction(){
        console.log("activating available session flash")
        activateSession();
    }


    return (
        <>

        {/* socket io */}
        <CalendarClient recipientList={JSON.parse(localStorage.getItem("recipientList"))} senderId={userName}/>

        <Button class="reload-page-btn" id="reload-page" onClick={handleUpdateReload}/>

        <Button class="update-recipient-users-div" id="update-recipient-users" onClick={getUsersForUpdate}/>

        <Button class="update-recipient-users-div" id="leaderBookingChange" onClick={handleLeaderBookingChange}/>

        <form onSubmit={handleGetCoachingSessionTimes}>
            <button class="formButton" id="record" onClick={handleGetCoachingSessionTimes}>GET SESSIONS</button>
        </form> 

        <div class="time-value" id="value-holder" value=""></div>

        <body onload="loadCalendarFirst()">

            <div class="calendar">
  
            {/* Leader book from available slot popup */}
            <div class="popup" id="popup-1">
                <div class="overlay"></div>
                <div class="content">
                <div class="close-btn" onClick={ () => closePopup()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                        <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                    </svg>
                </div>
                <p>Book Coaching Session</p>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                    <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                </svg>
                <p id="leader-confirm-booking-day"></p>
                <p id="leader-confirm-booking-time"></p>
                <p id="leader-confirm-booking-date"></p>
                <button class="confirm-booking-btn" id="leader-confirm-accepted-btn" value="default" onClick={handleLeaderBookingTimeSubmit}>Confirm</button>
                </div>
            </div>

            {/* Coach popup */}
            <div class="popup" id="popup-2">
                <div class="overlay"></div>
                <div class="content">
                <div class="close-btn" onClick={ () => closePopup()}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
                        <path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
                    </svg>
                </div>
                <p>Coaching Session Timeslot Availability</p>
                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock" viewBox="0 0 16 16">
                    <path d="M8 3.5a.5.5 0 0 0-1 0V9a.5.5 0 0 0 .252.434l3.5 2a.5.5 0 0 0 .496-.868L8 8.71V3.5z"/>
                    <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm7-8A7 7 0 1 1 1 8a7 7 0 0 1 14 0z"/>
                </svg>
                <p id="coach-confirm-booking-day"></p>
                <p id="coach-confirm-booking-time"></p>
                <p id="coach-confirm-booking-date"></p>
                <button class="confirm-booking-btn" id="coach-confirm-accepted-btn" value="default" onClick={handleCoachSelectedTimeSubmit}>Confirm</button>
                </div>
            </div>
            
            <table>
                <thead>
                <tr>
                    <th class="month-container" colspan="8">
                        <p class="long" id="month-text">Default</p>
                    </th>
                </tr>
                <tr>
                    <th>
                    
                    {/*  Week Navigation Buttons  */}
                    <button class="left-nav-btn" onClick={handlePreviousWeek}>
                        <svg class="left-arrow" xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-left-short" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z"/>
                        </svg>
                    </button>
                    <button class="right-nav-btn" onClick={handleNextWeek}>
                        <svg class="right-arrow" xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                        </svg>
                    </button>
                    
                    {/*   Table header with date, day of week, and month   */}

                    </th>
                    <th>
                    <span class="short">Mon</span>
                    <span class="day" id="monday-date"></span>
                    </th>
                    <th>
                    <span class="short">Tue</span>
                    <span class="day" id="tuesday-date"></span>
                    </th>
                    <th>
                    <span class="short">Wed</span>
                    <span class="day" id="wednesday-date"></span>
                    </th>
                    <th>
                    <span class="short">Thur</span>
                    <span class="day" id="thursday-date"></span>
                    </th>
                    <th>
                    <span class="short">Fri</span>
                    <span class="day active" id="friday-date"></span>
                    </th>
                    <th>
                    <span class="short">Sat</span>
                    <span class="day" id="saturday-date"></span>
                    </th>
                    <th>
                    <span class="short">Sun</span>
                    <span class="day" id="sunday-date"></span>
                    </th>
                </tr>
                </thead>

                {/*  Body of calendar with dates and timeslots  */}

                <tbody>
                <tr>
                    <td class="hour" rowspan="1"><span>8AM</span></td>

                    {/* Note: togglePopup('<day of week>-<start end time>'), value="<day of week>-<start end time>" */}

                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-0')} value="0-0"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-0')} value="1-0"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-0')} value="2-0"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-0')} value="3-0"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-0')} value="4-0"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-0')} value="5-0"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-0')} value="6-0"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>9AM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-1')} value="0-1"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-1')} value="1-1"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-1')} value="2-1"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-1')} value="3-1"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-1')} value="4-1"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-1')} value="5-1"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-1')} value="6-1"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>10AM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-2')} value="0-2"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-2')} value="1-2"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-2')} value="2-2"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-2')} value="3-2"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-2')} value="4-2"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-2')} value="5-2"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-2')} value="6-2"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>11AM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-3')} value="0-3"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-3')} value="1-3"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-3')} value="2-3"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-3')} value="3-3"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-3')} value="4-3"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-3')} value="5-3"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-3')} value="6-3"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>12PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-4')} value="0-4"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-4')} value="1-4"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-4')} value="2-4"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-4')} value="3-4"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-4')} value="4-4"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-4')} value="5-4"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-4')} value="6-4"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>1PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-5')} value="0-5"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-5')} value="1-5"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-5')} value="2-5"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-5')} value="3-5"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-5')} value="4-5"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-5')} value="5-5"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-5')} value="6-5"></button></td>
                </tr>
                
                <tr>
                    <td class="hour" rowspan="1"><span>2PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-6')} value="0-6"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-6')} value="1-6"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-6')} value="2-6"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-6')} value="3-6"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-6')} value="4-6"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-6')} value="5-6"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-6')} value="6-6"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>3PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-7')} value="0-7"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-7')} value="1-7"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-7')} value="2-7"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-7')} value="3-7"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-7')} value="4-7"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-7')} value="5-7"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-7')} value="6-7"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>4PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-8')} value="0-8"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-8')} value="1-8"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-8')} value="2-8"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-8')} value="3-8"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-8')} value="4-8"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-8')} value="5-8"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-8')} value="6-8"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>5PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-9')} value="0-9"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-9')} value="1-9"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-9')} value="2-9"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-9')} value="3-9"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-9')} value="4-9"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-9')} value="5-9"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-9')} value="6-9"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>6PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-10')} value="0-10"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-10')} value="1-10"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-10')} value="2-10"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-10')} value="3-10"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-10')} value="4-10"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-10')} value="5-10"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-10')} value="6-10"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>7PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-11')} value="0-11"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-11')} value="1-11"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-11')} value="2-11"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-11')} value="3-11"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-11')} value="4-11"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-11')} value="5-11"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-11')} value="6-11"></button></td>
                </tr>
                <tr>
                    <td class="hour" rowspan="1"><span>8PM</span></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('0-12')} value="0-12"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('1-12')} value="1-12"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('2-12')} value="2-12"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('3-12')} value="3-12"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('4-12')} value="4-12"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('5-12')} value="5-12"></button></td>
                    <td><button class="timeslot-button" id="cal-btn" onClick={ () => togglePopup('6-12')} value="6-12"></button></td>
                </tr>
                
                </tbody>
            </table>
            </div>


            {/*  Script for getting available / booked sessions upon first load  */}
            <script language = "javascript" type = "text/javascript">
                
                {/* function loadCalendarFirst(){

                    // getSelectedWeek(new Date()),
                    window.addEventListener('load', function () {
                        generateCalendar()
                        saveTimeslotBtns() 
                        setTimeslots()
                        
                      })


                }    */}
            </script>

            </body>

        </>


    );


}