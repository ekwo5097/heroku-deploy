import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Button } from "@mui/material";
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';


const Notification = () => {

    const [booked, setBooked] = React.useState(false);
    const [notBooked, setNotBooked] = React.useState(false);
    const [available, setAvailable] = React.useState(false);
    const [notAvailable, setNotAvailable] = React.useState(false);
    const [warning, setWarning] = React.useState(false);
    const [reload, setReload] = React.useState(false);
    const [globalUnavailable, setGlobalUnavailable] = React.useState(false);

  const Alert = React.forwardRef(function Alert(props, ref) {
      return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
    });


    //====== activating notifications ========//

    const openSetBooked = () => {
        setBooked(true)
      };
    
      const openSetNotBooked = () => {
        setNotBooked(true)
      };
    
      const openSetAvailable = () => {
        setAvailable(true)
      };
    
      const openSetNotAvailable = () => {
        setNotAvailable(true)
      };
    
      const openSetWarning = () => {
        setWarning(true)
      };
    
      const openSetReload = () => {
        setReload(true)
      };

      const openSetGlobalUnavailable = () => {
        setGlobalUnavailable(true)
      };


    //====== deactivating notifications ========//

  const handleSetBooked = () => {
    setBooked(false)
  };

  const handleSetNotBooked = () => {
    setNotBooked(false)
  };

  const handleSetAvailable = () => {
    setAvailable(false)
  };

  const handleSetNotAvailable = () => {
    setNotAvailable(false)
  };

  const handleSetWarning = () => {
    setWarning(false)
  };

  const handleSetReload = () => {
    setReload(false)
  };

  const handleSetGlobalUnavailable = () => {
    setGlobalUnavailable(false)
  };



  return (
        <>
        <Button class="notification-class-btn" id="booked-noti-btn" onClick={openSetBooked}> </Button>
        <Button class="notification-class-btn" id="not-booked-noti-btn" onClick={openSetNotBooked}> </Button>
        <Button class="notification-class-btn" id="available-noti-btn" onClick={openSetAvailable}> </Button>
        <Button class="notification-class-btn" id="not-available-noti-btn" onClick={openSetNotAvailable}> </Button>
        <Button class="notification-class-btn" id="warning-noti-btn" onClick={openSetWarning}> </Button>
        <Button class="notification-class-btn" id="reload-noti-btn" onClick={openSetReload}> </Button>
        <Button class="notification-class-btn" id="global-unavailable-noti-btn" onClick={openSetGlobalUnavailable}> </Button>
        
        <Snackbar open={booked} autoHideDuration={5000} onClose={handleSetBooked}>
            <Alert onClose={handleSetBooked} severity="info" sx={{ width: '70%' }}>
                Booking successful!
            </Alert>
        </Snackbar>

        <Snackbar open={notBooked} autoHideDuration={5000} onClose={handleSetNotBooked}>
            <Alert onClose={handleSetNotBooked} severity="info" sx={{ width: '70%' }}>
                Successfully removed booking
            </Alert>
        </Snackbar>

        <Snackbar open={available} autoHideDuration={5000} onClose={handleSetAvailable}>
            <Alert onClose={handleSetAvailable} severity="info" sx={{ width: '70%' }}>
                Created available session!
            </Alert>
        </Snackbar>

        <Snackbar open={notAvailable} autoHideDuration={5000} onClose={handleSetNotAvailable}>
            <Alert onClose={handleSetNotAvailable} severity="info" sx={{ width: '70%' }}>
                Successfully removed available session
            </Alert>
        </Snackbar>

        <Snackbar open={warning} autoHideDuration={5000} onClose={handleSetWarning}>
            <Alert onClose={handleSetWarning} severity="warning" sx={{ width: '70%' }}>
                Cannot book more than one session in a week.
                Please remove your current session for the week or book a session in a different week.
            </Alert>
        </Snackbar>

        <Snackbar open={reload} autoHideDuration={5000} onClose={handleSetReload}>
            <Alert onClose={handleSetReload} severity="info" sx={{ width: '70%' }}>
                Reloading calendar...
            </Alert>
        </Snackbar>

        <Snackbar open={globalUnavailable} autoHideDuration={5000} onClose={handleSetGlobalUnavailable}>
            <Alert onClose={handleSetGlobalUnavailable} severity="info" sx={{ width: '70%' }}>
                This timeslot is already booked.
            </Alert>
        </Snackbar>
        
        </>


  );
};

export default Notification;