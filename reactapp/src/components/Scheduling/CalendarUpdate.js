import React, { useState } from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { Button} from "@mui/material";

/**
 *
 * @param {Object} props - React props
 * @param {boolean} props.paired - boolean to heck if matching conversation found
 * @param {Function} props.updateMessages - function to update array of messages
 * in parent object
 * @returns {JSX} - JSX representing messaging input
 */

const CalendarUpdate = ({ updateMessages, senderId }) => {
  // stores currently typed message
  const [message, updateMessage] = useState("");

  const userData = useSelector((state) => state.user);

  const handleChange = (event) => {
    updateMessage(event.target.value);
  };

  const submitMessage = () => {
    if (message !== "") {
      // updateMessages({ text: message, sender: userData._id });
      updateMessages({ text: message, sender: senderId });
      updateMessage("");
    }else{
        // console.log("message is empty")
        updateMessages({ text: "text-placeholder", sender: senderId });
    }
  };

  return (

            <Button id="calendar-update-btn" value="calendar-update-btn-value" onClick={submitMessage}> </Button>

  );
};

CalendarUpdate.propTypes = {
//   paired: PropTypes.bool,
  updateMessages: PropTypes.func,
};

export default CalendarUpdate;
