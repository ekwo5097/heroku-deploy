const server = require("http").createServer();
const io = require("socket.io")(server, {
  cors: {
    origin: 'http://127.0.0.1:8000',
    //origin: 'http://localhost:3000',
    methods: ["GET", "POST"]
  },
});
server.listen(3005, ()=>console.log('listening on port 3005'))

let users = [];

const addUser = (userId, socketId) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
    console.log("ADDING A USER --- userId is:" + userId + "socketId is:" + socketId)
};

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return users.find((user) => user.userId === userId);
};

io.on("connection", (socket) => {
  console.log("socket: a user connected.");

  socket.on("addUser", (userId) => {
    // Verify user exists

    addUser(userId, socket.id);
    io.emit("getUsers", users);
  });

  // modify depending on whether it is a message or a calendar update
  socket.on("sendMessage", ({ senderId, receiverList, text }) => {
    // test start

    // print out each element in the users list
    console.log("printing out all users on server database")
    for (var i = 0; i < users.length; i++){
        console.log(users[i])
    }

    console.log(receiverList)

    for (var i = 0 ; i < receiverList.length; i++){
        console.log(">> in loop: SENDING A MESSAGE --- senderId is:" + senderId + " receiverId is:" + receiverList[i] + " text is:" + text)
        
        // check if userId is in users list
        for(var j = 0; j < users.length; j++){
            if (users[j].userId === receiverList[i]){
                const user = getUser(receiverList[i]);
                io.to(user?.socketId).emit("getMessage", "~~~~~~~hi there from the server! :D "); 
                // console.log("attempted to send client " + receiverList[i] + " a server message!")
                break;
            } 
        }
    }
    // test end

    // console.log("SENDING A MESSAGE --- senderId is:" + senderId + " receiverId is:" + receiverId + " text is:" + text)
    // const user = getUser(receiverId);
    // io.to(user?.socketId).emit("getMessage", {
    //   senderId,
    //   text,
    // });
  });

  socket.on("disconnect", () => {
    console.log("a user disconnected!");

    removeUser(socket.id);
    io.emit("getUsers", users);
  });

});

// app.use(express.static(__dirname + "../../../client/build/"));
// app.get("/*", (req, res) => {
//   res.sendFile("index.html", { root: __dirname + "../../../client/build" });
// });

// module.exports = { app, server };
module.exports = { server };