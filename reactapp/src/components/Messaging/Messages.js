import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import io from "socket.io-client";
import PropTypes from "prop-types";
import MessageList from "./MessageList";
import MessageInput from "./MessageInput";
import axios from 'axios';

/**
 * @param {Object} props - props passed into react component
 * @param {String} props.recipientId - id of matched leader/coach
 * @returns {JSX} - JSX representing wrapper for message list and input
 */
const Messages = ({ recipientId, senderId }) => {
  console.log("Messages.js senderId:"+senderId)
  const [messages, updateMessages] = useState([]);
  const [conversation, setConversation] = useState();
  const [socket, setSocket] = useState(null);
  const userState = useSelector((state) => state.user);



  useEffect(() => {


    axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderid/', {
            leadername: localStorage.getItem("messageLeader"),
            
        })
            .then(response => {
                setConversation(response.data['leaderid']);
                console.log("conversation is "+ conversation);
                axios.post('https://new-emerge-website.herokuapp.com/api/getMessages/', {
                roomid:response.data.leaderid,
                sender:senderId,


                   })
            .then(response => {
              updateMessages(response.data.messages)   
              console.log(response.data.messages)                 
            })
            .catch(err => {
            });
              
                
            })
            .catch(err => {
            });
    // const newSocket = io(process.env.REACT_APP_BACKEND_URL);
    const newSocket = io('http://localhost:3000');
    newSocket.emit('create',conversation)
    newSocket.on("getMessage", (data) => {
      if(senderId!=data.senderId) {
      updateMessages((messages) => [
        ...messages,
        {
          sender: data.senderId,
          text: data.text,
        },
      ]);
    }
    });
  
    //setConversation()
    // newSocket.emit("addUser", userState._id);
    newSocket.emit("addUser", {userId:senderId,roomid:conversation});
    setSocket(newSocket);
    // return () => newSocket.close();
    
    
  },[recipientId,senderId, setSocket]);
  //, [setSocket, senderId]);

  // }, [setSocket, userState._id]);

  // retrieve conversation
  // useEffect(() => {
  //   const getConversation = async () => {
  //     if (!recipientId) {
  //       return;
  //     }
  //     try {
  //       const url =
  //         process.env.REACT_APP_BACKEND_URL +
  //         "/api/conversation/" +
  //         recipientId;

  //       const conversation = await fetch(url, {
  //         method: "GET",
  //         credentials: "include",
  //         headers: {
  //           "Content-Type": "application/json",
  //         },
  //       }).then((data) => data.json());

  //       setConversation(conversation);
  //     } catch (err) {
  //       console.log(err);
  //     }
  //   };
  //   getConversation();
  // }, [setConversation, recipientId]);

  // retrieve messages for matching conversation

  const addNewMessage = (message) => {
    updateMessages([...messages, message]);
    console.log(messages)
    //saving messages on backend
    axios.post('https://new-emerge-website.herokuapp.com/api/saveMessages/', {
                senderId: senderId,
                roomid:conversation,
                text: message.text,
            
        })
            .then(response => {
                           
            })
            .catch(err => {
            });
    // Pass through to socket
    // Send through to database
    // const saveMessage = async (body) => {
    //   try {
    //     await fetch(process.env.REACT_APP_BACKEND_URL + "/api/message", {
    //       method: "POST",
    //       credentials: "include",
    //       headers: {
    //         "Content-Type": "application/json",
    //       },
    //       body: JSON.stringify(body),
    //     }).then((data) => data.json());
    //   } catch (e) {
    //     console.log(e);
    //   }
    // };

    // saveMessage({
    //   conversationId: conversation._id,
    //   text: message.text,
    // });

    /**
     * Socket.io
     */

    socket.emit("sendMessage", {
      // senderId: userState._id,

      senderId: senderId,
      receiverId: recipientId,
      roomid:conversation,
      text: message.text,
    });
  };
  
  

  return (
   
    <div>
      <MessageList 
      messages={messages}
      senderId={senderId} />
      <div>
        <MessageInput
          paired={conversation !== null}
          updateMessages={addNewMessage}
          senderId={senderId}
        />
      </div>
    </div>
  );
};

Messages.propTypes = {
  recipientId: PropTypes.string,
};

export default Messages;
