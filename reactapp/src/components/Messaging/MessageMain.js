import React, { useState,useEffect } from "react";
import styled from "styled-components";
import { Fab, Collapse } from "@mui/material";
import MessageRoundedIcon from "@mui/icons-material/MessageRounded";
import { useSelector } from "react-redux";
import MessagePopup from "./MessagePopup";
import axios from 'axios';

const MessageWrapper = styled.div`
  position: fixed;
  bottom: 4vh;
  right: 4vw;
  text-align: right;
`;




export default function MessageMain() {

    const [open, setOpen] = useState(false);

    const toggleOpen = () => {
        setOpen(!open);
    };

    const [leaderid, setleaderid]=useState();
    const [coachid, setcoachid]=useState();

    // const userData = useSelector((state) => state.user);

    let coachName = "sample-leader";
    // if (userData.pairedCoach) {
    //     coachName =
    //     userData.pairedCoach.firstName + " " + userData.pairedCoach.lastName;
    // }

    useEffect(() => {
        
        axios.post('https://new-emerge-website.herokuapp.com/api/getLeaderCoachid/', {
            username: localStorage.getItem("username"),
            
        })
            .then(response => {
                setleaderid(response.data.leaderid);
                setcoachid(response.data.coachid);

                
            })
            .catch(err => {
            });
        },[]);
    
    return (
        <>
        
        <MessageWrapper>
            <Collapse in={open} sx={{ flexDirection: "column-reverse" }}>
            <MessagePopup
                name={coachName}
                toggleOpen={toggleOpen}
                popup={true}
                recipientId={coachid}
                senderId={leaderid}

                
            />
            {/* <MessagePopup
                name={coachName}
                toggleOpen={toggleOpen}
                popup={true}
                recipientId={userData.pairedCoach?._id}
            /> */}
            {/* <p>This are some messages</p>
            <p>This are some messages</p>
            <p>This are some messages</p>
            <p>This are some messages</p> */}
            
            </Collapse>

            <Fab
            color="secondary"
            aria-label="message"
            onClick={() => toggleOpen()}
            sx={{ mt: 1 }}
            >
            <MessageRoundedIcon />
            </Fab>
        </MessageWrapper>
        </>
    );
}