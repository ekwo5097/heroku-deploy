const server = require("http").createServer();
const io = require("socket.io")(server, {
  cors: {
    origin: 'http://127.0.0.1:8000',
    //origin: 'http://localhost:3000',
    methods: ["GET", "POST"]
  },
});
server.listen(3000, ()=>console.log('listening on port 3000'))

let users = [];

const addUser = (userId, socketId) => {
  !users.some((user) => user.userId === userId) &&
    users.push({ userId, socketId });
    console.log("ADDING A USER --- userId is:" + userId + "socketId is:" + socketId)
};

const removeUser = (socketId) => {
  users = users.filter((user) => user.socketId !== socketId);
};

const getUser = (userId) => {
  return users.find((user) => user.userId === userId);
};

io.on("connection", (socket) => {
  console.log("socket: a user connected 2.");
  socket.on('create', function(room) {
    socket.join(room);
  });

  socket.on("addUser", ({userId,roomid}) => {
    // Verify user exists

    addUser(userId, roomid);
    io.to(roomid).emit("getUsers", users);
  });

  socket.on("sendMessage", ({ senderId, receiverId,roomid, text }) => {
    console.log("SENDING A MESSAGE --- senderId is:" + senderId + "receiverId is:" + receiverId + "text is:" + text)
    //const user = getUser(receiverId);
    io.to(roomid).emit("getMessage", {
      senderId,
      text,
    });
  });

  socket.on("disconnect", ({roomid}) => {
    console.log("a user disconnected!");

    removeUser(roomid);
    io.to(roomid).emit("getUsers", users);
  });

});

// app.use(express.static(__dirname + "../../../client/build/"));
// app.get("/*", (req, res) => {
//   res.sendFile("index.html", { root: __dirname + "../../../client/build" });
// });

// module.exports = { app, server };
module.exports = { server };