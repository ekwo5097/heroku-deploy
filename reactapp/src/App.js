import React, { Component }  from 'react';
import './App.css';
import Navbar from './components/LandingPage/Navbar';
import Home from './components/LandingPage/Home';
import Register from './components/RegisterPage/Register';
import RegisterCoach from './components/RegisterPage/RegisterCoach';
import RegisterCoachAdmin from './components/RegisterPage/RegisterCoachAdmin';
import Login from './components/LoginPage/Login';
import WhatWeDoPage from './components/LandingPage/WhatWeDoPage';
import OurCoachesPage from './components/LandingPage/OurCoachesPage';

import UserHome from './components/pages/UserHome';
import { connect } from 'react-redux';
import CoacheesPage from './components/pages/CoacheesPage';
import CoachPage from './components/LeaderPages/MyCoachPage/CoachPage';
import FeedbackPage from './components/pages/FeedbackPage';
import Questionnaire from './components/pages/Questionnaire';
import QuestionnaireCoach from './components/pages/QuestionnairCoach';
import SettingsLeader from './components/pages/SettingsLeader';
import Payment from './components/pages/Payment';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import * as actions from './store/actions/auth';

import GoalSettingPage from './components/pages/GoalSettingPage';
import CoachFeedbackPage from './components/pages/CoachFeedbackPage';
import Calendar from './components/Scheduling/Calendar';
import CounterTest from './components/LeaderPages/LeaderHomePage/CounterTest';
// import Messages  from './components/pages/Messages';
import MessageMain from './components/Messaging/MessageMain';
import MessageMain2 from './components/Messaging/MessageMain2';
import CoacheePageMod from './components/pages/CoacheePageMod';
import CircularProgressBar from './components/Actions/CircularProgressBar';
import ActionProgressCircle from './components/Actions/ActionProgressCircle';

import AdminPage from './components/AdminPages/AdminHomePage/AdminPage';
import EOIAdmin from './components/AdminPages/AdminEOIPage/EOIAdmin';
import AdminLink from './components/AdminPages/AdminLinkPage/AdminLink';
import AdminUnlink from './components/AdminPages/AdminUnlinkPage/AdminUnlink';
import AdminFeedback from './components/AdminPages/AdminFeedbackPage/AdminFeedback';
import ZoomMeeting from './components/Zoom/ZoomMeeting';

import RegisterInterest from './components/RegisterPage/RegisterInterest';

import ZoomSDKTest from './components/Zoom/ZoomSDKTest';

import CoachPageDesign from './components/LeaderPages/MyCoachPage/CoachPageDesign';

class App extends Component {
  
  render() {
   
  return (
    <>
      <Router>
          {
            this.props.isAuthenticated ?
            <>
              {}
            </>
            :
            <>
               {/* {<Navbar {...this.props}>
              </Navbar>}  */}
              <Route path='/scheduletest' exact component={Calendar} />
              <Route path='/countertest' exact component={CounterTest} />
              
              <Route path ='/zoomMeeting' exact component={ZoomMeeting}/>
             
              <Route path='/circularprogressbar' exact component={CircularProgressBar} />
              <Route path='/actiontest' exact component={ActionProgressCircle} />
              <Route path='/zoomsdktest' exact component={ZoomSDKTest} />
              <Route path='/coachpagedesign' exact component={CoachPageDesign} />
            </>
          }
        <Switch>

          {
            this.props.isAuthenticated ?
            <>
              {/* Landing page related components */}
              <Route path='/' exact component={Home} />
              <Route path='/register' exact component={UserHome} />
              <Route path='/registerInterest' exact component={UserHome} />
              <Route path='/registerCoach' exact component={UserHome} />
              <Route path='/registerCoachAdmin' exact component={RegisterCoachAdmin} />

              <Route path='/login' exact component={UserHome} />
              <Route path='/whatwedo' exact component={UserHome} />
              <Route path='/ourcoaches' exact component={UserHome} />

              <Route path='/payment' exact component={Payment} />
              <Route path='/payment?success=true' exact component={Payment} />
              <Route path='/payment?canceled=true' exact component={Payment} />
              <Route path='/userhome' exact component={UserHome} />
              <Route path='/coachees' exact component={CoacheesPage} />
              <Route path='/coaches' exact component={CoachPage} />
              <Route path='/feedback' exact component={FeedbackPage} />
              <Route path='/goalsettings' exact component={GoalSettingPage} />
              <Route path='/coachfeedback' exact component={CoachFeedbackPage} />
              <Route path='/settings' exact component={SettingsLeader} />
              {/* <Route path='/messages' exact component={Messages} /> */}
              <Route path='/questionnaire' exact component={Questionnaire} />
              <Route path='/questionnairecoach' exact component={QuestionnaireCoach} />
              <Route path='/coacheepagemod' exact component={CoacheePageMod} />
              
              {/* Admin related routes */}
              <Route path='/adminpage' exact component={AdminPage} />
              <Route path='/adminlinkpage' exact component={AdminLink} />
              <Route path='/adminunlinkpage' exact component={AdminUnlink} /> 
              <Route path='/eoicoach' exact component={EOIAdmin} />
              <Route path='/adminfeedback' exact component={AdminFeedback} />

              <Route path='/message1' exact component={MessageMain} />
              <Route path='/message2' exact component={MessageMain2} />

            </>
            :
            <>
              {/* Landing page related components */}
              {/* {<Navbar {...this.props}>
              </Navbar>}  */}
              <Route path='/' exact component={Home} />
              <Route path='/register' exact component={Register} />
              <Route path='/registerCoach' exact component={RegisterCoach} />
              <Route path='/registerInterest' exact component={RegisterInterest} />
              <Route path='/login' exact component={Login} />
              <Route path='/whatwedo' exact component={WhatWeDoPage} />
              <Route path='/ourcoaches' exact component={OurCoachesPage} />

              <Route path='/payment' exact component={Payment} />
              <Route path='/payment?success=true' exact component={Payment} />
              <Route path='/payment?canceled=true' exact component={Payment} />

             
            </>
          }
          
        </Switch>

        </Router>
    </>
    );
  }
}
const mapStateToProps = state => {
  return {
    isAuthenticated: localStorage.getItem("token") !== null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTryAutoSignup: () => dispatch(actions.authCheckState())

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
